import socket
import time
import configparser
import os
import sys


class udp_messager(object):
    sock = '' 
    config = ''
    host_ip = ''
    host_port = 5888
    current_msg = ''
    current_sessid = 0
    current_subjid = 0
    current_trial_num = 0
    current_message_status = 'V' #S-Start message/T-Trial Trigger/E-EndSession/V-Viod
    ts = ''

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
        self.config = configparser.ConfigParser()
        self.config.sections()
        self.config.read(ini_path)


    def setNonBlocking(self):
        self.sock.setblocking(0)

    def getConnection(self):
        print("connecting with "+self.config['vid']['host']+" port "+self.config['vid']['port'])
        self.sock.bind((self.config['vid']['host'],int(self.config['vid']['port'])))

    def read_message(self):
        try:
            buf, addr = self.sock.recvfrom(40960)
            self.current_msg = buf.decode()
        except socket.error:
            self.current_msg = 'None'

    def decode_message(self):
        if not ('A' in self.current_msg and 'E' in self.current_msg):
            # wrong message format
            print("wrong message format!")
            self.current_subjid = '0'
            self.current_sessid = '0'
            self.ts = '2000-01-01 00:00:01'

        self.current_trial_num = 0

        self.current_subjid = self.current_msg[self.current_msg.index("subj")+5:self.current_msg.index("timestamp")-1] #or int()
        self.current_sessid = self.current_msg[self.current_msg.index("sessid")+7:self.current_msg.index("subj")-1]
        self.ts = self.current_msg[self.current_msg.index("timestamp")+10:self.current_msg.index("E")]

        if 'STARTVID' in self.current_msg:
            self.current_message_status = 'S'
            self.current_trial_num = 0        
        elif 'TRIALTRIG' in self.current_msg:
            self.current_message_status = 'T'
            self.current_trial_num = self.current_msg[self.current_msg.index("ndt")+4:self.current_msg.index("timestamp")-1]           
        elif 'STOP' in self.current_msg:
            self.current_message_status = 'E'
        else: #not matching any known message
            self.current_message_status = 'V'
            self.current_subjid = '0'
            self.current_sessid = '0'
            self.ts = '2000-01-01 00:00:01'

    def printMessage(self): # this is for debugging
        if self.current_message_status == 'S':
            status_to_display = 'Starting Video'
        elif self.current_message_status == 'T':
            status_to_display = 'TrialRec'
        elif self.current_message_status == 'E':
            status_to_display = 'Ending video'
        else:
            print("wrong format")
        print('message received: '+status_to_display+' subjid: '+str(self.current_subjid)+' sessid: '+str(self.current_sessid)+' ts: '+self.ts+' trial: '+str(self.current_trial_num))



if __name__ == "__main__":
    ctx = udp_messager()
    ctx.getConnection()
    ctx.setNonBlocking()
    while True:
        try:
            ctx.read_message()
            if ctx.current_msg != 'None':
                ctx.decode_message()
                ctx.printMessage()
                time.sleep(0.1)
        except KeyboardInterrupt:
            print('exiting')
            ctx.sock.close()
            sys.exit()



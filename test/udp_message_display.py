import socket
import time
import configparser
import os
import sys

config = configparser.ConfigParser()
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
config.read(ini_path)
sock.bind((config['vid']['host'],config['vid']['port']))

while True:
    try:
        buf, addr = sock.recvfrom(40960)
        print(buf)
    except KeyboardInterrupt:
        print("Bye")
        sock.close()
        sys.exit()

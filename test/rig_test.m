function [parsed_events] = rig_test()
%rig_test()
%   Example rig_test functions, to test the SMA

% To control the LEDs using PA, we need to create some shorcut for serial
% command first. We specify the port we are going to use for each state
% (only PA ports, otherwise ignored),
% The program will generate the code and replace the light command with
% serial code command in sma.AddState

SoundsObj =  PsychSoundServer(48000);
current_sf = SoundsObj.getSF;
SoundsObj.load('GoSound', GenerateSineWave(current_sf, 4000, 0.2));

SoundsObj.sync();
global BpodSystem;
BpodSystem.PluginObjects.Sound = SoundsObj;
snd = SoundsObj.SoundStruct;

PA_led_to_use = int.create_PA_led_cell({'MidRpwm',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'MidRblue',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'TopRpwm',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'TopRblue',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'TopLpwm',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'TopLblue',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'MidLpwm',255});
PA_led_to_use = int.create_PA_led_cell(PA_led_to_use,{'MidLblue',255});

SMA = sma.newStateMachine(PA_led_to_use);

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_MidR', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidRIn', 'InPoke_MidR'},...
   'OutputActions', {'MidRpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_MidR', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidROut', 'WaitForPoke_TopR'},...
   'OutputActions', {'MidRblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_TopR', ...
   'Timer', 0,...
   'StateChangeConditions', {'TopRIn', 'InPoke_TopR'},...
   'OutputActions', {'TopRpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_TopR', ...
   'Timer', 0,...
   'StateChangeConditions', {'TopROut', 'WaitForPoke_BotR'},...
   'OutputActions', {'TopRblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_BotR', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotRIn', 'InPoke_BotR'},...
   'OutputActions', {'BotRpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_BotR', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotROut', 'WaitForPoke_MidC'},...
   'OutputActions', {'BotRblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_MidC', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidCIn', 'InPoke_MidC'},...
   'OutputActions', {'MidCpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_MidC', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidCOut', 'WaitForPoke_TopL'},...
   'OutputActions', {'MidCblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_TopL', ...
   'Timer', 0,...
   'StateChangeConditions', {'TopLIn', 'InPoke_TopL'},...
   'OutputActions', {'TopLpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_TopL', ...
   'Timer', 0,...
   'StateChangeConditions', {'TopLOut', 'WaitForPoke_BotL'},...
   'OutputActions', {'TopLblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_BotL', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotLIn', 'InPoke_BotL'},...
   'OutputActions', {'BotLpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_BotL', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotLOut', 'WaitForPoke_MidL'},...
   'OutputActions', {'BotLblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_MidL', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidLIn', 'InPoke_MidL'},...
   'OutputActions', {'MidLpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_MidL', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidLOut', 'WaitForPoke_BotC'},...
   'OutputActions', {'MidLblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForPoke_BotC', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotCIn', 'InPoke_BotC'},...
   'OutputActions', {'BotCpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InPoke_BotC', ...
   'Timer', 0,...
   'StateChangeConditions', {'BotCOut', 'WaitForEndPoke_MidC'},...
   'OutputActions', {'BotCblue',255,'PlaySound', snd.GoSound.id});

SMA = sma.AddState(SMA, 'Name', 'WaitForEndPoke_MidC', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidCIn', 'InEndPoke_MidC'},...
   'OutputActions', {'MidCpwm',255});

SMA = sma.AddState(SMA, 'Name', 'InEndPoke_MidC', ...
   'Timer', 0,...
   'StateChangeConditions', {'MidCOut', 'exit'},...
   'OutputActions', {'MidCblue',255,'PlaySound', snd.GoSound.id});

SendStateMatrix(SMA);
RawEvents = RunStateMatrix;

SoundsObj.stopAll;
SoundsObj.delete;

BpodSystem.Status.BeingUsed = 0;
parsed_events = int.AddTrialEventsAlias(BpodSystem.Data,RawEvents);

end


import sys
import os
import time
import cv2 as cv
import udp_message_decoder as udp_mess
if __name__ == "__main__":
    ctx = udp_mess.udp_messager()
    ctx.getConnection()
    ctx.setNonBlocking()

    recording_status = False
    current_running_sessid = 0

    while True:
        try:
            ctx.read_message()
            if ctx.current_msg != 'None':
                ctx.decode_message()
                ctx.printMessage()
                if  (ctx.current_message_status == 'S' and recording_status == False) or (ctx.current_message_status == 'T' and recording_status == False): #get a message to start a new session
                    print("starting new video recording")
                    cap = cv.VideoCapture(0)
                    fourcc = cv.VideoWriter_fourcc(*'XVID')
                    filename =  "%s_%s_%s.avi" % (str(ctx.current_subjid), str(ctx.current_sessid),ctx.ts)
                    filename = os.path.join(os.path.expanduser('~'),'in_process',filename)
                    out = cv.VideoWriter(filename, fourcc, 20.0, (640,  480))
                    recording_status = True
                    current_running_sessid = ctx.current_sessid
                elif (ctx.current_message_status == 'S' and recording_status == True) or (ctx.current_message_status == 'T' and current_running_sessid != ctx.current_sessid): # running an old video recording, restarting as a new recording
                    print("stop video recording")
                    cap.release()
                    out.release()
                    cv.destroyWindow(filename)
                    cv.destroyAllWindows()
                    print("re-starting video recording")
                    cap = cv.VideoCapture(0)
                    fourcc = cv.VideoWriter_fourcc(*'XVID')
                    filename =  "%s_%s_%s.avi" % (str(ctx.current_subjid), str(ctx.current_sessid),ctx.ts)
                    filename = os.path.join(os.path.expanduser('~'),'in_process',filename)
                    out = cv.VideoWriter(filename, fourcc, 20.0, (640,  480))
                    recording_status = True
                    current_running_sessid = ctx.current_sessid
                elif  (ctx.current_message_status == 'E' and recording_status == True):
                    # stop sess video recording
                    print("stop video recording")
                    cap.release()
                    out.release()
                    cv.destroyWindow(filename)
                    cv.destroyAllWindows()
                    recording_status = False
            if recording_status == True and cap.isOpened():
                ret, frame = cap.read()
                if ret:
                    cv.imshow(filename, frame)
                    out.write(frame)   
                cv.waitKey(1)
            else:
                time.sleep(0.1)
        except KeyboardInterrupt:
            print('exiting')
            ctx.sock.close()
            cap.release()
            out.release()
            cv.destroyAllWindows()
            sys.exit()
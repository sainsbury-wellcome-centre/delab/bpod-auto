function [gitowner, slackowner] = getOwner(subjid)

	if nargin==0 || isempty(subjid)
		gitowner = '';
		slackowner = '';
		return;
	end

	dbc = db.labdb.getConnection();
	dbc.use('met')
	out = dbc.get('select owner from animals where subjid = "%s" and owner is not null',{subjid});
	if isempty(out)
		gitowner = '';
		slackowner = '';
	else
		gitowner = out{1};
		slackowner = dbc.get('select slackuser from experimenters where gitlabuser = "%s"',{gitowner});
		slackowner=slackowner{1};
	end

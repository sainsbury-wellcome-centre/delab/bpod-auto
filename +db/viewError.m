function out = viewError(sessid)

dbc = db.labdb.getConnection();
out = dbc.query('select * from met.error_log where sessid = %d',{sessid});

for sx = 1:numel(out.sessid)
    out.stack{sx} = json.mloads(out.stack{sx});
    if ~isnan(out.commentid(sx))
        out.comment(sx) = dbc.get('select comment from met.comments where commentid=%d',{out.commentid(sx)});
    else
        out.comment(sx)={[]};
    end
end
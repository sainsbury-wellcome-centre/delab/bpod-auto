function clearOldSessionStatus(rigid)
% clearOldSessionStatus(rigid)
% Clears an old session status, presumably from a rig that had a hardware
% or network error, so failed to properly "close" 

if rigid==0
	return
end

try
	dbc = db.labdb.getConnection();
	[oldsessid, oldstat] = dbc.get('select sessid, sess_status from beh.current_sessview where rigid=%d',{rigid});
	if ~strcmp(oldstat,'finished') && ~isempty(oldsessid)
		S.sessid = oldsessid;
		S.status = 'error';
		dbc.saveData('beh.sess_status',S)
	end
catch me
	utils.showerror(me);
end

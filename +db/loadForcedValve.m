function valve_num = loadForcedValve(rigid)
% Loads forced valve number from the db so that we can use two valves in
% case one breaks

if nargin==0 || isnan(rigid) || rigid==0 
    fprintf(2,'In db.loadForcedValve: this is not a real rig.\n');
    valve_num = 0;
    return
end

dbc = db.labdb.getConnection();
valve_num = dbc.get('select valve_num from met.current_forced_valve where rigid=%d',{rigid});

if isempty(valve_num)
   valve_num=0;
elseif numel(valve_num)>1
    fprintf(2,'In db.loadForcedValve: get more than one number for valve number. Please notify administrator.\n')
    valve_num = 0;
end

end
function [ok] = addSubjectComment(subjid, cmt, experid)
% [ok] = addComment(subjid, cmt, experid)

try
    dbc = db.labdb.getConnection('manage');

    if nargin <3 || isempty(experid)
        experid = db.getExperimenter();
        if isempty(experid)
            warning('You need to input your experid, you can get that from met.experimenters.')
            return
        end
    end
    
    if isempty(cmt) || length(cmt) > 1000
        warning('comment should not be an empty string and it should be less than 1000 characters.')
        return
    end
    
    fieldnames = {'subjid','experid','comment'};
    data = {subjid, experid, cmt};
    
    dbc.dbconn.datainsert('met.subject_comment',fieldnames,data);

    ok = 1;
catch me
    utils.showerror(me);
    ok = 0;
end
end
function ok = markRunning(rigid)
try
	if nargin == 0
		rigid = db.getRigID();
	end
	dbc = db.labdb.getConnection();
	sqlS.rigid = rigid;
    sqlS.status = 'running';
    sqlS.isbroken = 0;
    dbc.saveData('met.rig_status',sqlS);
	ok = 0;
catch me
	utils.showerror(me)
	ok = 1;
end

function loadPokeSettings(rigid)
% Loads pokes threshold from the `current_poke_thresholds` table and sends
% them to the arduino so that each poke can have its own thresholds.



	if nargin==0 || isnan(rigid) || rigid==0 || rigid>500
        fprintf(2,'In db.loadPokeSettings: this is not a real rig.\n');
        return
	end
	
	dbc = db.labdb.getConnection();
	[poke, low, high] = dbc.get('select poke_num, low_thresh, high_thresh from met.current_poke_thresholds where rigid=%d',{rigid});
	if isempty(poke)
		low = 200;
		high = 500;
		for px = 1:numel(poke)
			SetPokeThreshold(poke(px),low , high);
		end
	else
		for px = 1:numel(poke)
			SetPokeThreshold(poke(px),low(px),high(px));
		end
	end

function peh = getParsedEvents(sessid)
% peh = getParsedEvents(sessid)
% Get parsed events for a sessid or list of sessids.
% if a sinlge sessid is passed, return an array of structs.
% if multiple sessid are passed return a cell array that is the same shape as the sessid input.


	dbc = db.labdb.getConnection();
	if numel(sessid)>1
		peh = arrayfun(@(x)(get_peh(x, dbc)), sessid, 'UniformOutput',0);
	else
		peh = get_peh(sessid, dbc);
	end

end

function out = get_peh(sessid, dbc)

	trialsout = dbc.query('select parsed_events from beh.trialsview where sessid=%d order by trialnum',{sessid});

	these_json_pe = trialsout.parsed_events;
            % By going backwards we allocate memory for the struct at once to
            % save time. 
    if isjava(these_json_pe)
        java_flag = true;
    else
        java_flag = false;
    end
        
    num_trials = numel(these_json_pe);
    if num_trials>0
	    for tx = num_trials:-1:1
	        if java_flag
	            this_data = json.mloads(char(these_json_pe(tx))');
	        else
	            this_data = json.mloads(these_json_pe{tx});
	        end
	        out(tx) = this_data;      
  		end
    else
    	out = [];
    end
end
function [status] = notify_owner(subjid,message)
%[status] = notify_owner(subjid,message)
%   notify the owner of this animal for certain information
[gitowner, slackowner] = db.getOwner(subjid);
message = sprintf('%s  <@%s>',message,slackowner);
status = net.sendslack(message);
end
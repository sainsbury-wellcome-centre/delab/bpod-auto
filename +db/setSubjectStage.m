function [ok] = setSubjectStage(subjlist, stage, experid, clear_subject_settings)
% [ok] = setSubjectStage(subjlist, stage, experid, clear_subject_settings)
try
    if nargin < 3 || isempty(experid)
        warning("You must input your experid, you can get this from met.experimenters.");
        return
    end
    
    if nargin < 4 || isempty(clear_subject_settings)
        clear_subject_settings = false;
    end
    
    dbc = db.labdb.getConnection('manage');
    for sx = 1:length(subjlist)
        current_settings = dbc.query('select subjid, subj_data, protocol, settingsname, stage from met.current_settings where subjid = "%s"',{subjlist{sx}});
        new_setting = dbc.query('select a.settingsid, protocol, settingsname, stage from met.settings a, met.animals b where b.subjid="%s" and a.expgroupid=b.expgroupid and a.stage=%d',{subjlist(sx),stage});
        if isempty(new_setting)
            warning(sprtinf('It seems that %s has not yet been assigned to an experimental group. Please do so before running this function',subjlist{sx}))
            return
        end
        
        S.subjid = subjlist{sx};
        S.settingsid = new_setting.settingsid;
        S.saved_on_ip = db.get_ip();
        
        if experid ~= 0
            S.saved_by_experid = experid;
        end
        
        if ~isempty(current_settings) % This is an old animal
            if clear_subject_settings
                comment = 'and data was cleared.';
            else
                comment = 'and subj_data was copied.';
                S.settings_data = current_settings.subj_data;
            end
        end
        
        dbc.saveData('met.subject_settings',S);
        
        fprintf('Subject "%s" moved from %s stage %d (%s) to %s stage %d (%s) %s',...
            current_settings.subjid, current_settings.protocol{1}, current_settings.stage, current_settings.settingsname{1},...
            new_setting.protocol{1}, new_setting.stage, new_setting.settingsname{1},...
            comment)
        
    end
    ok = 1;
catch me
    utils.showerror(me);
    ok = 0;
end
end

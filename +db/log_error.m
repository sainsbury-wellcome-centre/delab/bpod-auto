function log_error(err, varargin)
% log_error(err,['force_save',false])
% logs an error to the errors table

[subjid, args] = utils.inputordefault('subjid', [], varargin);
[sessid, args] = utils.inputordefault('sessid', 0, args);
[force_save, args] = utils.inputordefault('force_save', false, args);
[caught, args] = utils.inputordefault('caught',0, args);
[comment, args] = utils.inputordefault('comment',[],args);
[notify, args] = utils.inputordefault('notify',false,args);
[print_stack, args] = utils.inputordefault('print_stack',true,args);
utils.inputordefault(args)

if nargin == 0
	err = lasterror();
end

sqlS.rigid = db.getRigID();
if sqlS.rigid == 0 && ~force_save
    fprintf(1,'Error logging by default only works on real rigs. Use [force_save, 1] to override');
    return
end
sqlS.ip = db.get_ip();
sqlS.message = err.message;
sqlS.identifier = err.identifier;
sqlS.caught = caught;
if ~isempty(comment)
	commentid = db.log_comment('met.error_log', comment);
	sqlS.commentid = commentid;
end

if sessid>0
	sqlS.sessid = sessid;
end


tmp = json.mdumps(err.stack,'compress',false);
% We can't handle struct arrays at this time convert to cell array of structs.	

if ~isempty(tmp)
	sqlS.stack=tmp;
end
	

dbc = db.labdb.getConnection();

dbc.saveData('met.error_log', sqlS);

if notify
	try
		errmsg = utils.showerror(err, 'print_stack',print_stack);
	 
		[gitowner,slackowner] = db.getOwner(subjid);
		tech = db.getCurrentTech();

		%if sessid == 0 % don't need to notify tech for test session
			%message = sprintf('Error in *Rig %d*  <@%s> <@%s> \n```\n %s \n```',sqlS.rigid, slackowner,tech, errmsg);
			%net.sendslack(message);
		if sessid ~= 0
			message = sprintf('Error in *Rig %d Session %d* <@%s> \n ```\n %s \n```',sqlS.rigid, sessid,slackowner, errmsg);
			net.sendslack(message);
		end
	catch me
		fprintf(2,'Failed to notify')

	end

end


    

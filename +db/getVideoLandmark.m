function [video_data] = getVideoLandmark(sessid_list,varargin)
%[video_data] = getVideoLandmark(sessid,use_db)
%   download video keypoint data from db
iod = @utils.inputordefault;
use_db = iod('use_db','client',varargin);

dbc = db.labdb.getConnection(use_db);

sess_str = sprintf('%d,',sessid_list);
sess_str(end)=[];

video_data = dbc.query('select * from beh.video_landmark where sessid in (%s)',{sess_str});
video_data.ts = cellfun(@(x)typecast(x,'double'), video_data.ts, 'UniformOutput',0);
video_data.data = cellfun(@(x)json.mloads(x), video_data.data, 'UniformOutput',0);
video_data.header = cellfun(@(x)json.mloads(x), video_data.header, 'UniformOutput',0);

end
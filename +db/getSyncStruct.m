function S = getSyncStruct(sessid)
	dbc = db.labdb.getConnection();
	sync_data = dbc.get('select sync_data from phy.phys_sess where sessid=%d',{sessid});
	S = json.mloads(sync_data{1});

end
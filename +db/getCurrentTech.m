function out = getCurrentTech()

	dbc = db.labdb.getConnection();
	dbc.use('met');
	try
	techid = dbc.get('select experid from mass where massid = (select max(massid) from mass)');
	out = dbc.get('select slackuser from experimenters where experid=%d',{techid});
	out = out{1};
catch
	out = '';
end
end
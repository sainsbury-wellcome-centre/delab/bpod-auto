function ok = setTesting(bit, rigid)
try
	if nargin == 1
		rigid = db.getRigID();
	end
	dbc = db.labdb.getConnection();
	dbc.call(sprintf('met.setTesting(%d,%d)',rigid,bit))
	ok = 0;
catch me
	utils.showerror(me)
	ok = 1;
end

function species = getSpecies(subjid)
    
dbc = db.labdb.getConnection();
species = dbc.get('select species from met.animals where subjid="%s"',{subjid});
if iscell(species)
    species = species{1};
end

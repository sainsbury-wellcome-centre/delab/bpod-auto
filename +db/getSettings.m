function [setout, settings_data] = getSettings(subjid, varargin)
% [setout, settings_data] = getSettings(subjid, varargin)
% Input:
% subjid 
% date      [] Default is most recent setting.
%
% Output:
% setout     should appear in the protocol.useSettings switch statement.
% settings_data

sdate = utils.inputordefault('date','',varargin);
use_specific__db = utils.inputordefault('db','',varargin);

if isempty(use_specific__db)
    dbc = db.labdb.getConnection();
else
    dbc = db.labdb.getConnection(use_specific__db);
end

if isempty(sdate)
    sqlstr = 'select * from met.subject_settings where subjid = "%s" order by settings_date desc limit 1';
    setout = dbc.query(sqlstr, {subjid});
else
    sqlstr = 'select * from met.subject_settings where subjid = "%s" and date(settings_date)="%s" order by settings_date desc limit 1';
    setout = dbc.query(sqlstr, {subjid, sdate});
end



if isempty(setout) || strcmpi(setout.settings_data,'null') || isempty(setout.settings_data) || isempty(setout.settings_data{1})
    settings_data = [];
    fprintf(2,'db.getSettings: The settings of this animal is empty!\n');
else
    for sx = 1:numel(setout.subjid)
    try
        if isjava(setout.settings_data)
            settings_data(sx) = json.mloads(char(setout.settings_data(1))');
        else
            settings_data(sx) = json.mloads(setout.settings_data{1});
        end
    catch me
        db.log_error(me,'notify',1,'caught',1);
        setout.settings_data
        fprintf(2,'Problem loading settings.\n');
        utils.showerror(me);
        subj_data = [];
    end
    end
    % convert the JSON in the database to a matlab struct
end

end

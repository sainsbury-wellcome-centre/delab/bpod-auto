function [poke_list, poke_lower] = measure_ir_pairs
%%

poke_list = getPokeList();

LOWER_THRESH = 920;
poke_lower = zeros(size(poke_list));
for px = 1:numel(poke_list)
    [tm, tse] = PokeCalibration(px - 1); % Bpod pokes start with 0, matlab with 1.

    poke_lower(px) = tm - tse*4;
end

warn_bits = poke_lower < LOWER_THRESH;

bad_poke_list = sprintf('%s, ', poke_list{warn_bits});
if any(warn_bits)
	fprintf(2,'Bad pokes! %s\n', bad_poke_list(1:end-2))
else
	fprintf(1,'Poke IR levels OK!\n')
end

% D.message = ['The following pokes in Rig %d need to be cleaned or fixed.\n' bad_poke_list];
% D.title = sprintf('Rig %d Broken: failed poke calibration');
% D.


% db.createGitlabIssue('poke',)

    




end
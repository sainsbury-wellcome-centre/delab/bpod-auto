function log_injections(D)
% log_injection(data)
% log_injection('data.csv')
% data can be a table or a struct array or a csv filename
% The columns (and format) should be:
% drug (char), subjid (int), ts (YYYY-MM-DD hh:mm:ss), dose (mg/ml), 
% method (SC, IM, IP, IV, IC), comment, experid (int or first name), volume (mL)

dbc = db.labdb.getConnection('manage');

if ischar(D)
    tab = readtable(D);
    % check column names
elseif isstruct(D)
    tab = struct2table(D);
else
    tab = D;
end

tab_names = tab.Properties.VariableNames;
col_names = dbc.column_names('met.injection_log');
assert(all(cellfun(@(x)ismember(x, col_names), tab_names)),'Mismatch of CSV columns %s \n and DB columns: %s, %s',sprintf('%s,',tab_names{:}), sprintf('%s,',col_names{:}))

% Automatically get the sessids
tab.sessid = arrayfun(@(x,y)getSessidFromSubjidDate(dbc, x,y),tab.subjid, tab.ts);

% Convert experimenter names to experid if necessary
if ismember('experid',tab_names)
    if ischar(tab.experid(1))
        % We need to convert to ID
        tab.experid = arrayfun(@(x)db.getExperimenter(x), tab.experid);
    end
end

% Convert dates if necessary
if isdatetime(tab.ts(1))
    tab.ts = datestr(tab.ts, 31);
end

% save
dbc.saveData('met.injection_log',tab);

end

function sessid = getSessidFromSubjidDate(dbc, subjid, ts)
if isdatetime(ts)
    ts = datestr(ts,31);
end
[sessid, num_trials] = dbc.get('select sessid, num_trials from beh.sessview where subjid = %d and sessiondate=date("%s")', {subjid, ts});
if numel(sessid) > 1
    warning('%s had more than one session on %s, using the one with more trials')
    [~, ti]=max(num_trials);
    sessid = sessid(ti);
end
end


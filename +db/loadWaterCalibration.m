function calstruct = loadWaterCalibration(rigid)

if nargin == 0 
    [rigid, roomid] = db.getRigID();
    if isempty(rigid) || rigid==0 || isnan(roomid)
        fprintf(1,'This is not a real rig, no calibration info on DB\n');
        calstruct(1) = fake_cal_struct;
        calstruct(2) = fake_cal_struct;
        return;
    end
elseif nargin == 1 && rigid==0
       calstruct(1) = fake_cal_struct;
       calstruct(2) = fake_cal_struct;
        return;
end
dbc = db.labdb.getConnection();
caltab = dbc.query('select valve, volume, duration, calts from met.water_calibration where valid = 1 and rigid = %d',{rigid});

assert(~isempty(caltab),'You cannot run a protocol on a rig before running water calibration.')

valve = unique(caltab.valve);

for vx = 1:numel(valve)
	thisvalve = caltab.valve == valve(vx);
	calstruct(vx).Table = [caltab.duration(thisvalve) caltab.volume(thisvalve)]; %#ok<*AGROW>
	caldates = caltab.calts(thisvalve);
	calstruct(vx).LastDateModified = datenum(caldates{1});
	calstruct(vx).CalibrationTargetRange = range(caltab.volume(thisvalve));
    beta0 = [0.1353,0.01177,0.0294];
	calstruct(vx).beta = nlinfit(caltab.volume(thisvalve), caltab.duration(thisvalve), @stats.invsoftplus, beta0);
end

function fakes = fake_cal_struct()

	fakes(1).Table = [200 10; 300 15; 400 20];
	fakes(1).LastDateModified = now();
	fakes(1).CalibrationTargetRange = [10 20];
	pp = polyfit(fakes(1).Table(:,2), fakes(1).Table(:,1), 1);
	fakes(1).TrinomialCoeffs = [0 0 pp];
    fakes(1).beta = [0.1353,0.01177,0.0294];



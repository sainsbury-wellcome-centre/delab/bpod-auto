function result = saveWaterCalibration(varargin)
% result = saveWaterCalibration(LiquidCal)
% result = saveWaterCalibration(valve, duration, volume)
% The 2nd form of the function is highly recommended.
% valve     the id of the valve you are calibrating (usually 1).
% duration  a vector of durations the valve was opened (in milliseconds)
% volume    a vector of the volume (in microliters) of each drop for each duration.
%
% E.g. if you delivered 50 drops of 100ms and it weighed 1gram. 
% Then duration = 100, and volume = 1g/(50 drops) * 1mL/g * 1000 µ/m = 20 µL/drop 
% 
% Note: if saving the data on a rig that is not the rig where you did water calibration you will 
% need to pass that in.
% e.g. db.saveWaterCalibration(1, duration, volume, 'rigid', 104)


rigid = utils.inputordefault('rigid',db.getRigID(),varargin);
if isempty(rigid) || rigid==0
    fprintf(1,'This is not a real rig, not saving to DB\n');
    result = 1;
    return;
end

if nargin ==1 && isstruct(varargin{1})
	calstruct = varargin{1};
	nvalves = numel(calstruct);


    calind = 1;

    for vx = 1:nvalves
        thistab = calstruct(vx).Table;
        for cx = 1:size(thistab,1)
            sqlS(calind).rigid = rigid; %#ok<*AGROW>
            sqlS(calind).valve = vx;
            sqlS(calind).duration = thistab(cx,1)/1000; % Use seconds in the DB.
            sqlS(calind).volume = thistab(cx,2); % In microliters
            sqlS(calind).valid = 1;
            calind = calind + 1;
        end
    end

    dbc = db.labdb.getConnection();
    dbc.saveData('met.water_calibration',sqlS)
    result = 0; % Saved
elseif nargin==3
    % We have a time and a volume.
   
	sqlS.rigid = rigid; %#ok<*AGROW>
	sqlS.valve = varargin{1};
    sqlS.duration = varargin{2}/1000; % Use seconds in the DB.
    sqlS.volume = varargin{3}; % In microliters
    sqlS.valid = 1;
    dbc = db.labdb.getConnection();
    dbc.saveData('met.water_calibration',sqlS)
    result = 0; % Saved

end
    
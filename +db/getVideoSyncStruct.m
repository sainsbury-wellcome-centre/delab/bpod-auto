function S = getVideoSyncStruct(sessid,use_db)
    if nargin < 2
        use_db = 'client';
    end
	dbc = db.labdb.getConnection(use_db);
	sync_data = dbc.get('select data from met.video_sync where sessid=%d',{sessid});
	S = jsondecode(sync_data{1});
end
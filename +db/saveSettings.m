function [ok] = saveSettings(setout, settings_data, comment, experid, settings_date)
% [ok] = saveSettings(setout, subj_data, comment, [experid], [settings_date])
% setout is the first output of db.getSettings
% settings_data is the second output of db.getSettings, i.e. the thing you
% want to change
% if you want the settings_date to not be the current time of your computer, 
% then use the optional 5th argument. the format should be {'yyyy-mm-dd HH:MM:SS'}
assert(nargin>2 && ~isempty(comment),'You must provide a comment')
try
    dbc = db.labdb.getConnection('manage');
    if ~isempty(settings_data)
        setout.settings_data = json.mdumps(settings_data);
    else
        setout.settings_data = [];
    end
    % Check if subject is running
    sessid = dbc.get('select sessid from beh.current_sessview where subjid="%s" and sess_status="running"',{setout.subjid{1}});
    if isempty(sessid)
        if nargin < 5
            setout.settings_date = datestr(now,31);
        else
            setout.settings_date = settings_date;
        end
    else
        setout.settings_date = [datestr(now,29) ' 23:00:00'];
        warning('SAVE:RUNNING','This subject is running, saving settings for 11pm, they will be used tomorrow')
    end    
    setout.subjsettingsid =[];

    % Put the IP of this machine.
    ip = db.get_network_info();
    setout.saved_on_ip = ip;
    % Get the current experimenter
    if nargin < 4
        try
        experid = db.getExperimenter();
        setout.saved_by_experid = experid;
        catch
            fprintf(2,'Failed to match experimenter')
        end
    else
        setout.saved_by_experid = experid;
    end

    dbc.saveData('met.subject_settings',setout);
    
    ok = 1;
    if nargin > 2
        db.addSubjectComment(setout.subjid{1}, comment,experid);
    end
catch me
    utils.showerror(me);
    ok = 0;
end


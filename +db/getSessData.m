function [S, extra_args]=getSessData(varargin)
% [S, extra_args]=get_sessdata(varargin)
% [S, extra_args]=get_sessdata(sessid)
% [S, extra_args]=get_sessdata(subjname, date)
% [S, extra_args]=get_sessdata(subjname, daterange)
%
% A frontend to get data from the sessions table that does some nice input
% parsing.   Useful to use in other functions to avoid having to parse
% inputs.  If you pass it all the args from a parent function the leftover
% args are returned as extra_args.  For a good example of this see
% psychoplot_delori.m (in ExperPort/Analysis/SameDifferent)
%
% S by default contains fields: sessid, sessiondate,protocol_data and peh
%
% sessid can be a single sessid or a vector of sessids
% date should be of the form "YYYY-MM-DD" or a relative date like -5
% daterange should be a numeric vector in relative form like -10:-1 or a
% cell array of date string of the from "YYYY-MM-DD"
%


if nargin==0 || isempty(varargin{1})
    S.sessid=[];
    S.data={};
    S.peh={};
    S.subjname={};
    S.sessiondate={};
    S.protocol={};
    return;
end
       

if iscell(varargin{1})
	varargin=varargin{1};
	nargs=numel(varargin);
else
	nargs=nargin;

end

%% parse inputs
use_sessid=0;
use_db = 'client';
if nargin==1 && isnumeric(varargin{1})
	% Case 1, simple sitaution we've got a vector of sessids
	sessid=varargin{1};
    sessid=sessid(~isnan(sessid));
	use_sessid=1;
	%[subjname, experimenter]=bdata('select subjname, experimenter from sessions where sessid="{S}"',sessid(1));
	varargin=varargin(2:end);
    dbc = db.labdb.getConnection(); 
elseif nargs>=2
	% Case 2, complex imput vars
    iod = @utils.inputordefault;
    sessid = iod('sessid',[],varargin);
    use_db = iod('use_db','client',varargin);
    subjname = iod('subjid','',varargin);
    datein = iod('date','',varargin);
    dbc = db.labdb.getConnection(use_db); 
    if ~isempty(subjname) && ~isempty(datein)
        if isnumeric(datein)
            %Case 2a, we've got relative dates (e.g. -10:0)
            for dx=1:numel(datein)
                dates{dx}=utils.to_string_date(datein(dx));
            end
        elseif ischar(datein)
            %Case 2b, we've got a single date (e.g. '2009-05-01')
            dates{1}=datein;
        else
            %Case 2c, we've got a cell array of dates
            dates=datein;
        end
    elseif ~isempty(sessid) && isnumeric(sessid)
        use_sessid=1;
        sessid=sessid(~isnan(sessid));
    end

else
	S=[];
	warning('Failed to parse inputs.');
	extra_args=varargin;
	return
end

do_tracking=false;

fetch_peh=true;  % The PEH can be large, sometimes we don't need it.


%% get data from sql

if ~use_sessid

    % If we are not in Case 1 (see above)
    % then transform the cell array of strings into a long comma separated
    % string.
    datestr='';
    for dx=1:numel(dates)
        datestr=sprintf('%s , "%s"', datestr, dates{dx});
    end
    datestr = datestr(3:end);
    % Use the datestr for a select ... where sessiondate in (datestr) type sql command to get all the relevant sessions.
    sqlquery = sprintf('select distinct(a.sessid) from beh.sessions a, beh.trialsview b where a.sessid=b.sessid and a.subjid="%s" and sessiondate in ( %s ) order by sessiondate',subjname, datestr(2:end));
    sqlout = dbc.query(sqlquery);
    sessid = sqlout.sessid;
    
end



% We have a list of sessids.  Transform that into a comma separated string
sessstr='';
for sx=1:numel(sessid)
    sessstr=sprintf('%s, %d', sessstr, sessid(sx));
end
sessstr = sessstr(2:end);

% Now get the data
if fetch_peh
    sqlquery = sprintf('select sessid, trialnum, data, parsed_events from beh.trialsview where sessid in ( %s ) order by sessid, trialnum', sessstr);
    trialsout = dbc.query(sqlquery);
else
    sqlquery = sprintf('select sessid, trialnum, data from beh.trialsview where sessid in ( %s ) order by sessid, trialnum', sessstr);
    trialsout = dbc.query(sqlquery);
end

sqlquery = sprintf('select sessid, sessiondate, starttime, hostip, rigid, protocol, subjid from beh.sessions where sessid in ( %s ) order by sessid', sessstr);
sessout = dbc.query(sqlquery);

% Combine the data from the sessions table with data from the trials table.
assert(~isempty(sessout),'Sessid(s) did not return any data');
S = combineData(sessout, trialsout, fetch_peh);


PD = db.getProtocolData([S.sessid],'use_db',use_db);
for sx = 1:numel(S)
    S(sx).protocol_data = PD.protocol_data{sx};
end

if false && do_tracking % This is not implemented yet.
    S.a=cell(numel(S.sessid),1);

    [T.sessid, T.ts, T.theta]=bdata(['select sessid, ts, theta from tracking where sessid in (' sessstr ')']);

    for sx=1:numel(S.sessid)

        tx=find(T.sessid==S.sessid(sx));
        if ~isempty(tx)
            a.ts=T.ts{tx};
            a.theta=T.theta{tx};

            S.a{sx}=a(:);
        end
    end
end
end

function S = combineData(sessout, trialsout, fetch_peh)

    
% This let's people with the faster json code use it while the slow pokes are stuck with the other one.

    S = table2struct(sessout);
    for sx = 1:numel(S)
        these_trial_ind = trialsout.sessid == S(sx).sessid;
        num_trials = sum(these_trial_ind);

        %% First handle the trial data.
        these_json_data = trialsout.data(these_trial_ind);
        clear sessdata
        % By going backwards we allocate memory for the struct at once to
        % save time.
        if isjava(these_json_data)
            java_flag = true;
        else
            java_flag = false;
        end
        if num_trials>0
        for tx = num_trials:-1:1
            if java_flag
                this_data = json.mloads(char(these_json_data(tx))');
            else
                this_data = json.mloads(these_json_data{tx});
            end
            try
            sessdata(tx) = this_data;      
            catch me
                disp(me);
                continue;
            end
        end
        
        S(sx).videoURL = videoURL(S(sx));
        
        S(sx).data = sessdata(:);
        end
        %% Then handle the parsed events if we got it.

        if fetch_peh
            these_json_pe = trialsout.parsed_events(these_trial_ind);
            clear sessdata;
            % By going backwards we allocate memory for the struct at once to
            % save time. 
            if num_trials>0
            for tx = num_trials:-1:1
                if java_flag
                    this_data = json.mloads(char(these_json_pe(tx))');
                else
                    this_data = json.mloads(these_json_pe{tx});
                end
                sessdata(tx) = this_data;      
            end
            S(sx).peh = sessdata(:);
            end
        end
    end

end

function url = videoURL(S)

    sd = S.sessiondate([1:4 6 7 9 10]);
    st = S.starttime([1 2 4 5]);
    url = sprintf('https://int.erlichlab.org/video/%d/%d-%d-%s_%s.mp4',S.subjid, S.subjid, S.sessid, sd, st);

end

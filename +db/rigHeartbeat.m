function [ok] = rigHeartbeat()

try
	dbc = db.labdb.getConnection();
	id = db.getRigID();
	if ~isempty(id) && ~isnan(id)
%		dbc.call(sprintf('met.righeartbeat(%d)',id));
	end
	ok = 0;
catch me
	utils.showerror(me);
	ok = 1;
end

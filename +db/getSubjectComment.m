function out = getSubjectComment(subjid)
% get subject comment from met.commentlogger

dbc = db.labdb.getConnection();
dbc.use('met');
try
    out = dbc.query('select * from met.commentlogger where tableid = "%s"',{subjid});
catch
    out = '';
end
end
function PD = getProtocolData(sessid,varargin)
% PD = getProtocolData(sessid)
% 
% Takes a list of sessid and returns a cell array 
% If there is no protocol data for a session that element of the cell array will be empty.
% Otherwise it will have a table.

iod = @utils.inputordefault;
use_db = iod('use_db','client',varargin);

dbc = db.labdb.getConnection(use_db);
% Which protocol are these sessid?

sessstr = sprintf('%d,', sessid);
sessstr(end) = [];
sqlstr = 'select protocol, sessid from beh.sessions where sessid in (%s)';
[PD] = dbc.query(sqlstr,{sessstr});

get_it = @(prtcl,sid)dbc.query('select * from prt.%s_view a left join beh.opto_trials b on (a.trialid=b.trialid) where a.sessid=%d',{lower(prtcl{1}),sid});

pd_cell = rowfun(get_it, PD, 'OutputFormat','cell');
PD.protocol_data = pd_cell;

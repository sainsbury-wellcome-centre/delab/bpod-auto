function [] = check_fsm_config()
%Check whether fsm config file is compatible with this bpod, drop error if
%not matching
%   
global BpodSystem
if exist('~/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('~/bpod_fsm_config.ini');
elseif exist('example_config_files/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('example_config_files/bpod_fsm_config.ini');
    fprintf(2,'no FSM config file found, load default example file\n');
end

if any(contains(fsm_config.port_list.port_module,'PA')) %have PA module in fsm config file
    % check whether PA module is existed in bpod
    assert(any(contains(BpodSystem.Modules.Name,'PA')),'Error: Did not found PA module, please check bpod hardware, or your fsm config file at ~/bpod_fsm_config.ini!')
end

if isfield(fsm_config,'lick_motor') %whether lick motor module was declared in the fsm config file
    % check whether LM module is existed in bpod
    assert(any(contains(BpodSystem.Modules.Name,'LM')),'Error: Did not found LM module, please check bpod hardware, your fsm config file at ~/bpod_fsm_config.ini!')
end

end
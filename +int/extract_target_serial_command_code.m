function [serial_code] = extract_target_serial_command_code(PA_led_command_mat,sma)
%[serial_code] = extract_target_serial_command_code(PA_led_command_mat,sma)
%   extract serial code command for PA led operation based on the table
%   store in sma (sma.Serial_PA_leds_status_mat)
serial_code = [];
if isfield(sma,'Serial_PA_leds_status_mat')
    for ii = 1:length(sma.Serial_PA_leds_status_mat)
        comparing_status_mat = sma.Serial_PA_leds_status_mat{ii};
        comparing_status_mat = reshape(comparing_status_mat,[1,numel(comparing_status_mat)]);
        target_status_mat = reshape(PA_led_command_mat,[1,numel(comparing_status_mat)]);
        if all(target_status_mat==comparing_status_mat)
            serial_code = sma.Serial_available_serial_command(ii);
        end
    end
else
    error('Please load led serial command for Port array module before AddState');
end
end


function ResetMotorXY(module_name)
% module_name is BpodSystem.Modules.Name
    assert(any(contains(module_name,'LM')),'Error: Did not found LM module, please check bpod hardware!')
    ModuleWrite('LM1','H');
end
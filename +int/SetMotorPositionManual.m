function SetMotorPositionManual(module_name)
% module_name is BpodSystem.Modules.Name
    assert(any(contains(module_name,'LM')),'Error: Did not found LM module, please check bpod hardware!')
    f = figure;
    annotation('textbox', [0.05, 0.85, 0.1, 0.1], 'String','Setting Motor Position With Keyboard','EdgeColor','none','FontSize',20)
    annotation('textbox', [0.05, 0.7, 0.1, 0.1], 'String','a for nudge left (+x)','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.65, 0.1, 0.1], 'String','d for nudge right (-x)','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.6, 0.1, 0.1], 'String','w for nudge forward (+y)','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.55, 0.1, 0.1], 'String','s for nudge backward (-y)','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.5, 0.1, 0.1], 'String','f for full forward','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.45, 0.1, 0.1], 'String','b for full backward','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.4, 0.1, 0.1], 'String','h for reset x = 0','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.35, 0.1, 0.1], 'String','q for reset x = 0, y=0, y target','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.3, 0.1, 0.1], 'String','r toggle both water valve','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.25, 0.1, 0.1], 'String','space for exiting','EdgeColor','none','FontSize',10)
    annotation('textbox', [0.05, 0.2, 0.1, 0.1], 'String','l for set target Y position','EdgeColor','none','FontSize',10)
    while true
        waitforbuttonpress;
        if  strcmp(get(gcf,'currentcharacter'),'a')
            ModuleWrite('LM1','a');
        elseif  strcmp(get(gcf,'currentcharacter'),'d')
            ModuleWrite('LM1','d');
        elseif  strcmp(get(gcf,'currentcharacter'),'w')
            ModuleWrite('LM1','w');
        elseif  strcmp(get(gcf,'currentcharacter'),'s')
            ModuleWrite('LM1','s');
        elseif  strcmp(get(gcf,'currentcharacter'),'f')
            ModuleWrite('LM1','F');
        elseif  strcmp(get(gcf,'currentcharacter'),'b')
            ModuleWrite('LM1','R');
        elseif  strcmp(get(gcf,'currentcharacter'),'h')
            ModuleWrite('LM1','h');
        elseif  strcmp(get(gcf,'currentcharacter'),'q')
            ModuleWrite('LM1','H');
        elseif  strcmp(get(gcf,'currentcharacter'),'l')
            ModuleWrite('LM1','L');
        elseif  strcmp(get(gcf,'currentcharacter'),'r')
            % open both water valve for 100ms to 'release the pinch'
            SMA = NewStateMatrix();
            SMA = AddState(SMA, 'Name', 'TriggerRewards', ...
                'Timer', .1,...
                'StateChangeConditions', {'Tup', '>exit'},...
                'OutputActions', {'ValveState',3});
            SendStateMatrix(SMA);
            RunStateMatrix;
        elseif  strcmp(get(gcf,'currentcharacter'),' ') % space bar exits the function
            close(f)
            clc;
          break
        end
    end
end
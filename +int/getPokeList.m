function out = getPokeList(ind)
% out = getPokeList
% return a list of the poke names. You can append these with 'in','out','led','blue' to use in AddState.
out = {'MidR', 'TopR', 'BotR', 'MidC', 'TopL', 'BotL', 'MidL','BotC'};

if nargin > 0
	if isnumeric(ind)
		out = out(ind);
	else

      switch ind
         case 'left'
            out = out(cellfun(@(x)x(end)=='L', out));
         case 'right'
            out = out(cellfun(@(x)x(end)=='R', out));
         case 'top'
            out = out(cellfun(@(x)x(1)=='T', out));
         case 'middle'
            out = out(cellfun(@(x)x(1)=='M', out));
         case 'bottom'
			out = out(cellfun(@(x)x(1)=='B', out));					  
      end
		
			
	end
end

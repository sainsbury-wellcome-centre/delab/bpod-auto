function [newStateChangeCond] = convert_anyLR(StateChangeCond)
%[newStateChangeCond] = convert_anyLR(StateChangeCond)
%   Convert AnyL -> {BotL, MidL, TopL}
%   any -> {all ports} will be handeled in sma.AddState with handleSpecial
idx_to_be_deleted = [];
newStateChangeCond = {};
for ii = 1:2:length(StateChangeCond)
    this_StateChangeConditions = StateChangeCond{ii};
    if contains(this_StateChangeConditions,'Any') && (this_StateChangeConditions(4)=='L' || this_StateChangeConditions(4)=='R')
        idx_to_be_deleted = [idx_to_be_deleted,ii,ii+1];
        spec_str = this_StateChangeConditions(4:end);
        adding_StateChangeCond = {['Bot',spec_str],StateChangeCond{ii+1},['Mid',spec_str],StateChangeCond{ii+1},['Top',spec_str],StateChangeCond{ii+1}};
        newStateChangeCond = [newStateChangeCond,adding_StateChangeCond];
    %elseif contains(this_StateChangeConditions,'Any') %this Any means the real Any (any for all ports)
        %idx_to_be_deleted = [idx_to_be_deleted,ii,ii+1];
        %spec_str = this_StateChangeConditions(4:end);
        %adding_StateChangeCond1 = {['BotL',spec_str],StateChangeCond{ii+1},['MidL',spec_str],StateChangeCond{ii+1},['TopL',spec_str],StateChangeCond{ii+1}};
        %adding_StateChangeCond2 = {['BotR',spec_str],StateChangeCond{ii+1},['MidR',spec_str],StateChangeCond{ii+1},['TopR',spec_str],StateChangeCond{ii+1}};
        %adding_StateChangeCond3 = {['BotC',spec_str],StateChangeCond{ii+1},['MidC',spec_str],StateChangeCond{ii+1}};
        %newStateChangeCond = [newStateChangeCond,adding_StateChangeCond1,adding_StateChangeCond2,adding_StateChangeCond3];
    end
end
if isempty(idx_to_be_deleted)
    newStateChangeCond = StateChangeCond;
else
    StateChangeCond(idx_to_be_deleted) = [];
    newStateChangeCond = [newStateChangeCond,StateChangeCond];
end
end

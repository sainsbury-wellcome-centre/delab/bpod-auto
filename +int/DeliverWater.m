function peh = DeliverWater(on_time, drops, off_time, valve_idx)
% DeliverWater(on_time, drops, off_time)
% Inputs:
% on_time   Time (in seconds) to open the valve 
% off_time  Time (in seconds) to close the valve
% drops     # of times to open and close the valve. (Should be a multiple of 50) 
% valve_idx 1/2, valve index
% Output:
% peh       Returns a parsed events history, where each
%           trial is 50 drops.
% 

if nargin<4
    valve_idx = 1;
end

global BpodSystem
BpodSystem.trial_time_limit = 300;
BpodSystem.Data = struct();

assert(mod(drops,50)==0,'BPOD:WATER','Drops must be a multiple of 50');
assert(valve_idx==1 |valve_idx == 2,'valve must be 1 or 2');

n_loops=round(drops/50);
% check that drops is multiple of 50.

sma = NewStateMatrix();
% Each state matrix is limited to 128 states. This SMA will have 100 states. It would also be possible to write this using a GlobalCounter with just two states. However, there is currently a bug with GlobalCounters in the emulator.

for sx=1:50
    sma = AddState(sma,'Name',sprintf('valve_open_%d',sx),'Timer',on_time,...
        'StateChangeConditions',{'Tup',sprintf('valve_closed_%d',sx)},...
        'OutputActions',{'ValveState',valve_idx,'PWM4',255});
    if sx==50
        sma=AddState(sma,'Name', sprintf('valve_closed_%d',sx),'Timer',off_time,'StateChangeConditions',{'Tup','exit'},'OutputActions',{});
    else
        sma=AddState(sma,'Name', sprintf('valve_closed_%d',sx),'Timer',off_time,'StateChangeConditions',{'Tup',sprintf('valve_open_%d',sx+1)},'OutputActions',{});
    end

end

    
SendStateMatrix(sma); 

for lx=1:n_loops
    RawEvents = RunStateMatrix;
    BpodSystem.Data = AddTrialEvents(BpodSystem.Data,RawEvents);
    peh(lx) = BpodSystem.Data.RawEvents.Trial{end};
end

RunProtocol('Stop');

end


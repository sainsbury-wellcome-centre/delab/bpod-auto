
function stop_session = CheckForStop(sessid)

stop_session = false;

global BpodSystem;
if isempty(BpodSystem.zmqconn_sub)
    % no zmq class, or not alive, ignore
    data = '';
else
    [addr, data] = BpodSystem.zmqconn_sub.recvjson();
end

if ~isempty(data)
    fprintf(1,'Got message for rig %s\n', addr);

    age = 24*60*60*(now()-datenum(datetime(data.ts,'InputFormat','yyyy-MM-dd_HH:mm:ss')));% age is in seconds
    input_command = data.event;

    if strcmp(input_command,'stop_session') && age < 180
        sessid_zmq = data.sessid;
        if abs(str2double(sessid_zmq) - sessid) < 1 %same sessid, avoid fload error
            stop_session = true;
        end
    elseif strcmp(input_command,'stop_rigtest') && age < 180
        % stop rigtest, no need for check
        stop_session = true;
    elseif age >= 180
        fprintf(1,'too old zmq message, ignored \n');
    end
end

end
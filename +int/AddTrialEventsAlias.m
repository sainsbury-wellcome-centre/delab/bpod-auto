function newTE = AddTrialEventsAlias(TE, RawTrialEvents)
%newTE = int.AddTrialEventsAlias(TE, RawTrialEvents)
%   convert raw_event to human readable events, replace port name with
%   given alias
%   
%   Jingjie Li, 2021-12-26
%
if exist('~/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('~/bpod_fsm_config.ini');
elseif exist('example_config_files/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('example_config_files/bpod_fsm_config.ini');
    fprintf(2,'no FSM config file found, load default example file\n');
end
    
TE = AddTrialEvents(TE, RawTrialEvents);

last_peh = TE.RawEvents.Trial{end};
peh_event_names = fieldnames(last_peh.Events);
peh_event_names_to_replace = peh_event_names;

FSM_ports_name = fsm_config.port_list.port_alias(strcmp(fsm_config.port_list.port_module,'FSM'));
FSM_ports_idx = fsm_config.port_list.port_id(strcmp(fsm_config.port_list.port_module,'FSM'));

if any(contains(fsm_config.port_list.port_module,'PA')) %no PA module config for this rig
    PA_ports_name = fsm_config.port_list.port_alias(strcmp(fsm_config.port_list.port_module,'PA1'));
    PA_ports_idx = fsm_config.port_list.port_id(strcmp(fsm_config.port_list.port_module,'PA1'));
end

for ii = 1:length(peh_event_names)
    if contains(peh_event_names{ii},'PA1_Port') && any(contains(fsm_config.port_list.port_module,'PA')) % extra PA ports poke events 
        port_idx = peh_event_names{ii}(9);
        port_name = PA_ports_name{PA_ports_idx==str2double(port_idx)};
        peh_event_names_to_replace{ii} = [port_name peh_event_names{ii}(10:end)];
    elseif contains(peh_event_names{ii},'Port') % normal FSM ports poke events
        port_idx = peh_event_names{ii}(5);
        port_name = FSM_ports_name{FSM_ports_idx==str2double(port_idx)};
        peh_event_names_to_replace{ii} = [port_name peh_event_names{ii}(6:end)];
    else % not an poke port events
        peh_event_names_to_replace{ii} = '';
    end
end

for ii = 1:length(peh_event_names_to_replace)
    if ~isempty(peh_event_names_to_replace{ii})
        last_peh.Events.(peh_event_names_to_replace{ii}) = last_peh.Events.(peh_event_names{ii}); %add a field with updated name
        last_peh.Events = rmfield(last_peh.Events,peh_event_names{ii}); %remove old file
    end
end

TE.RawEvents.Trial{end} = last_peh;
newTE = TE;
end


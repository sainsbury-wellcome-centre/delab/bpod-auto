function [target_mat,OutputActions_rm] = conv_PA_led_mat(fsm_config,PA_leds_cell)
%[output_mat] = conv_PA_led_mat(fsm_config,PA_leds_cell)
%[target_mat,OutputActions_rm] = conv_PA_led_mat(fsm_config,PA_leds_cell)
%
% convert each state (led control) to a matrix to aid processing (2x4 matrix)
    % for that matrix, each column indicate the PA_port#, the first row
    % indicate the LED1 PWM intensity (0-255), the second row indicate the
    % LDE2 PWM intensity (0-255).
% OutputActions_rm is the OA that removed all PA led related stuff.
    
    target_mat = zeros(2,4);
    OutputActions_rm = PA_leds_cell;
    cell_idx_to_remove = [];
    port_name_length = numel(fsm_config.port_list.port_alias{1});
    for jj = 1:2:length(PA_leds_cell)
        % decoding led info
        if numel(PA_leds_cell{jj}) < 7 %too short, not an led/pwm state
            continue
        end
        port_name = PA_leds_cell{jj}(1:port_name_length);
        led_name = PA_leds_cell{jj}((port_name_length+1):end);
        port_PA_id = fsm_config.port_list.port_id(strcmp(port_name,fsm_config.port_list.port_alias));
        port_idx = cellfun(@(x) contains(port_name,x), fsm_config.port_list.port_alias);
        if isempty(port_PA_id) || isempty(port_idx) %not an led/pwm state
            continue
        end
        if ~strcmp(fsm_config.port_list.port_module{port_idx},'FSM') % not an FSM defult port, only process PA ports
            led_idx = fsm_config.led_alias.led_idx(strcmp(led_name,fsm_config.led_alias.available_alias));
            led_intensity = PA_leds_cell{jj+1};
            % now fill processing mat with decoded led info
            target_mat(led_idx,port_PA_id) = led_intensity;
            % now remove PA cell element 
            cell_idx_to_remove = [cell_idx_to_remove,jj,jj+1];
        end
    end
    OutputActions_rm(cell_idx_to_remove) = [];
        
end


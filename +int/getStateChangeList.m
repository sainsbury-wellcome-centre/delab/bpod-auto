
function out = getStateChangeList(list)
% out = getStateChangeList(['pokes','pokein','pokeout'])
% Get a list of the available event names that can be used in AddState StateChangeConditions

%EventList = {'MidRin','MidRout','TopRin','TopRout','BotRin','BotRout','MidCin','MidCout','TopLin' ,'TopLout' ,'BotLin','BotLout', 'MidLin','MidLout','BotCin' ,'BotCout','exin','exout',              'LeftSoundOn','LeftSoundOff','RightSoundOn','RightSoundOff', 'Wire1High',   'Wire1Low',   'Wire2High',   'Wire2Low',   'Wire3High',   'Wire3Low',   'Wire4High',   'Wire4Low',   'SoftCode1',   'SoftCode2',   'SoftCode3',   'SoftCode4',   'SoftCode5',   'SoftCode6',   'SoftCode7',   'SoftCode8',   'SoftCode9',   'SoftCode10',   'Unused',   'Tup',   'GlobalTimer1_End',   'GlobalTimer2_End',   'GlobalTimer3_End',   'GlobalTimer4_End','GlobalTimer5_End',   'GlobalCounter1_End',   'GlobalCounter2_End',   'GlobalCounter3_End', 'GlobalCounter4_End',   'GlobalCounter5_End'};
% old  EventList = {'TopCin','TopCout','MidRin','MidRout','MidCin','MidCout','MidLin','MidLout','BotRRin','BotRRout','BotRin','BotRout', 'BotLin','BotLout','BotLLin','BotLLout','BotCin','BotCout','LeftSoundOn','LeftSoundOff','RightSoundOn','RightSoundOff', 'Wire1High',   'Wire1Low',   'Wire2High',   'Wire2Low',   'Wire3High',   'Wire3Low',   'Wire4High',   'Wire4Low',   'SoftCode1',   'SoftCode2',   'SoftCode3',   'SoftCode4',   'SoftCode5',   'SoftCode6',   'SoftCode7',   'SoftCode8',   'SoftCode9',   'SoftCode10',   'Unused',   'Tup',   'GlobalTimer1_End',   'GlobalTimer2_End',   'GlobalTimer3_End',   'GlobalTimer4_End','GlobalTimer5_End',   'GlobalCounter1_End',   'GlobalCounter2_End',   'GlobalCounter3_End', 'GlobalCounter4_End',   'GlobalCounter5_End'};
global BpodSystem
EventList = BpodSystem.StateMachineInfo.EventNames;


if nargin == 0
	out = EventList;
else
	switch list
	case {'pokes'}
		out = EventList([1:8,95:102]);
	case {'pokein'}
		out = EventList([1:2:7,95:2:101]);
	case {'pokeout'}
		out = EventList([2:2:8,96:2:102]);
	end
end
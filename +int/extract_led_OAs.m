function [OutputActions_rm,OutputActions_led] = extract_led_OAs(fsm_config,OutputActions)
%OutputActions_out = extract_led_OAs(OutputActions)
%   extract_led_OAs(OutputActions)
OutputActions_led = {};
OutputActions_rm = OutputActions;
led_OA_idx = 1;
cell_idx_to_remove = [];
port_name_length = numel(fsm_config.port_list.port_alias{1});
for ii = 1:2:length(OutputActions)
    if length(OutputActions{ii}) > 6 % at least 7 chars
        potential_port_name = OutputActions{ii}(1:port_name_length);
        port_idx = find(cellfun(@(x) strcmp(potential_port_name,x), fsm_config.port_list.port_alias), 1);
        
        potential_led_name = OutputActions{ii}((port_name_length+1):end);
        light_idx = find(cellfun(@(x) strcmp(potential_led_name,x), fsm_config.led_alias.available_alias), 1);
        
        if ~isempty(port_idx) && ~isempty(light_idx)
            OutputActions_led{led_OA_idx} = OutputActions{ii};
            OutputActions_led{led_OA_idx+1} = OutputActions{ii+1};
            led_OA_idx = led_OA_idx + 2;
            % now, remove led related cell element from the cell array
            cell_idx_to_remove = [cell_idx_to_remove,ii,ii+1];
        end
    end
end
OutputActions_rm(cell_idx_to_remove) = [];
end


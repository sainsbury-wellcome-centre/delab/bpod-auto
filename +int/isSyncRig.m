function out = isSyncRig()

    if exist('~/.photometry','file')
        out.secsperbit = 4/130;
        out.trialbits = 12;
        out.header = 5;
    elseif exist('~/.ephys','file')
        out.secsperbit = 1e-3;
        out.trialbits = 16;
        out.header = 5;
    else
        out = false;
    end
    
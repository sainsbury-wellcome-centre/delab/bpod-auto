function [PA_led_to_use_out] = create_PA_led_cell(PA_led_array,PA_led_array_adding)
%[PA_led_to_use_out] = create_PA_led_cell(PA_led_to_use)
%   add or create PA_led_cells
%   input: none for creating. PA_led_to_use_out = int.create_PA_led_cell()
%   or
%   a cell array of let OutputActions will be used for one single state
%   PA_led_to_use_out = int.create_PA_led_cell({'TopLled',255,'MidRblue',100})
%   PA_led_to_use_out = int.create_PA_led_cell(PA_led_to_use_out,{'TopLled',255,'MidRblue',100})
if nargin < 1
    PA_led_to_use_out = {};
elseif nargin < 2 && numel(PA_led_array) > 1
    PA_led_to_use_out = {PA_led_array};
elseif nargin < 2
    PA_led_to_use_out = PA_led_array;
elseif numel(PA_led_array_adding) > 1
    PA_led_to_use_out = [PA_led_array,{PA_led_array_adding}];
elseif numel(PA_led_array_adding) == 1
    PA_led_to_use_out = [PA_led_array,PA_led_array_adding];
else
    error('something wrong woti PA led array formatting')
end
end


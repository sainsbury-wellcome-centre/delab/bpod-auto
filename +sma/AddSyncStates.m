function sma = AddSyncStates(sma, trialnum, varargin)
% sma = AddSyncStates(sma, trialnum, varargin)
% Adds sync states to the state_machine so that bpod can sync with an external
% device (e.g. open-ephys)
%
% Inputs:
% sma           A "StateMachine" struct
% trialnum      The trialnum to send
%
% Optional Inputs [defaults]:
% timebin       [0.001 s] How long each bit should send for
% bits          [16] How many bits to use to encode the trial number
% header        [5] (or 101 in binary) will be used to start each sync pulse
% return_state  [] If this is empty, this function will insert the sync_pulse at
%               the end of the trial before the exit state. It will actually find 
%               the exit states and replace them with sync_states, which will exit
%               after it is done. If you specify the return state (e.g. "trial_start")
%               we will just add states in the normal way.

iod = @utils.inputordefault;
[timebin, varargin] = iod('secsperbit',1e-3,varargin);
% The open-ephys sample rate is 30KHz, so 1ms is 30samples.
% for photometry, the sample rate is 130Hz, so input varargin to change secsperbit = 4/130 to have 4samples/bit

[bits, varargin] = iod('trialbits',16,varargin);
% We will use 16 bits to encode the trial #, allowing 65536 trials per session
% for photometry, we use input varargin to change trialbits = 12 to have 4096 trials per session

[header, varargin] = iod('header',5, varargin);
% We will use a 3 bit 101 header (decimal = 5) to indicate the start of the sync state. 
% This also allows us to measure the samples/bit if the sampling rate changes.
% same for photometry

[return_state, varargin] = iod('return_state','>exit',varargin);
% After the sync_state is done which state should we go to?
% By default go to "trial_start"

[state_name, varargin] = iod('state_name','sync_states',varargin);
% What name should the states have in the parsed_events

[bncchannel, varargin] = iod('bncchannel', 2, varargin);
% BNC Channel of BPod. Not the same as BNC channel of OpenEphys

ind2name = @(x) sprintf('%s_%02d',state_name,x);

if isempty(return_state)
% Those code were for old bpod, not working for this version

%     state_number = size(sma.InputMatrix,1) + 1; 
%     % The first sync_state's state_number is the # of existing states + 1 
%     sma.StateNames{end+1} = ind2name(1);
%     sma.InputMatrix(isnan(sma.InputMatrix)) = state_number;
%     sma.StatesDefined(end+1)=0;
%     % Any events that transitioned to exit will now transition to the sync_states
%     return_state = 'exit';
%     % Once the sync_states are done, then exit.
end

int2send = bitshift(header,bits) + trialnum;
char2send = dec2bin(int2send);

nextchar = '0';
done = false;
state_times = [];
while ~done
    ind = find(char2send==nextchar,1,'first');
    if isempty(ind)
        state_times(end+1) = numel(dec2bin(header)) + bits - sum(state_times);
        break
    end
    state_times(end+1) = ind - 1;
    char2send = char2send(ind:end);
    if nextchar == '0'
        nextchar = '1';
    else
        nextchar = '0';
    end
end

for sx = 1:numel(state_times)
   this_name = ind2name(sx); 
   if sx==numel(state_times)
       next_name = return_state;
   else
       next_name = ind2name(sx+1);
   end
       
   if mod(sx,2) == 1
        sma = AddState(sma, 'name', this_name, 'Timer', state_times(sx)*timebin, ...
                  'StateChangeConditions',{'Tup',next_name},...
                  'OutputActions',{'BNCState',bncchannel});
   else
        sma = AddState(sma, 'name', this_name, 'Timer', state_times(sx)*timebin, ...
                  'StateChangeConditions',{'Tup',next_name},...
                  'OutputActions',{});
   end 
    
end

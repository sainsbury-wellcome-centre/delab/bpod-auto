function sma_out = AddState(sma, varargin)
%using the same input as AddState
%   converting bpod FSM alias ~/bpod_fsm_config.ini
% all alias are managed in 
% supporting alias:
% PokePort name - TopL,TopR,MidL,MidC,MidR,MidL,BotL,BotC,BotR for
% yellow/blue for in/out
% LickPort Name - LickL, LickR for in/out
% PlaySound/StopSound (same for both psychSound of HiFi Sound)
% MoveMotor - {'MoveMotor','FullForward'}
% {'AirValve1','on','WaterValve1',0,'AirValve2',true,'WaterValve2','off'} -> {'ValveState',12}
%
% By Jingjie Li

MAX_TIMER = 100;  % We want all trials to end within 100 seconds if the animal is sleeping.
iod = @utils.inputordefault;

[StateName, varargin] = iod('Name',[],varargin);
[StateTimer, varargin] = iod('Timer', MAX_TIMER, varargin);
[StateChangeConditions, varargin] = iod('StateChangeConditions',{},varargin); % Eventually this should be current state+1.
[OutputActions, varargin] = iod('OutputActions',{},varargin);
[shutPAleds, varargin] = iod('ShutDownPALeds',1,varargin);

% if exist('~/bpod_fsm_config.ini','file')
%     fsm_config = utils.ini2struct('~/bpod_fsm_config.ini');
% elseif exist('example_config_files/bpod_fsm_config.ini','file')
%     fsm_config = utils.ini2struct('example_config_files/bpod_fsm_config.ini');
%     fprintf(2,'no FSM config file found, load default example file');
% end
fsm_config = sma.fsm_config;
OutputActions = assign_play_sound_string(OutputActions);%convert 'PlaySound' action to working one like 'SoftCode' for psychsound
OutputActions = assign_move_motor_string(OutputActions,fsm_config);% convert {'MoveMotor','FullForward'} to {'LM1','F'}
OutputActions = assign_valve_string(OutputActions,fsm_config); % convert {'AirValve1','on','WaterValve1',0,'AirValve2',true,'WaterValve2','off'} to {'ValveState',12}

if ~any(contains(fsm_config.port_list.port_module,'PA')) %no PA module config for this rig
    shutPAleds = false;
end

[StateChangeConditions] = int.convert_anyLR(StateChangeConditions);
[OutputActions] = int.convert_anyLR(OutputActions);

for ii = 1:2:length(StateChangeConditions)
    this_StateChangeConditions = StateChangeConditions{ii};
    replaced_cond_id = find(cellfun(@(x) contains(this_StateChangeConditions,x), fsm_config.port_list.port_alias));
    if isempty(replaced_cond_id) % not known replaceable Event
        newName = StateChangeConditions{ii};%keep using old names
    elseif strcmp(fsm_config.port_list.port_module{replaced_cond_id},'FSM') && contains(fsm_config.port_list.port_alias{1},'Lick')%head-fix config, input are LickL/R
        if numel(this_StateChangeConditions)<7
            %short command, like LickL, LickR, by default it will be 'in'
            in_or_out = 'In';
        else %have specific in/out command, like LickLin, LickRout
            in_or_out = correct_event_case(this_StateChangeConditions(6:end));
        end
        newName = sprintf('Port%d%s',fsm_config.port_list.port_id(replaced_cond_id),in_or_out);
    elseif strcmp(fsm_config.port_list.port_module{replaced_cond_id},'FSM') % PokeWall Config
        % Converting like 'MidCIn' to 'Port2In'. For FSM default ports
        in_or_out = correct_event_case(this_StateChangeConditions(5:end));
        newName = sprintf('Port%d%s',fsm_config.port_list.port_id(replaced_cond_id),in_or_out);
    else % in certain PortArray module
        % Converting like 'TopRIn' to 'PA1_Port1In'. For PortArray Ports
        in_or_out = correct_event_case(this_StateChangeConditions(5:end));
        newName = sprintf('%s_Port%d%s',fsm_config.port_list.port_module{replaced_cond_id},fsm_config.port_list.port_id(replaced_cond_id),in_or_out);
    end
    StateChangeConditions{ii} = newName;
end

[OutputActions_rm,OutputActions_led] = int.extract_led_OAs(fsm_config,OutputActions);
port_name_length = numel(fsm_config.port_list.port_alias{1});
% go though all led actions to extract the control of FSM default leds
for ii = 1:2:length(OutputActions_led)
    potential_port_name = OutputActions_led{ii}(1:port_name_length);
    if any(contains(fsm_config.port_list.port_alias,potential_port_name)) %this output is start by a port alias name, might be a port light control
        port_idx = find(cellfun(@(x) contains(potential_port_name,x), fsm_config.port_list.port_alias));
        light_alias = OutputActions_led{ii}((port_name_length+1):end);
        light_id = find(cellfun(@(x) strcmp(light_alias,x), fsm_config.led_alias.available_alias));
        if strcmp(fsm_config.port_list.port_module{port_idx},'FSM')
            % default FSM ports, Converting like 'MidCled' to 'PWM2'
            newName = sprintf('%s%d',fsm_config.led_alias.using_led_name{light_id},fsm_config.port_list.port_id(port_idx));
            OutputActions_led{ii} = newName;
        end
    end
    % adjust brightness, normal brightness is from 0-255, with interger,
    % however we want to accept input from 0 to 1
    if OutputActions_led{ii+1} <= 1 %treat as float input, convert to int
        new_led_val = ceil(OutputActions_led{ii+1}*255);
        OutputActions_led{ii+1} = new_led_val;
    end
end

if any(contains(fsm_config.port_list.port_module,'PA')) %no PA module config for this rig
    %only process those stuff below if we have PA module, we need to
    %extract the led name for PA module stuff

    % extract PA led command
    [PA_led_command_mat,OutputActions_led] = int.conv_PA_led_mat(fsm_config,OutputActions_led);
    if any(PA_led_command_mat(:))
        serial_code = int.extract_target_serial_command_code(PA_led_command_mat,sma);
    
        if isempty(serial_code)
            error('Please load CORRECT led serial command for Port array module before AddState');
        end
        
        OutputActions_PA = {'PA1',serial_code};
        OutputActions = [OutputActions_rm,OutputActions_led,OutputActions_PA];
    elseif shutPAleds
        OutputActions_PA = {'PA1',255}; %code to close all PA leds
        OutputActions = [OutputActions_rm,OutputActions_led,OutputActions_PA];
    else
        % no PA command need to be send
        OutputActions = [OutputActions_rm,OutputActions_led];
    end

else
    %no PA module, skip
    OutputActions = [OutputActions_rm,OutputActions_led];
end % if any(contains(fsm_config.port_list.port_module,'PA'))
%disp(OutputActions);
%sma_out = sma;%for testing
StateChangeConditions = LinkSync(StateChangeConditions);
StateChangeConditions = handleSpecial(StateChangeConditions);
StateChangeConditions = handel_current_state(StateChangeConditions,StateName);

sma_out = AddState(sma, 'Name', StateName, 'Timer', StateTimer, 'StateChangeConditions', StateChangeConditions, 'OutputActions', OutputActions);
end

function OA_new = assign_valve_string(OA,fsm_config)
    % converting valve alias to ValveState byte control
    % for example:
    % {'AirValve1',1,'WaterValve1',0,'AirValve2',1,'WaterValve2',1}
    % will be converted to {'ValveState',14} i.e.(8+5+2)
    % 
    % {'Port1led',1,'PlaySound',2,'AirValve1','on','WaterValve1',0,'AirValve2',true,'WaterValve2','off'}
    % will be converted to {'ValveState',12} i.e.(8+4)
    % Valve alias are managed in bpod_fsm_config.ini
    % you can control each valve with on/off,1/0,true/false
    OA_new = OA;
    if isfield(fsm_config,'valves') %have valves settings in config file, do conversion
        cell_idx_to_remove = [];
        valve_byte_value = [];
        for ii = 1:2:length(OA)
            this_oa = OA{ii};
            if any(contains(fsm_config.valves.command_alias ,this_oa)) 
                valve_idx = find(cellfun(@(x) contains(this_oa,x), fsm_config.valves.command_alias));
                this_valve_byte = fsm_config.valves.command_byte(valve_idx);
                valve_cmd = OA{ii+1};
                if ischar(valve_cmd)
                    if strcmpi(valve_cmd,'on')
                        disp(''); % do nothing here
                    elseif strcmpi(valve_cmd,'off')
                        this_valve_byte = 0;
                    end
                elseif isnumeric(valve_cmd) || islogical(valve_cmd)
                    valve_cmd = logical(valve_cmd);
                    if ~valve_cmd
                        this_valve_byte = 0;
                    end
                else
                    error('use on/off or 0/1 or true/false to control each valve!')
                end
                valve_byte_value = [valve_byte_value,this_valve_byte];
                cell_idx_to_remove = [cell_idx_to_remove,ii,ii+1];
            end
        end
        valve_byte_value = valve_byte_value(valve_byte_value>0);
        assert(numel(valve_byte_value)==numel(unique(valve_byte_value)),'Error! Duplicated Valve Control! Pease Check!')
        OA_valve = {'ValveState',sum(valve_byte_value)};
        OA_new(cell_idx_to_remove) = [];
        OA_new = [OA_new,OA_valve];
    end
end

function OA = assign_move_motor_string(OA,fsm_config)
    global BpodSystem
    motor_name = '';
    if any(contains(BpodSystem.Modules.Name,'LM'))
        LM_module_posi = find(contains(BpodSystem.Modules.Name,'LM'));
        motor_name = BpodSystem.Modules.Name{LM_module_posi(1)};
    end
    for ii = 1:2:length(OA)
        if strcmpi(OA{ii},'MoveMotor')
            assert(isfield(fsm_config,'lick_motor'),'Error: please incorrect FSM config file, see example_config_files/head_fixed_config_example.ini');
            assert(~isempty(motor_name),'Error: no lick motor module connected! Need LM Module!')
            OA{ii} = motor_name;
            if length(OA{ii+1}) > 1 %the command is only one byte, this is a alias, need to be converted to a one byte command
                replace_alias_id = find(cellfun(@(x) strcmpi(OA{ii+1},x), fsm_config.lick_motor.command_alias));
                assert(~isempty(replace_alias_id),sprintf('Error: %s is not a correct command for lick motor! see your fsm config file!',OA{ii+1}));
                newName = fsm_config.lick_motor.command_byte{replace_alias_id};
                OA{ii+1} = newName;
            end
        end
    end
end

function OA = assign_play_sound_string(OA)
    global BpodSystem
    if strcmp(BpodSystem.SoundModule,'PsychSound')
        for ii = 1:2:length(OA)
            if strcmpi(OA{ii},'PlaySound') 
                OA{ii} = 'SoftCode';
            elseif strcmpi(OA{ii},'StopSound') 
                OA{ii} = 'SoftCode';
                OA{ii+1} = OA{ii+1} + 128; %id+128 means stop sound for psychsound
            elseif strcmpi(OA{ii},'StopAllSound') 
                OA{ii} = 'SoftCode';
                OA{ii+1} = 255; %this code stops all sound
            end
        end
    elseif strcmp(BpodSystem.SoundModule,'PiSound')
        %other options for further dev, like Piound, etc
    elseif strcmp(BpodSystem.SoundModule,'BpodHiFi')
        for ii = 1:2:length(OA)
            if strcmpi(OA{ii},'PlaySound') 
                OA{ii} = 'HiFi1';
            elseif strcmpi(OA{ii},'StopSound') 
                OA{ii} = 'HiFi1';
            elseif strcmpi(OA{ii},'StopAllSound') 
                OA{ii} = 'HiFi1';
                OA{ii+1} = 'X'; %this code stops all sound
            end
        end
    end
end

% This checks if this state points to exit. If it does, it will point to the first sync state.
% However, it seems like this should be done explicitly in AddSyncStates.m?
% For now, we 
function out = LinkSync(cond)
    global BpodSystem
    out = cond;
    if isstruct(BpodSystem.Sync)
        for cx = 1:2:numel(cond)
            if strcmpi(cond{cx+1},'exit')
                out{cx+1} = 'sync_states_01';%point to the first
            end
        end
    end
end


function out = handleSpecial(cond)
% This function handles the special conditions AnyIn / AnyOut / OtherIn / OtherOut
not_special = true;
for cx = 1:2:numel(cond)
    if strncmpi(cond{cx},'any',3)
        not_special = false;
        if numel(intersect(cond(1:2:end), int.getStateChangeList('pokes')))
            error('Bpod:AddState','If using AnyIn / AnyOut keyword you cannot specify other ports')
        else
            transitioncondition = cond{cx};
            if strncmpi(transitioncondition(4:end),'in',2)
                allp = int.getStateChangeList('pokein');
            elseif strncmpi(transitioncondition(4:end),'out',3)
                allp = int.getStateChangeList('pokeout');
            end
            for px = 1:numel(allp)
                out{2*px-1} = allp{px};
                out{2*px} = cond{cx+1};
            end
            cond(cx:cx+1) = [];
            out = [cond out];
        end
    elseif strncmpi(cond{cx},'other',5)
        not_special = false;
        out = handleOther(cond);
    end
end

if not_special
    out = cond;
end
end

function out = handel_current_state(cond,this_name)
out = cond;
    for cx = 2:2:numel(cond)
        if strcmp(cond{cx},'current_state')%current_state found
            out{cx} = this_name;
        end
    end
end

function out = handleOther(cond)
    pokes = int.getStateChangeList('pokes');
    spec = {};
    cind = [];
    oind = [];
    ocmd = {};
    pind = [];
    out = {};
    for cx = 1:2:numel(cond)
        % Check each condition
        this_cond = cond{cx};
        for px = 1:numel(pokes)
            if strcmpi(this_cond, pokes{px})
                % Does this condition match a poke?
                % Get which poke it matches and the index in the condition
                spec{end+1} = this_cond(1:(end-2-mod(px+1,2)));
                % We want to cut off the "in / out" at the end of the condition. 
                % Since the in / out are interleaved, with In==odd and Out==even we use mod to take off the extra character.
                pind(end+1) = px;
                cind(end+1) = cx;
                break
            elseif strncmpi(this_cond, 'other',5)
                % Does this condition match "other"
                oind(end+1) = cx;
                ocmd{end+1} = this_cond(6:end);
                break
            end
        end
    end

    if numel(oind) > 1
        if any(strcmpi(ocmd{1}, ocmd(2:end)))
            error('Bpod:AddState','When using "Other" you can specify at most 1 "OtherIn" and 1 "OtherOut"');
        end
    end

    % Put the "normal" conditions back in:

    for cx = 1:2:numel(cond)
        if all(cx ~= oind)
            % Make sure this condition is not an "other" condition and put it back into the list
            out{end+1} = cond{cx};
            out{end+1} = cond{cx+1};
        end
    end
    
    for ox = 1:numel(oind)
       if strcmpi(ocmd{ox},'in')
           % We have an OtherIn command.
           pokes_to_add = setdiff(1:2:numel(pokes), pind);
       else
           pokes_to_add = setdiff(2:2:numel(pokes), pind);
       end
       for px = 1:numel(pokes_to_add)
           out{end+1} = pokes{pokes_to_add(px)};
           out{end+1} = cond{oind(ox)+1}; % THis is the state associated with this command.
       end
    end
end

function out = correct_event_case(in_name)
    if strcmp(in_name,'in')
        out = 'In';
    elseif strcmp(in_name,'out')
        out = 'Out';
    else
        out = in_name;
    end
end

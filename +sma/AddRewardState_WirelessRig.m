function SMA = AddRewardState_WirelessRig(SMA, varargin)
% This function is written for temporary use in wireless optogenetics rig
% only. It is to counter a bug in the wireless system that blocks reward
% port from responding to pokes, so this function will dispense reward
% without the need to settle in the reward port

% SMA = AddRewardState(SMA, varargin)
% Inputs				Default
%-------- 			-----------------
% name				'wait_for_rewardpoke'
% volume			3	the volume of reward in microliters
% drink_time		6	the time in seconds to allow the subject to drink before it is a violation
% lights				'both'   (can be 'blue'|'led'|'both')
% violation_state 	'violationstate'. Pokes in other ports will to 'violationstate'. Set this to empty to ignore violations.
% post_rew_state  	'ITI'.
% wait_timeout  	6000s after this time go to the post_rew_state 
% reward_sound_id 	0
% wait_for_reward_sound_id 0
% time_scale [] 	A struct with fields [rewmin, rewmax, t, n_steps]. Will decrease reward from max to min over n_steps until time t. After time t, subject gets min reward.
% short_viol_sound_id a sound to play when poking in unlit port.
% loops 			1 If loops == 0, do not deliver any reward. If loops > 1 open and close the valve `loops` times giving `volume` each time.
%
% e.g. 
% SMA = AddRewardState(SMA, 'name','myreward','volume',24);
% SMA = AddRewardState(SMA, 'name','myreward','volume',24);
% SMA = AddRewardState(SMA, 'name','reward_two','volume',24, 'wait_timeout',1,'lights','blue');


inpd = @utils.inputordefault;

name 			= inpd('name','wait_for_rewardpoke', varargin);
volume 			= inpd('volume', 3, varargin);
drink_time 		= inpd('drink_time', 3, varargin);
lights 			= inpd('lights', 'both', varargin);
extra_oa = inpd('extra_output_action', {}, varargin);
violation_state = inpd('violation_state', 'violationstate', varargin);
post_rew_state	= inpd('post_rew_state', 'ITI', varargin);
wait_timeout 	= inpd('wait_timeout',30, varargin);
reward_sound_id    = inpd('reward_sound_id', 0 , varargin);
wait_for_reward_sound_id = inpd('wait_for_reward_sound_id', 0 , varargin);
time_scale = inpd('time_scale', [], varargin);
short_viol_sound_id = inpd('short_viol_sound_id', 0 , varargin);
loops = inpd('loops', 1 ,varargin);
valve_off_time = inpd('valve_off_time', 0.15 ,varargin);

play_sound_each_loop = inpd('play_sound_each_loop', false, varargin);
soft_drink_time = inpd('soft_drink_time', true, varargin);
SMA.NumRewardStates = SMA.NumRewardStates + 1;
suffix = sprintf('_%d', SMA.NumRewardStates);
if isempty(violation_state)
	violation_state = 'current_state';
end

assert(loops >= 0 && mod(loops,1) == 0 && loops < 50, 'AddRewardState: loops must be a positive integer less than 20, not %f', loops)

switch lights
case 'both'
	OA = {'BotCyellow',255, 'BotCblue',255};
case 'blue'
	OA = {'BotCblue',255};
case 'led'
	OA = {'BotCyellow',255};
otherwise
	fprintf(2,'\nDo not know how to handle %s lights\n',lights);
end

if loops > 0
    valve_time = GetValveTimes(volume, 1); 
else
	valve_time = 0.01; 
	% We use this variable to control the duration of the 
	% rewardelivery state even though we do not open the valve
end

SMA = sma.AddState(SMA, 'name', name,'Timer', 0.3,...
		'StateChangeConditions', {'Tup', [ 'settle_in_rewardport' suffix ],'OtherIn',violation_state},...
		'OutputActions', {'PlaySound', wait_for_reward_sound_id});

if ~isempty(time_scale)
	warning('scaled rewards not implemented yet')

end	

SMA = sma.AddState(SMA, 'name',['settle_in_rewardport0' suffix],'Timer', wait_timeout,...
	'StateChangeConditions', {'Tup',post_rew_state,'BotCIn', ['settle_in_rewardport' suffix],'OtherIn',['rew_short_viol' suffix]},...
	'OutputActions', [OA,extra_oa]);

SMA = sma.AddState(SMA, 'name', [ 'rew_short_viol' suffix ], 'Timer', 0.05,...
	'StateChangeConditions', {'Tup', ['settle_in_rewardport0' suffix] },...
	'OutputActions', {'PlaySound',short_viol_sound_id});

SMA = sma.AddState(SMA, 'name', [ 'settle_in_rewardport' suffix ],'Timer', 0.05,...
	'StateChangeConditions', {'BotCOut', ['settle_in_rewardport0' suffix],'Tup',['reward_delivery' suffix],'BotCIn','current_state','OtherIn',['rew_short_viol' suffix]},...
	'OutputActions', [OA,extra_oa]);

if loops == 0	
  % Do not open the valve
	SMA = sma.AddState(SMA, 'name', sprintf('reward_delivery%s', suffix),'Timer',valve_time,...
		'StateChangeConditions',{'Tup', sprintf('drink_state_in%s', suffix),}, 'OutputActions',[OA,{'PlaySound', reward_sound_id}]);

elseif loops == 1 % Don't mess with the default behavior.
	SMA = sma.AddState(SMA, 'name', sprintf('reward_delivery%s', suffix),'Timer',valve_time,...
		'StateChangeConditions',{'Tup', sprintf('drink_state_in%s', suffix),}, 'OutputActions',[OA,{'ValveState',1,'PlaySound', reward_sound_id}]);
else 
	% loops is 2 or more 
	% We need to use the same initial state name as the first state. 
	% So we deliver water the first time and then loop
	SMA = sma.AddState(SMA, 'name', sprintf('reward_delivery%s', suffix),'Timer',valve_time,...
		'StateChangeConditions',{'Tup', sprintf('reward_off%s_%d', suffix ,1)}, 'OutputActions',[OA,{'ValveState',1,'PlaySound', reward_sound_id}]);

	if ~play_sound_each_loop
		reward_sound_id = 0; % Set ID to zero so that the sound is not played each time.
	end

	for lx = 2:loops

		SMA = sma.AddState(SMA, 'name', sprintf('reward_off%s_%d', suffix ,lx-1),'Timer', valve_off_time,...
				'StateChangeConditions',{'Tup', sprintf('reward_delivery%s_%d', suffix ,lx)});

		if lx==loops % The last in the loop
			next_state = sprintf('drink_state_in%s', suffix);
		else
			next_state = sprintf('reward_off%s_%d', suffix ,lx);
		end

		SMA = sma.AddState(SMA, 'name', sprintf('reward_delivery%s_%d', suffix ,lx),'Timer',valve_time,...
			'StateChangeConditions',{'Tup', next_state}, ...
			'OutputActions',[OA,{'ValveState',1,'PlaySound', reward_sound_id}]);

			
	end

	
end
if soft_drink_time
	gtind = find(SMA.GlobalTimers.IsSet==0,1,'first');
else
	gtind = 0;
end

if gtind
	SMA = SetGlobalTimer(SMA, gtind, drink_time);
	gtend = sprintf('GlobalTimer%d_End', gtind);

	SMA = sma.AddState(SMA, 'name', ['drink_state_in' suffix],'Timer',0,...
		'StateChangeConditions',{'BotCOut', ['drink_state_out' suffix], 'Tup', ['drink_state_in2' suffix]},...
		'OutputActions',[OA, {'GlobalTimerTrig', gtind}]);

    
	SMA = sma.AddState(SMA, 'name', ['drink_state_in2' suffix],'Timer',0,...
		'StateChangeConditions',{'BotCOut', ['drink_state_out' suffix], gtend, post_rew_state},...
		'OutputActions',OA);
    
	SMA = sma.AddState(SMA, 'Name', ['drink_state_out' suffix], ...
                'Timer', .4,...
                'StateChangeConditions', {'Tup', post_rew_state,'BotCIn',['drink_state_in2' suffix],gtend, post_rew_state,'OtherIn',violation_state},...
                'OutputActions', OA);
else
	% All the global timers are already used, so we can't use one.
	SMA = sma.AddState(SMA, 'name', ['drink_state_in' suffix],'Timer',drink_time,...
		'StateChangeConditions',{'BotCOut', 'drink_state_out'},...
		'OutputActions',OA);

	SMA = sma.AddState(SMA, 'Name', ['drink_state_out' suffix], ...
                'Timer', .4,...
                'StateChangeConditions', {'Tup', post_rew_state,'BotCIn',['drink_state_in' suffix],'OtherIn',violation_state},...
                'OutputActions', OA);
end

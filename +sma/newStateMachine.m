function sma = newStateMachine(PA_leds_to_use)
% sma = newStateMachine(PA_leds_to_use)
%   leds on TopL, TopR, MidL, MidR are on PortArray, controling those leds
%   is performed by sending specific serial command, Serial Command can
%   only be send once per state, and the serial messages need to be
%   pre-loaded before running the state machine.
%
%   In order to use those PortArray Leds, you need to specify which leds
%   you will need to use and the PWM intensity for each one, then the
%   serial messages will be loaded and you will be able to send serial
%   command to the PA module to turn those LEDs on.
%
%   leave input empty is you are not using TopL, TopR, MidL, MidR leds
%
%   Input Example:
%   sma = newStateMachine({{'MidLled',255,'MidRled',255},{'TopLblue',255}})
%   for the example above you may have two states using PA leds, one using
%   MidLled and MidRled, another using TopLblue only.
if nargin < 1
    PA_leds_to_use = [];
end

if exist('~/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('~/bpod_fsm_config.ini');
elseif exist('example_config_files/bpod_fsm_config.ini','file')
    fsm_config = utils.ini2struct('example_config_files/bpod_fsm_config.ini');
    fprintf(2,'no FSM config file found, load default example file\n');
end

sma = NewStateMachine();
Ack = ResetSerialMessages;
if any(contains(fsm_config.port_list.port_module,'PA'))
    Ack = LoadSerialMessages('PA1', ['L' 0],255); %close all LEDS on PA1 as message 255
end

if ~isempty(PA_leds_to_use)
    % first, convert each state (led control) to a matrix to aid next
    % processing (2x4 matrix)
    % for that matrix, each column indicate the PA_port#, the first row
    % indicate the LED1 PWM intensity (0-255), the second row indicate the
    % LDE2 PWM intensity (0-255).
    PA_leds_status_mat = cell(1,length(PA_leds_to_use));
    for ii = 1:length(PA_leds_to_use)
        PA_leds_status_mat{ii} = int.conv_PA_led_mat(fsm_config,PA_leds_to_use{ii});
    end
    
    % go though each PA_leds_status_mat and add serial command, prefer
    % using bit control is all 255 (100% intensity)
    available_serial_command = 1:length(PA_leds_status_mat);
    for ii = 1:length(PA_leds_status_mat)
        this_mat = PA_leds_status_mat{ii};
        this_mat = [this_mat(1,:),this_mat(2,:)];%move to one single row
        this_mat_intensity_uq = unique(this_mat);
        this_mat_intensity_uq(this_mat_intensity_uq==0) = [];% ignore closed led
        if all(this_mat(:)==0)
            %no led need to be configured, skip
            continue
        elseif numel(this_mat_intensity_uq)==1 && this_mat_intensity_uq(1) == 255
            this_mat(this_mat>0) = 1;
            % all 100% intensity, using bit control
            PA_mat_str = sprintf('%d%d%d%d%d%d%d%d',this_mat(8),this_mat(7),...
                this_mat(6),this_mat(5),this_mat(4),this_mat(3),this_mat(2),this_mat(1));
            control_binary = bin2dec(PA_mat_str);
            Ack = LoadSerialMessages('PA1', ['L' control_binary],ii); %load control binary serial message, message id is the looping index
            % using this will turn off all other unspesified leds
            %fprintf('Loading Serial Message: PA1 L %d as serial message %d\n',control_binary,ii) %print for testing/debugging 
        elseif numel(this_mat_intensity_uq)==1 && sum(this_mat>0)==1
            % control one single PWM, but this will not turn off
            % unspesified leds
            this_mat = [this_mat(1:4);this_mat(5:8)];
            [rows,cols] = find(this_mat>0);
            control_command_bit = {'P','Q'};%P for yellow led, Q for blue led
            Ack = LoadSerialMessages('PA1', [control_command_bit{rows} cols-1 this_mat(rows,cols)],ii);% note that led id start from 0
            %fprintf('Loading Serial Message: PA1 %s %d %d as serial message %d\n',control_command_bit{rows},cols-1,this_mat(rows,cols),ii) %print for testing/debugging 
        else
            % this is the most slowest way to do control, need to send 9
            % bits code to control all leds
            %this_mat = [this_mat(1,:),this_mat(2,:)];%move to one single row
            err.message = 'not implemented yet';
            err.identifier = 'NewStateMachine:PAModule:NotImplemented';
            err.stack = dbstack;
            error(err)
            %Ack = LoadSerialMessages('PA1', ['Y' this_mat],ii);
            %fprintf('Loading Serial Message: PA1 Y %s as serial message %d\n',num2str(this_mat),ii) %print for testing/debugging 
        end
    end
    sma.Serial_PA_leds_status_mat = PA_leds_status_mat;
    sma.Serial_available_serial_command = available_serial_command;
end
sma.fsm_config = fsm_config;
end


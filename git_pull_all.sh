#!/bin/bash

cd ~/repos
find . -type d -maxdepth 1 -exec git -C {} pull \;


global BpodSystem
try 
    eval('BpodSystem.Status.BeingUsed')
catch
    start_bpod
end

db.setTesting(1);
old_use_db = BpodSystem.use_db;
BpodSystem.use_db = false;
addpath(fullfile(BpodSystem.Path.ParentDir,'bpod-protocols','rig_test_fm'))
sl = sys.SaveLoad;
sl.test = true;%set video as testing, no saving, just testing

obj = eval('rig_test_fm');
dispatch = sys.dispatchObj(obj,'name','JLI-T-0001','saveload',sl,'use_db',false,'notify',0,'video_trigger',true);
dispatch.runAll();

%rmpath(fullfile(BpodSystem.Path.ParentDir,'bpod-protocols','rig_test_fm'))
BpodSystem.use_db = old_use_db;


db.setTesting(0);
db.markReady();
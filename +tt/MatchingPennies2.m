classdef MatchingPennies2 < tt.TrialSelector
    properties (SetAccess=public,GetAccess=public)
        % Inherited:
        % choice_history = {};
        % reward_history = [];
        % tt_history = {};
        % trial_types
        
        trials_back
        alpha
        ngram
        choice_reward_combination = {}
        mp1

    end

    methods
        function obj = MatchingPennies2(N, alpha)
            trial_types = ["L", "R"];
            obj@tt.TrialSelector(trial_types)
            obj.trials_back = N;
            for trial_back = 1:N 
                obj.ngram{trial_back} = initialize_ngram(obj, trial_back); %creating ngram for each trial sequence eg for 1 trial back, 2 trials back 
            end
            obj.alpha = alpha;
            obj.mp1 = tt.MatchingPennies1(N, obj.alpha);
        end


        function update(obj, last_choice, last_reward)
            obj.mp1.update(last_choice, last_reward);

            assert(ismember(last_choice, [obj.trial_types, "M"]))
            last_trial = join([last_choice, last_reward]);

            if last_trial ~= "M 0"
                for trial_back = 1:obj.trials_back
                    update_ngram(obj,trial_back,last_trial)
                end
            end
            
            obj.choice_history{end + 1} = last_choice;
            obj.reward_history(end + 1) = last_reward;
            obj.choice_reward_combination{end + 1} = last_trial;
        end 


        function ngram = initialize_ngram(~,n)
            ngram = num2cell(zeros(4^n, 3) .*[0 0 nan]); % Cols: # of Left, Total, p-value from binotest

            number_permutator = utils.combinator(4,n,'p','r');
            number_permutator = number_permutator - 1;
            all_combinations = num2cell(number_permutator);
            all_combinations(number_permutator==0) = {"L 0"};
            all_combinations(number_permutator==1) = {"R 0"};
            all_combinations(number_permutator==2) = {"L 1"};
            all_combinations(number_permutator==3) = {"R 1"};
            if n > 1
                all_combinations = join(reshape(string(all_combinations), [4^n,n]));
            else
                all_combinations = reshape(string(all_combinations), [4^n,n]);
            end
            rows = arrayfun(@(x) number_permutator(x,:), 1:length(number_permutator), 'UniformOutput', false);
            idx = cellfun(@(x) dot(4.^(0:numel(x)-1),x)+1, rows);

            for i = 1:length(rows)
                ngram{i,4} = all_combinations(idx(i)); % ngram in cell format with left right combinations added as column

            end
        end


        function update_ngram(obj,trial_back,last_trial)
            if trial_back < length(obj.choice_history)
                idx = history_to_index(obj, trial_back);
                ngram = obj.ngram{trial_back};
                ngram(idx,1:3) = num2cell(cell2mat(ngram(idx,1:3)) + [extract(last_trial,1)=="L", 1, 0]);
                ngram{idx,3} = stats.binotest(ngram{idx,1}, ngram{idx,2});
                obj.ngram{trial_back} = ngram;
            end

        end


        function idx = history_to_index(obj, trial_back)
            trial_combination = obj.choice_reward_combination; 
            trial_combination(cellfun(@(x) x=="M 0", trial_combination)) = [];
            trial_combination = trial_combination(end-(trial_back-1):end);
            barcode = trial_combination;
            barcode(cellfun(@(x) x=="L 0", trial_combination)) = {0};
            barcode(cellfun(@(x) x=="R 0", trial_combination)) = {1};
            barcode(cellfun(@(x) x=="L 1", trial_combination)) = {2};
            barcode(cellfun(@(x) x=="R 1", trial_combination)) = {3};
            barcode = cell2mat(barcode);
            idx =  dot(4.^(0:numel(barcode)-1),barcode) + 1;

        end

        function rows = relevant_rows(obj)
            rows = arrayfun(@(x)obj.ngram{x}(history_to_index(obj, x),:), 1:obj.trials_back, 'UniformOutput',false);
            rows = [rows, obj.mp1.relevant_rows()];
        end

        function [choice] = sample(obj)
            if obj.trials_back < length(obj.choice_history)
                rows = relevant_rows(obj);
                [pvalue, idx] = min(cellfun(@(x)x{3},rows));
                if pvalue > obj.alpha
                    choice = utils.pick_n(obj.trial_types,1);
                else
                    choice = obj.trial_types((rows{idx}{1}/rows{idx}{2} > 0.5) + 1);
                end
            else
                choice = utils.pick_n(obj.trial_types,1);
            end

            obj.tt_history{end + 1} = choice;
        end

    end
end

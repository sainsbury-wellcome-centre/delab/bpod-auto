classdef (Abstract) TrialSelector < handle
    properties (SetAccess=public,GetAccess=public)
        choice_history = {};
        reward_history = [];
        tt_history = {};
        trial_types
    end

    methods
        function obj = TrialSelector(trial_types)
            obj.trial_types = trial_types;
        end

        function tt = sample(obj)
            tt = utils.pick_n(obj.trial_types, 1);
        end

        function update(obj, last_choice, last_reward)
            obj.choice_history = {obj.choice_history, last_choice};
            obj.reward_history = [obj.reward_history, last_reward];
        end
    end
end
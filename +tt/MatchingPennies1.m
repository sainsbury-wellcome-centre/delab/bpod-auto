classdef MatchingPennies1 < tt.TrialSelector
    properties (SetAccess=public,GetAccess=public)
        % Inherited:
        % choice_history = {};
        % reward_history = [];
        % tt_history = {};
        % trial_types
        
        trials_back
        alpha
        ngram

    end

    methods
        function obj = MatchingPennies1(N, alpha)
            trial_types = ["L", "R"];
            obj@tt.TrialSelector(trial_types)
            obj.trials_back = N;
            obj.alpha = alpha
            for trial_back = 0:N 
                obj.ngram{trial_back+1} = initialize_ngram(obj, trial_back); %creating ngram for each trial sequence eg for 1 trial back, 2 trials back 
            end
        end


        function update(obj, last_choice, last_reward)
            assert(ismember(last_choice, [obj.trial_types, "M"]))
            if nargin < 3
                last_reward = 1;
            end
            
            if ismember(last_choice,obj.trial_types)
                for trial_back = 0:obj.trials_back
                    update_ngram(obj,trial_back,last_choice)
                end
            end
            
            obj.choice_history{end + 1} = last_choice;
            obj.reward_history(end + 1) = last_reward;
        end 


        function ngram = initialize_ngram(~,n)
            ngram = num2cell(zeros(2^n, 3) .*[0 0 nan]); % Cols: # of Left, Total, p-value from binotest 

            if n > 0
                number_permutator = utils.combinator(2,n,'p','r') > 1; 
                all_combinations = num2cell(number_permutator);
                all_combinations(number_permutator==1) = {"L"};
                all_combinations(number_permutator==0) = {"R"};
                if n > 1
                    all_combinations = join(reshape(string(all_combinations), [2^n,n]));
                else
                    all_combinations = reshape(string(all_combinations), [2^n,n]);
                end
                rows = arrayfun(@(x) number_permutator(x,:), 1:length(number_permutator), 'UniformOutput', false);
                idx = cellfun(@(x) dot(2.^(0:numel(x)-1),x)+1, rows);

                for i = 1:length(rows)
                    ngram{i,4} = all_combinations(idx(i)); % ngram in cell format with left right combinations added as 4th column
                end
            else
                ngram{1,4} = "L";
            end
        end


        function update_ngram(obj,trial_back,last_choice)
            if trial_back < length(obj.choice_history)
                idx = history_to_index(obj, trial_back);
                ngram = obj.ngram{trial_back+1};
                ngram(idx,1:3) = num2cell(cell2mat(ngram(idx,1:3)) + [last_choice=="L", 1, 0]);
                ngram{idx,3} = stats.binotest(ngram{idx,1}, ngram{idx,2});
                obj.ngram{trial_back+1} = ngram;
            end

        end


        function idx = history_to_index(obj, trial_back)
            if trial_back == 0
                idx = 1;
            else
                choice_history = obj.choice_history;
                choice_history(cellfun(@(x) x=="M",choice_history)) = [];
                last_n = cellfun(@(x)x=="L",choice_history(end-(trial_back-1):end));
                idx =  dot(2.^(0:numel(last_n)-1),last_n) + 1;

            end
        end

        function rows = relevant_rows(obj)
            rows = arrayfun(@(x)obj.ngram{x+1}(history_to_index(obj, x),:), 0:obj.trials_back, 'UniformOutput',false);
            
        end


        function [choice] = sample(obj)
            if obj.trials_back < length(obj.choice_history) 
                rows = relevant_rows(obj);
                [pvalue, idx] = min(cellfun(@(x)x{3},rows));
                if pvalue > obj.alpha
                    choice = utils.pick_n(obj.trial_types,1);
                else
                    choice = obj.trial_types((rows{idx}{1}/rows{idx}{2} > 0.5) + 1);
                end
            else
                choice = utils.pick_n(obj.trial_types,1);
            end

            obj.tt_history{end + 1} = choice;
        end



        
    end
end
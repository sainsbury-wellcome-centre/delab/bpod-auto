classdef CoinFlip < tt.TrialSelector
    properties (SetAccess=public,GetAccess=public)
        % Inherited:
        % choice_history = {};
        % reward_history = [];
        % tt_history = {};
        % trial_types
    end

    methods
        function obj = CoinFlip()
            trial_types = ["L", "R"];
            obj@tt.TrialSelector(trial_types)
        end

        function tt = sample(obj)
            tt = utils.pick_n(obj.trial_types, 1);
            obj.tt_history{end + 1} = tt;
        end

        function update(obj, last_choice, last_reward)
            obj.choice_history{end + 1} = last_choice;
            obj.reward_history(end + 1) = last_reward;
        end
    end
end
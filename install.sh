#!/bin/bash

setup_cron() {
    # Setup cron
    echo "Overwriting existing crontab"
    # Add cron job to run every minute
    echo "01 05 * * * /home/bpod/repos/bpod-auto/matlab_bpod.sh" > mycron
    crontab mycron
    rm mycron
}

setup_cron
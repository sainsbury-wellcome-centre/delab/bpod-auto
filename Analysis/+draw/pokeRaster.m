function ax = pokeRaster(peh, ev, poke, varargin)
% pokeRaster(peh, event, poke, ...)
% Inputs
% peh 		parsed events history structure
% poke 		a poke to draw (or a cell array of pokes)
% 
% Optional Inputs
% pre 		time in seconds to show before the event
% post 		time in seconds to show after the event
% poke_clr 	A cell array or N x 3 matrix of colors to use for each poke in poke (defaults from )
% ax 		An axis to draw into
% width 	width of the raster plot
% height 	height of the raster plot
% sort_by 	A matrix with height of the raster plot


inpd = @utils.inputordefault;

[pre, varargin] = inpd('pre',5,varargin);
[post, varargin] = inpd('post',5,varargin);
[ax, varargin] = inpd('ax',[],varargin);
[ax_width, varargin] = inpd('width',.8,varargin);
[ax_height, varargin] = inpd('height',.5,varargin);
[row_order] = inpd('sort_by',[],varargin);

function SubmitCalibrationData(rigid, data, valve, invalid_prev)
% SubmitCalibrationData(rigid, data, valve)
% 
% Inputs
% rigid             The rigid of the rig you just calibrated
% valve             The valve you are calibrating (defaults to valve 1)
% invalid_prev      invalide previous calibration data (defaults true)
% data              The format of the data should be as follows:
%   [duration tube_weight total_weight number_of_drops]
%   duration in seconds
%   tube_weight and total_weight in grams
%   number_of_drops # 
%
% For example, if you calibrated rig 1 valve 1 and you measured each duration three times with and you measured
% 100, 200, 500 ms then data would be:
% 
% data = [0.1  1.80 2.01 100; 
%         0.1  1.83 2.33 100;
%         0.1  1.75 2.22 100;
%         0.2  1.81 3.01 50;
%         0.2  1.85 2.93 50;
%         0.2  1.71 2.99 50;
%         0.5  1.91 4.51 50;
%         0.5  1.87 4.43 50;
%         0.5  1.72 4.79 50;]
%
% And you would call: 
% SubmitCalibrationData(1, data); 
% or 
% SubmitCalibrationData(1, data, 1);

if nargin<4
    invalid_prev = true;
end

if nargin<3
    valve = 1;
end

if size(data,2) < 4
    fprintf(2,'You need to have 4 columns in the data \n');
    return
end

%% First take the average and compute the CV

tab = array2table(data, 'VariableNames',{'duration','tube_weight','total_weight','number_of_drops'});
tab.volume = (tab.total_weight - tab.tube_weight)./tab.number_of_drops.*1000;


gs = grpstats(tab, {'duration'},{'mean','std'},'datavars','volume');
gs.cv = gs.std_volume ./ gs.mean_volume;

beta0 = [0.1353,0.01177,0.0294];
mod = fitnlm(tab.volume, tab.duration, @(b,x)stats.invsoftplus(b,x), beta0);
newx = min(tab.volume):0.001:max(tab.volume);
newy = mod.predict(newx(:));

%%
figure(1);clf
ax = subplot(2,1,1);
draw.errorplot(ax, gs.duration, gs.mean_volume, gs.std_volume);
ax.NextPlot = 'add';
plot(ax,newy,newx,'g-');
xlabel('Valve open duration','FontWeight','bold');
ylabel('Mean volume (ul)','FontWeight','bold');

ax = subplot(2,1,2);
plot(ax, gs.duration, gs.cv,'o-');
draw.xhairs(ax,'r:', 0, .1);
xlabel('Valve open duration','FontWeight','bold');
ylabel('Std volume /mean volume','FontWeight','bold');


ButtonName = questdlg('Does the data look good?', ...
                         'Good Data', ...
                         'Yes', 'No', 'Yes');
                     
if strcmp(ButtonName,'No')
    warndlg('Please re-weigh the bad samples.', 'Re-weigh.');
    return
end

savetab = tab(:,{'duration','volume'});
savetab.rigid = tab.duration*0 + rigid;
savetab.valve = tab.duration*0 + valve;
savetab.valid = tab.duration*0 + 1;

%%
dbc = db.labdb.getConnection();

% invalid previous calibration data if necessary
if invalid_prev
    dbc.call(sprintf('met.invalidateCalibration(%d,%d)',rigid,valve));
end
dbc.saveData('met.water_calibration',savetab);


fprintf('New calibration data for rig %d saved\n',rigid);
function [all_pokes, poke_type] = getAllPokes(pevents, varargin)
% [all_pokes, poke_type] = getAllPokes(pevents, [optional inputs])
%
% Inputs:
% pevents          A array of parsed events
%
% Optional Inputs
% in_only       [true] If true, only include poke-in events. Otherwise in
% and out events.
%
% Output:
% all_pokes      a list of the times relative to the first trial in the input
% poke_type      a cell array of pke_type, same length as all_pokes.

if nargin==0
    help(mfilename)
    return
end


inpd = @utils.inputordefault;

in_only = inpd('in_only',true,varargin);


    session_start = pevents(1).StartTime;
    pokes = cell(1,numel(pevents));
    t_poke_type = pokes;
    for tx = 1:numel(pevents)
       [t_pokes, t_poke_type{tx}] = getpokes_int(pevents(tx), in_only);
       pokes{tx} = t_pokes + pevents(tx).StartTime - session_start;     
    end
    all_pokes = [pokes{:}];
    poke_type = [t_poke_type{:}];

end

function [all_pokes, poke_type] = getpokes_int(thisevents, in_only)
    % This is a si

    pokein = getStateChangeList();
    if in_only
        pokein = pokein(1:2:18); % This will change if we change num pokes.
    else
        pokein = pokein(1:1:18);
    end
    enames = fieldnames(thisevents.Events);

    % We could optimize this by computing the sum of all the pokes first and then allocating the memory once... not not sure it is worth it.
    all_pokes_i = cell(1,numel(thisevents.Events));
    poke_type_i = all_pokes_i;
    for ex = 1:numel(enames)
        if any(strcmpi(enames{ex}, pokein))
            all_pokes_i{ex} = thisevents.Events.(enames{ex});
            poke_type_i{ex} = repmat(enames(ex),1,numel(thisevents.Events.(enames{ex})));
        end
    end

    all_pokes = [all_pokes_i{:}];
    poke_type = [poke_type_i{:}];
    
    [all_pokes, si] = sort(all_pokes);
    poke_type = poke_type(si);
    
end
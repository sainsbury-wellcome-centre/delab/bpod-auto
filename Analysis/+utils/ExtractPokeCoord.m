function [cordx,cordy] = ExtractPokeCoord(this_poke,animal)
Pokes = {'MidR','TopR','BotR','MidC','TopL','MidL','BotL','BotC','null'};
if sum(strcmp(this_poke,Pokes))==0
    Pokes = {'D','B','F','G','A','C','E','H'};
end
if strcmp(animal,'Rat')
    cord = [129,58;101,83;101,33;73,58;...
        45,83;17,58;45,33;73,18;nan,nan];
elseif strcmp(animal,'Mice')
    cord = [129,40;100,55;100,25;71,40;...
        52,55;13,40;42,25;71,17;nan,nan];
end
% recenter
     cord(:,1) = cord(:,1) - nanmedian(cord(:,1));
    cord(:,2) = cord(:,2) - nanmedian(cord(:,2));
cordx = cord(strcmp(this_poke,Pokes),1);
cordy = cord(strcmp(this_poke,Pokes),2);

end
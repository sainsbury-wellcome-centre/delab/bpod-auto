function ats = adjust_frame_to_bpod(ts,S)
% ats = adjustts(ts,M)
% Takes the timestamps ts, and the structure returned from sync_times
% and returns timestamps which are interpolated to be in
% bpod time. Only works if video_times > 2

    video_times = S.video_valve_onset_frame;
    bpod_times = S.bpod_valve_onset_times;

    % this line might not be very flat, fit the slope between each two
    % pairs
    % This line finds which timestamps are in which trials.
    ind = stats.qfind(ts, video_times); % The 2nd column is the OE times.
    
    ats = nan+ts; % initialize the adjusted TS to be a nan array same size as ts.
    slope = nan+ind;
    intcpt = nan+ind;
    for tx = 2:numel(ind) % loop through the trials
        % Find the slope and the intercept of a 2 point line (the sync_times
        % from one trial to the next) and use that to shift the OE times in
        % that segment.
        slope(tx) = (bpod_times(tx)-bpod_times(tx-1))/(video_times(tx)-video_times(tx-1));
        intcpt(tx) = bpod_times(tx) - slope(tx)*video_times(tx);
        ats((ind(tx-1)+1):ind(tx)) = slope(tx)*ts((ind(tx-1)+1):ind(tx)) + intcpt(tx);
    end
    % calculate ats before the first reward time and after the
    % last reward time of the session
    ats(1:ind(1)) = slope(2)*ts(1:ind(1)) + intcpt(2);
    ats(ind(tx)+1:end) = slope(tx)*ts(ind(tx)+1:end) + intcpt(tx);

end

function [events_tab] = getAllEvents(pevents, varargin)
% [all_ev, ev_type] = getAllPokes(pevents, [optional inputs])
%
% Inputs:
% pevents          A array of parsed events
%
%
% Output:
% all_ev         a list of the times relative to the first trial in the input
% ev_type        a cell array of ev_type, same length as all_pokes.
% in_out         1 if an "in" event, 0 if an "out" event.

if nargin==0
    help(mfilename)
    return
end

session_start = pevents(1).StartTime;
events = cell(1,numel(pevents));
io_type = cell(1,numel(pevents));
t_event_type = cell(1,numel(pevents));
t_tnum = cell(1,numel(pevents));
for tx = 1:numel(pevents)
    [t_pokes, t_event_type{tx}, t_io] = getev_int(pevents(tx));
    events{tx} = t_pokes + pevents(tx).StartTime - session_start;
    io_type{tx} = t_io;
    t_tnum{tx} = repmat(tx, size(t_pokes));
end

event_time = vertcat(events{:});
event_type = vertcat(t_event_type{:});
event_io = vertcat(io_type{:});
trial_num = vertcat(t_tnum{:});



events_tab = table(event_time, event_type, event_io, trial_num, 'VariableNames',{'time', 'type', 'io', 'trialnum'});


end

function [all_ev, ev_type, ev_io] = getev_int(thisevents)
% This is a si

enames = fieldnames(thisevents.Events);

% We could optimize this by computing the sum of all the pokes first and then allocating the memory once... not not sure it is worth it.
all_ev_i = cell(1,numel(thisevents.Events));
ev_type_i = cell(numel(thisevents.Events),1);
ev_io = all_ev_i;
for ex = 1:numel(enames)
        all_ev_i{ex} = thisevents.Events.(enames{ex});
        t_ev_io = 1-contains(enames(ex),{'out','Low','Off'});
        t_ename = regexprep(enames(ex),'in|out|High|Low|On|Off','');
        ev_type_i{ex} = repmat(t_ename,numel(thisevents.Events.(enames{ex})),1);
        ev_io{ex} = repmat(t_ev_io,numel(thisevents.Events.(enames{ex})),1);        
end

all_ev = horzcat(all_ev_i{:})';
ev_type = vertcat(ev_type_i{:});
ev_io = vertcat(ev_io{:});

[all_ev, si] = sort(all_ev);
ev_type = ev_type(si);
ev_io = ev_io(si);


end
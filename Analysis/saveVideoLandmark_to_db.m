function saveVideoLandmark_to_db(sessid,filepath,varargin)
%saveVideoLandmark_to_db
%   Example:
%   saveVideoLandmark_to_db(10531,path_to_h5_file,'header',{'Nose','Left Ear','Right Ear','Body_1','Back'},'use_db','manage')
inpd = @utils.inputordefault;

use_db = inpd('use_db','client',varargin);
header = inpd('header','',varargin);

assert(~isempty(header),"please input a header for key pointes information: {'Nose','Left Ear','Right Ear','Body_1','Back'}")
dbc = db.labdb.getConnection(use_db);
assert(isempty(dbc.query('select * from beh.video_landmark where sessid = %d',{sessid})),'video already exist, please check');

try 
    S =db. getVideoSyncStruct(sessid,'manage');
catch
    fprintf(2,'this video was not synced to db yet, please sync the video first before upload \n')
    return
end

occupancy_matrix = h5read(filepath,'/track_occupancy');
tracks_matrix = h5read(filepath,'/tracks');

assert(numel(header) == size(tracks_matrix,2),'the header you input was not match to the number of key points in data, please check')

frame_idx = 1:size(tracks_matrix,1);

Dat = struct();
Dat.sessid = sessid;
Dat.ts = typecast(adjust_frame_to_bpod(frame_idx,S), 'uint8');
Dat.data = json.mdumps(tracks_matrix,'compress',1);
Dat.header = json.mdumps(header);

dbc.saveData('beh.video_landmark', Dat);


end
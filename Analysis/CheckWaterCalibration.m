function CheckWaterCalibration(rigid, varargin)
% CheckWaterCalibration(rigid, 'valve',valve,'on_time',on_time,'number_of_drops',times,'volume',volume)
%
% check water delivary situation with one tube
% compare with previous full calibration result
% will plot the pld calibration curve from db, and plot the calibraiton data point fo this calibration
% and will print the error between the previous calibration and this calibratio check point
% If the error is getting large, the rig might be blocked, you need to replace valve/tubing
% or some sitaution was changed, need to redo water calibration
%
% Inputs
% rigid             The rigid of the rig you just calibrated
% valve             The valve you are calibrating (defaults to valve 1, for hf rigs: 1-left 2-right)
% on_time           The opening time of the valve in a vector, in secounds, default
% 0.04  also can be [0,04,0.2]
% number_of_drops   The number of drops used per tube. (defaults to 100)
% volume            Amount of water delivered (weight difference of the
% test tube pre and post water delivary) in a vector [vol1,vol2], or single
% amount
% example: CheckWaterCalibration(373103, 'valve',1,'on_time',[0.04,0.2],'number_of_drops',100,'volume',[0.19,1.7])
%
% By Jingjie Li, 2023-03-11


inpd = @utils.inputordefault;

valve = inpd('valve',1,varargin);
on_time = inpd('on_time',0.04,varargin);
times = inpd('times',100,varargin);
check_vol = inpd('volume',0.2,varargin);

% first, get current calibration data
dbc = db.labdb.getConnection('manage');
caltab = dbc.query('select valve, volume, duration, calts from met.water_calibration where valid = 1 and rigid = %d and valve = %d',{rigid,valve});

assert(~isempty(caltab),'No water calibration data exist for this rig')

% then, fit the invsoftplus model for the previous calibration data
gs = grpstats(caltab, {'duration'},{'mean','std'},'datavars','volume');
gs.cv = gs.std_volume ./ gs.mean_volume;

beta0 = [0.1353,0.01177,0.0294];
mod = fitnlm(caltab.volume, caltab.duration, @(b,x)stats.invsoftplus(b,x), beta0);
newx = min(caltab.volume):0.001:max(caltab.volume);
newy = mod.predict(newx(:));

% plot the previous calibration data
figure(1);clf
ax = draw.jaxes();
draw.errorplot(ax, gs.duration, gs.mean_volume, gs.std_volume);
ax.NextPlot = 'add';
plot(ax,newy,newx,'g-');

% overlay the calibration of this check point
this_check_volume = 1000.*check_vol./times;
plot(ax,on_time,this_check_volume,'*','MarkerSize',10)
xlabel('Valve open duration','FontWeight','bold');
ylabel('Mean volume (ul)','FontWeight','bold');

% find out the error, print it.
for ii = 1:numel(on_time)
    idx = abs(newy-on_time(ii))==min(abs(newy-on_time(ii)));
    prev_calb_val = newx(idx);
    calb_error = abs(this_check_volume(ii)-prev_calb_val)./prev_calb_val;
    fprintf('Sample %d Valve opening time: %.2f, Previous Calibration Volume: %.4fuL, This Checking Amount: %.4fuL ',ii,on_time(ii),prev_calb_val,this_check_volume(ii));
    fprintf('Calibration Error: %.2f%%\n',calb_error*100);
end
% potential function: notify in bpod-bot if error is too large to let people know

end
function [ev_times] = getEvent(pevents, ename)
% [e_times] = getEvent(pevents, event_name)
%
% Inputs:
% pevents          A array of parsed events
%
% Output:
% event_times    a list of the times relative to the first trial in the input

    if nargin==0
        help(mfilename)
        return
    end

    session_start = pevents(1).StartTime;
    all_events = cell(1,numel(pevents));
    for tx = 1:numel(pevents)
       [t_events] = pevents(tx).Events.(ename);
       all_events{tx} = t_events + pevents(tx).StartTime - session_start;     
    end
    ev_times = [all_events{:}];

end


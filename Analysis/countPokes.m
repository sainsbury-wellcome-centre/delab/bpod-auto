function num = countPokes(parsed_events, state, varargin)
% num = countPokes(parsed_events, state, varargin)
% returns all the pokes that occurred in a state.
%
% Inputs
% parsed_events 	An array of parsed_events (e.g. obj.peh from any Protocol)
% state 			The name of the state to count events in.
% Optional Inputs
% poke_names 		[], A string or cell array of strings of things to count
%					e.g. 'BotCin'  or {'MidLout', 'TopRin'}. If empty count all pokes.
% 
% Output:
% num 		     an array of number of pokes in the state described for each parsed_event

if nargin==0
    help(mfilename)
    return
end


inpd = @utils.inputordefault;

poke_names = inpd('poke_names',[],varargin);

num = arrayfun(@(x)(countpokes_int(x, state, poke_names)), parsed_events);

end

function [count] = countpokes_int(thisevents, state, poke_names)
    % This is a si

    if ~iscell(poke_names) && ~isempty(poke_names)
        poke_names = {poke_names};
    end
    
    pokein = getStateChangeList();
    pokein = pokein(1:2:18); % This will change if we change num pokes.
    enames = fieldnames(thisevents.Events);
    state_times = thisevents.States.(state);
    [ptime, ptype]=getAllPokes(thisevents);

    count = 0;
    for sx = 1:size(state_times,1)  % look at each row
        [~,matched_pokes_ind] = stats.qbetween(ptime, state_times(sx,1), state_times(sx,2)+5e-7); % add a 1/2 microsecond buffer 
        if isempty(poke_names)
            count = count + numel(matched_pokes_ind);
        else
            for px = 1:numel(poke_names)
                count = count + sum(strcmp(poke_names{px}, ptype(matched_pokes_ind)));
            end
        end
    end
    
end






% in_events = getStateChangeList();
% in_events = in_events(1:2:18);
% ev_type = utils.inputordefaults('event_type', in_events, varargin);

% DOES NOTHING.


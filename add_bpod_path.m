function add_bpod_path

if ~exist('Bpod.m', 'file') %add bpod path only if not been added yet
    bpodstr = which('start_bpod');
    
    BPODHOME = bpodstr(1:find(bpodstr==filesep,1,'last'));
    path_parts = strsplit(BPODHOME, filesep);
    repo_path = strjoin(path_parts(1:end-2), filesep);%extact repo dir
    
    javaaddpath(fullfile(repo_path,'elutils','+db','mysql-connector-java-5.1.42-bin.jar'));
    javaaddpath(fullfile(repo_path,'elutils','+net','jeromq.jar'));
    
    
    addpath(fullfile(repo_path,'elutils'))
    addpath(fullfile(repo_path,'bpod-auto'))
    addpath(fullfile(repo_path,'Bpod_Gen2'))
    addpath(genpath([BPODHOME, 'Plugins']));
end

%addpath([BPODHOME, filesep, 'Protocols']);
%addpath([BPODHOME, filesep, 'Analysis']);

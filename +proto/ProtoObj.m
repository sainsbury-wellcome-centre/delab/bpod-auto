classdef (Abstract) ProtoObj < handle
% This class defines a Protocol Object.
% The methods of this class match the 'actions' that were implemented in BControl style classes.
% Instantiations of this class should not directly access the global BpodSystem but rather get passed relevant elements of that global through methods.

	properties (SetAccess=public,GetAccess=public)
		settings % a struct with the protocol level settings. See useSettings method.
		n_done_trials = 0;
		n_started_trials = 0;
		hit_history = [];
		reward_history = [];
		choice_history = {};
		violation_history = [];
		RT_history = [];
		peh
        hit
        reward
        base_reward = 1; % this is based on the species and body-weight of the animal.
        choice
        viol
		saveload
		RT
		statematrix
		trial_time_limit = 400; % seconds
        opto = 0;% a switch for doing opto
		% subjectname
		% sessiondate
		% sessionstart
		% sessionend
		% sessid
		% All of these will go into the session manager.
		
		% These are 


	end % properties

	properties (SetAccess=protected,GetAccess=protected)
		rigid
		prev_settings = [];
	end

	methods
		function obj = ProtoObj(varargin)
			% by can be 'subject', 'subjID', 'settings'
			% if by == 'subject' value should be a string
			% if by == 'subjID' value should be an int
			% if by == 'settings' value should be a struct

			
			
			% return the class if called without arguments.

		end

		function [x, y] = init(obj, varargin)
		% [x,y] = init(varargin)
		% Inputs:
		% usually none
		% Outputs
		% [x,y] the location of the next GUI element to plot in pixels
		%
		% In the initialization function you should put code that you want to run once
		% at the beginning of a session.

			x = 20;
			y = 20;
		end

		function proplist = trialPropertiesToSave(obj)
        % A list of Object properties that get included in "data" to save
        % to the DB at the end of each trial.
			proplist = {'n_done_trials';
						'hit';
						'viol';
						'reward'; 
                        'choice'};
		end


		function proplist = sessionPropertiesToSave(obj)
            % Not currently implemented. So either remove or implement
			proplist = {
				'settings';
				'n_done_trials';
				'n_started_trials';
				'hit_history';
				'reward_history';
				'choice_history';
				'violation_history';
				'peh'};

		end

		function saveTrial(obj,opto_rig)
		% Uses the saveload object to save trial data to somewhere.
            
            if nargin < 2 
                opto_rig = false;
            end
            
            if ~isfield(obj.settings,'opto_sess')
                opto_sess = false;
            else
                opto_sess = obj.settings.opto_sess;
            end
            
            opto_saving = (opto_sess & opto_rig);
            %save to opto table only if this session is an opto session and running in an opto rig
        
			meta.data = obj.getTrialData();
			meta.hit = obj.hit;
			meta.viol = obj.viol;
			meta.reward = obj.reward;
			meta.trialnum = obj.n_done_trials;
			meta.parsed_events = obj.peh(end);
			meta.RT = obj.RT;
			meta.state_matrix = obj.statematrix;
			meta.n_pokes = numel(ana.getAllPokes(meta.parsed_events));
			if isempty(obj.prev_settings)
				meta.settings = obj.settings;
				obj.prev_settings = obj.settings;
			else
				meta.settings = utils.diff_struct(obj.prev_settings, obj.settings);
				obj.prev_settings = obj.settings;
			end

			spec = obj.getProtoTrialData();
            
            if opto_saving
            % opto_saving is a flag to save opto data during an opto session.
            % for opto rigs, this opto_sess will be turned on. All trials
            % running in the opto rigs will be saved.
                pevents = obj.peh(end);
                if ~isfield(pevents.Events,'BNC1High') && ~isfield(pevents.Events,'BNC1Low')
                    obj.opto = 0;
                end
                if obj.opto
                    % get optodata only within opto trials
                    optodata = obj.getOptoData();
                else
                    % default empty struct
                    optodata=struct();
                end
                optodata.isopto = obj.opto;
                obj.saveload.saveTrial(meta, spec, optodata);
                obj.opto = 0;
            else
            % default saveTrial, no opto
                obj.saveload.saveTrial(meta, spec);
            end
        end
        
        function optodata = getOptoData(obj)
            % start_time, duration, details
            optodata=struct();
            optodata.start_time=[];
            optodata.duration=[];
            optodata.trial_type = '';
            optodata.details=[];
        end


		function savedata = getTrialData(obj)
			list = trialPropertiesToSave(obj);
			for fx = 1:numel(list)
				savedata.(list{fx}) = obj.(list{fx});
			end
		end

		function savedata = getProtoTrialData(obj)
			% This should be a struct that matches a protocol specific table.
			savedata = [];
		end


		function savedata = getSessionData(obj)
			list = sessionPropertiesToSave(obj);
			for fx = 1:numel(list)
				savedata.(list{fx}) = obj.(list{fx});
			end
		end

		function loadSettings(obj, settings_name, expg_data, subj_data)
			% First getting information from the database about:
			% settingsname 		a default "set"
			% expg_data 		customizations for that experimental group
			% subj_data 		customizations for that subject
			
			if nargin == 1
			% If nargin == 1 that means the object method was called without any further inputs.
			% In this case get info from DB.
				[settings_name, expg_data, subj_data] = getSettings(obj.saveload);
			end
			
			% Pass info to the ProtoObj to create the obj.settings structure.
			useSettings(obj, settings_name, expg_data, subj_data);
		end

		function updateSettings(obj, settings_data)
			subj_data = getLatestSettings(obj.saveload);
			obj.settings = utils.apply_struct(obj.settings, subj_data);
		end

		function useSettings(obj, settings_name, expg_data, subj_data)
			obj.settings = struct();
			% This method is where protocol level or even block level settings should be set.
			% The idea is that prepareNextTrial uses the obj.settings struct to generate trial specific 
			% parameters. 
			% Those parameters are then used in generateSM
			%
			% The general structure of useSettings should have a switch statement on the settings_name
			% with each case creating some rules for the task.
			%
			% At the end of useSettings, the expg_data and subj_data are "applied" to the obj.settings
			% struct to generate a subject, experimental group specific obj.settings.
			%
			% There is an advantage to NOT using expg_data and subj_data: then all the settings are specified 
			% in CODE, which is versioned via git.


		end

		function endSession(obj)
			if obj.n_done_trials > 0
				try
					sqlS.bias = 1 - stats.norm_entropy(obj.choice_history);
		            if isnan(sqlS.bias)
		                sqlS.bias = 1;
		            end
	        	catch
	        		sqlS.bias = 1;
	        	end

				sqlS.hitfrac = mean(obj.hit_history,'omitnan');
				if isnan(sqlS.hitfrac)
					sqlS.hitfrac = 0;
				end

				sqlS.violfrac = mean(obj.violation_history>0,'omitnan');
				if isnan(sqlS.violfrac)
					sqlS.violfrac = 0;
				end

				
				sqlS.total_profit = sum(obj.reward_history,'omitnan');
				if isnan(sqlS.total_profit)
					sqlS.total_profit = 0;
				end
				
			end
			sqlS.num_trials = obj.n_done_trials;

			obj.saveload.saveSession(sqlS,getSessionData(obj),prepareNextSession(obj)); 

			% reset sound and motor position (if needed)
			try
                global BpodSystem
                % stop sound server
                if isfield(BpodSystem.PluginObjects,'Sound')
                    if ~isempty(BpodSystem.PluginObjects.Sound)
                        BpodSystem.PluginObjects.Sound.stopAll;
                        BpodSystem.PluginObjects.Sound.delete;%stop sound server if running, will re-initilize it soon after
                    end
                end
                % try to re-centered the lick module
                if any(contains(BpodSystem.Modules.Name,'LM'))
                    LM_module_posi = find(contains(BpodSystem.Modules.Name,'LM'));
                    for ii  = 1:length(LM_module_posi)
                        ModuleWrite(BpodSystem.Modules.Name{LM_module_posi(ii)},'H');%reset to X=0 Y=0
                    end
                end
				
			catch me
				utils.showerror(me)
			end

        end
        
        function ok = opto_feedback_check(obj,notify)
            % this basically check the laser feed-back of opto
            % for an opto trial, it will print (and sent out) some warnings
            % if opto feedback is not working correctly (missing feedback or not correct feedback)
            
            if nargin < 2 
                notify = false;
            end
            
            ok = 1;
            pevents = obj.peh(end);
            if obj.opto
                err_str = sprintf('trial: %d, check opto meachine! laser feedback problem!!',obj.n_done_trials);
                try
                    assert(isfield(pevents.Events,'BNC1High')&isfield(pevents.Events,'BNC1Low'),err_str);
                catch me   % opto feedback is missing, check meachine, send
                    ok = 0;
                    utils.showerror(me);
                    sessid = obj.saveload.sessid;
                    subjid = obj.saveload.subjid;
                    db.log_error(me,'caught',1,'sessid',sessid,'subjid',subjid,'notify',notify,'print_stack',false,'comment',err_str);
                end
            else
                % not an opto trial, we are not expecting any BNC input,
                % but this check apply only for opto sess
                if isfield(obj.settings,'opto_sess')
                    err_str = sprintf('trial: %d, laser detected in control trials!!',obj.n_done_trials);
                    try
                        assert(~(isfield(pevents.Events,'BNC1High')|isfield(pevents.Events,'BNC1Low')),err_str);
                    catch me   % opto feedback is missing, check meachine, send
                        ok = 0;
                        utils.showerror(me);
                        sessid = obj.saveload.sessid;
                        subjid = obj.saveload.subjid;
                        db.log_error(me,'caught',1,'sessid',sessid,'subjid',subjid,'notify',notify,'print_stack',false,'comment',err_str);
                    end
                end          
            end
        end

		function snames = getSettingNames(obj)
			snames = {};
		end % This is used by the SettingsManager to help save settings.

		function nextsettings = prepareNextSession(obj)
			nextsettings = [];
		end

		function state_name = syncReturnState(obj)
			state_name = '>exit';
			% if you want to control where your sync states are places, override this function.
		end
		
	end
	methods (Abstract)
	    		
		prepareNextTrial(obj)

		SMA = generateSM(obj)

		trialCompleted(obj)

	end % methods
end % methods
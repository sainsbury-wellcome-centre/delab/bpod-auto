function SoftCodeHandler_PlaySound(SoundID)
global BpodSystem
if SoundID == 255
    BpodSystem.PluginObjects.Sound.stopAll;
elseif SoundID > 128
    BpodSystem.PluginObjects.Sound.stop(bitset(SoundID, 8, 0));
elseif SoundID > 0
    BpodSystem.PluginObjects.Sound.play(SoundID);
end
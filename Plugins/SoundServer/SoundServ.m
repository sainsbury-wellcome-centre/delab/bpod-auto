classdef (Abstract) SoundServ < dynamicprops
    %SoundServ class
    %   A template for handeling sound processing
    
    properties
        SoundStruct =  struct();
        SF = 48000%default sound sampling rate
    end
    
    properties (Access = protected)
        latency = 'high';
        sound_list = {};
        num_sounds = 0;
        synced = 0;
    end
    
    methods
        function id = load(obj, name ,wav, varargin)
            % to load sounds
            [loop, varargin] = utils.inputordefault('loop',0,varargin);
            [vol, varargin] = utils.inputordefault('volume',0.5,varargin);
            [rep, varargin] = utils.inputordefault('repeation',1,varargin);%this rep is for the times of repetation, 0 means rep for ever
            [bal, varargin] = utils.inputordefault('balance',0,varargin);
            %soundid, sound wave, loop in a struct named sound name
            %obj.SoundStruct = setfield(obj.SoundStruct,name,struct());
            if any(abs(wav(:))>1) 
                fprintf(2,'SoundServ: abs(wav) cannot be greater than 1\n')
                wav(wav>1) = 1;
                wav(wav<-1) = -1;
            end
            if isfield(obj.SoundStruct,name)
                % this name(sound) is already loaded, just do replacing
                id = obj.SoundStruct.(name).soundid;
            else
                % loading new sound
                obj.num_sounds = obj.num_sounds + 1;
                obj.SoundStruct.(name).soundid = obj.num_sounds;%soundid in the system starts from 1 by default
                obj.SoundStruct.(name).id = obj.SoundStruct.(name).soundid;
                obj.SoundStruct.(name).name = name;
                obj.sound_list{1,obj.num_sounds} = name;
                id = obj.num_sounds;
            end
            obj.SoundStruct.(name).wave = wav;
            obj.SoundStruct.(name).loop = loop;
            obj.SoundStruct.(name).vol = vol;
            obj.SoundStruct.(name).rep = rep;
            obj.SoundStruct.(name).bal = bal;
            %the two fileds below are for BpodHifi
            obj.SoundStruct.(name).loop_mode = loop;
            obj.SoundStruct.(name).loop_dur = (size(wav,2)*rep)./(obj.SF);
            obj.synced = 0;
        end
        
        function id = GetSoundid(soundname,playORstop)
            id = nan;
        end
        
        function isSuss = sync(obj)
            % sync the sound between local and server
            isSuss = 1;
        end
        
        function OK = startServ(obj)
            % RPi start listening on the serial port
            % leave this empty
            OK = 1;
        end
        
        function OK = closeServ(obj)
            % RPi start listening on the serial port
            % leave this empty
            OK = 1;
        end
        
        function closeConn(obj)
            % close socket connection
            % leave this empty
        end
        function SF = getSF(obj)
            SF = obj.SF;
            %add sound funcs to set sf?
        end
    end
    
    methods (Static = true)
        function str = trigger()
            global BpodSystem
            if strcmp(BpodSystem.PluginObjects.SoundServer,'Rpi')
                str = 'Serial1Code';
            else
                str = 'PlaySound';
            end
            
        end
    end
    
end


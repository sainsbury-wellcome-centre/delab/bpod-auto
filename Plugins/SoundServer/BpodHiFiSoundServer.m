classdef BpodHiFiSoundServer < SoundServ
    % BpodHiFiSoundServer, from SoundServ
    %   handel PsychSound input/ouput, dealing with soundid by name, 
    %
    % How to use:
    %
    % At the begining, initlize the sound server, laod sound and put it to
    % bpodsystem
    % sampling_rate = 192000;
    % SoundsObj =  BpodHiFiSoundServer(sampling_rate);
    % GoSound = GenerateSineWave(current_sf, 4000, .05); %GoSound can be any name
    % SoundsObj.load('GoSound', GoSound);
    % SoundsObj.sync();
    % global BpodSystem;BpodSystem.PluginObjects.Sound = SoundsObj;
    %
    % before declaring SMA, extract sound server from bpod system
    % snd = BpodSystem.PluginObjects.Sound
    % in your SMA, define OutputActions like this:
    % 'OutputActions', {'PlaySound', snd.playSnd('GoSound')};
    % 'OutputActions', {'StopSound', snd.playSnd('GoSound')};
    % 'OutputActions', {'StopAllSound', 'PlaceHolder'};
    % 'PlaceHolder' can be anything
    %
    % full example: see bpod-protocols/rig_test_hf
    %
    % By Jingjie Li
    
    properties
        SoundDriverObj
        ModuleName
    end
    
    methods
        function obj = BpodHiFiSoundServer(SF)
            if nargin > 0
                obj.SF = SF;
            else
                obj.SF = 192000; %default sampling rate
            end

            global BpodSystem
            assert(isfield(BpodSystem.ModuleUSB, 'HiFi1'),'Error: HiFi module not paired with USB port')
            obj.SoundDriverObj = BpodHiFi(BpodSystem.ModuleUSB.HiFi1);
            BpodSystem.SoundModule = 'BpodHiFi';%using psychsound as the sound module, this could also be like PiSound, or BpodHiFi
            obj.setHiFiSF();
            obj.ModuleName = 'HiFi1';
        end

        function obj = setHiFiSF(obj)
            if obj.SoundDriverObj.Info.isHD
                % HD is the traditional one we purchased from sanworks
                switch obj.SF
                    case 44100
                    case 48000
                    case 96000
                    case 192000
                    case 384000
                    otherwise
                        error('Error: Invalid sampling rate.');
                end
                obj.SoundDriverObj.SamplingRate = obj.SF;
            else
                % PRO soundcard is using another DAC: PCM5122, which is
                % running on a doubled sampling rate
                switch obj.SF
                    case 96000
                    case 192000
                    case 384000
                    otherwise
                        error('Error: Invalid sampling rate.');
                end
                obj.SoundDriverObj.SamplingRate = obj.SF/2;
            end
        end

        function obj = setDigitalAttenuation(obj,attenatuion_level)
            % this is to turn down the volume
            obj.SoundDriverObj.DigitalAttenuation_dB = attenatuion_level;
        end
        
        function isSuss = sync(obj)
            for i=1:obj.num_sounds
                % psych sound id start at 1
                obj.SoundStruct.(obj.sound_list{i}).soundid = i;%correct the sound id to start from 1
                obj.SoundStruct.(obj.sound_list{i}).id = obj.SoundStruct.(obj.sound_list{i}).soundid;
                wave_to_load = obj.SoundStruct.(obj.sound_list{i}).vol .* obj.SoundStruct.(obj.sound_list{i}).wave;
                obj.SoundDriverObj.load(obj.SoundStruct.(obj.sound_list{i}).soundid, wave_to_load,'LoopMode', obj.SoundStruct.(obj.sound_list{i}).loop_mode, 'LoopDuration', obj.SoundStruct.(obj.sound_list{i}).loop_dur);
            end
            obj.SoundDriverObj.push();
            isSuss = 1;
            obj.synced = 1;
        end
        
        function id = GetSoundid(obj,soundname)
            is = isfield(obj.SoundStruct,soundname);
            if is
                id = obj.SoundStruct.(soundname).soundid;
            else
                id  = nan;
            end
        end
        
        function update_wav_by_name(obj,soundname,new_wave)
            if size(new_wave,1)==1
                new_wave = [new_wave;new_wave];
            end
            if any(abs(new_wave(:))>1)
                fprintf(2,'BpodHiFiSoundServer: abs(wav) cannot be greater than 1')
                new_wave(new_wave>1) = 1;
                new_wave(new_wave<-1) = -1;
            end
            id = obj.SoundStruct.(soundname).soundid;
            volume = obj.SoundStruct.(soundname).vol;
            obj.SoundDriverObj.load(id, new_wave.*volume,'LoopMode', obj.SoundStruct.(soundname).loop_mode, 'LoopDuration', obj.SoundStruct.(soundname).loop_dur);
            obj.SoundDriverObj.push();
        end
        function play(obj,soundid)
            obj.SoundDriverObj.play(soundid);
        end
        function str_to_serial = playSnd(obj,soundname)
            soundid = obj.GetSoundid(soundname);
            str_to_serial = ['P' soundid-1];
        end

        function str_to_serial = stopSnd(obj,soundname)
            soundid = obj.GetSoundid(soundname);
            str_to_serial = ['x', soundid-1];
        end

        function soundmodule = getSoundModule(obj)
            soundmodule = obj.ModuleName;
        end
        
        function stop(obj)
            obj.SoundDriverObj.stop();
        end
        
        function delete(obj)
             obj.SoundDriverObj.delete;
        end

        function stopAll(obj)
            if isvalid(obj)
                obj.SoundDriverObj.stop();
            end
        end
    end
    
end
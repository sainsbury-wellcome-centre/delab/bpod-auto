classdef PsychSoundServer < SoundServ
    % PsychSoundServer, from SoundServ
    %   handel PsychSound input/ouput, dealing with soundid by name, 
    % How to use:
    %
    % At the begining, initlize the sound server, laod sound and put it to
    % bpodsystem
    % sampling_rate = 48000;
    % SoundsObj =  PsychSoundServer(sampling_rate);
    % GoSound = GenerateSineWave(current_sf, 4000, .05); %GoSound can be any name
    % SoundsObj.load('GoSound', GoSound);
    % SoundsObj.sync();
    % global BpodSystem;BpodSystem.PluginObjects.Sound = SoundsObj;
    %
    % before declaring SMA, extract sound server from bpod system
    % snd = BpodSystem.PluginObjects.Sound
    % in your SMA, define OutputActions like this:
    % 'OutputActions', {'PlaySound', snd.playSnd('GoSound')};
    % 'OutputActions', {'StopSound', snd.playSnd('GoSound')};
    % 'OutputActions', {'StopAllSound', 'PlaceHolder'};
    % 'PlaceHolder' can be anything
    %
    % full example: see bpod-protocols/rig_test_fm
    %
    % By Jingjie Li
    properties
        SoundDriverObj
    end
    
    methods
        function obj = PsychSoundServer(SF)
            if nargin > 0
                current_sf = SF;
                obj.SF = SF;
            else
                current_sf = obj.SF; %use default params
            end
             obj.SoundDriverObj = PsychToolboxAudio(current_sf);

             global BpodSystem
             BpodSystem.SoundModule = 'PsychSound';%using psychsound as the sound module, this could also be like PiSound, or BpodHiFi
             BpodSystem.SoftCodeHandlerFunction = 'SoftCodeHandler_PlaySound';
        end
        
        function isSuss = sync(obj)
            for i=1:obj.num_sounds
                % psych sound id start at 1
                obj.SoundStruct.(obj.sound_list{i}).soundid = i;%correct the sound id to start at 1 in PsychSound
                obj.SoundStruct.(obj.sound_list{i}).id = obj.SoundStruct.(obj.sound_list{i}).soundid;
                wave_to_load = obj.SoundStruct.(obj.sound_list{i}).vol .* obj.SoundStruct.(obj.sound_list{i}).wave;
                if size(wave_to_load,1)==1
                    wave_to_load = [wave_to_load;wave_to_load];
                end
                obj.SoundDriverObj.load(obj.SoundStruct.(obj.sound_list{i}).soundid, wave_to_load);

                if obj.SoundStruct.(obj.sound_list{i}).loop
                    %set n_rep = 0 in order to loop thr sound
                    obj.SoundStruct.(obj.sound_list{i}).rep = 0;
                end
            end
            isSuss = 1;
            obj.synced = 1;
        end
        
        function id = GetSoundid(obj,soundname)
            is = isfield(obj.SoundStruct,soundname);
            if is
                id = obj.SoundStruct.(soundname).soundid;
            else
                id  = nan;
            end
        end
        
        function update_wav_by_name(obj,soundname,new_wave)
            if size(new_wave,1)==1
                new_wave = [new_wave;new_wave];
            end
            if any(abs(new_wave(:))>1)
                fprintf(2,'PsychSoundServer: abs(wav) cannot be greater than 1')
                new_wave(new_wave>1) = 1;
                new_wave(new_wave<-1) = -1;
            end
            id = obj.SoundStruct.(soundname).soundid;
            volume = obj.SoundStruct.(soundname).vol;
            obj.SoundDriverObj.load(id, new_wave.*volume);
        end
        function play(obj,soundid)
            n_rep = obj.SoundStruct.(obj.sound_list{soundid}).rep;
            obj.SoundDriverObj.play(soundid,n_rep);
        end

        function soundid = playSnd(obj,soundname)
            soundid = obj.GetSoundid(soundname);
        end

        function soundid = stopSnd(obj,soundname)
            soundid = obj.GetSoundid(soundname);
        end
        
        function stop(obj,soundid)
            obj.SoundDriverObj.stop(soundid);
        end
        
        function delete(obj)
             obj.SoundDriverObj.delete;
        end

        function stopAll(obj)
            obj.SoundDriverObj.stopAll;
        end
    end
    
end

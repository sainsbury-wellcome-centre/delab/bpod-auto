classdef LickMotorController < dynamicprops
    %LickMotorController
    %   Packing all the lick motor controlling code
    %
    % Usage:
    % As the begining of your session:
    % MotorCnrl = LickMotorController()
    %
    % Before running your sma
    % 
    % MotorCnrl.ActivateMotor()
    % or LickMotorController.StartMotor() % using the static function
    % 
    % In your protocol sma, specify motor movement in OutputAction:
    % 'OutputActions', {'MoveMotor',MotorCnrl.GoFullForward()}
    % 'OutputActions', {'MoveMotor',MotorCnrl.GoFullBackward()}
    % 'OutputActions', {'MoveMotor',MotorCnrl.GoFullLeft()}
    % 'OutputActions', {'MoveMotor',MotorCnrl.GoFullRight()}
    %
    % Or Using the static method without class declaring
    % 'OutputActions', {'MoveMotor',LickMotorController.GoFullForward()}
    % 'OutputActions', {'MoveMotor',LickMotorController.GoFullBackward()}
    % 'OutputActions', {'MoveMotor',LickMotorController.GoFullLeft()}
    % 'OutputActions', {'MoveMotor',LickMotorController.GoFullRight()}
    %
    % Or Using using alias (see sma.AddState and example_config_files/head_fixed_config_example.ini)
    % 'OutputActions', {'MoveMotor','FullForward'}
    % 'OutputActions', {'MoveMotor','FullBackward'}
    % 'OutputActions', {'MoveMotor','FullLeft'}
    % 'OutputActions', {'MoveMotor','FullRight'}
    %
    % After the finish of this SMA
    % MotorCnrl.DeactivateMotor()
    % or LickMotorController.ResetMotor() % using the static function
    %
    % Jingjie Li, 2022-12-23

    properties
        conn;
        name;
        running;
    end

    methods
        function obj = LickMotorController()
            %LickMotorController()
            obj.name = '';
            obj.conn = 0;
            obj.running = 0;
            global BpodSystem
            if any(contains(BpodSystem.Modules.Name,'LM'))
                LM_module_posi = find(contains(BpodSystem.Modules.Name,'LM'));
                obj.name = BpodSystem.Modules.Name{LM_module_posi(1)};
                obj.conn=1;
            end
        end

        function delete(obj)
            % if this class is been removed
            ModuleWrite(obj.name,'P') %reset the position of the motor, and stop listening
            obj.name = '';
            obj.conn = 0;
            obj.running = 0;
        end

        function name = getModuleName(obj)
            assert(obj.conn,'Motor Module Not Connected!')
            name = obj.name;
        end

        function name = MoveMotor(obj)
            assert(obj.conn,'Motor Module Not Connected!')
            name = obj.name;
        end

        function ActivateMotor(obj)
            assert(obj.conn,'Motor Module Not Connected!')
            assert(~obj.running,'Motor Already Running! Stop First!')
            ModuleWrite(obj.name,'Z')
            obj.running = 1;
        end

        function DeactivateMotor(obj)
            assert(obj.conn,'Motor Module Not Connected!')
            if ~obj.running
                ModuleWrite(obj.name,'P')
                obj.running = 0;
            else
                fprintf(2,'Motor is not started, cannot stop')
            end
        end
    end
    methods(Static)
        function name = MoveMotorDefault()
            name = 'LM1';
        end
        function cmd = StartMotor()
            %Motor will go full forward
            cmd = 'Z';
        end
        function cmd = ResetMotor()
            %Motor will go full forward
            cmd = 'P';
        end
        function cmd = GoFullForward()
            %Motor will go full forward
            cmd = 'F';
        end

        function cmd = GoFullBackward()
            %Motor will go full backward
            cmd = 'R';
        end

        function cmd = GoFullLeft()
            %Motor will go full left
            cmd = 'A';
        end

        function cmd = GoFullRight()
            %Motor will go full right
            cmd = 'D';
        end
    end
end
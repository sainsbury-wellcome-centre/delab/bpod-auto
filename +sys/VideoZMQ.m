classdef VideoZMQ < sys.VideoMsg

	properties
	end
	methods
		function obj = VideoZMQ()
            db_config = utils.ini2struct('~/.dbconf');
            obj.vid_method = db_config.vid.method;
            obj.conn = [];
            if ~strcmp(obj.vid_method,'ZMQ')
                error("error for video method, not ZMQ");
            else %method correct, generate video conection
                % set-up connection
                global BpodSystem
                obj.conn = BpodSystem.zmqconn_pub;
                obj.msgkey = sprintf('%d_videoserver',db.getRigID());
            end
        end

        function out = checkConn(obj)
            out = ~isempty(obj.conn);
        end
        
        function send_message(obj, message2send)
            if obj.checkConn()
                % send message2send
                obj.conn.sendkv(obj.msgkey,message2send);
            end
        end

        function delete(obj) % Destructor
            % disconnect everything
            % but not doing anything here
        end

		
	end


end
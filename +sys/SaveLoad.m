classdef SaveLoad < handle

	properties
		subjid = 'JLI-T-0001'
        species = 'rat'
		sessiondate
		starttime
		protocol
		startstage = 1;
		stage = 1;
		sessid;
        video_handler;
        test = false;
        trial_info;       %this will be send through zmq video stuff for each trial, can be accessed and updated remotely

	end
	methods
		function obj = SaveLoad()

		end

		function saveTrial(obj, tdata, pdata)
		end

		function saveSession(obj, meta, data, next_settings)
        end

        function setRunning(obj)
        end


		function [settings_name, expg_data, subj_data] = getSettings(obj)
			[settings_name, expg_data, subj_data] = deal([]);

		end

	end


end
% abstract class for zmq/udp messages for the video messages
% abstract class for the sys.vidZMQ and sys.vidUDP classes

classdef VideoMsg < handle
    properties
        target_ip;
        target_port;
        conn; %store connection
        msgkey;
        vid_method;
    end

    methods
        function obj = VideoMsg()
        end

        function out = checkConn(obj)
        end

        function send_message(obj, message2send)
        end

        function delete(obj) % Destructor
            % disconnect everything
            % but not doing anything here
        end

        function start_video(obj, sessid, subjid, test)
            if isempty(subjid)
                subjid = 'JLI-T-0001';
            end
            if isempty(sessid)
                sessid = -1;
            end
            msg = struct();
            msg.command = 'start';
            msg.sessid = sessid;
            msg.subjid = subjid;
            if test
            % convert boolean to string for python hand
                msg.test = 'true';
            else
                msg.test = 'false';
            end
            msg.ts = datestr(now(),'yyyy_mm_dd_HH_MM_SS');

            % msg_to_send = jsonencode(msg);

            obj.send_message(msg);

        end

        function triger_video_trial(obj, sessid, subjid, test,ndt, trial_info)
            %send n_done_trial from dispatch
            if nargin < 6
                trial_info = '';
            end
            msg = struct();
            msg.command = 'trial';
            msg.sessid = sessid;
            msg.subjid = subjid;
            msg.trial_num = ndt;
            if test
            % convert boolean to string for python hand
                msg.test = 'true';
            else
                msg.test = 'false';
            end
            msg.ts = datestr(now(),'yyyy_mm_dd_HH_MM_SS');
            if ~isempty(trial_info)
                msg.info = trial_info;
            end

            % msg_to_send = jsonencode(msg);

            obj.send_message(msg);

        end

        function terminate_video(obj, sessid, subjid, test)
            msg = struct();
            msg.command = 'stop';
            msg.sessid = sessid;
            msg.subjid = subjid;
            msg.ts = datestr(now(),'yyyy_mm_dd_HH_MM_SS');
            if test
            % convert boolean to string for python hand
                msg.test = 'true';
            else
                msg.test = 'false';
            end
            obj.send_message(msg);
            obj.delete;
        end

    end
end

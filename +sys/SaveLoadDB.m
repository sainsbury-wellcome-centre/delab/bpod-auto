classdef SaveLoadDB < sys.SaveLoad
    
    properties
        dbc             % the database connection
        rigid           % maps to met.rigs
        %species         % usually mouse or rat, maps to met.subjects (should be met.animals now)
        settingsid      % maps to met.settings
        subjsettingid   % maps to met.subjsettings
        settingsname    % maps to met.settings
        expg_data       % a struct to augment the default settingsname settings for a particular experimental group. 
        subj_data       % a struct to augment the default settingsname settings for a particular subject. 
        expgroupid         % get the experid and assume it is not changing during a session.
        % expg_data and subj_data are not fully implemented

        %% Inherited from SaveLoad
        %
        % sessid         % maps to beh.sessions 
        % subjid
        % sessiondate
        % starttime
        % protocol
        % startstage
        % stage
    end
    
    methods
        
        function obj = SaveLoadDB(protocol, by, val)
            dbc = db.labdb.getConnection();
            dbc.use('beh');
            [obj.rigid] = db.getRigID();

            if strcmp(by, 'name')
                obj.subjid = val;
                % We need to create a new row in the sessions table
                sessinfo.sessiondate = datestr(now(),29);
                sessinfo.starttime = datestr(now(),'HH:MM:SS');
                sessinfo.hostip = db.get_ip();
                sessinfo.rigid = obj.rigid;
                sessinfo.protocol = protocol;
                sessinfo.subjid = val;
                
                % Include a reference to the weighing that started the
                % session
                massid = dbc.get('select massid from met.mass where subjid = "%s" and timestampdiff(minute, mdate, now())<4',{val});
                if ~isempty(massid) && isnumeric(massid)
                    sessinfo.massid = max(massid);
                end
                                
                sqlstr = 'select stage from met.current_settings where subjid = "%s"';
                
                [startstage] = dbc.get(sqlstr, {obj.subjid});
                if isempty(startstage) || isnan(startstage)
                % The animal has never been run before so set up the settings based on experimental group.
                    startstage = 1;
                    [settingsid] = dbc.get('select settingsid from met.animals b, met.settings a where subjid = "%s" and a.expgroupid=b.expgroupid and stage=1', {obj.subjid});
                    setS.subjid = obj.subjid;
                    setS.settingsid = settingsid;
                    setS.settings_date = datestr(now(),31);
                    dbc.saveData('met.subject_settings',setS);
                end
                sessinfo.startstage = startstage;
                dbc.saveData('beh.sessions', sessinfo);
                obj.sessid = dbc.last_insert_id();
            elseif strcmp(by,'sessid')
                % This means the session is already in the sessions table
                obj.sessid = val;
                sessinfo = dbc.query('select * from beh.sessions where sessid = %d', {obj.sessid});
            else
                error('SaveLoadDB:invalid_by','Do not know how to process by %s\n',by)
            end
            
            obj.subjid = sessinfo.subjid;
            obj.species = db.getSpecies(obj.subjid);
            obj.sessiondate = sessinfo.sessiondate;
            obj.starttime = sessinfo.starttime;
            obj.protocol = sessinfo.protocol;
            obj.startstage = sessinfo.startstage;
            obj.stage = sessinfo.startstage;

            obj.dbc = dbc;
            rng(obj.sessid); % This is to make sure that the random number generator is seeded with a unique number for each session.

        end

        function [settingsname, expg_data, subj_data] = getSettings(obj)
            % [settingsname, expg_data, subj_data] = get_settings(obj)
            % Output:
            % settingsname      should appear in the protocol.useSettings switch statement.
            % expg_data         
            
            sqlstr = 'select * from met.current_settings where subjid = "%s"';
            setout = obj.dbc.query(sqlstr, {obj.subjid});
            
            if ~strcmpi(setout.protocol, obj.protocol)
                error('SaveLoadDB:getSettings','Protocol does not match settings protocol');
            end

            obj.settingsid = setout.settingsid;
            obj.subjsettingid = setout.subjsettingsid;
            obj.expgroupid = setout.expgroupid;
            settings_name = setout.settingsname;

            % make sure settings_name is a string
            if iscell(settings_name)
                settingsname = settings_name{1};
            else
                settingsname = settings_name;
            end
            
            if strcmpi(settingsname,'null')
                settingsname = '';
            end
            try
                obj.dbc.call('beh.saveSettingsName(%d,"%s")',{obj.sessid,settingsname});
            catch me
                    db.log_error(me, 'sessid', obj.sessid ,'notify',1,'caught',1,'comment','Failed to save settingsname');
                    fprintf(2,'Problem saving settingsname.\n');
                    utils.showerror(me);
            end


            if strcmpi(setout.expg_data,'null') || isempty(setout.expg_data)
                expg_data = [];
            else
                if isjava(setout.expg_data)
                    expg_data = json.mloads(char(setout.expg_data(1))');
                else
                    expg_data = json.mloads(setout.expg_data{1});
                end
                % convert the JSON in the database to a matlab struct
            end

            if strcmpi(setout.subj_data,'null') || isempty(setout.subj_data)
                subj_data = [];
            else
                try
                    if isjava(setout.subj_data)
                        subj_data = json.mloads(char(setout.subj_data(1))');
                    else
                        subj_data = json.mloads(setout.subj_data{1});
                    end
                catch me
                    db.log_error(me, 'sessid', obj.sessid ,'notify',1,'caught',1);
                    setout.subj_data
                    fprintf(2,'Problem loading settings.\n');
                    utils.showerror(me);
                    subj_data = [];
                end
                % convert the JSON in the database to a matlab struct
            end

            obj.settingsname = settingsname;
            obj.expg_data = expg_data;
            obj.subj_data = subj_data;

        end

        function setRunning(obj)
            sqlS.sessid = obj.sessid;
            sqlS.status = 'running';
            obj.dbc.saveData('beh.sess_status',sqlS);
        end
        
        function saveTrial(obj, totrials, toprotocol, toopto)
        % saveTrial(totrials, toprotocol)
        % saves the data from each trial to the DB
        % Inputs:
        % totrials   a struct with fields that match the beh.trials table
        % toprotocol a struct with fields that match the prt.(protocol) table
        
        if nargin < 4
            toopto = [];
        end

            totrials.sessid = obj.sessid;
            totrials.subjid = obj.subjid;
             if isnan(totrials.hit)
                totrials = rmfield(totrials, 'hit');
                % Can't pass NaN to DB and hit is default NULL so by
                % excluding it we have the same effect.
            end
            if isnan(totrials.n_pokes)
                totrials = rmfield(totrials, 'n_pokes');
                % Can't pass NaN to DB and hit is default NULL so by
                % excluding it we have the same effect.
            end
            
            state_matrix = totrials.state_matrix;
            parsed_events = totrials.parsed_events;
            data = totrials.data;
            sets = totrials.settings;

            % We have moved all the blobs to seperate tables. So don't save them to trials.
            totrials = rmfield(totrials,{'data', 'parsed_events', 'settings','state_matrix'});
            obj.dbc.saveData('beh.trials', totrials);
            trialid = obj.dbc.last_insert_id;
            
            todata.data = json.mdumps(data);
            todata.trialid = trialid;
            obj.dbc.saveData('beh.trial_data', todata);
            try
                toSM.state_matrix = json.mdumps(state_matrix,'compress',1);
                toSM.trialid = trialid;
                obj.dbc.saveData('beh.state_matrix', toSM);
            catch me
                utils.showerror(me)
            end
            
            toPE.parsed_events = json.mdumps(parsed_events);
            toPE.trialid = trialid;
            obj.dbc.saveData('beh.parsed_events', toPE);

            toSettings.settings = json.mdumps(sets);
            toSettings.trialid = trialid;
            obj.dbc.saveData('beh.trial_settings', toSettings);


            if ~isempty(toprotocol)
                try
                    toprotocol.trialid = trialid;
                    obj.dbc.saveData(['prt.' lower(obj.protocol)], toprotocol);
                catch me
                    % In the future do a better job at catching errors here.
                    utils.showerror(me);
                end
            end
            
            if ~isempty(toopto)
                if isfield(toopto,'details')
                    % in order to save opto data(which is a blob) into a
                    % seperate table, we first need to remove that field
                    % from toopto struct to prevent been inserted to the
                    % opto_trials table.
                    opto_details = toopto.details;
                    toopto = rmfield(toopto,'details');
                end
                try
                    toopto.trialid = trialid;
                    obj.dbc.saveData('beh.opto_trials', toopto);
                catch me
                    utils.showerror(me);
                end
                if exist('opto_details','var')
                    %save opto_Trials_data as a blob
                    try
                        optodata = struct();
                        optodata.trialid = trialid;
                        optodata.data = opto_details;
                        optodata.data = json.mdumps(optodata.data);%encode struct/var to json
                        obj.dbc.saveData('beh.opto_trials_data', optodata);
                    catch me
                        utils.showerror(me);
                    end
                end
            end


        end % saveTrial
        

        function updateStatus(obj, status)
        % Updates the session status
            sqlS1.sessid = obj.sessid;
            sqlS1.status = status;
            obj.dbc.saveData('beh.sess_status',sqlS1);
        end

        function saveSession(obj, meta, data, next_settings)
            % saveSession(obj, meta, data, next_settings)
            % meta should have fields that match the columns of the sess_ended table
            % data is end of session data, as follows:
            % sessid	int(11) unsigned	NO	PRI	NULL	
            % num_trials	mediumint(9)	NO		NULL	
            % end_time	timestamp	NO		CURRENT_TIMESTAMP	
            % end_stage	smallint(4)	YES		NULL	
            % total_profit	float	YES		NULL	
            % hitfrac	float	YES		NULL	
            % violfrac	float	YES		NULL	
            % bias	float	YES		NULL	
            %
            % next_settings has some information for the next days session. Will get loaded as subj_data in getSettings.


            
            meta.sessid = obj.sessid;
            meta.end_stage = obj.stage;
            try
                obj.dbc.saveData('beh.sess_ended',meta);
            catch me
                fprintf(2,'Failed to save sess ended\n')
                db.log_error(me, 'sessid', obj.sessid ,'notify',1,'caught',1,'comment',json.mdumps(meta));
                utils.showerror(me)
            end
            if ~isempty(data)
                sqlS.sessid = obj.sessid;
                sqlS.session_data = json.mdumps(data);
                obj.dbc.saveData('beh.sess_ended_data',sqlS);
            end

            % Save the settings for tomorrow
            sql2.subjid = obj.subjid;
            % Use the expgroup and stage info to setup the correct settings for tomorrow.
            sql2.settingsid = obj.dbc.get('select settingsid from met.settings where expgroupid=%d and stage=%d',{obj.expgroupid, obj.stage});
            sql2.saved_by_experid = 0; % This indicates that the settings were set automatically
            sql2.settings_date = datestr(now(),31);
            sql2.saved_on_ip = db.get_ip();
            if ~isempty(next_settings)
                sql2.settings_data = json.mdumps(next_settings);
            end
            try
                obj.dbc.saveData('met.subject_settings',sql2);
            catch me
                fprintf(2,'Failed to save settings\n')
                db.log_error(me, 'sessid', obj.sessid ,'notify',1,'caught',1,'comment',json.mdumps(sql2));
                utils.showerror(me)
            end
            obj.updateStatus('finished');
        end % saveSession
        

        
    end % methods
end % classdef
classdef dispatchObj
%% dispatch(ProtocolObject)
% ProtocolObject(a input calss if this) is a protocol class that inherits from ProtoObj(a template class).
% this function acts as a "bridge" between Bpod (in particular the global BpodSystem variable)
% and BPod protocols.
% Inputs:
% ProtocolObject    Required. An instance of a class that is a ProtoObj (or a child of ProtoObj).
% action            [all]
% sessid            [] if sessid is passed in, dispatch will use information in the sessions table to setup the session.
% name              [] name is the subjid of the subject to run. Generally, either name or sessid needs to be specified.
% saveload          [] normally this is left empty, it is used in the `test==true` case of start_protocol. If saveload 
%                      is not empty name and sessid are ignored.
% use_db            [BpodSystem.use_db] mostly for testing purposes, can set this to false.
%
% The most common use of dispatch is to specify the protocol and a subject. e.g.:
% dobj = dispatchObj(Operant, 'name', 666) %to initlize the dispatchobj
% then call dobj to run the protocol:
% dobj.runAll();
%
% This will 1) initialize the saveload for 666 (if not specified) and the Operant Protocol 2) load settings for 666 and 3) start running the protocol.


    properties
        name
        sessid
        saveload
        use_db
        notify
        video_trigger
        video_handler
        protoObj
        fsm_stop_button
    end

    methods
        function obj = dispatchObj(protoObj,varargin)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here

            obj.sessid = utils.inputordefault('sessid', [], varargin);
            obj.name   = utils.inputordefault('name'  , ''  , varargin);
            obj.saveload = utils.inputordefault('saveload'  , []  , varargin);
            obj.use_db	= utils.inputordefault('use_db', false, varargin);
            obj.notify	= utils.inputordefault('notify', false, varargin);
            obj.video_trigger	= utils.inputordefault('video_trigger', obj.use_db, varargin);%by default, triggering video with db

            if isempty(obj.sessid) && isempty(obj.name) && isempty(obj.saveload)
                global BpodSystem
                obj.name = BpodSystem.Status.CurrentSubjectName;
                % This code attempts to get subjid information from the BpodGUI if it was not specified in any way.
            end
            obj.protoObj = protoObj;

            obj.fsm_stop_button = false;

            obj.video_handler = [];
            if isempty(obj.video_trigger)
                obj.video_trigger = false;
            end
            if obj.video_trigger
                % use the `.dbconf` file to make the appropriate video_handler
                db_config = utils.ini2struct('~/.dbconf');
                if strcmp(db_config.vid.method,'UDP')
                    obj.video_handler = sys.VideoUDP; %use UDP message for video triggering
                elseif strcmp(db_config.vid.method,'ZMQ')
                    obj.video_handler = sys.VideoZMQ; %use ZMQ message for video triggering
                end
            end

        end

        function obj = runAll(obj)
            obj.init;
            obj.load_settings;
            obj.run;
            obj.end_session;
        end

        function obj = init(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            global BpodSystem

            if contains(BpodSystem.HW.Inputs,'Q')
                % have a stop button detected for bpod, enable button stop
                obj.fsm_stop_button = true;
            end
            BpodSystem.Status.CurrentProtocolName = class(obj.protoObj);
            
            %  determine the path of the protocol currently running
            BpodSystem.Status.CurrentProtocolDir = which(BpodSystem.Status.CurrentProtocolName);
            path_parts = strsplit(BpodSystem.Status.CurrentProtocolDir, filesep);
            BpodSystem.Status.CurrentProtocolDir = strjoin(path_parts(1:end-1), filesep);


            if isfield(BpodSystem.PluginObjects,'Sound')
                if ~isempty(BpodSystem.PluginObjects.Sound) && isprop(BpodSystem.PluginObjects.Sound,'SoundDriverObj')
                    BpodSystem.PluginObjects.Sound.delete;%stop sound server if running, will re-initilize it soon after
                end
            end

            if isempty(obj.saveload)
                % saveload takes priority over name or sessid
                if obj.use_db
                    if ~isempty(obj.sessid)
                        % sessid gets 2nd priority
                        % Create a SaveLoadDB based on sessid
                        sl = sys.SaveLoadDB(class(obj.protoObj),'sessid', obj.sessid);
                    elseif ~isempty(obj.name)
                        % name gets 3rd (and last) priority
                        % Create a SaveLoadDB based on subjid
                        % JCE: probably should check for conflict between name/sessid/saveload
                        sl = sys.SaveLoadDB(class(obj.protoObj),'name', obj.name);
                    else
                        error('dispatch','Cannot use database without sessid or name specified.')
                    end
                else
                    sl = sys.SaveLoadMAT(class(obj),'name',obj.name);
                end
                obj.protoObj.saveload = sl; % The protocol will use this object for loading and saving!
            else % if isempty(saveload)
                obj.protoObj.saveload = obj.saveload;
            end

            if isempty(obj.video_trigger) || isempty(obj.video_handler)
                obj.video_trigger = false;
            end
            if obj.video_trigger

                obj.video_handler.start_video(obj.protoObj.saveload.sessid, obj.protoObj.saveload.subjid, obj.protoObj.saveload.test);
            end

            obj.protoObj.base_reward = db.getBaseReward(obj.name);
            % Since we use multiple species we need to have variable reward sizes.

            obj.protoObj.init(); % Let the protocol initialize itself

            BpodSystem.SoftCodeHandlerFunction = 'SoftCodeHandler_PlaySound';
            BpodSystem.trial_time_limit = obj.protoObj.trial_time_limit;
            % This defines the function to use for software codes sent back from bpod.
        end

        function obj = load_settings(obj)
            obj.protoObj.loadSettings();
            % Tell the object to load settings.
            obj.sessid = obj.protoObj.saveload.sessid;
            global BpodSystem
            BpodSystem.sessid = obj.sessid;
        end

        function obj = run(obj)
            global BpodSystem
            BpodSystem.Status.BeingUsed = true;
            if obj.use_db
                obj.protoObj.saveload.setRunning();
            end
            while BpodSystem.Status.BeingUsed
                if BpodSystem.Debug
                    keyboard
                end
                obj.run_once;
            end
        end

        function obj = run_once(obj)
            global BpodSystem
            this_trial_error_flag = false;
            obj.protoObj.n_started_trials = obj.protoObj.n_started_trials + 1;
            obj.protoObj.prepareNextTrial();
            SMA = obj.protoObj.generateSM();
            if isstruct(BpodSystem.Sync)
                SMA = sma.AddSyncStates(SMA, obj.protoObj.n_started_trials, 'return_state', obj.protoObj.syncReturnState, 'secsperbit', BpodSystem.Sync.secsperbit, 'trialbits', BpodSystem.Sync.trialbits, 'header', BpodSystem.Sync.header);
            end
            sent = SendStateMatrix(SMA);
            times_sent = 2;
            while sent == 0 && BpodSystem.Status.BeingUsed
                % SendStateMatrix returns 0 if there is a problem
                fprintf(2,'Failed to Send State Matrix, resending. attempt %d\n',times_sent);
                % failed to send state matric is a big problme, need instant
                % notify. It may indicate that bpod arduino could be dead
                err_str_sma = 'Failed to Send State Matrix!! Bpod might be dead!! Please Check!!!!';
                % This error message is over the top

                % TODO: Document / improve this logic? 
                % I think it would be better to just call `warning` :
                % warning(err_str_sma)
                % db.log_error(me,'caught',1,'sessid',obj.sessid,'subjid',obj.name,'notify',obj.notify,'print_stack',false,'comment',err_str_sma);

                try
                    assert(false,err_str_sma)
                catch me
                    utils.showerror(me);
                    db.log_error(me,'caught',1,'sessid',obj.sessid,'subjid',obj.name,'notify',obj.notify,'print_stack',false,'comment',err_str_sma);
                end

                RunProtocol('Stop'); % Should we do anything else to reinitialize?
                pause(0.02); % Give matlab a chance to do some cleanup if there is stuff going on in the background.
                sent = SendStateMatrix(SMA);
                times_sent = times_sent + 1;
            end


            try
                RawEvents = RunStateMatrix();
                if isnumeric(RawEvents)
                    this_trial_error_flag = true;
                end
            catch me
                this_trial_error_flag = true;
                if ~contains(class(obj.protoObj),'rig_test')
                    % Don't send errors to database and slack for rigtest.
                    if obj.use_db
                        %enable after finishing testing
                        %db.log_error(me, 'subjid',obj.name,'sessid',obj.sessid,'comment',json.mdumps(SMA), 'caught',1,'notify',obj.notify);  
                        % TODO: Why is this commented out? because log_error can error?
                    end
                    if strcmp(me.identifier, 'RunStateMatrix:MaxEvents') || strcmp(me.identifier, 'RunStateMatrix:NoEvents')
                        fprintf(2,'This trials data is bad. Discarding.\n');
                        % rethrow(me);
                    end
                end
            end
            if BpodSystem.Status.BeingUsed
                if this_trial_error_flag && contains(class(obj.protoObj),'rig_test')
                    fprintf(2, 'Error in RunStateMatrix, skipping.\n')
                elseif this_trial_error_flag
                    err_str_sma = 'StateMatrix exited with error.\nCheck if obj.protoObj.trial_time_limit reached';
                    try
                        assert(false,err_str_sma)
                    catch me
                        utils.showerror(me);
                        db.log_error(me,'caught',1,'sessid',obj.sessid,'subjid',obj.name,'notify',obj.notify,'print_stack',false,'comment',err_str_sma);
                    end
                else
                    % If this is false, the protocol was stopped mid-trial
                    obj.protoObj.n_done_trials = obj.protoObj.n_done_trials + 1;
                    try
                        BpodSystem.Data = int.AddTrialEventsAlias(BpodSystem.Data,RawEvents);
                        parsed_events = BpodSystem.Data.RawEvents.Trial{end};
                        parsed_events(1).StartTime = BpodSystem.Data.TrialStartTimestamp(end);
                        BpodSystem.Data = AddTrialEvents(BpodSystem.Data,RawEvents);
                        if obj.protoObj.n_done_trials == 1
                            obj.protoObj.peh = parsed_events;
                        else
                            obj.protoObj.peh(obj.protoObj.n_done_trials) = parsed_events;
                        end
                        obj.protoObj.trialCompleted();

                        if obj.video_trigger && ~contains(class(obj.protoObj),'rig_test') %send trial/sync trigger only for normal session
                            obj.video_handler.triger_video_trial(obj.protoObj.saveload.sessid, obj.protoObj.saveload.subjid, obj.protoObj.saveload.test,obj.protoObj.n_done_trials, obj.protoObj.saveload.trial_info);
                        end

                        obj.protoObj.saveTrial(BpodSystem.Opto); %save with opto flag
                    catch me
                        fprintf('Problem parsing trial %d.\n',obj.protoObj.n_done_trials)
                        utils.showerror(me);
                        obj.protoObj.n_done_trials = obj.protoObj.n_done_trials - 1;
                        if obj.use_db
                            db.log_error(me,'caught',1,'subjid',obj.name,'sessid',obj.sessid,'notify',obj.notify,'comment',sprintf('Problem parsing trial. Not saving data for trial %d',obj.protoObj.n_done_trials),'notify',1);
                        end
                    end
                end

                HandlePauseCondition;
            end
            if int.CheckForStop(obj.sessid)
                RunProtocol('Stop');
            end
        end

        function obj = end_session(obj)
            obj.protoObj.endSession();
            obj.sessid = [];
            obj.name = [];
            obj.saveload = [];
            obj.use_db = [];
            if obj.video_trigger && ~contains(class(obj.protoObj),'rig_test') %send ending msg only for normal session
                obj.video_handler.terminate_video(obj.protoObj.saveload.sessid, obj.protoObj.saveload.subjid, obj.protoObj.saveload.test);
                obj.video_trigger = [];
            end
            global BpodSystem
            if any(contains(BpodSystem.Modules.Name,'PA1'))
                % If found PortArray module is connected, shut the output
                % off manually
                ModuleWrite('PA1',['L',0]) %this command manully turn off port array LEDs
            end
            try
                rmpath(fullfile(BpodSystem.Status.CurrentProtocolDir));
            catch
                rmpath(fullfile(BpodSystem.Path.ParentDir, 'bpod-protocols', BpodSystem.Status.CurrentProtocolName));
            end
            BpodSystem.Status.CurrentProtocolName = '';
            RunProtocol('Stop') % This clears up the bpod object so the next session gets a clean set of globals.
            %Remove the current protocol from the path to avoid conflicts.
        end

    end
end

function bpod_robot()
    zmqlistener = timer();
    zmqlistener.StartFcn = @setup_zmq;
    zmqlistener.TimerFcn = @wait_for_msg;
    zmqlistener.ExecutionMode = 'fixedSpacing';
    zmqlistener.BusyMode = 'drop';
    zmqlistener.Period = 2;
    zmqlistener.TasksToExecute = +inf;
    zmqlistener.StartDelay = 0.1;

    global BpodSystem;
    BpodSystem.robot_timer = zmqlistener;

    start(BpodSystem.robot_timer)

end

function setup_zmq(obj,event)
    global BpodSystem;
    
    if isempty(BpodSystem)
        error('Bpod need to be started before run this')
    end

    this_rigid = db.getRigID();
    sub_topics = {sprintf('%d_startsession',this_rigid),sprintf('%d_stopsession',this_rigid),sprintf('%d_rigtest',this_rigid),sprintf('%d_stoprigtest',this_rigid)};

    BpodSystem.zmqconn_sub = net.zmqsub(sub_topics);

    obj.userdata = [];
    fprintf(1,'Waiting for zmq message...\n');
end

function wait_for_msg(obj,event)
global BpodSystem;
if BpodSystem.Status.BeingUsed
    data = '';%this will return if the protocol is running
else
    [addr, data] = BpodSystem.zmqconn_sub.recvjson();
end
if isempty(data)
    return
end
fprintf(1,'Got message for rig %s\n', addr);

try
    db.rigHeartbeat(); % This let's the DB know that the rig is alive and listening for new sessions

    fprintf(1,'Got a run signal, ');

    age = 24*60*60*(now()-datenum(datetime(data.ts,'InputFormat','yyyy-MM-dd_HH:mm:ss')));% age is in seconds
    input_command = data.event;

    if strcmp(input_command,'start_session')
        action = 'run';
        subjid = data.subjid;
    elseif strncmp(input_command,'rig_test',5)
        action = 'rigtest';
    else
        fprintf(2,'incorrect messege format, please check')
        action = 'unknown';
    end

    if age < 180  % Allow the file to be 3 minutes old.
        switch action
            case 'run'
                fprintf(1,'starting session for %s.\n', subjid);
                start_protocol(subjid);
            case 'rigtest'
                fprintf(1,'starting rigtest.\n');
                rigtest_fm()
            case 'water'
                % eventually something will go here
                fprintf(2,'Water not implemented yet\n')
            otherwise
                warning('Unknown action in bpod_robot.m')
        end
    else
        fprintf(1,', Got starting message late. Ignored.\n');
    end

catch me
    utils.showerror(me);
    db.log_error(me);
end

end
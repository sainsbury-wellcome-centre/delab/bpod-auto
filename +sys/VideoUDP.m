classdef VideoUDP < sys.VideoMsg

	properties
	end
	methods
		function obj = VideoUDP()
            db_config = utils.ini2struct('~/.dbconf');
            obj.vid_method = db_config.vid.method;
            obj.conn = [];
            if ~strcmp(obj.vid_method,'UDP')
                error("error for video method, not UDP");
            else %method correct, generate video conection
                obj.conn = udpport("IPV4");
                obj.target_ip = db_config.vid.host;
                obj.target_port = db_config.vid.port;
            end
        end

        function out = checkConn(obj)
            out = ~isempty(obj.conn);
        end
        
        function send_message(obj, message2send)
            msg_converted = jsonencode(message2send);
            write(obj.conn,msg_converted,"string",obj.target_ip,obj.target_port);
        end

        function out = readAll(obj)
            out = read(obj.conn,obj.conn.NumBytesAvailable,"string");
        end

        function delete(obj) % Destructor
            flush(obj.conn,"output")
            obj.conn = [];
        end

		
	end


end
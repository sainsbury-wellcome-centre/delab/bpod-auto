#!/bin/bash


export DISPLAY=:0
export SHELL=/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

killall MATLAB
sleep 2
cd ~/repos
ls -R --directory --color=never */.git | sed 's/\/.git//' | xargs -P10 -I{} git -C {} pull
cd
#n_lines=`wc -l bad_bpod_loop | awk '{print $1}'`
#if [[ $n_lines > 3 ]]; then
#    echo "Failed 3 times, using the update firmware trick"
#   cd repos/bpod/Bpod\ Firmware/
#     ./update_firmware.sh Bpod_Firmware_with_NewPCB/Bpod_Firmware_with_NewPCB.ino

# fi

#echo "failed" >> bad_bpod_loop
#touch ~/runsubject_rigtest
cd ~/repos/bpod-auto
/usr/bin/ptb3-matlab -desktop -r 'start_bpod;'

# Release note for FSM PCB V1.0
2021-12-29  Jingjie Li

# Updates
## Powering
for 5V external power input, using AMS1117/LM1117-ADJ instead (more comonn and cheaper parts), set ext input to ~5.3V to give ext power input more priority to be used. (when connected with external power, will use ext rather than USB)

add a 7V boost power module for comparator. 7V is achieved by TPS61040 DC booster circuit. Cheaper solution: MT3608. 

add anouther 3.3V powering for IR detector IOREF (pokewall), by LM1117/AMS1119-3.3.

## Digital input for IR ports
using hysteresis comparator for more stableized conversion, repalced josh's CD4050 buffer IC.

hysteresis is achieved by LM339 digital comparator, VL/VH/HYST: 1.96V/2.6V/0.64V

## BNC I/O
removed 3V3-5V voltage conversion, use 3V3 for non-isolation side powering directly.

## Isolator
add 0603 reistor at ISO_input, for potential input impedance matching. Normally left NC.

# Fixes
+ package layout of B0505S DC-DC module
+ isolator in/out order. Isolator MUST use π122M31/ISO7221





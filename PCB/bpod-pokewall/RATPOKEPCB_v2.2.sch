<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Seeed-OPL-Diode">
<packages>
<package name="LED-0603">
<wire x1="-1.3335" y1="0.635" x2="1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="0.635" x2="1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="-0.635" x2="-1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.3335" y1="-0.635" x2="-1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.381" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1905" x2="0.127" y2="0" width="0.127" layer="21"/>
<wire x1="0.127" y1="0" x2="0" y2="0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="-0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.0635" y1="0.254" x2="-0.0635" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<smd name="+" x="-0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<smd name="-" x="0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<text x="-1.905" y="0.889" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.143" y="-0.3175" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.5715" x2="1.27" y2="0.5715" layer="39"/>
</package>
<package name="LED-0805">
<wire x1="1.5875" y1="0.889" x2="1.5875" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-0.889" x2="-1.5875" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-0.889" x2="-1.5875" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="0.889" x2="1.5875" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.508" x2="-0.1905" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.254" x2="-0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0" x2="-0.1905" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="-0.254" x2="-0.1905" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="-0.508" x2="0" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="-0.1905" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.508" x2="0.127" y2="-0.0635" width="0.127" layer="21"/>
<wire x1="0.127" y1="-0.0635" x2="-0.1905" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0.254" x2="0.0635" y2="-0.127" width="0.127" layer="21"/>
<wire x1="0.0635" y1="-0.127" x2="-0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="0" x2="0" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="-0.254" x2="-0.1905" y2="-0.254" width="0.127" layer="21"/>
<smd name="+" x="-0.889" y="0" dx="0.889" dy="1.27" layer="1" roundness="25"/>
<smd name="-" x="0.889" y="0" dx="0.889" dy="1.27" layer="1" roundness="25"/>
<text x="-1.905" y="1.143" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-1.397" y="-0.3175" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.5875" y1="-0.889" x2="1.5875" y2="0.889" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="LED-1">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-5.08" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-LED-CLEAR-BLUE(0603)" prefix="D">
<description>304090045</description>
<gates>
<gate name="G$1" symbol="LED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-0603">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="19-217-BHC-ZL1M2RY-3T" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-LED-CLEAR-YELLOW(0603)" prefix="D">
<description>304090044</description>
<gates>
<gate name="G$1" symbol="LED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-0603">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="19-217-Y5C-AM1N1VY-3T" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-LED-CLEAR-RED(0805)" prefix="D">
<description>304090046</description>
<gates>
<gate name="G$1" symbol="LED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-0805">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="17-215SURC/S530-A2/TR8" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Resistor">
<packages>
<package name="R0603">
<wire x1="0.635" y1="1.397" x2="0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-0.635" y2="1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<text x="-1.016" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="R0402">
<wire x1="-0.4445" y1="0.762" x2="-0.3175" y2="0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.3175" y1="0.889" x2="0.3175" y2="0.889" width="0.0762" layer="21"/>
<wire x1="0.3175" y1="0.889" x2="0.4445" y2="0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.4445" y1="0.762" x2="0.4445" y2="-0.762" width="0.0762" layer="21"/>
<wire x1="0.4445" y1="-0.762" x2="0.3175" y2="-0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.3175" y1="-0.889" x2="-0.3175" y2="-0.889" width="0.0762" layer="21"/>
<wire x1="-0.3175" y1="-0.889" x2="-0.4445" y2="-0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.4445" y1="-0.762" x2="-0.4445" y2="0.762" width="0.0762" layer="21"/>
<smd name="1" x="0" y="0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<smd name="2" x="0" y="-0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.27" size="0.889" layer="27" font="vector" ratio="11" rot="R270">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="0.889" y1="-1.651" x2="-0.889" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="-0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="-1.651" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<text x="1.016" y="1.905" size="0.889" layer="25" font="vector" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.905" y="1.905" size="0.889" layer="27" ratio="11" rot="R270">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<wire x1="-1.27" y1="0.508" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-RES-150R-1%-1/10W(0603)" prefix="R" uservalue="yes">
<description>301010188</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0603FR-07150RL" constant="no"/>
<attribute name="VALUE" value="150R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-51R-1%-1/10W(0603)" prefix="R" uservalue="yes">
<description>301010225</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0603FR-0751RL" constant="no"/>
<attribute name="VALUE" value="51R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-22R-5%-1/8W(0805)" prefix="R" uservalue="yes">
<description>301010329</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0805JR-0722RL" constant="no"/>
<attribute name="VALUE" value="22R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-56.2R-1%-1/10W(0603)" prefix="R" uservalue="yes">
<description>301010693</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0603FR-0756R2L" constant="no"/>
<attribute name="VALUE" value="56.2R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-470R-1%-1/16W(0402)" prefix="R" uservalue="yes">
<description>301010012</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0402FR-07470RL" constant="no"/>
<attribute name="VALUE" value="470R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-1.5K-1%-1/8W(0805)" prefix="R" uservalue="yes">
<description>301010394</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0805FR-071K5L" constant="no"/>
<attribute name="VALUE" value="1.5K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-4.7K-5%-1/8W(0805)" prefix="R" uservalue="yes">
<description>301010306</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0805JR-074K7L" constant="no"/>
<attribute name="VALUE" value="4.7K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-RES-2.2K-5%-1/8W(0805)" prefix="R" uservalue="yes">
<description>301010333</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="RC0805JR-072K2L" constant="no"/>
<attribute name="VALUE" value="2.2K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Transistor">
<packages>
<package name="SOT-23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-0.254" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N">
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.2225" y1="-0.4445" x2="1.905" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.4445" x2="0.9525" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="2.2225" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="0.9525" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.3175" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="0.3175" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-1.397" y2="0.127" width="0.254" layer="94"/>
<circle x="0" y="1.905" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.254" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="-0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-3.4925" y="0" size="0.8128" layer="93">G</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" prefix="Q" uservalue="yes">
<description>305030015</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CJ2302" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SFH4050N">
<packages>
<package name="SFH4050N">
<wire x1="-0.75" y1="-1.55" x2="-0.75" y2="-2.75" width="0.005" layer="51"/>
<wire x1="-0.75" y1="-2.75" x2="0.75" y2="-2.75" width="0.005" layer="51"/>
<wire x1="0.75" y1="-2.75" x2="0.75" y2="-1.55" width="0.005" layer="51"/>
<wire x1="0.75" y1="-1.55" x2="-0.75" y2="-1.55" width="0.005" layer="51"/>
<wire x1="-0.75" y1="-1.55" x2="-0.55" y2="-0.35" width="0.005" layer="51"/>
<wire x1="0.75" y1="-1.55" x2="0.55" y2="-0.35" width="0.005" layer="51"/>
<wire x1="0.2" y1="0" x2="0.55" y2="-0.35" width="0.005" layer="51" curve="-90"/>
<wire x1="-0.55" y1="-0.35" x2="-0.2" y2="0" width="0.005" layer="51" curve="-90"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0" width="0.005" layer="51"/>
<smd name="A" x="-1.15" y="-2.15" dx="1.2" dy="0.8" layer="1" roundness="15" rot="R90"/>
<smd name="C" x="1.15" y="-2.15" dx="1.2" dy="0.8" layer="1" roundness="15" rot="R90"/>
<wire x1="0.566" y1="-1.55" x2="0.566" y2="-2.242" width="0.127" layer="21"/>
<wire x1="-0.496" y1="-1.55" x2="0.566" y2="-1.55" width="0.127" layer="21"/>
<text x="-1.27" y="-4.09" size="0.8128" layer="25">&gt;NAME</text>
<wire x1="0" y1="-5.106034375" x2="0" y2="0.280034375" width="0.0762" layer="21"/>
<wire x1="-3.175" y1="-2.947" x2="3.302" y2="-2.947" width="0.127" layer="21"/>
<wire x1="-1.7526" y1="-2.8956" x2="-1.7526" y2="-1.3462" width="0.127" layer="21"/>
<wire x1="-1.7526" y1="-1.3462" x2="1.7526" y2="-1.3462" width="0.127" layer="21"/>
<wire x1="1.7526" y1="-1.3462" x2="1.7526" y2="-2.921" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SFH4050N">
<wire x1="-3" y1="1" x2="3" y2="1" width="0.254" layer="94"/>
<wire x1="3" y1="1" x2="2" y2="0" width="0.254" layer="94"/>
<wire x1="2" y1="0" x2="0" y2="-2" width="0.254" layer="94"/>
<wire x1="0" y1="-2" x2="-3" y2="1" width="0.254" layer="94"/>
<wire x1="-3" y1="-2" x2="0" y2="-2" width="0.254" layer="94"/>
<wire x1="0" y1="-2" x2="3" y2="-2" width="0.254" layer="94"/>
<wire x1="3" y1="1" x2="5" y2="-0.5" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="5" y2="-0.5" width="0.254" layer="94"/>
<wire x1="5" y1="-0.5" x2="4.5" y2="-0.5" width="0.254" layer="94"/>
<wire x1="2" y1="0" x2="4" y2="-1.5" width="0.254" layer="94"/>
<wire x1="4" y1="-1" x2="4" y2="-1.5" width="0.254" layer="94"/>
<wire x1="4" y1="-1.5" x2="3.5" y2="-1.5" width="0.254" layer="94"/>
<pin name="C" x="0" y="-4.5" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="3.5" visible="off" length="short" direction="pas" rot="R270"/>
<text x="5.08" y="-2.54" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SFH4045N" prefix="IR">
<gates>
<gate name="G$1" symbol="SFH4050N" x="0" y="0"/>
</gates>
<devices>
<device name="SFH4050N_SMD" package="SFH4050N">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="box_header">
<packages>
<package name="BOX_HEADER">
<smd name="P$1" x="20.32" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$2" x="20.32" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$3" x="17.78" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$4" x="17.78" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$5" x="15.24" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$6" x="15.24" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$7" x="12.7" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$8" x="12.7" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$9" x="10.16" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$10" x="10.16" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$11" x="7.62" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$12" x="7.62" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$13" x="5.08" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$14" x="5.08" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$15" x="2.54" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$16" x="2.54" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$17" x="0" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$18" x="0" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$19" x="-2.54" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$20" x="-2.54" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$21" x="-5.08" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$22" x="-5.08" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$23" x="-7.62" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$24" x="-7.62" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$25" x="-10.16" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$26" x="-10.16" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$27" x="-12.7" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$28" x="-12.7" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$29" x="-15.24" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$30" x="-15.24" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$31" x="-17.78" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$32" x="-17.78" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$33" x="-20.32" y="2.985" dx="0.89" dy="4.8" layer="1"/>
<smd name="P$34" x="-20.32" y="-3.515" dx="0.89" dy="4.8" layer="1"/>
<wire x1="21.086" y1="5.716" x2="21.086" y2="4.7" width="0.127" layer="21"/>
<wire x1="-21.594" y1="5.97" x2="-21.594" y2="-6.476" width="0.127" layer="21"/>
<wire x1="-21.594" y1="-6.476" x2="21.594" y2="-6.476" width="0.127" layer="21"/>
<wire x1="21.594" y1="-6.476" x2="21.594" y2="5.97" width="0.127" layer="21"/>
<wire x1="-21.594" y1="5.97" x2="21.594" y2="5.97" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BOX_HEADER">
<wire x1="-3.81" y1="22.86" x2="-3.81" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-22.86" x2="3.81" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="22.86" x2="3.81" y2="22.86" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-22.86" x2="-3.81" y2="-22.86" width="0.254" layer="94"/>
<circle x="-2.54" y="20.32" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="17.78" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="15.24" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="10.16" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="7.62" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-7.62" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-10.16" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-15.24" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-17.78" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-20.32" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="20.32" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="17.78" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="15.24" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="10.16" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="7.62" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-7.62" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-10.16" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-12.7" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-15.24" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-17.78" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-20.32" radius="0.635" width="0.254" layer="94"/>
<pin name="34" x="-7.62" y="20.32" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="32" x="-7.62" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="30" x="-7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="28" x="-7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="26" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="24" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="22" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="20" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-7.62" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="7.62" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="19" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="21" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="23" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="25" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="27" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="29" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="31" x="7.62" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="33" x="7.62" y="20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-5.08" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOX_HEADER">
<gates>
<gate name="G$1" symbol="BOX_HEADER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BOX_HEADER">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="10" pad="P$10"/>
<connect gate="G$1" pin="11" pad="P$11"/>
<connect gate="G$1" pin="12" pad="P$12"/>
<connect gate="G$1" pin="13" pad="P$13"/>
<connect gate="G$1" pin="14" pad="P$14"/>
<connect gate="G$1" pin="15" pad="P$15"/>
<connect gate="G$1" pin="16" pad="P$16"/>
<connect gate="G$1" pin="17" pad="P$17"/>
<connect gate="G$1" pin="18" pad="P$18"/>
<connect gate="G$1" pin="19" pad="P$19"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="20" pad="P$20"/>
<connect gate="G$1" pin="21" pad="P$21"/>
<connect gate="G$1" pin="22" pad="P$22"/>
<connect gate="G$1" pin="23" pad="P$23"/>
<connect gate="G$1" pin="24" pad="P$24"/>
<connect gate="G$1" pin="25" pad="P$25"/>
<connect gate="G$1" pin="26" pad="P$26"/>
<connect gate="G$1" pin="27" pad="P$27"/>
<connect gate="G$1" pin="28" pad="P$28"/>
<connect gate="G$1" pin="29" pad="P$29"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="30" pad="P$30"/>
<connect gate="G$1" pin="31" pad="P$31"/>
<connect gate="G$1" pin="32" pad="P$32"/>
<connect gate="G$1" pin="33" pad="P$33"/>
<connect gate="G$1" pin="34" pad="P$34"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
<connect gate="G$1" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="1.016" y1="1.016" x2="2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="-1.016" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="3.175" y1="0" x2="3.3528" y2="0" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="-1.016" x2="-2.7686" y2="1.016" width="0.127" layer="21"/>
<wire x1="-2.7686" y1="1.016" x2="-1.016" y2="1.016" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="-0.368296875" x2="-3.48741875" y2="0.3556" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<wire x1="-3.489959375" y1="0.37591875" x2="-3.48741875" y2="0.373378125" width="0.254" layer="21"/>
<wire x1="-3.48741875" y1="0.373378125" x2="-3.48741875" y2="-0.370840625" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PT12-21BTR8">
<packages>
<package name="PT12-21B/TR8">
<smd name="C" x="1.4" y="-1.65" dx="1" dy="1" layer="1"/>
<smd name="E" x="-1.4" y="-1.65" dx="1" dy="1" layer="1" rot="R90"/>
<wire x1="-1" y1="-0.923" x2="1" y2="-0.923" width="0.05" layer="21" curve="-180"/>
<wire x1="-2.067" y1="-1.14" x2="-2.067" y2="-2.287" width="0.05" layer="21"/>
<wire x1="-2.067" y1="-2.317" x2="2.067" y2="-2.317" width="0.05" layer="21"/>
<wire x1="2.067" y1="-2.317" x2="2.067" y2="-0.983" width="0.05" layer="21"/>
<wire x1="2.067" y1="-0.983" x2="-2.067" y2="-0.983" width="0.05" layer="21"/>
<wire x1="-2.067" y1="-0.983" x2="-2.067" y2="-1.12" width="0.05" layer="21"/>
<wire x1="0" y1="1.016" x2="0" y2="-4.265" width="0.05" layer="21"/>
<text x="-1.64" y="-3.06" size="0.6096" layer="21">&gt;NAME</text>
<wire x1="-3.302" y1="-2.413" x2="2.159" y2="-2.413" width="0.127" layer="21"/>
<wire x1="2.159" y1="-2.413" x2="3.556" y2="-2.413" width="0.127" layer="21"/>
<wire x1="2.159" y1="-2.413" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PT12-21B/TR8">
<circle x="1.27" y="0" radius="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="0.254" y1="2.032" x2="0.254" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="1.524" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-3.048" y1="1.778" x2="-1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.778" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-2.286" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.048" y1="-0.254" x2="-1.778" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.27" x2="-1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.27" x2="-2.286" y2="-1.27" width="0.254" layer="94"/>
<pin name="C" x="2.54" y="4.826" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="E" x="2.54" y="-4.826" visible="pad" length="short" direction="pas" rot="R90"/>
<text x="-7.62" y="-5.08" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PT12-21B/TR8" prefix="PT">
<gates>
<gate name="G$1" symbol="PT12-21B/TR8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PT12-21B/TR8">
<connects>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIYSMD17">
<packages>
<package name="DIYSMD17(COMBINED)">
<smd name="P$1" x="0" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$2" x="2.54" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$3" x="5.08" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$4" x="7.62" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$5" x="10.16" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$6" x="12.7" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$7" x="15.24" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$8" x="17.78" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$9" x="20.32" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$10" x="22.86" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$11" x="25.4" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$12" x="27.94" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$13" x="30.48" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$14" x="33.02" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$15" x="35.56" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$16" x="38.1" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<smd name="P$17" x="40.64" y="0" dx="1.6002" dy="1.6002" layer="1" roundness="100"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="19.05" y2="-2.54" width="0.127" layer="21"/>
<wire x1="19.05" y1="-2.54" x2="41.91" y2="-2.54" width="0.127" layer="21"/>
<wire x1="41.91" y1="-2.54" x2="41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="41.91" y1="2.54" x2="19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.05" y1="2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.05" y1="2.54" x2="19.05" y2="-2.54" width="0.127" layer="21"/>
<text x="26.67" y="-2.54" size="1.27" layer="21">PWM</text>
<text x="6.35" y="-2.54" size="1.27" layer="21">DIGITAL</text>
<wire x1="17" y1="-2" x2="21" y2="-2" width="1.27" layer="21"/>
<wire x1="17" y1="2" x2="21" y2="2" width="1.27" layer="21"/>
<text x="0" y="3.81" size="1.27" layer="21">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="DIYSMD17">
<wire x1="0" y1="0" x2="0" y2="-45.72" width="0.254" layer="94"/>
<wire x1="0" y1="-45.72" x2="7.62" y2="-45.72" width="0.254" layer="94"/>
<wire x1="7.62" y1="-45.72" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="12.7" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="P$2" x="12.7" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="P$3" x="12.7" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="P$4" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="P$5" x="12.7" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="P$6" x="12.7" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="P$7" x="12.7" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="P$8" x="12.7" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="P$9" x="12.7" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="P$10" x="12.7" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="P$11" x="12.7" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="P$12" x="12.7" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="P$13" x="12.7" y="-33.02" length="middle" direction="pas" rot="R180"/>
<pin name="P$14" x="12.7" y="-35.56" length="middle" direction="pas" rot="R180"/>
<pin name="P$15" x="12.7" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="P$16" x="12.7" y="-40.64" length="middle" direction="pas" rot="R180"/>
<pin name="P$17" x="12.7" y="-43.18" length="middle" direction="pas" rot="R180"/>
<text x="2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIYSMD17(COMBINED)" prefix="BUSS_RES">
<gates>
<gate name="G$1" symbol="DIYSMD17" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIYSMD17(COMBINED)">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$17" pad="P$17"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIY_SMT_DUALROW_04">
<packages>
<package name="DIY_SMT_DUALROW_4">
<smd name="P$1" x="0" y="2.2225" dx="1.27" dy="3.683" layer="1" rot="R180"/>
<smd name="P$2" x="2.54" y="2.2225" dx="1.27" dy="3.683" layer="1" rot="R180"/>
<smd name="P$3" x="5.08" y="2.2225" dx="1.27" dy="3.683" layer="1"/>
<smd name="P$4" x="7.62" y="2.2225" dx="1.27" dy="3.683" layer="1"/>
<smd name="P$5" x="0" y="-2.2225" dx="1.27" dy="3.683" layer="1"/>
<smd name="P$6" x="2.54" y="-2.2225" dx="1.27" dy="3.683" layer="1"/>
<smd name="P$7" x="5.08" y="-2.2225" dx="1.27" dy="3.683" layer="1"/>
<smd name="P$8" x="7.62" y="-2.2225" dx="1.27" dy="3.683" layer="1"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="8.89" y2="-7.62" width="0.127" layer="21"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="7.62" width="0.127" layer="21"/>
<wire x1="8.89" y1="7.62" x2="-1.27" y2="7.62" width="0.127" layer="21"/>
<text x="0" y="-8.89" size="1.27" layer="21">&gt;NAME</text>
<wire x1="-0.762" y1="4.826" x2="0.762" y2="4.826" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIY_SMT_DUALROW_04">
<wire x1="0" y1="0" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-5.08" y="-2.54" length="middle"/>
<pin name="P$2" x="-5.08" y="-5.08" length="middle"/>
<pin name="P$3" x="-5.08" y="-7.62" length="middle"/>
<pin name="P$4" x="-5.08" y="-10.16" length="middle"/>
<pin name="P$5" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="P$6" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="P$7" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="P$8" x="20.32" y="-10.16" length="middle" rot="R180"/>
<text x="0" y="2.54" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIY_SMT_DUALROW_04">
<gates>
<gate name="G$1" symbol="DIY_SMT_DUALROW_04" x="-7.62" y="7.62"/>
</gates>
<devices>
<device name="" package="DIY_SMT_DUALROW_4">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIY_SMT_02">
<packages>
<package name="DIY_SMT_02">
<smd name="P$1" x="-1.27" y="2.825" dx="1.85" dy="1.02" layer="1" rot="R90"/>
<smd name="P$2" x="1.27" y="2.825" dx="1.85" dy="1.02" layer="1" rot="R90"/>
<smd name="P$3" x="-1.27" y="-2.825" dx="1.85" dy="1.02" layer="1" rot="R90"/>
<smd name="P$4" x="1.27" y="-2.825" dx="1.85" dy="1.02" layer="1" rot="R90"/>
<wire x1="-3" y1="6.016" x2="-3" y2="-6.016" width="0.127" layer="21"/>
<wire x1="-3" y1="-6.016" x2="3" y2="-6.016" width="0.127" layer="21"/>
<wire x1="3" y1="-6.016" x2="3" y2="6.016" width="0.127" layer="21"/>
<wire x1="3" y1="6.016" x2="-3" y2="6.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="4.318" x2="-0.508" y2="4.318" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIY_SMT_02">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="P$1" x="-2.54" y="12.7" length="middle" rot="R270"/>
<pin name="P$2" x="2.54" y="12.7" length="middle" rot="R270"/>
<pin name="P$3" x="-2.54" y="-12.7" length="middle" rot="R90"/>
<pin name="P$4" x="2.54" y="-12.7" length="middle" rot="R90"/>
<text x="7.62" y="2.54" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIY_SMT_02">
<gates>
<gate name="G$1" symbol="DIY_SMT_02" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIY_SMT_02">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microusb">
<packages>
<package name="MICRO-USB">
<wire x1="-3.75" y1="0" x2="-3.75" y2="5" width="0.127" layer="21"/>
<wire x1="3.75" y1="5" x2="3.75" y2="0" width="0.127" layer="21"/>
<smd name="5" x="1.3" y="4.525" dx="1.75" dy="0.4" layer="1" rot="R90"/>
<smd name="1" x="-1.3" y="4.525" dx="1.75" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="0.65" y="4.525" dx="1.75" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="0" y="4.525" dx="1.75" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="-0.65" y="4.525" dx="1.75" dy="0.4" layer="1" rot="R90"/>
<text x="-5.2" y="0.5" size="0.762" layer="25" rot="R90">&gt;NAME</text>
<text x="5.2" y="0.5" size="0.762" layer="27" rot="R90" align="top-left">&gt;VALUE</text>
<pad name="GND1" x="-3.725" y="1.7" drill="1.2" diameter="2.54" shape="square"/>
<pad name="GND2" x="3.725" y="1.7" drill="1.2" diameter="2.54" shape="square"/>
<hole x="-2.425" y="4.73" drill="1.05"/>
<hole x="2.425" y="4.73" drill="1.05"/>
<wire x1="-3.75" y1="5" x2="-3.25" y2="5" width="0.127" layer="21"/>
<wire x1="3.75" y1="5" x2="3.25" y2="5" width="0.127" layer="21"/>
</package>
<package name="MICRO-USB-2">
<rectangle x1="-3.675" y1="-0.5" x2="3.675" y2="4.5" layer="46"/>
<pad name="P$1" x="-4.85" y="5.05" drill="0.6" diameter="1.4" shape="long" rot="R90"/>
<smd name="1" x="-1.3" y="5.275" dx="0.4" dy="1.05" layer="1"/>
<smd name="2" x="-0.65" y="5.275" dx="0.4" dy="1.05" layer="1"/>
<smd name="3" x="0" y="5.275" dx="0.4" dy="1.05" layer="1"/>
<smd name="4" x="0.65" y="5.275" dx="0.4" dy="1.05" layer="1"/>
<smd name="5" x="1.3" y="5.275" dx="0.4" dy="1.05" layer="1"/>
<pad name="P$3" x="4.85" y="5.05" drill="0.6" diameter="1.4" shape="long" rot="R90"/>
<pad name="P$2" x="-4.85" y="1.55" drill="0.6" diameter="1.4" shape="long" rot="R90"/>
<pad name="P$4" x="4.85" y="1.55" drill="0.6" diameter="1.4" shape="long" rot="R90"/>
<rectangle x1="4.55" y1="4.2" x2="5.15" y2="5.9" layer="46"/>
<rectangle x1="-5.15" y1="4.2" x2="-4.55" y2="5.9" layer="46"/>
<rectangle x1="-5.15" y1="0.7" x2="-4.55" y2="2.4" layer="46"/>
<rectangle x1="4.55" y1="0.7" x2="5.15" y2="2.4" layer="46"/>
<wire x1="-4" y1="0" x2="4" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MICRO-USB">
<description>Micro USB connector</description>
<wire x1="12.7" y1="0" x2="12.7" y2="-15.24" width="0.508" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="0" y2="-15.24" width="0.508" layer="94"/>
<wire x1="0" y1="-15.24" x2="0" y2="0" width="0.508" layer="94"/>
<wire x1="0" y1="0" x2="12.7" y2="0" width="0.508" layer="94"/>
<text x="0.254" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="0.254" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="15.24" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="D-" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="D+" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="ID" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="GND" x="15.24" y="-12.7" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MICRO-USB">
<description>Micro USB B connector</description>
<gates>
<gate name="G$1" symbol="MICRO-USB" x="0" y="15.24"/>
</gates>
<devices>
<device name="" package="MICRO-USB">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5 GND1 GND2"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V2" package="MICRO-USB-2">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5 P$1 P$2 P$3 P$4"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="EIA3216">
<description>Generic EIA 3216 (1206) polarized tantalum capacitor</description>
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="51"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="51"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.2032" layer="21"/>
<smd name="-" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="+" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="0603-POLAR">
<description>&lt;p&gt;&lt;b&gt;Polarized 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.1" y1="-0.8" x2="-1.7" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="-1.1" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-0.8" x2="1.5" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-0.8" x2="1.9" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="-0.4" x2="1.9" y2="0.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="0.4" x2="1.5" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.5" y1="0.8" x2="1.1" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="-" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="+" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="1.651" y1="0.508" x2="1.651" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="EIA3528">
<description>Generic EIA 3528 polarized tantalum capacitor</description>
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="51"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="51"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="51"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="51"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="51"/>
<wire x1="2.641" y1="1.311" x2="2.641" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="0" y="1.778" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="CPOL-RADIAL-2.5MM-5MM">
<description>2.5 mm spaced PTHs with 5 mm diameter outline and standard solder mask</description>
<pad name="1" x="1.25" y="0" drill="0.7" diameter="1.651" shape="square"/>
<pad name="2" x="-1.25" y="0" drill="0.7" diameter="1.651"/>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-0.742" y1="1.397" x2="-1.758" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.758" y1="1.397" x2="0.742" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.25" y1="1.905" x2="1.25" y2="0.889" width="0.2032" layer="21"/>
</package>
<package name="CPOL-RADIAL-2.5MM-5MM-KIT">
<description>2.5 mm spaced PTHs with top copper masked</description>
<circle x="0" y="0" radius="2.5" width="0.2032" layer="21"/>
<pad name="1" x="1.25" y="0" drill="0.7" diameter="1.651" shape="square" stop="no"/>
<pad name="2" x="-1.25" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.25" y="0" radius="0.3556" width="0" layer="29"/>
<circle x="-1.25" y="0" radius="0.9652" width="0" layer="30"/>
<circle x="1.25" y="0" radius="0.3556" width="0" layer="29"/>
<rectangle x1="0.2848" y1="-0.9652" x2="2.2152" y2="0.9652" layer="30"/>
<wire x1="-0.742" y1="1.397" x2="-1.758" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.758" y1="1.397" x2="0.742" y2="1.397" width="0.2032" layer="21"/>
<wire x1="1.25" y1="1.905" x2="1.25" y2="0.889" width="0.2032" layer="21"/>
</package>
<package name="EIA6032-NOM">
<description>Metric Size Code EIA 6032-25 Median (Nominal) Land Protrusion&lt;br /&gt;
http://www.kemet.com/Lists/ProductCatalog/Attachments/254/KEM_T2005_T491.pdf</description>
<wire x1="-3.91" y1="1.5" x2="-2" y2="1.5" width="0.127" layer="51"/>
<wire x1="-3.91" y1="1.5" x2="-3.91" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.91" y1="-1.5" x2="-2" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2" y1="1.5" x2="3.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="1.5" x2="3.91" y2="1" width="0.127" layer="51"/>
<wire x1="3.91" y1="1" x2="3.91" y2="-1" width="0.127" layer="51"/>
<wire x1="3.91" y1="-1" x2="3.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-1.5" x2="2" y2="-1.5" width="0.127" layer="51"/>
<smd name="C" x="-2.47" y="0" dx="2.37" dy="2.23" layer="1" rot="R180"/>
<smd name="A" x="2.47" y="0" dx="2.37" dy="2.23" layer="1" rot="R180"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="3.91" y1="1" x2="3.91" y2="-1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10UF-POLAR" prefix="C">
<description>&lt;h3&gt;10.0µF polarized capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="-EIA3216-16V-10%(TANT)" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00811"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-0603-6.3V-20%(TANT)" package="0603-POLAR">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13210"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-EIA3528-20V-10%(TANT)" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08063"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-RADIAL-2.5MM-25V-20%" package="CPOL-RADIAL-2.5MM-5MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08440"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-RADIAL-2.5MM-KIT-25V-20%" package="CPOL-RADIAL-2.5MM-5MM-KIT">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08440"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-EIA6032-25V-10%" package="EIA6032-NOM">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12984"/>
<attribute name="VALUE" value="10µF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D1" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D2" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R1" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R2" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q1" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="D3" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D4" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R3" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R4" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q2" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="D5" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D6" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R5" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R6" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q3" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="D7" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D8" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R7" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R8" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q4" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="D9" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D10" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R9" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R10" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q5" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="D11" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D12" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R11" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R12" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q6" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="D13" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="D14" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-YELLOW(0603)" device="" value=""/>
<part name="R13" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="R14" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q7" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="R15" library="Seeed-OPL-Resistor" deviceset="SMD-RES-150R-1%-1/10W(0603)" device="" value="150R"/>
<part name="Q8" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="D16" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D17" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R16" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R17" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q9" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="D18" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D19" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R18" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R19" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q10" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="D20" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D21" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R20" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R21" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q11" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="D22" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D23" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R22" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R23" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q12" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="D24" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D25" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R24" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R25" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q13" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="D26" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D27" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R26" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R27" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q14" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="D28" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="D29" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-BLUE(0603)" device="" value=""/>
<part name="R28" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="R29" library="Seeed-OPL-Resistor" deviceset="SMD-RES-51R-1%-1/10W(0603)" device="" value="51R"/>
<part name="Q15" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="R54" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R55" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R56" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R57" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R58" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R59" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R60" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R61" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R62" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R63" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R64" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R65" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R66" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R67" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R68" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R30" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R31" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R32" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R33" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="R34" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R35" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="R36" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R37" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="R38" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R39" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="R40" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R41" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="R42" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R43" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="R44" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="R45" library="Seeed-OPL-Resistor" deviceset="SMD-RES-56.2R-1%-1/10W(0603)" device="" value="56.2R"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="IR1" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR2" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR3" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR4" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR5" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR6" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR7" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR8" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR9" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR10" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR11" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR12" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR13" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR14" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR15" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="IR16" library="SFH4050N" deviceset="SFH4045N" device="SFH4050N_SMD"/>
<part name="R46" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="R47" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="R48" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="R49" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="R50" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="R51" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="R52" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="R53" library="Seeed-OPL-Resistor" deviceset="SMD-RES-470R-1%-1/16W(0402)" device="" value="470R"/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="CON1" library="box_header" deviceset="BOX_HEADER" device=""/>
<part name="R75" library="Seeed-OPL-Resistor" deviceset="SMD-RES-1.5K-1%-1/8W(0805)" device="" value="1.5K"/>
<part name="R76" library="Seeed-OPL-Resistor" deviceset="SMD-RES-1.5K-1%-1/8W(0805)" device="" value="1.5K"/>
<part name="Q16" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="R69" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="R73" library="Seeed-OPL-Resistor" deviceset="SMD-RES-4.7K-5%-1/8W(0805)" device="" value="4.7K"/>
<part name="R74" library="Seeed-OPL-Resistor" deviceset="SMD-RES-2.2K-5%-1/8W(0805)" device="" value="2.2K"/>
<part name="D30" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-RED(0805)" device="" value=""/>
<part name="D31" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-RED(0805)" device="" value=""/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="P+30" library="supply1" deviceset="+5V" device=""/>
<part name="D15" library="SparkFun-LED" deviceset="LED" device="1206"/>
<part name="PT1" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT2" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT3" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT4" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT5" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT6" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT7" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT8" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT9" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT10" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT11" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT12" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT13" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT14" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT15" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="PT16" library="PT12-21BTR8" deviceset="PT12-21B/TR8" device=""/>
<part name="Q17" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="R70" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="Q18" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="R71" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="Q19" library="Seeed-OPL-Transistor" deviceset="SMD-MOSFET-N-CH-20V-2.1A(SOT-23)" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="R72" library="Seeed-OPL-Resistor" deviceset="SMD-RES-22R-5%-1/8W(0805)" device="" value="22R"/>
<part name="BUSS_RES1" library="DIYSMD17" deviceset="DIYSMD17(COMBINED)" device=""/>
<part name="IR17" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-RED(0805)" device="" value=""/>
<part name="IR18" library="Seeed-OPL-Diode" deviceset="SMD-LED-CLEAR-RED(0805)" device="" value=""/>
<part name="CON2" library="DIY_SMT_DUALROW_04" deviceset="DIY_SMT_DUALROW_04" device=""/>
<part name="CON3" library="DIY_SMT_DUALROW_04" deviceset="DIY_SMT_DUALROW_04" device=""/>
<part name="CON5" library="DIY_SMT_02" deviceset="DIY_SMT_02" device=""/>
<part name="CON6" library="DIY_SMT_02" deviceset="DIY_SMT_02" device=""/>
<part name="CON4" library="DIY_SMT_02" deviceset="DIY_SMT_02" device=""/>
<part name="U$4" library="microusb" deviceset="MICRO-USB" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="10UF-POLAR" device="-EIA3216-16V-10%(TANT)" value="10uF"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D1" gate="G$1" x="20.32" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="21.59" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="17.78" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D2" gate="G$1" x="30.48" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="31.75" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="27.94" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R1" gate="G$1" x="20.32" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="19.05" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="22.86" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="30.48" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="33.02" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q1" gate="G$1" x="30.48" y="0" smashed="yes">
<attribute name="NAME" x="26.67" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="30.48" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND1" gate="1" x="30.48" y="-12.7" smashed="yes">
<attribute name="VALUE" x="27.94" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="76.2" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="77.47" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="73.66" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D4" gate="G$1" x="86.36" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="87.63" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="83.82" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R3" gate="G$1" x="76.2" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="78.74" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="86.36" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="88.9" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q2" gate="G$1" x="86.36" y="0" smashed="yes">
<attribute name="NAME" x="82.55" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="86.36" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND2" gate="1" x="86.36" y="-12.7" smashed="yes">
<attribute name="VALUE" x="83.82" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="132.08" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="133.35" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="129.54" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D6" gate="G$1" x="142.24" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="143.51" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="139.7" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R5" gate="G$1" x="132.08" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="130.81" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="134.62" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="142.24" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="140.97" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="144.78" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q3" gate="G$1" x="142.24" y="0" smashed="yes">
<attribute name="NAME" x="138.43" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="142.24" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND3" gate="1" x="142.24" y="-12.7" smashed="yes">
<attribute name="VALUE" x="139.7" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D7" gate="G$1" x="187.96" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="189.23" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="185.42" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D8" gate="G$1" x="198.12" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="199.39" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="195.58" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R7" gate="G$1" x="187.96" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="186.69" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="190.5" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="198.12" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="196.85" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.66" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q4" gate="G$1" x="198.12" y="0" smashed="yes">
<attribute name="NAME" x="194.31" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="198.12" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND4" gate="1" x="198.12" y="-12.7" smashed="yes">
<attribute name="VALUE" x="195.58" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="243.84" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="245.11" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="241.3" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D10" gate="G$1" x="254" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="255.27" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="251.46" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R9" gate="G$1" x="243.84" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="242.57" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="246.38" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R10" gate="G$1" x="254" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="252.73" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="256.54" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q5" gate="G$1" x="254" y="0" smashed="yes">
<attribute name="NAME" x="250.19" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="254" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND5" gate="1" x="254" y="-12.7" smashed="yes">
<attribute name="VALUE" x="251.46" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D11" gate="G$1" x="299.72" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="300.99" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="297.18" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D12" gate="G$1" x="309.88" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="311.15" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="307.34" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R11" gate="G$1" x="299.72" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="298.45" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="302.26" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="309.88" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="308.61" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="312.42" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q6" gate="G$1" x="309.88" y="0" smashed="yes">
<attribute name="NAME" x="306.07" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="309.88" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND6" gate="1" x="309.88" y="-12.7" smashed="yes">
<attribute name="VALUE" x="307.34" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D13" gate="G$1" x="355.6" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="356.87" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="353.06" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D14" gate="G$1" x="365.76" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="367.03" y="35.56" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="363.22" y="35.56" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R13" gate="G$1" x="355.6" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="354.33" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="358.14" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="365.76" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="364.49" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="368.3" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q7" gate="G$1" x="365.76" y="0" smashed="yes">
<attribute name="NAME" x="361.95" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="365.76" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND7" gate="1" x="365.76" y="-12.7" smashed="yes">
<attribute name="VALUE" x="363.22" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="411.48" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="410.21" y="13.97" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="414.02" y="13.97" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q8" gate="G$1" x="421.64" y="0" smashed="yes">
<attribute name="NAME" x="417.83" y="2.54" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="421.64" y="2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND8" gate="1" x="421.64" y="-12.7" smashed="yes">
<attribute name="VALUE" x="419.1" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="D16" gate="G$1" x="20.32" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="21.59" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="17.78" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D17" gate="G$1" x="30.48" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="31.75" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="27.94" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R16" gate="G$1" x="20.32" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="19.05" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="22.86" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R17" gate="G$1" x="30.48" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="33.02" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q9" gate="G$1" x="30.48" y="-81.28" smashed="yes">
<attribute name="NAME" x="26.67" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="30.48" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND10" gate="1" x="30.48" y="-93.98" smashed="yes">
<attribute name="VALUE" x="27.94" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D18" gate="G$1" x="76.2" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="77.47" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="73.66" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D19" gate="G$1" x="86.36" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="87.63" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="83.82" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R18" gate="G$1" x="76.2" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="78.74" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="86.36" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="88.9" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q10" gate="G$1" x="86.36" y="-81.28" smashed="yes">
<attribute name="NAME" x="82.55" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="86.36" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND11" gate="1" x="86.36" y="-93.98" smashed="yes">
<attribute name="VALUE" x="83.82" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D20" gate="G$1" x="132.08" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="133.35" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="129.54" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D21" gate="G$1" x="142.24" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="143.51" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="139.7" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R20" gate="G$1" x="132.08" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="130.81" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="134.62" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R21" gate="G$1" x="142.24" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="140.97" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="144.78" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q11" gate="G$1" x="142.24" y="-81.28" smashed="yes">
<attribute name="NAME" x="138.43" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="142.24" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND12" gate="1" x="142.24" y="-93.98" smashed="yes">
<attribute name="VALUE" x="139.7" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D22" gate="G$1" x="187.96" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="189.23" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="185.42" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D23" gate="G$1" x="198.12" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="199.39" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="195.58" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R22" gate="G$1" x="187.96" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="186.69" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="190.5" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R23" gate="G$1" x="198.12" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="196.85" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.66" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q12" gate="G$1" x="198.12" y="-81.28" smashed="yes">
<attribute name="NAME" x="194.31" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="198.12" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND13" gate="1" x="198.12" y="-93.98" smashed="yes">
<attribute name="VALUE" x="195.58" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D24" gate="G$1" x="243.84" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="245.11" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="241.3" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D25" gate="G$1" x="254" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="255.27" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="251.46" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R24" gate="G$1" x="243.84" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="242.57" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="246.38" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R25" gate="G$1" x="254" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="252.73" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="256.54" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q13" gate="G$1" x="254" y="-81.28" smashed="yes">
<attribute name="NAME" x="250.19" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="254" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND14" gate="1" x="254" y="-93.98" smashed="yes">
<attribute name="VALUE" x="251.46" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D26" gate="G$1" x="299.72" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="300.99" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="297.18" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D27" gate="G$1" x="309.88" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="311.15" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="307.34" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R26" gate="G$1" x="299.72" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="298.45" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="302.26" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="309.88" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="308.61" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="312.42" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q14" gate="G$1" x="309.88" y="-81.28" smashed="yes">
<attribute name="NAME" x="306.07" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="309.88" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND15" gate="1" x="309.88" y="-93.98" smashed="yes">
<attribute name="VALUE" x="307.34" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="D28" gate="G$1" x="355.6" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="356.87" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="353.06" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D29" gate="G$1" x="365.76" y="-38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="367.03" y="-33.02" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="363.22" y="-33.02" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R28" gate="G$1" x="355.6" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="354.33" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="358.14" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="365.76" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="364.49" y="-54.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="368.3" y="-54.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="Q15" gate="G$1" x="365.76" y="-81.28" smashed="yes">
<attribute name="NAME" x="361.95" y="-78.74" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="365.76" y="-78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND16" gate="1" x="365.76" y="-93.98" smashed="yes">
<attribute name="VALUE" x="363.22" y="-96.52" size="1.778" layer="96"/>
</instance>
<instance part="R54" gate="G$1" x="20.32" y="0" smashed="yes">
<attribute name="NAME" x="16.51" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="16.51" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R55" gate="G$1" x="76.2" y="0" smashed="yes">
<attribute name="NAME" x="72.39" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="72.39" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R56" gate="G$1" x="132.08" y="0" smashed="yes">
<attribute name="NAME" x="128.27" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="128.27" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R57" gate="G$1" x="187.96" y="0" smashed="yes">
<attribute name="NAME" x="184.15" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="184.15" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R58" gate="G$1" x="243.84" y="0" smashed="yes">
<attribute name="NAME" x="240.03" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="240.03" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R59" gate="G$1" x="299.72" y="0" smashed="yes">
<attribute name="NAME" x="295.91" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="295.91" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R60" gate="G$1" x="355.6" y="0" smashed="yes">
<attribute name="NAME" x="351.79" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="351.79" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R61" gate="G$1" x="411.48" y="0" smashed="yes">
<attribute name="NAME" x="407.67" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="407.67" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R62" gate="G$1" x="20.32" y="-81.28" smashed="yes">
<attribute name="NAME" x="16.51" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="16.51" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R63" gate="G$1" x="76.2" y="-81.28" smashed="yes">
<attribute name="NAME" x="72.39" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="72.39" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R64" gate="G$1" x="132.08" y="-81.28" smashed="yes">
<attribute name="NAME" x="128.27" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="128.27" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R65" gate="G$1" x="187.96" y="-81.28" smashed="yes">
<attribute name="NAME" x="184.15" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="184.15" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R66" gate="G$1" x="243.84" y="-81.28" smashed="yes">
<attribute name="NAME" x="240.03" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="240.03" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R67" gate="G$1" x="299.72" y="-81.28" smashed="yes">
<attribute name="NAME" x="295.91" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="295.91" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R68" gate="G$1" x="355.6" y="-81.28" smashed="yes">
<attribute name="NAME" x="351.79" y="-80.01" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="351.79" y="-83.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R30" gate="G$1" x="22.86" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="21.59" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="25.4" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="38.1" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.64" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="30.48" y="-149.86" smashed="yes">
<attribute name="VALUE" x="27.94" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R32" gate="G$1" x="78.74" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="77.47" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="81.28" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R33" gate="G$1" x="93.98" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="92.71" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="96.52" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="86.36" y="-149.86" smashed="yes">
<attribute name="VALUE" x="83.82" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="134.62" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="133.35" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="137.16" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R35" gate="G$1" x="149.86" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="148.59" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="152.4" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND21" gate="1" x="142.24" y="-149.86" smashed="yes">
<attribute name="VALUE" x="139.7" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="190.5" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="189.23" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="193.04" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R37" gate="G$1" x="205.74" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="204.47" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="208.28" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="198.12" y="-149.86" smashed="yes">
<attribute name="VALUE" x="195.58" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="246.38" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="245.11" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="248.92" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R39" gate="G$1" x="261.62" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="260.35" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="264.16" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND23" gate="1" x="254" y="-149.86" smashed="yes">
<attribute name="VALUE" x="251.46" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="302.26" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="300.99" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="304.8" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R41" gate="G$1" x="317.5" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="316.23" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="320.04" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND24" gate="1" x="309.88" y="-149.86" smashed="yes">
<attribute name="VALUE" x="307.34" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R42" gate="G$1" x="358.14" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="356.87" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="360.68" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R43" gate="G$1" x="373.38" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="372.11" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="375.92" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND25" gate="1" x="365.76" y="-149.86" smashed="yes">
<attribute name="VALUE" x="363.22" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="R44" gate="G$1" x="414.02" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="412.75" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="416.56" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R45" gate="G$1" x="429.26" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="427.99" y="-135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="431.8" y="-135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="421.64" y="-149.86" smashed="yes">
<attribute name="VALUE" x="419.1" y="-152.4" size="1.778" layer="96"/>
</instance>
<instance part="IR1" gate="G$1" x="22.86" y="-121.92" smashed="yes">
<attribute name="NAME" x="27.94" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR2" gate="G$1" x="38.1" y="-121.92" smashed="yes">
<attribute name="NAME" x="43.18" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR3" gate="G$1" x="78.74" y="-121.92" smashed="yes">
<attribute name="NAME" x="83.82" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR4" gate="G$1" x="93.98" y="-121.92" smashed="yes">
<attribute name="NAME" x="99.06" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR5" gate="G$1" x="134.62" y="-121.92" smashed="yes">
<attribute name="NAME" x="139.7" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR6" gate="G$1" x="149.86" y="-121.92" smashed="yes">
<attribute name="NAME" x="154.94" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR7" gate="G$1" x="190.5" y="-121.92" smashed="yes">
<attribute name="NAME" x="195.58" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR8" gate="G$1" x="205.74" y="-121.92" smashed="yes">
<attribute name="NAME" x="210.82" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR9" gate="G$1" x="246.38" y="-121.92" smashed="yes">
<attribute name="NAME" x="251.46" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR10" gate="G$1" x="261.62" y="-121.92" smashed="yes">
<attribute name="NAME" x="266.7" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR11" gate="G$1" x="302.26" y="-121.92" smashed="yes">
<attribute name="NAME" x="307.34" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR12" gate="G$1" x="317.5" y="-121.92" smashed="yes">
<attribute name="NAME" x="322.58" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR13" gate="G$1" x="358.14" y="-121.92" smashed="yes">
<attribute name="NAME" x="363.22" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR14" gate="G$1" x="373.38" y="-121.92" smashed="yes">
<attribute name="NAME" x="378.46" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR15" gate="G$1" x="414.02" y="-121.92" smashed="yes">
<attribute name="NAME" x="419.1" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="IR16" gate="G$1" x="429.26" y="-121.92" smashed="yes">
<attribute name="NAME" x="434.34" y="-124.46" size="1.27" layer="95"/>
</instance>
<instance part="R46" gate="G$1" x="43.18" y="-215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="41.91" y="-219.71" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="45.72" y="-219.71" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND28" gate="1" x="43.18" y="-226.06" smashed="yes">
<attribute name="VALUE" x="40.64" y="-228.6" size="1.778" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="96.52" y="-218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="95.25" y="-222.25" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="99.06" y="-222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND29" gate="1" x="96.52" y="-228.6" smashed="yes">
<attribute name="VALUE" x="93.98" y="-231.14" size="1.778" layer="96"/>
</instance>
<instance part="R48" gate="G$1" x="154.94" y="-218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="153.67" y="-222.25" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="157.48" y="-222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND30" gate="1" x="154.94" y="-228.6" smashed="yes">
<attribute name="VALUE" x="152.4" y="-231.14" size="1.778" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="208.28" y="-220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="207.01" y="-224.79" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="210.82" y="-224.79" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND31" gate="1" x="208.28" y="-231.14" smashed="yes">
<attribute name="VALUE" x="205.74" y="-233.68" size="1.778" layer="96"/>
</instance>
<instance part="R50" gate="G$1" x="266.7" y="-220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="265.43" y="-224.79" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="269.24" y="-224.79" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND32" gate="1" x="266.7" y="-231.14" smashed="yes">
<attribute name="VALUE" x="264.16" y="-233.68" size="1.778" layer="96"/>
</instance>
<instance part="R51" gate="G$1" x="320.04" y="-223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="318.77" y="-227.33" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="322.58" y="-227.33" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND33" gate="1" x="320.04" y="-233.68" smashed="yes">
<attribute name="VALUE" x="317.5" y="-236.22" size="1.778" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="378.46" y="-223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="377.19" y="-227.33" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="381" y="-227.33" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND34" gate="1" x="378.46" y="-233.68" smashed="yes">
<attribute name="VALUE" x="375.92" y="-236.22" size="1.778" layer="96"/>
</instance>
<instance part="R53" gate="G$1" x="431.8" y="-226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="430.53" y="-229.87" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="434.34" y="-229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND35" gate="1" x="431.8" y="-236.22" smashed="yes">
<attribute name="VALUE" x="429.26" y="-238.76" size="1.778" layer="96"/>
</instance>
<instance part="CON1" gate="G$1" x="-43.18" y="-22.86" smashed="yes">
<attribute name="NAME" x="-48.26" y="0" size="1.778" layer="95"/>
<attribute name="VALUE" x="-48.26" y="-48.26" size="1.778" layer="96"/>
</instance>
<instance part="R75" gate="G$1" x="-114.3" y="-119.38" smashed="yes">
<attribute name="NAME" x="-118.11" y="-118.11" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-118.11" y="-121.92" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R76" gate="G$1" x="-114.3" y="-139.7" smashed="yes">
<attribute name="NAME" x="-118.11" y="-138.43" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-118.11" y="-142.24" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="Q16" gate="G$1" x="-187.96" y="-200.66" smashed="yes">
<attribute name="NAME" x="-191.77" y="-198.12" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-187.96" y="-198.12" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND37" gate="1" x="-187.96" y="-213.36" smashed="yes">
<attribute name="VALUE" x="-190.5" y="-215.9" size="1.778" layer="96"/>
</instance>
<instance part="R69" gate="G$1" x="-198.12" y="-200.66" smashed="yes">
<attribute name="NAME" x="-201.93" y="-199.39" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-201.93" y="-203.2" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R73" gate="G$1" x="-193.04" y="-116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="-191.77" y="-113.03" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="-195.58" y="-113.03" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="R74" gate="G$1" x="-175.26" y="-114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="-176.53" y="-118.11" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="-172.72" y="-118.11" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D30" gate="G$1" x="-193.04" y="-132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="-191.77" y="-127" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="-195.58" y="-127" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D31" gate="G$1" x="-175.26" y="-129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="-173.99" y="-124.46" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="-177.8" y="-124.46" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="GND39" gate="1" x="-193.04" y="-142.24" smashed="yes">
<attribute name="VALUE" x="-195.58" y="-144.78" size="1.778" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="-175.26" y="-142.24" smashed="yes">
<attribute name="VALUE" x="-177.8" y="-144.78" size="1.778" layer="96"/>
</instance>
<instance part="P+30" gate="1" x="-193.04" y="-104.14" smashed="yes">
<attribute name="VALUE" x="-195.58" y="-109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D15" gate="G$1" x="411.48" y="30.48" smashed="yes">
<attribute name="NAME" x="415.036" y="25.908" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="417.195" y="25.908" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PT1" gate="G$1" x="20.32" y="-187.96" smashed="yes">
<attribute name="NAME" x="12.7" y="-193.04" size="1.778" layer="95"/>
</instance>
<instance part="PT2" gate="G$1" x="40.64" y="-187.96" smashed="yes">
<attribute name="NAME" x="33.02" y="-193.04" size="1.778" layer="95"/>
</instance>
<instance part="PT3" gate="G$1" x="73.66" y="-190.5" smashed="yes">
<attribute name="NAME" x="66.04" y="-195.58" size="1.778" layer="95"/>
</instance>
<instance part="PT4" gate="G$1" x="93.98" y="-190.5" smashed="yes">
<attribute name="NAME" x="86.36" y="-195.58" size="1.778" layer="95"/>
</instance>
<instance part="PT5" gate="G$1" x="132.08" y="-190.5" smashed="yes">
<attribute name="NAME" x="124.46" y="-195.58" size="1.778" layer="95"/>
</instance>
<instance part="PT6" gate="G$1" x="152.4" y="-190.5" smashed="yes">
<attribute name="NAME" x="144.78" y="-195.58" size="1.778" layer="95"/>
</instance>
<instance part="PT7" gate="G$1" x="185.42" y="-193.04" smashed="yes">
<attribute name="NAME" x="177.8" y="-198.12" size="1.778" layer="95"/>
</instance>
<instance part="PT8" gate="G$1" x="205.74" y="-193.04" smashed="yes">
<attribute name="NAME" x="198.12" y="-198.12" size="1.778" layer="95"/>
</instance>
<instance part="PT9" gate="G$1" x="243.84" y="-193.04" smashed="yes">
<attribute name="NAME" x="236.22" y="-198.12" size="1.778" layer="95"/>
</instance>
<instance part="PT10" gate="G$1" x="264.16" y="-193.04" smashed="yes">
<attribute name="NAME" x="256.54" y="-198.12" size="1.778" layer="95"/>
</instance>
<instance part="PT11" gate="G$1" x="297.18" y="-195.58" smashed="yes">
<attribute name="NAME" x="289.56" y="-200.66" size="1.778" layer="95"/>
</instance>
<instance part="PT12" gate="G$1" x="317.5" y="-195.58" smashed="yes">
<attribute name="NAME" x="309.88" y="-200.66" size="1.778" layer="95"/>
</instance>
<instance part="PT13" gate="G$1" x="355.6" y="-195.58" smashed="yes">
<attribute name="NAME" x="347.98" y="-200.66" size="1.778" layer="95"/>
</instance>
<instance part="PT14" gate="G$1" x="375.92" y="-195.58" smashed="yes">
<attribute name="NAME" x="368.3" y="-200.66" size="1.778" layer="95"/>
</instance>
<instance part="PT15" gate="G$1" x="408.94" y="-198.12" smashed="yes">
<attribute name="NAME" x="401.32" y="-203.2" size="1.778" layer="95"/>
</instance>
<instance part="PT16" gate="G$1" x="429.26" y="-198.12" smashed="yes">
<attribute name="NAME" x="421.64" y="-203.2" size="1.778" layer="95"/>
</instance>
<instance part="Q17" gate="G$1" x="-190.5" y="-246.38" smashed="yes">
<attribute name="NAME" x="-194.31" y="-243.84" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-190.5" y="-243.84" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND9" gate="1" x="-190.5" y="-259.08" smashed="yes">
<attribute name="VALUE" x="-193.04" y="-261.62" size="1.778" layer="96"/>
</instance>
<instance part="R70" gate="G$1" x="-200.66" y="-246.38" smashed="yes">
<attribute name="NAME" x="-204.47" y="-245.11" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-204.47" y="-248.92" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="Q18" gate="G$1" x="-193.04" y="-294.64" smashed="yes">
<attribute name="NAME" x="-196.85" y="-292.1" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-193.04" y="-292.1" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND17" gate="1" x="-193.04" y="-307.34" smashed="yes">
<attribute name="VALUE" x="-195.58" y="-309.88" size="1.778" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="-203.2" y="-294.64" smashed="yes">
<attribute name="NAME" x="-207.01" y="-293.37" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-207.01" y="-297.18" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="Q19" gate="G$1" x="-195.58" y="-340.36" smashed="yes">
<attribute name="NAME" x="-199.39" y="-337.82" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-195.58" y="-337.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND18" gate="1" x="-195.58" y="-353.06" smashed="yes">
<attribute name="VALUE" x="-198.12" y="-355.6" size="1.778" layer="96"/>
</instance>
<instance part="R72" gate="G$1" x="-205.74" y="-340.36" smashed="yes">
<attribute name="NAME" x="-209.55" y="-339.09" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-209.55" y="-342.9" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="BUSS_RES1" gate="G$1" x="-83.82" y="63.5" smashed="yes">
<attribute name="NAME" x="-81.28" y="66.04" size="1.27" layer="95"/>
</instance>
<instance part="IR17" gate="G$1" x="-109.22" y="-124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="-107.95" y="-119.38" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="-111.76" y="-119.38" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="IR18" gate="G$1" x="-109.22" y="-144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="-107.95" y="-139.7" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="-111.76" y="-139.7" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="CON2" gate="G$1" x="-287.02" y="-81.28" smashed="yes">
<attribute name="NAME" x="-287.02" y="-78.74" size="1.27" layer="95"/>
</instance>
<instance part="CON3" gate="G$1" x="-287.02" y="-106.68" smashed="yes">
<attribute name="NAME" x="-287.02" y="-104.14" size="1.27" layer="95"/>
</instance>
<instance part="CON5" gate="G$1" x="-96.52" y="-86.36" smashed="yes">
<attribute name="NAME" x="-88.9" y="-83.82" size="1.27" layer="95"/>
</instance>
<instance part="CON6" gate="G$1" x="-58.42" y="-86.36" smashed="yes">
<attribute name="NAME" x="-50.8" y="-83.82" size="1.27" layer="95"/>
</instance>
<instance part="CON4" gate="G$1" x="-233.68" y="-22.86" smashed="yes">
<attribute name="NAME" x="-226.06" y="-20.32" size="1.27" layer="95"/>
</instance>
<instance part="U$4" gate="G$1" x="-157.48" y="-15.24" smashed="yes">
<attribute name="NAME" x="-157.226" y="-12.192" size="1.778" layer="95"/>
<attribute name="VALUE" x="-157.48" y="-14.986" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="-137.16" y="-20.32" smashed="yes">
<attribute name="NAME" x="-136.144" y="-19.685" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-136.144" y="-24.511" size="1.778" layer="96" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="-"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="26.67" x2="20.32" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="-"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="30.48" y1="26.67" x2="30.48" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="20.32" y1="15.24" x2="20.32" y2="13.97" width="0.1524" layer="91"/>
<wire x1="20.32" y1="13.97" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
<wire x1="20.32" y1="10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="10.16" x2="30.48" y2="13.97" width="0.1524" layer="91"/>
<wire x1="30.48" y1="13.97" x2="30.48" y2="15.24" width="0.1524" layer="91"/>
<junction x="30.48" y="10.16"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="20.32" y="13.97"/>
<pinref part="R2" gate="G$1" pin="1"/>
<junction x="30.48" y="13.97"/>
<wire x1="30.48" y1="10.16" x2="30.48" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="30.48" y1="-5.08" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="86.36" y1="-5.08" x2="86.36" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="S"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="142.24" y1="-5.08" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="S"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="198.12" y1="-5.08" x2="198.12" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="254" y1="-5.08" x2="254" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q6" gate="G$1" pin="S"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="309.88" y1="-5.08" x2="309.88" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q7" gate="G$1" pin="S"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="365.76" y1="-5.08" x2="365.76" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q8" gate="G$1" pin="S"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="421.64" y1="-5.08" x2="421.64" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q9" gate="G$1" pin="S"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="30.48" y1="-86.36" x2="30.48" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q10" gate="G$1" pin="S"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="86.36" y1="-86.36" x2="86.36" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q11" gate="G$1" pin="S"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="142.24" y1="-86.36" x2="142.24" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q12" gate="G$1" pin="S"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="198.12" y1="-86.36" x2="198.12" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q13" gate="G$1" pin="S"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="254" y1="-86.36" x2="254" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q14" gate="G$1" pin="S"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="309.88" y1="-86.36" x2="309.88" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q15" gate="G$1" pin="S"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="365.76" y1="-86.36" x2="365.76" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="22.86" y1="-134.62" x2="22.86" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-135.89" x2="22.86" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-142.24" x2="30.48" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-142.24" x2="38.1" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-142.24" x2="38.1" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<junction x="22.86" y="-135.89"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-135.89" x2="38.1" y2="-134.62" width="0.1524" layer="91"/>
<junction x="38.1" y="-135.89"/>
<wire x1="30.48" y1="-142.24" x2="30.48" y2="-147.32" width="0.1524" layer="91"/>
<junction x="30.48" y="-142.24"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="78.74" y1="-134.62" x2="78.74" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-135.89" x2="78.74" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-142.24" x2="86.36" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-142.24" x2="93.98" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-142.24" x2="93.98" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<junction x="78.74" y="-135.89"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-135.89" x2="93.98" y2="-134.62" width="0.1524" layer="91"/>
<junction x="93.98" y="-135.89"/>
<wire x1="86.36" y1="-142.24" x2="86.36" y2="-147.32" width="0.1524" layer="91"/>
<junction x="86.36" y="-142.24"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="134.62" y1="-134.62" x2="134.62" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-135.89" x2="134.62" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-142.24" x2="142.24" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-142.24" x2="149.86" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-142.24" x2="149.86" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<junction x="134.62" y="-135.89"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="149.86" y1="-135.89" x2="149.86" y2="-134.62" width="0.1524" layer="91"/>
<junction x="149.86" y="-135.89"/>
<wire x1="142.24" y1="-142.24" x2="142.24" y2="-147.32" width="0.1524" layer="91"/>
<junction x="142.24" y="-142.24"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="190.5" y1="-134.62" x2="190.5" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-135.89" x2="190.5" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-142.24" x2="198.12" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-142.24" x2="205.74" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-142.24" x2="205.74" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<junction x="190.5" y="-135.89"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="205.74" y1="-135.89" x2="205.74" y2="-134.62" width="0.1524" layer="91"/>
<junction x="205.74" y="-135.89"/>
<wire x1="198.12" y1="-142.24" x2="198.12" y2="-147.32" width="0.1524" layer="91"/>
<junction x="198.12" y="-142.24"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="246.38" y1="-134.62" x2="246.38" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-135.89" x2="246.38" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-142.24" x2="254" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="254" y1="-142.24" x2="261.62" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-142.24" x2="261.62" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<junction x="246.38" y="-135.89"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="261.62" y1="-135.89" x2="261.62" y2="-134.62" width="0.1524" layer="91"/>
<junction x="261.62" y="-135.89"/>
<wire x1="254" y1="-142.24" x2="254" y2="-147.32" width="0.1524" layer="91"/>
<junction x="254" y="-142.24"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="302.26" y1="-134.62" x2="302.26" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-135.89" x2="302.26" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-142.24" x2="309.88" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-142.24" x2="317.5" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="317.5" y1="-142.24" x2="317.5" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<junction x="302.26" y="-135.89"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="317.5" y1="-135.89" x2="317.5" y2="-134.62" width="0.1524" layer="91"/>
<junction x="317.5" y="-135.89"/>
<wire x1="309.88" y1="-142.24" x2="309.88" y2="-147.32" width="0.1524" layer="91"/>
<junction x="309.88" y="-142.24"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="358.14" y1="-134.62" x2="358.14" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-135.89" x2="358.14" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-142.24" x2="365.76" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-142.24" x2="373.38" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-142.24" x2="373.38" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<junction x="358.14" y="-135.89"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="373.38" y1="-135.89" x2="373.38" y2="-134.62" width="0.1524" layer="91"/>
<junction x="373.38" y="-135.89"/>
<wire x1="365.76" y1="-142.24" x2="365.76" y2="-147.32" width="0.1524" layer="91"/>
<junction x="365.76" y="-142.24"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="414.02" y1="-134.62" x2="414.02" y2="-135.89" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-135.89" x2="414.02" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-142.24" x2="421.64" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="421.64" y1="-142.24" x2="429.26" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-142.24" x2="429.26" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<junction x="414.02" y="-135.89"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="429.26" y1="-135.89" x2="429.26" y2="-134.62" width="0.1524" layer="91"/>
<junction x="429.26" y="-135.89"/>
<wire x1="421.64" y1="-142.24" x2="421.64" y2="-147.32" width="0.1524" layer="91"/>
<junction x="421.64" y="-142.24"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="43.18" y1="-218.44" x2="43.18" y2="-219.71" width="0.1524" layer="91"/>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-219.71" x2="43.18" y2="-223.52" width="0.1524" layer="91"/>
<junction x="43.18" y="-219.71"/>
</segment>
<segment>
<wire x1="96.52" y1="-220.98" x2="96.52" y2="-222.25" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-222.25" x2="96.52" y2="-226.06" width="0.1524" layer="91"/>
<junction x="96.52" y="-222.25"/>
</segment>
<segment>
<wire x1="154.94" y1="-220.98" x2="154.94" y2="-222.25" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="154.94" y1="-222.25" x2="154.94" y2="-226.06" width="0.1524" layer="91"/>
<junction x="154.94" y="-222.25"/>
</segment>
<segment>
<wire x1="208.28" y1="-223.52" x2="208.28" y2="-224.79" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="208.28" y1="-224.79" x2="208.28" y2="-228.6" width="0.1524" layer="91"/>
<junction x="208.28" y="-224.79"/>
</segment>
<segment>
<wire x1="266.7" y1="-223.52" x2="266.7" y2="-224.79" width="0.1524" layer="91"/>
<pinref part="GND32" gate="1" pin="GND"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="266.7" y1="-224.79" x2="266.7" y2="-228.6" width="0.1524" layer="91"/>
<junction x="266.7" y="-224.79"/>
</segment>
<segment>
<wire x1="320.04" y1="-226.06" x2="320.04" y2="-227.33" width="0.1524" layer="91"/>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="320.04" y1="-227.33" x2="320.04" y2="-231.14" width="0.1524" layer="91"/>
<junction x="320.04" y="-227.33"/>
</segment>
<segment>
<wire x1="378.46" y1="-226.06" x2="378.46" y2="-227.33" width="0.1524" layer="91"/>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="378.46" y1="-227.33" x2="378.46" y2="-231.14" width="0.1524" layer="91"/>
<junction x="378.46" y="-227.33"/>
</segment>
<segment>
<wire x1="431.8" y1="-228.6" x2="431.8" y2="-229.87" width="0.1524" layer="91"/>
<pinref part="GND35" gate="1" pin="GND"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="431.8" y1="-229.87" x2="431.8" y2="-233.68" width="0.1524" layer="91"/>
<junction x="431.8" y="-229.87"/>
</segment>
<segment>
<pinref part="Q16" gate="G$1" pin="S"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="-187.96" y1="-205.74" x2="-187.96" y2="-210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-193.04" y1="-134.62" x2="-193.04" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="D30" gate="G$1" pin="-"/>
<wire x1="-193.04" y1="-135.89" x2="-193.04" y2="-139.7" width="0.1524" layer="91"/>
<junction x="-193.04" y="-135.89"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="-193.04" y1="-139.7" x2="-193.04" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-193.04" y="-139.7"/>
</segment>
<segment>
<wire x1="-175.26" y1="-132.08" x2="-175.26" y2="-133.35" width="0.1524" layer="91"/>
<pinref part="D31" gate="G$1" pin="-"/>
<wire x1="-175.26" y1="-133.35" x2="-175.26" y2="-139.7" width="0.1524" layer="91"/>
<junction x="-175.26" y="-133.35"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q17" gate="G$1" pin="S"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-190.5" y1="-251.46" x2="-190.5" y2="-256.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q18" gate="G$1" pin="S"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="-193.04" y1="-299.72" x2="-193.04" y2="-304.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q19" gate="G$1" pin="S"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="-195.58" y1="-345.44" x2="-195.58" y2="-350.52" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-135.89" y1="-27.94" x2="-134.62" y2="-27.94" width="0.1524" layer="91"/>
<label x="-127" y="-27.94" size="1.778" layer="95" xref="yes"/>
<pinref part="C1" gate="G$1" pin="-"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="-134.62" y1="-27.94" x2="-127" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-25.4" x2="-137.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-27.94" x2="-142.24" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-135.89" y1="-27.94" x2="-137.16" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-137.16" y="-27.94"/>
</segment>
<segment>
<wire x1="-289.56" y1="-111.76" x2="-292.1" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$2"/>
<wire x1="-292.1" y1="-111.76" x2="-294.64" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-292.1" y="-111.76"/>
<wire x1="-289.56" y1="-109.22" x2="-292.1" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$1"/>
<wire x1="-292.1" y1="-109.22" x2="-294.64" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-292.1" y="-109.22"/>
<wire x1="-294.64" y1="-111.76" x2="-294.64" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-289.56" y1="-116.84" x2="-292.1" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$4"/>
<wire x1="-292.1" y1="-116.84" x2="-294.64" y2="-116.84" width="0.1524" layer="91"/>
<junction x="-292.1" y="-116.84"/>
<wire x1="-289.56" y1="-114.3" x2="-292.1" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$3"/>
<wire x1="-292.1" y1="-114.3" x2="-294.64" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-292.1" y="-114.3"/>
<wire x1="-294.64" y1="-116.84" x2="-294.64" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-294.64" y1="-111.76" x2="-294.64" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-294.64" y="-111.76"/>
<junction x="-294.64" y="-114.3"/>
<wire x1="-294.64" y1="-109.22" x2="-297.18" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-294.64" y="-109.22"/>
<label x="-297.18" y="-109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-236.22" y1="-33.02" x2="-236.22" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="P$3"/>
<wire x1="-236.22" y1="-35.56" x2="-236.22" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-236.22" y="-35.56"/>
<label x="-236.22" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-231.14" y1="-33.02" x2="-231.14" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="P$4"/>
<wire x1="-231.14" y1="-35.56" x2="-231.14" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-231.14" y="-35.56"/>
<label x="-231.14" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="25.4" y1="0" x2="24.13" y2="0" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="24.13" y1="0" x2="22.86" y2="0" width="0.1524" layer="91"/>
<junction x="24.13" y="0"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="-"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="76.2" y1="26.67" x2="76.2" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="-"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="86.36" y1="26.67" x2="86.36" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="76.2" y1="15.24" x2="76.2" y2="13.97" width="0.1524" layer="91"/>
<wire x1="76.2" y1="13.97" x2="76.2" y2="10.16" width="0.1524" layer="91"/>
<wire x1="76.2" y1="10.16" x2="86.36" y2="10.16" width="0.1524" layer="91"/>
<wire x1="86.36" y1="10.16" x2="86.36" y2="13.97" width="0.1524" layer="91"/>
<wire x1="86.36" y1="13.97" x2="86.36" y2="15.24" width="0.1524" layer="91"/>
<junction x="86.36" y="10.16"/>
<pinref part="R3" gate="G$1" pin="1"/>
<junction x="76.2" y="13.97"/>
<pinref part="R4" gate="G$1" pin="1"/>
<junction x="86.36" y="13.97"/>
<wire x1="86.36" y1="10.16" x2="86.36" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="81.28" y1="0" x2="80.01" y2="0" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="80.01" y1="0" x2="78.74" y2="0" width="0.1524" layer="91"/>
<junction x="80.01" y="0"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="-"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="132.08" y1="26.67" x2="132.08" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="-"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="142.24" y1="26.67" x2="142.24" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="132.08" y1="15.24" x2="132.08" y2="13.97" width="0.1524" layer="91"/>
<wire x1="132.08" y1="13.97" x2="132.08" y2="10.16" width="0.1524" layer="91"/>
<wire x1="132.08" y1="10.16" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="10.16" x2="142.24" y2="13.97" width="0.1524" layer="91"/>
<wire x1="142.24" y1="13.97" x2="142.24" y2="15.24" width="0.1524" layer="91"/>
<junction x="142.24" y="10.16"/>
<pinref part="R5" gate="G$1" pin="1"/>
<junction x="132.08" y="13.97"/>
<pinref part="R6" gate="G$1" pin="1"/>
<junction x="142.24" y="13.97"/>
<wire x1="142.24" y1="10.16" x2="142.24" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="137.16" y1="0" x2="135.89" y2="0" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="135.89" y1="0" x2="134.62" y2="0" width="0.1524" layer="91"/>
<junction x="135.89" y="0"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="-"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="187.96" y1="26.67" x2="187.96" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="-"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="198.12" y1="26.67" x2="198.12" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="187.96" y1="15.24" x2="187.96" y2="13.97" width="0.1524" layer="91"/>
<wire x1="187.96" y1="13.97" x2="187.96" y2="10.16" width="0.1524" layer="91"/>
<wire x1="187.96" y1="10.16" x2="198.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="198.12" y1="10.16" x2="198.12" y2="13.97" width="0.1524" layer="91"/>
<wire x1="198.12" y1="13.97" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
<junction x="198.12" y="10.16"/>
<pinref part="R7" gate="G$1" pin="1"/>
<junction x="187.96" y="13.97"/>
<pinref part="R8" gate="G$1" pin="1"/>
<junction x="198.12" y="13.97"/>
<wire x1="198.12" y1="10.16" x2="198.12" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="G"/>
<wire x1="193.04" y1="0" x2="191.77" y2="0" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="191.77" y1="0" x2="190.5" y2="0" width="0.1524" layer="91"/>
<junction x="191.77" y="0"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="-"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="243.84" y1="26.67" x2="243.84" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="-"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="254" y1="26.67" x2="254" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="243.84" y1="15.24" x2="243.84" y2="13.97" width="0.1524" layer="91"/>
<wire x1="243.84" y1="13.97" x2="243.84" y2="10.16" width="0.1524" layer="91"/>
<wire x1="243.84" y1="10.16" x2="254" y2="10.16" width="0.1524" layer="91"/>
<wire x1="254" y1="10.16" x2="254" y2="13.97" width="0.1524" layer="91"/>
<wire x1="254" y1="13.97" x2="254" y2="15.24" width="0.1524" layer="91"/>
<junction x="254" y="10.16"/>
<pinref part="R9" gate="G$1" pin="1"/>
<junction x="243.84" y="13.97"/>
<pinref part="R10" gate="G$1" pin="1"/>
<junction x="254" y="13.97"/>
<wire x1="254" y1="10.16" x2="254" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="248.92" y1="0" x2="247.65" y2="0" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="247.65" y1="0" x2="246.38" y2="0" width="0.1524" layer="91"/>
<junction x="247.65" y="0"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="-"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="299.72" y1="26.67" x2="299.72" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="-"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="309.88" y1="26.67" x2="309.88" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="299.72" y1="15.24" x2="299.72" y2="13.97" width="0.1524" layer="91"/>
<wire x1="299.72" y1="13.97" x2="299.72" y2="10.16" width="0.1524" layer="91"/>
<wire x1="299.72" y1="10.16" x2="309.88" y2="10.16" width="0.1524" layer="91"/>
<wire x1="309.88" y1="10.16" x2="309.88" y2="13.97" width="0.1524" layer="91"/>
<wire x1="309.88" y1="13.97" x2="309.88" y2="15.24" width="0.1524" layer="91"/>
<junction x="309.88" y="10.16"/>
<pinref part="R11" gate="G$1" pin="1"/>
<junction x="299.72" y="13.97"/>
<pinref part="R12" gate="G$1" pin="1"/>
<junction x="309.88" y="13.97"/>
<wire x1="309.88" y1="10.16" x2="309.88" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="G"/>
<wire x1="304.8" y1="0" x2="303.53" y2="0" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="303.53" y1="0" x2="302.26" y2="0" width="0.1524" layer="91"/>
<junction x="303.53" y="0"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D13" gate="G$1" pin="-"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="355.6" y1="26.67" x2="355.6" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="-"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="365.76" y1="26.67" x2="365.76" y2="21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="355.6" y1="15.24" x2="355.6" y2="13.97" width="0.1524" layer="91"/>
<wire x1="355.6" y1="13.97" x2="355.6" y2="10.16" width="0.1524" layer="91"/>
<wire x1="355.6" y1="10.16" x2="365.76" y2="10.16" width="0.1524" layer="91"/>
<wire x1="365.76" y1="10.16" x2="365.76" y2="13.97" width="0.1524" layer="91"/>
<wire x1="365.76" y1="13.97" x2="365.76" y2="15.24" width="0.1524" layer="91"/>
<junction x="365.76" y="10.16"/>
<pinref part="R13" gate="G$1" pin="1"/>
<junction x="355.6" y="13.97"/>
<pinref part="R14" gate="G$1" pin="1"/>
<junction x="365.76" y="13.97"/>
<wire x1="365.76" y1="10.16" x2="365.76" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="Q7" gate="G$1" pin="G"/>
<wire x1="360.68" y1="0" x2="359.41" y2="0" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="359.41" y1="0" x2="358.14" y2="0" width="0.1524" layer="91"/>
<junction x="359.41" y="0"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="411.48" y1="26.67" x2="411.48" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="C"/>
<wire x1="411.48" y1="25.4" x2="411.48" y2="21.59" width="0.1524" layer="91"/>
<junction x="411.48" y="25.4"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="411.48" y1="15.24" x2="411.48" y2="13.97" width="0.1524" layer="91"/>
<wire x1="411.48" y1="13.97" x2="411.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="411.48" y1="10.16" x2="421.64" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<junction x="411.48" y="13.97"/>
<wire x1="421.64" y1="10.16" x2="421.64" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q8" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="G"/>
<wire x1="416.56" y1="0" x2="415.29" y2="0" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="415.29" y1="0" x2="414.02" y2="0" width="0.1524" layer="91"/>
<junction x="415.29" y="0"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="-"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-41.91" x2="20.32" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="D17" gate="G$1" pin="-"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-41.91" x2="30.48" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="Q9" gate="G$1" pin="G"/>
<wire x1="25.4" y1="-81.28" x2="24.13" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="24.13" y1="-81.28" x2="22.86" y2="-81.28" width="0.1524" layer="91"/>
<junction x="24.13" y="-81.28"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="D18" gate="G$1" pin="-"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="76.2" y1="-41.91" x2="76.2" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="D19" gate="G$1" pin="-"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-41.91" x2="86.36" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="Q10" gate="G$1" pin="G"/>
<wire x1="81.28" y1="-81.28" x2="80.01" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="80.01" y1="-81.28" x2="78.74" y2="-81.28" width="0.1524" layer="91"/>
<junction x="80.01" y="-81.28"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="D20" gate="G$1" pin="-"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="132.08" y1="-41.91" x2="132.08" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="D21" gate="G$1" pin="-"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="142.24" y1="-41.91" x2="142.24" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="Q11" gate="G$1" pin="G"/>
<wire x1="137.16" y1="-81.28" x2="135.89" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="135.89" y1="-81.28" x2="134.62" y2="-81.28" width="0.1524" layer="91"/>
<junction x="135.89" y="-81.28"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="D22" gate="G$1" pin="-"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="187.96" y1="-41.91" x2="187.96" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="D23" gate="G$1" pin="-"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="198.12" y1="-41.91" x2="198.12" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="Q12" gate="G$1" pin="G"/>
<wire x1="193.04" y1="-81.28" x2="191.77" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="191.77" y1="-81.28" x2="190.5" y2="-81.28" width="0.1524" layer="91"/>
<junction x="191.77" y="-81.28"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="D24" gate="G$1" pin="-"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="243.84" y1="-41.91" x2="243.84" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="-"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="254" y1="-41.91" x2="254" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="G"/>
<wire x1="248.92" y1="-81.28" x2="247.65" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="247.65" y1="-81.28" x2="246.38" y2="-81.28" width="0.1524" layer="91"/>
<junction x="247.65" y="-81.28"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="D26" gate="G$1" pin="-"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="299.72" y1="-41.91" x2="299.72" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="D27" gate="G$1" pin="-"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="309.88" y1="-41.91" x2="309.88" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="G"/>
<wire x1="304.8" y1="-81.28" x2="303.53" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R67" gate="G$1" pin="2"/>
<wire x1="303.53" y1="-81.28" x2="302.26" y2="-81.28" width="0.1524" layer="91"/>
<junction x="303.53" y="-81.28"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="D28" gate="G$1" pin="-"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="355.6" y1="-41.91" x2="355.6" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="D29" gate="G$1" pin="-"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="365.76" y1="-41.91" x2="365.76" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="G"/>
<wire x1="360.68" y1="-81.28" x2="359.41" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="359.41" y1="-81.28" x2="358.14" y2="-81.28" width="0.1524" layer="91"/>
<junction x="359.41" y="-81.28"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<wire x1="22.86" y1="-124.46" x2="22.86" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="22.86" y1="-126.42" x2="22.86" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-128.27" x2="22.86" y2="-129.54" width="0.1524" layer="91"/>
<junction x="22.86" y="-128.27"/>
<pinref part="IR1" gate="G$1" pin="C"/>
<junction x="22.86" y="-126.42"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<wire x1="38.1" y1="-124.46" x2="38.1" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-126.42" x2="38.1" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-128.27" x2="38.1" y2="-129.54" width="0.1524" layer="91"/>
<junction x="38.1" y="-128.27"/>
<pinref part="IR2" gate="G$1" pin="C"/>
<junction x="38.1" y="-126.42"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<wire x1="78.74" y1="-124.46" x2="78.74" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="78.74" y1="-126.42" x2="78.74" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-128.27" x2="78.74" y2="-129.54" width="0.1524" layer="91"/>
<junction x="78.74" y="-128.27"/>
<pinref part="IR3" gate="G$1" pin="C"/>
<junction x="78.74" y="-126.42"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<wire x1="93.98" y1="-124.46" x2="93.98" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-126.42" x2="93.98" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-128.27" x2="93.98" y2="-129.54" width="0.1524" layer="91"/>
<junction x="93.98" y="-128.27"/>
<pinref part="IR4" gate="G$1" pin="C"/>
<junction x="93.98" y="-126.42"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<wire x1="134.62" y1="-124.46" x2="134.62" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="134.62" y1="-126.42" x2="134.62" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-128.27" x2="134.62" y2="-129.54" width="0.1524" layer="91"/>
<junction x="134.62" y="-128.27"/>
<pinref part="IR5" gate="G$1" pin="C"/>
<junction x="134.62" y="-126.42"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<wire x1="149.86" y1="-124.46" x2="149.86" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="149.86" y1="-126.42" x2="149.86" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-128.27" x2="149.86" y2="-129.54" width="0.1524" layer="91"/>
<junction x="149.86" y="-128.27"/>
<pinref part="IR6" gate="G$1" pin="C"/>
<junction x="149.86" y="-126.42"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<wire x1="190.5" y1="-124.46" x2="190.5" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="190.5" y1="-126.42" x2="190.5" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-128.27" x2="190.5" y2="-129.54" width="0.1524" layer="91"/>
<junction x="190.5" y="-128.27"/>
<pinref part="IR7" gate="G$1" pin="C"/>
<junction x="190.5" y="-126.42"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="205.74" y1="-124.46" x2="205.74" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="205.74" y1="-126.42" x2="205.74" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-128.27" x2="205.74" y2="-129.54" width="0.1524" layer="91"/>
<junction x="205.74" y="-128.27"/>
<pinref part="IR8" gate="G$1" pin="C"/>
<junction x="205.74" y="-126.42"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<wire x1="246.38" y1="-124.46" x2="246.38" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="246.38" y1="-126.42" x2="246.38" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-128.27" x2="246.38" y2="-129.54" width="0.1524" layer="91"/>
<junction x="246.38" y="-128.27"/>
<pinref part="IR9" gate="G$1" pin="C"/>
<junction x="246.38" y="-126.42"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<wire x1="261.62" y1="-124.46" x2="261.62" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="261.62" y1="-126.42" x2="261.62" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-128.27" x2="261.62" y2="-129.54" width="0.1524" layer="91"/>
<junction x="261.62" y="-128.27"/>
<pinref part="IR10" gate="G$1" pin="C"/>
<junction x="261.62" y="-126.42"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<wire x1="302.26" y1="-124.46" x2="302.26" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="302.26" y1="-126.42" x2="302.26" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-128.27" x2="302.26" y2="-129.54" width="0.1524" layer="91"/>
<junction x="302.26" y="-128.27"/>
<pinref part="IR11" gate="G$1" pin="C"/>
<junction x="302.26" y="-126.42"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<wire x1="317.5" y1="-124.46" x2="317.5" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="317.5" y1="-126.42" x2="317.5" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="317.5" y1="-128.27" x2="317.5" y2="-129.54" width="0.1524" layer="91"/>
<junction x="317.5" y="-128.27"/>
<pinref part="IR12" gate="G$1" pin="C"/>
<junction x="317.5" y="-126.42"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<wire x1="358.14" y1="-124.46" x2="358.14" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="358.14" y1="-126.42" x2="358.14" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-128.27" x2="358.14" y2="-129.54" width="0.1524" layer="91"/>
<junction x="358.14" y="-128.27"/>
<pinref part="IR13" gate="G$1" pin="C"/>
<junction x="358.14" y="-126.42"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<wire x1="373.38" y1="-124.46" x2="373.38" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="373.38" y1="-126.42" x2="373.38" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-128.27" x2="373.38" y2="-129.54" width="0.1524" layer="91"/>
<junction x="373.38" y="-128.27"/>
<pinref part="IR14" gate="G$1" pin="C"/>
<junction x="373.38" y="-126.42"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<wire x1="414.02" y1="-124.46" x2="414.02" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="414.02" y1="-126.42" x2="414.02" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-128.27" x2="414.02" y2="-129.54" width="0.1524" layer="91"/>
<junction x="414.02" y="-128.27"/>
<pinref part="IR15" gate="G$1" pin="C"/>
<junction x="414.02" y="-126.42"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<wire x1="429.26" y1="-124.46" x2="429.26" y2="-126.42" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="429.26" y1="-126.42" x2="429.26" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-128.27" x2="429.26" y2="-129.54" width="0.1524" layer="91"/>
<junction x="429.26" y="-128.27"/>
<pinref part="IR16" gate="G$1" pin="C"/>
<junction x="429.26" y="-126.42"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="-193.04" y1="-106.68" x2="-193.04" y2="-113.03" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="-193.04" y1="-113.03" x2="-193.04" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-193.04" y="-113.03"/>
<pinref part="P+30" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="22.86" y1="-111.76" x2="22.86" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-118.42" x2="22.86" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-111.76" x2="38.1" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-111.76" x2="38.1" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR1" gate="G$1" pin="A"/>
<junction x="22.86" y="-118.42"/>
<pinref part="IR2" gate="G$1" pin="A"/>
<wire x1="38.1" y1="-118.42" x2="38.1" y2="-119.38" width="0.1524" layer="91"/>
<junction x="38.1" y="-118.42"/>
<label x="38.1" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="78.74" y1="-111.76" x2="78.74" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-118.42" x2="78.74" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-111.76" x2="93.98" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-111.76" x2="93.98" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR3" gate="G$1" pin="A"/>
<junction x="78.74" y="-118.42"/>
<pinref part="IR4" gate="G$1" pin="A"/>
<wire x1="93.98" y1="-118.42" x2="93.98" y2="-119.38" width="0.1524" layer="91"/>
<junction x="93.98" y="-118.42"/>
<label x="93.98" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="134.62" y1="-111.76" x2="134.62" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-118.42" x2="134.62" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-111.76" x2="149.86" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-111.76" x2="149.86" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR5" gate="G$1" pin="A"/>
<junction x="134.62" y="-118.42"/>
<pinref part="IR6" gate="G$1" pin="A"/>
<wire x1="149.86" y1="-118.42" x2="149.86" y2="-119.38" width="0.1524" layer="91"/>
<junction x="149.86" y="-118.42"/>
<label x="149.86" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="190.5" y1="-111.76" x2="190.5" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-118.42" x2="190.5" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-111.76" x2="205.74" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-111.76" x2="205.74" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR7" gate="G$1" pin="A"/>
<junction x="190.5" y="-118.42"/>
<pinref part="IR8" gate="G$1" pin="A"/>
<wire x1="205.74" y1="-118.42" x2="205.74" y2="-119.38" width="0.1524" layer="91"/>
<junction x="205.74" y="-118.42"/>
<label x="205.74" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="246.38" y1="-111.76" x2="246.38" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-118.42" x2="246.38" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-111.76" x2="261.62" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-111.76" x2="261.62" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR9" gate="G$1" pin="A"/>
<junction x="246.38" y="-118.42"/>
<pinref part="IR10" gate="G$1" pin="A"/>
<wire x1="261.62" y1="-118.42" x2="261.62" y2="-119.38" width="0.1524" layer="91"/>
<junction x="261.62" y="-118.42"/>
<label x="261.62" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="302.26" y1="-111.76" x2="302.26" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-118.42" x2="302.26" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-111.76" x2="317.5" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="317.5" y1="-111.76" x2="317.5" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR11" gate="G$1" pin="A"/>
<junction x="302.26" y="-118.42"/>
<pinref part="IR12" gate="G$1" pin="A"/>
<wire x1="317.5" y1="-118.42" x2="317.5" y2="-119.38" width="0.1524" layer="91"/>
<junction x="317.5" y="-118.42"/>
<label x="317.5" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="358.14" y1="-111.76" x2="358.14" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-118.42" x2="358.14" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-111.76" x2="373.38" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-111.76" x2="373.38" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR13" gate="G$1" pin="A"/>
<junction x="358.14" y="-118.42"/>
<pinref part="IR14" gate="G$1" pin="A"/>
<wire x1="373.38" y1="-118.42" x2="373.38" y2="-119.38" width="0.1524" layer="91"/>
<junction x="373.38" y="-118.42"/>
<label x="373.38" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="414.02" y1="-111.76" x2="414.02" y2="-118.42" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-118.42" x2="414.02" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-111.76" x2="429.26" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-111.76" x2="429.26" y2="-118.42" width="0.1524" layer="91"/>
<pinref part="IR15" gate="G$1" pin="A"/>
<junction x="414.02" y="-118.42"/>
<pinref part="IR16" gate="G$1" pin="A"/>
<wire x1="429.26" y1="-118.42" x2="429.26" y2="-119.38" width="0.1524" layer="91"/>
<junction x="429.26" y="-118.42"/>
<label x="429.26" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="43.18" x2="-68.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$8"/>
<label x="-68.58" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="40.64" x2="-68.58" y2="40.64" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$9"/>
<label x="-68.58" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-135.89" y1="-17.78" x2="-134.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="-17.78" x2="-128.27" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-128.27" y1="-17.78" x2="-128.27" y2="-19.05" width="0.1524" layer="91"/>
<label x="-128.27" y="-20.32" size="1.778" layer="95" xref="yes"/>
<pinref part="U$4" gate="G$1" pin="VCC"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="-142.24" y1="-17.78" x2="-137.16" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-17.78" x2="-135.89" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-137.16" y="-17.78"/>
</segment>
<segment>
<wire x1="-289.56" y1="-86.36" x2="-292.1" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$2"/>
<wire x1="-292.1" y1="-86.36" x2="-294.64" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-292.1" y="-86.36"/>
<wire x1="-289.56" y1="-83.82" x2="-292.1" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$1"/>
<wire x1="-292.1" y1="-83.82" x2="-294.64" y2="-83.82" width="0.1524" layer="91"/>
<junction x="-292.1" y="-83.82"/>
<wire x1="-294.64" y1="-86.36" x2="-294.64" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-289.56" y1="-91.44" x2="-292.1" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$4"/>
<wire x1="-292.1" y1="-91.44" x2="-294.64" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-292.1" y="-91.44"/>
<wire x1="-289.56" y1="-88.9" x2="-292.1" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$3"/>
<wire x1="-292.1" y1="-88.9" x2="-294.64" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-292.1" y="-88.9"/>
<wire x1="-294.64" y1="-91.44" x2="-294.64" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-294.64" y1="-86.36" x2="-294.64" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-294.64" y="-86.36"/>
<junction x="-294.64" y="-88.9"/>
<wire x1="-294.64" y1="-83.82" x2="-297.18" y2="-83.82" width="0.1524" layer="91"/>
<junction x="-294.64" y="-83.82"/>
<label x="-297.18" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-236.22" y1="-12.7" x2="-236.22" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="P$1"/>
<wire x1="-236.22" y1="-10.16" x2="-236.22" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-236.22" y="-10.16"/>
<label x="-236.22" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IOREF" class="0">
<segment>
<wire x1="22.86" y1="-185.42" x2="22.86" y2="-183.134" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-183.134" x2="22.86" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-177.8" x2="33.02" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-177.8" x2="43.18" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-177.8" x2="43.18" y2="-183.134" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-183.134" x2="43.18" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-177.8" x2="33.02" y2="-172.72" width="0.1524" layer="91"/>
<junction x="33.02" y="-177.8"/>
<label x="33.02" y="-172.72" size="1.778" layer="95" xref="yes"/>
<pinref part="PT1" gate="G$1" pin="C"/>
<junction x="22.86" y="-183.134"/>
<pinref part="PT2" gate="G$1" pin="C"/>
<junction x="43.18" y="-183.134"/>
</segment>
<segment>
<wire x1="76.2" y1="-187.96" x2="76.2" y2="-185.674" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-185.674" x2="76.2" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-180.34" x2="86.36" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-180.34" x2="96.52" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-180.34" x2="96.52" y2="-185.674" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-185.674" x2="96.52" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-180.34" x2="86.36" y2="-175.26" width="0.1524" layer="91"/>
<junction x="86.36" y="-180.34"/>
<label x="86.36" y="-175.26" size="1.778" layer="95" xref="yes"/>
<pinref part="PT3" gate="G$1" pin="C"/>
<junction x="76.2" y="-185.674"/>
<pinref part="PT4" gate="G$1" pin="C"/>
<junction x="96.52" y="-185.674"/>
</segment>
<segment>
<wire x1="134.62" y1="-187.96" x2="134.62" y2="-185.674" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-185.674" x2="134.62" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-180.34" x2="144.78" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-180.34" x2="154.94" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-180.34" x2="154.94" y2="-185.674" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-185.674" x2="154.94" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-180.34" x2="144.78" y2="-175.26" width="0.1524" layer="91"/>
<junction x="144.78" y="-180.34"/>
<label x="144.78" y="-175.26" size="1.778" layer="95" xref="yes"/>
<pinref part="PT5" gate="G$1" pin="C"/>
<junction x="134.62" y="-185.674"/>
<pinref part="PT6" gate="G$1" pin="C"/>
<junction x="154.94" y="-185.674"/>
</segment>
<segment>
<wire x1="187.96" y1="-190.5" x2="187.96" y2="-188.214" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-188.214" x2="187.96" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-182.88" x2="198.12" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-182.88" x2="208.28" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-182.88" x2="208.28" y2="-188.214" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-188.214" x2="208.28" y2="-190.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-182.88" x2="198.12" y2="-177.8" width="0.1524" layer="91"/>
<junction x="198.12" y="-182.88"/>
<label x="198.12" y="-177.8" size="1.778" layer="95" xref="yes"/>
<pinref part="PT7" gate="G$1" pin="C"/>
<junction x="187.96" y="-188.214"/>
<pinref part="PT8" gate="G$1" pin="C"/>
<junction x="208.28" y="-188.214"/>
</segment>
<segment>
<wire x1="246.38" y1="-190.5" x2="246.38" y2="-188.214" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-188.214" x2="246.38" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-182.88" x2="256.54" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-182.88" x2="266.7" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-182.88" x2="266.7" y2="-188.214" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-188.214" x2="266.7" y2="-190.5" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-182.88" x2="256.54" y2="-177.8" width="0.1524" layer="91"/>
<junction x="256.54" y="-182.88"/>
<label x="256.54" y="-177.8" size="1.778" layer="95" xref="yes"/>
<pinref part="PT9" gate="G$1" pin="C"/>
<junction x="246.38" y="-188.214"/>
<pinref part="PT10" gate="G$1" pin="C"/>
<junction x="266.7" y="-188.214"/>
</segment>
<segment>
<wire x1="299.72" y1="-193.04" x2="299.72" y2="-190.754" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-190.754" x2="299.72" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-185.42" x2="309.88" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-185.42" x2="320.04" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-185.42" x2="320.04" y2="-190.754" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-190.754" x2="320.04" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-185.42" x2="309.88" y2="-180.34" width="0.1524" layer="91"/>
<junction x="309.88" y="-185.42"/>
<label x="309.88" y="-180.34" size="1.778" layer="95" xref="yes"/>
<pinref part="PT11" gate="G$1" pin="C"/>
<junction x="299.72" y="-190.754"/>
<pinref part="PT12" gate="G$1" pin="C"/>
<junction x="320.04" y="-190.754"/>
</segment>
<segment>
<wire x1="358.14" y1="-193.04" x2="358.14" y2="-190.754" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-190.754" x2="358.14" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-185.42" x2="368.3" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="368.3" y1="-185.42" x2="378.46" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-185.42" x2="378.46" y2="-190.754" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-190.754" x2="378.46" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="368.3" y1="-185.42" x2="368.3" y2="-180.34" width="0.1524" layer="91"/>
<junction x="368.3" y="-185.42"/>
<label x="368.3" y="-180.34" size="1.778" layer="95" xref="yes"/>
<pinref part="PT13" gate="G$1" pin="C"/>
<junction x="358.14" y="-190.754"/>
<pinref part="PT14" gate="G$1" pin="C"/>
<junction x="378.46" y="-190.754"/>
</segment>
<segment>
<wire x1="411.48" y1="-195.58" x2="411.48" y2="-193.294" width="0.1524" layer="91"/>
<wire x1="411.48" y1="-193.294" x2="411.48" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="411.48" y1="-187.96" x2="421.64" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="421.64" y1="-187.96" x2="431.8" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-187.96" x2="431.8" y2="-193.294" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-193.294" x2="431.8" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="421.64" y1="-187.96" x2="421.64" y2="-182.88" width="0.1524" layer="91"/>
<junction x="421.64" y="-187.96"/>
<label x="421.64" y="-182.88" size="1.778" layer="95" xref="yes"/>
<pinref part="PT15" gate="G$1" pin="C"/>
<junction x="411.48" y="-193.294"/>
<pinref part="PT16" gate="G$1" pin="C"/>
<junction x="431.8" y="-193.294"/>
</segment>
<segment>
<wire x1="-35.56" y1="-2.54" x2="-33.02" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="33"/>
<label x="-33.02" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-175.26" y1="-104.14" x2="-175.26" y2="-110.49" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="-175.26" y1="-110.49" x2="-175.26" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-175.26" y="-110.49"/>
<label x="-175.26" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-116.84" x2="-266.7" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$8"/>
<wire x1="-266.7" y1="-116.84" x2="-264.16" y2="-116.84" width="0.1524" layer="91"/>
<junction x="-266.7" y="-116.84"/>
<label x="-264.16" y="-116.84" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-231.14" y1="-12.7" x2="-231.14" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="P$2"/>
<wire x1="-231.14" y1="-10.16" x2="-231.14" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-231.14" y="-10.16"/>
<label x="-231.14" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="1" class="0">
<segment>
<wire x1="20.32" y1="-35.56" x2="20.32" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-34.29" x2="20.32" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-30.48" x2="30.48" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-30.48" x2="30.48" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="+"/>
<junction x="20.32" y="-34.29"/>
<pinref part="D17" gate="G$1" pin="+"/>
<wire x1="30.48" y1="-34.29" x2="30.48" y2="-35.56" width="0.1524" layer="91"/>
<junction x="30.48" y="-34.29"/>
<junction x="30.48" y="-30.48"/>
<wire x1="30.48" y1="-30.48" x2="30.48" y2="-25.4" width="0.1524" layer="91"/>
<label x="30.48" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BUSS_RES1" gate="G$1" pin="P$1"/>
<wire x1="-71.12" y1="60.96" x2="-68.58" y2="60.96" width="0.1524" layer="91"/>
<label x="-68.58" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<wire x1="76.2" y1="-35.56" x2="76.2" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-34.29" x2="76.2" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-30.48" x2="86.36" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-30.48" x2="86.36" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="+"/>
<junction x="76.2" y="-34.29"/>
<pinref part="D19" gate="G$1" pin="+"/>
<wire x1="86.36" y1="-34.29" x2="86.36" y2="-35.56" width="0.1524" layer="91"/>
<junction x="86.36" y="-34.29"/>
<junction x="86.36" y="-30.48"/>
<wire x1="86.36" y1="-30.48" x2="86.36" y2="-25.4" width="0.1524" layer="91"/>
<label x="86.36" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="58.42" x2="-68.58" y2="58.42" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$2"/>
<label x="-68.58" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="3" class="0">
<segment>
<wire x1="132.08" y1="-35.56" x2="132.08" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-34.29" x2="132.08" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-30.48" x2="142.24" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-30.48" x2="142.24" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D20" gate="G$1" pin="+"/>
<junction x="132.08" y="-34.29"/>
<pinref part="D21" gate="G$1" pin="+"/>
<wire x1="142.24" y1="-34.29" x2="142.24" y2="-35.56" width="0.1524" layer="91"/>
<junction x="142.24" y="-34.29"/>
<junction x="142.24" y="-30.48"/>
<wire x1="142.24" y1="-30.48" x2="142.24" y2="-25.4" width="0.1524" layer="91"/>
<label x="142.24" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="55.88" x2="-68.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$3"/>
<label x="-68.58" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="4" class="0">
<segment>
<wire x1="187.96" y1="-35.56" x2="187.96" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-34.29" x2="187.96" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-30.48" x2="198.12" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-30.48" x2="198.12" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="+"/>
<junction x="187.96" y="-34.29"/>
<pinref part="D23" gate="G$1" pin="+"/>
<wire x1="198.12" y1="-34.29" x2="198.12" y2="-35.56" width="0.1524" layer="91"/>
<junction x="198.12" y="-34.29"/>
<junction x="198.12" y="-30.48"/>
<wire x1="198.12" y1="-30.48" x2="198.12" y2="-25.4" width="0.1524" layer="91"/>
<label x="198.12" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="53.34" x2="-68.58" y2="53.34" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$4"/>
<label x="-68.58" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="5" class="0">
<segment>
<wire x1="243.84" y1="-35.56" x2="243.84" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-34.29" x2="243.84" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-30.48" x2="254" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="254" y1="-30.48" x2="254" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D24" gate="G$1" pin="+"/>
<junction x="243.84" y="-34.29"/>
<pinref part="D25" gate="G$1" pin="+"/>
<wire x1="254" y1="-34.29" x2="254" y2="-35.56" width="0.1524" layer="91"/>
<junction x="254" y="-34.29"/>
<junction x="254" y="-30.48"/>
<wire x1="254" y1="-30.48" x2="254" y2="-25.4" width="0.1524" layer="91"/>
<label x="254" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="50.8" x2="-68.58" y2="50.8" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$5"/>
<label x="-68.58" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="6" class="0">
<segment>
<wire x1="299.72" y1="-35.56" x2="299.72" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-34.29" x2="299.72" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-30.48" x2="309.88" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-30.48" x2="309.88" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D26" gate="G$1" pin="+"/>
<junction x="299.72" y="-34.29"/>
<pinref part="D27" gate="G$1" pin="+"/>
<wire x1="309.88" y1="-34.29" x2="309.88" y2="-35.56" width="0.1524" layer="91"/>
<junction x="309.88" y="-34.29"/>
<junction x="309.88" y="-30.48"/>
<wire x1="309.88" y1="-30.48" x2="309.88" y2="-25.4" width="0.1524" layer="91"/>
<label x="309.88" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="48.26" x2="-68.58" y2="48.26" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$6"/>
<label x="-68.58" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="7" class="0">
<segment>
<wire x1="355.6" y1="-35.56" x2="355.6" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="355.6" y1="-34.29" x2="355.6" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="355.6" y1="-30.48" x2="365.76" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-30.48" x2="365.76" y2="-34.29" width="0.1524" layer="91"/>
<pinref part="D28" gate="G$1" pin="+"/>
<junction x="355.6" y="-34.29"/>
<pinref part="D29" gate="G$1" pin="+"/>
<wire x1="365.76" y1="-34.29" x2="365.76" y2="-35.56" width="0.1524" layer="91"/>
<junction x="365.76" y="-34.29"/>
<junction x="365.76" y="-30.48"/>
<wire x1="365.76" y1="-30.48" x2="365.76" y2="-25.4" width="0.1524" layer="91"/>
<label x="365.76" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="45.72" x2="-68.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$7"/>
<label x="-68.58" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="8" class="0">
<segment>
<wire x1="20.32" y1="33.02" x2="20.32" y2="34.29" width="0.1524" layer="91"/>
<wire x1="20.32" y1="34.29" x2="20.32" y2="38.1" width="0.1524" layer="91"/>
<wire x1="20.32" y1="38.1" x2="30.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="30.48" y1="38.1" x2="30.48" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="+"/>
<junction x="20.32" y="34.29"/>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="30.48" y1="34.29" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<junction x="30.48" y="34.29"/>
<junction x="30.48" y="38.1"/>
<wire x1="30.48" y1="38.1" x2="30.48" y2="43.18" width="0.1524" layer="91"/>
<label x="30.48" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="20.32" x2="-68.58" y2="20.32" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$17"/>
<label x="-68.58" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="9" class="0">
<segment>
<wire x1="76.2" y1="33.02" x2="76.2" y2="34.29" width="0.1524" layer="91"/>
<wire x1="76.2" y1="34.29" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="86.36" y2="38.1" width="0.1524" layer="91"/>
<wire x1="86.36" y1="38.1" x2="86.36" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="+"/>
<junction x="76.2" y="34.29"/>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="86.36" y1="34.29" x2="86.36" y2="33.02" width="0.1524" layer="91"/>
<junction x="86.36" y="34.29"/>
<junction x="86.36" y="38.1"/>
<wire x1="86.36" y1="38.1" x2="86.36" y2="43.18" width="0.1524" layer="91"/>
<label x="86.36" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="22.86" x2="-68.58" y2="22.86" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$16"/>
<label x="-68.58" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_ARD" class="0">
<segment>
<wire x1="-53.34" y1="-2.54" x2="-50.8" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="34"/>
<label x="-53.34" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-60.96" y1="-124.46" x2="-73.66" y2="-124.46" width="0.1524" layer="91"/>
<label x="-78.74" y="-124.46" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-73.66" y1="-124.46" x2="-78.74" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-119.38" x2="-118.11" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="-119.38" x2="-119.38" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="-119.38" x2="-119.38" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="-116.84" x2="-73.66" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-116.84" x2="-73.66" y2="-124.46" width="0.1524" layer="91"/>
<junction x="-73.66" y="-124.46"/>
<pinref part="R75" gate="G$1" pin="1"/>
<junction x="-118.11" y="-119.38"/>
</segment>
<segment>
<wire x1="-60.96" y1="-142.24" x2="-73.66" y2="-142.24" width="0.1524" layer="91"/>
<label x="-78.74" y="-142.24" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-73.66" y1="-142.24" x2="-78.74" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-139.7" x2="-118.11" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="-118.11" y1="-139.7" x2="-119.38" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="-139.7" x2="-119.38" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="-137.16" x2="-73.66" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-137.16" x2="-73.66" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-73.66" y="-142.24"/>
<pinref part="R76" gate="G$1" pin="1"/>
<junction x="-118.11" y="-139.7"/>
</segment>
<segment>
<wire x1="-99.06" y1="-76.2" x2="-99.06" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="P$1"/>
<wire x1="-99.06" y1="-73.66" x2="-99.06" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-99.06" y="-73.66"/>
<label x="-99.06" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-93.98" y1="-96.52" x2="-93.98" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="P$4"/>
<wire x1="-93.98" y1="-99.06" x2="-93.98" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-93.98" y="-99.06"/>
<label x="-93.98" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-60.96" y1="-76.2" x2="-60.96" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="CON6" gate="G$1" pin="P$1"/>
<wire x1="-60.96" y1="-73.66" x2="-60.96" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-60.96" y="-73.66"/>
<label x="-60.96" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-55.88" y1="-96.52" x2="-55.88" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="CON6" gate="G$1" pin="P$4"/>
<wire x1="-55.88" y1="-99.06" x2="-55.88" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-55.88" y="-99.06"/>
<label x="-55.88" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DG1" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="-43.18" x2="-33.02" y2="-43.18" width="0.1524" layer="91"/>
<label x="-33.02" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="17.78" y1="-81.28" x2="16.51" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="16.51" y1="-81.28" x2="12.7" y2="-81.28" width="0.1524" layer="91"/>
<junction x="16.51" y="-81.28"/>
<label x="12.7" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG2" class="0">
<segment>
<wire x1="-53.34" y1="-43.18" x2="-50.8" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="2"/>
<label x="-53.34" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="73.66" y1="-81.28" x2="72.39" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="72.39" y1="-81.28" x2="68.58" y2="-81.28" width="0.1524" layer="91"/>
<junction x="72.39" y="-81.28"/>
<label x="68.58" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG3" class="0">
<segment>
<wire x1="-35.56" y1="-40.64" x2="-33.02" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="3"/>
<label x="-33.02" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="129.54" y1="-81.28" x2="128.27" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="128.27" y1="-81.28" x2="124.46" y2="-81.28" width="0.1524" layer="91"/>
<junction x="128.27" y="-81.28"/>
<label x="124.46" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG4" class="0">
<segment>
<wire x1="-53.34" y1="-40.64" x2="-50.8" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="4"/>
<label x="-53.34" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="185.42" y1="-81.28" x2="184.15" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="184.15" y1="-81.28" x2="180.34" y2="-81.28" width="0.1524" layer="91"/>
<junction x="184.15" y="-81.28"/>
<label x="180.34" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG5" class="0">
<segment>
<wire x1="-35.56" y1="-38.1" x2="-33.02" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="5"/>
<label x="-33.02" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="241.3" y1="-81.28" x2="240.03" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="240.03" y1="-81.28" x2="236.22" y2="-81.28" width="0.1524" layer="91"/>
<junction x="240.03" y="-81.28"/>
<label x="236.22" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG6" class="0">
<segment>
<wire x1="-53.34" y1="-38.1" x2="-50.8" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="6"/>
<label x="-53.34" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="297.18" y1="-81.28" x2="295.91" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="295.91" y1="-81.28" x2="292.1" y2="-81.28" width="0.1524" layer="91"/>
<junction x="295.91" y="-81.28"/>
<label x="292.1" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG7" class="0">
<segment>
<wire x1="-35.56" y1="-35.56" x2="-33.02" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="7"/>
<label x="-33.02" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="353.06" y1="-81.28" x2="351.79" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="351.79" y1="-81.28" x2="347.98" y2="-81.28" width="0.1524" layer="91"/>
<junction x="351.79" y="-81.28"/>
<label x="347.98" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG8" class="0">
<segment>
<wire x1="-53.34" y1="-35.56" x2="-50.8" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="8"/>
<label x="-53.34" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-208.28" y1="-340.36" x2="-209.55" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="-209.55" y1="-340.36" x2="-215.9" y2="-340.36" width="0.1524" layer="91"/>
<junction x="-209.55" y="-340.36"/>
<label x="-215.9" y="-340.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG9" class="0">
<segment>
<wire x1="-35.56" y1="-33.02" x2="-33.02" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="9"/>
<label x="-33.02" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-205.74" y1="-294.64" x2="-207.01" y2="-294.64" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="-207.01" y1="-294.64" x2="-213.36" y2="-294.64" width="0.1524" layer="91"/>
<junction x="-207.01" y="-294.64"/>
<label x="-213.36" y="-294.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<wire x1="-53.34" y1="-33.02" x2="-50.8" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="10"/>
<label x="-53.34" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="17.78" y1="0" x2="16.51" y2="0" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="16.51" y1="0" x2="12.7" y2="0" width="0.1524" layer="91"/>
<junction x="16.51" y="0"/>
<label x="12.7" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<wire x1="-35.56" y1="-30.48" x2="-33.02" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="11"/>
<label x="-33.02" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="73.66" y1="0" x2="72.39" y2="0" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="72.39" y1="0" x2="68.58" y2="0" width="0.1524" layer="91"/>
<junction x="72.39" y="0"/>
<label x="68.58" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<wire x1="-53.34" y1="-30.48" x2="-50.8" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="12"/>
<label x="-53.34" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="129.54" y1="0" x2="128.27" y2="0" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="128.27" y1="0" x2="124.46" y2="0" width="0.1524" layer="91"/>
<junction x="128.27" y="0"/>
<label x="124.46" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<wire x1="-35.56" y1="-27.94" x2="-33.02" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="13"/>
<label x="-33.02" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="185.42" y1="0" x2="184.15" y2="0" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="184.15" y1="0" x2="180.34" y2="0" width="0.1524" layer="91"/>
<junction x="184.15" y="0"/>
<label x="180.34" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<wire x1="-53.34" y1="-27.94" x2="-50.8" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="14"/>
<label x="-53.34" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="241.3" y1="0" x2="240.03" y2="0" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="240.03" y1="0" x2="236.22" y2="0" width="0.1524" layer="91"/>
<junction x="240.03" y="0"/>
<label x="236.22" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<wire x1="-35.56" y1="-25.4" x2="-33.02" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="15"/>
<label x="-33.02" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="297.18" y1="0" x2="295.91" y2="0" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="295.91" y1="0" x2="292.1" y2="0" width="0.1524" layer="91"/>
<junction x="295.91" y="0"/>
<label x="292.1" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<wire x1="-53.34" y1="-25.4" x2="-50.8" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="16"/>
<label x="-53.34" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="353.06" y1="0" x2="351.79" y2="0" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="351.79" y1="0" x2="347.98" y2="0" width="0.1524" layer="91"/>
<junction x="351.79" y="0"/>
<label x="347.98" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWM9" class="0">
<segment>
<wire x1="-35.56" y1="-22.86" x2="-33.02" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="17"/>
<label x="-33.02" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="408.94" y1="0" x2="407.67" y2="0" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="407.67" y1="0" x2="403.86" y2="0" width="0.1524" layer="91"/>
<junction x="407.67" y="0"/>
<label x="403.86" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<wire x1="-35.56" y1="-20.32" x2="-33.02" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="19"/>
<label x="-33.02" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="22.86" y1="-190.5" x2="22.86" y2="-192.786" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-192.786" x2="22.86" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-192.024" x2="43.18" y2="-192.786" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-192.786" x2="43.18" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-195.58" x2="43.18" y2="-195.58" width="0.1524" layer="91"/>
<junction x="43.18" y="-195.58"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="43.18" y1="-212.09" x2="43.18" y2="-213.36" width="0.1524" layer="91"/>
<junction x="43.18" y="-212.09"/>
<wire x1="43.18" y1="-195.58" x2="43.18" y2="-212.09" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-195.58" x2="45.72" y2="-195.58" width="0.1524" layer="91"/>
<label x="45.72" y="-195.58" size="1.778" layer="95" xref="yes"/>
<pinref part="PT1" gate="G$1" pin="E"/>
<junction x="22.86" y="-192.786"/>
<pinref part="PT2" gate="G$1" pin="E"/>
<junction x="43.18" y="-192.786"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="-53.34" y1="-20.32" x2="-50.8" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="20"/>
<label x="-53.34" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="76.2" y1="-193.04" x2="76.2" y2="-195.326" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-195.326" x2="76.2" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-193.04" x2="96.52" y2="-195.326" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-195.326" x2="96.52" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-198.12" x2="96.52" y2="-198.12" width="0.1524" layer="91"/>
<junction x="96.52" y="-198.12"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-214.63" x2="96.52" y2="-215.9" width="0.1524" layer="91"/>
<junction x="96.52" y="-214.63"/>
<wire x1="96.52" y1="-198.12" x2="96.52" y2="-214.63" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-198.12" x2="99.06" y2="-198.12" width="0.1524" layer="91"/>
<label x="99.06" y="-198.12" size="1.778" layer="95" xref="yes"/>
<pinref part="PT3" gate="G$1" pin="E"/>
<junction x="76.2" y="-195.326"/>
<pinref part="PT4" gate="G$1" pin="E"/>
<junction x="96.52" y="-195.326"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<wire x1="-35.56" y1="-17.78" x2="-33.02" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="21"/>
<label x="-33.02" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="134.62" y1="-193.04" x2="134.62" y2="-195.326" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-195.326" x2="134.62" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-193.04" x2="154.94" y2="-195.326" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-195.326" x2="154.94" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-198.12" x2="154.94" y2="-198.12" width="0.1524" layer="91"/>
<junction x="154.94" y="-198.12"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="154.94" y1="-214.63" x2="154.94" y2="-215.9" width="0.1524" layer="91"/>
<junction x="154.94" y="-214.63"/>
<wire x1="154.94" y1="-198.12" x2="154.94" y2="-214.63" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-198.12" x2="157.48" y2="-198.12" width="0.1524" layer="91"/>
<label x="157.48" y="-198.12" size="1.778" layer="95" xref="yes"/>
<pinref part="PT5" gate="G$1" pin="E"/>
<junction x="134.62" y="-195.326"/>
<pinref part="PT6" gate="G$1" pin="E"/>
<junction x="154.94" y="-195.326"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<wire x1="-53.34" y1="-17.78" x2="-50.8" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="22"/>
<label x="-53.34" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="187.96" y1="-195.58" x2="187.96" y2="-197.866" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-197.866" x2="187.96" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-195.58" x2="208.28" y2="-197.866" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-197.866" x2="208.28" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-200.66" x2="208.28" y2="-200.66" width="0.1524" layer="91"/>
<junction x="208.28" y="-200.66"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="208.28" y1="-217.17" x2="208.28" y2="-218.44" width="0.1524" layer="91"/>
<junction x="208.28" y="-217.17"/>
<wire x1="208.28" y1="-200.66" x2="208.28" y2="-217.17" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-200.66" x2="210.82" y2="-200.66" width="0.1524" layer="91"/>
<label x="210.82" y="-200.66" size="1.778" layer="95" xref="yes"/>
<pinref part="PT7" gate="G$1" pin="E"/>
<junction x="187.96" y="-197.866"/>
<pinref part="PT8" gate="G$1" pin="E"/>
<junction x="208.28" y="-197.866"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<wire x1="-35.56" y1="-15.24" x2="-33.02" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="23"/>
<label x="-33.02" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="246.38" y1="-195.58" x2="246.38" y2="-197.866" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-197.866" x2="246.38" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-195.58" x2="266.7" y2="-197.866" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-197.866" x2="266.7" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-200.66" x2="266.7" y2="-200.66" width="0.1524" layer="91"/>
<junction x="266.7" y="-200.66"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="266.7" y1="-217.17" x2="266.7" y2="-218.44" width="0.1524" layer="91"/>
<junction x="266.7" y="-217.17"/>
<wire x1="266.7" y1="-200.66" x2="266.7" y2="-217.17" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-200.66" x2="269.24" y2="-200.66" width="0.1524" layer="91"/>
<label x="269.24" y="-200.66" size="1.778" layer="95" xref="yes"/>
<pinref part="PT9" gate="G$1" pin="E"/>
<junction x="246.38" y="-197.866"/>
<pinref part="PT10" gate="G$1" pin="E"/>
<junction x="266.7" y="-197.866"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<wire x1="-53.34" y1="-15.24" x2="-50.8" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="24"/>
<label x="-53.34" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="299.72" y1="-198.12" x2="299.72" y2="-200.406" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-200.406" x2="299.72" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-198.12" x2="320.04" y2="-200.406" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-200.406" x2="320.04" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-203.2" x2="320.04" y2="-203.2" width="0.1524" layer="91"/>
<junction x="320.04" y="-203.2"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="320.04" y1="-219.71" x2="320.04" y2="-220.98" width="0.1524" layer="91"/>
<junction x="320.04" y="-219.71"/>
<wire x1="320.04" y1="-203.2" x2="320.04" y2="-219.71" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-203.2" x2="322.58" y2="-203.2" width="0.1524" layer="91"/>
<label x="322.58" y="-203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="PT11" gate="G$1" pin="E"/>
<junction x="299.72" y="-200.406"/>
<pinref part="PT12" gate="G$1" pin="E"/>
<junction x="320.04" y="-200.406"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<wire x1="-35.56" y1="-12.7" x2="-33.02" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="25"/>
<label x="-33.02" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="358.14" y1="-198.12" x2="358.14" y2="-200.406" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-200.406" x2="358.14" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-198.12" x2="378.46" y2="-200.406" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-200.406" x2="378.46" y2="-203.2" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-203.2" x2="378.46" y2="-203.2" width="0.1524" layer="91"/>
<junction x="378.46" y="-203.2"/>
<wire x1="378.46" y1="-219.71" x2="378.46" y2="-203.2" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="378.46" y1="-219.71" x2="378.46" y2="-220.98" width="0.1524" layer="91"/>
<junction x="378.46" y="-219.71"/>
<wire x1="378.46" y1="-203.2" x2="381" y2="-203.2" width="0.1524" layer="91"/>
<label x="383.54" y="-203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="PT13" gate="G$1" pin="E"/>
<junction x="358.14" y="-200.406"/>
<pinref part="PT14" gate="G$1" pin="E"/>
<junction x="378.46" y="-200.406"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<wire x1="-53.34" y1="-12.7" x2="-50.8" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="26"/>
<label x="-53.34" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="411.48" y1="-200.66" x2="411.48" y2="-202.946" width="0.1524" layer="91"/>
<wire x1="411.48" y1="-202.946" x2="411.48" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-200.66" x2="431.8" y2="-202.946" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-202.946" x2="431.8" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="411.48" y1="-205.74" x2="431.8" y2="-205.74" width="0.1524" layer="91"/>
<junction x="431.8" y="-205.74"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="431.8" y1="-222.25" x2="431.8" y2="-223.52" width="0.1524" layer="91"/>
<junction x="431.8" y="-222.25"/>
<wire x1="431.8" y1="-205.74" x2="431.8" y2="-222.25" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-205.74" x2="434.34" y2="-205.74" width="0.1524" layer="91"/>
<label x="434.34" y="-205.74" size="1.778" layer="95" xref="yes"/>
<pinref part="PT15" gate="G$1" pin="E"/>
<junction x="411.48" y="-202.946"/>
<pinref part="PT16" gate="G$1" pin="E"/>
<junction x="431.8" y="-202.946"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<wire x1="-35.56" y1="-10.16" x2="-33.02" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="27"/>
<label x="-33.02" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-109.22" x2="-266.7" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$5"/>
<wire x1="-266.7" y1="-109.22" x2="-264.16" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-266.7" y="-109.22"/>
<label x="-264.16" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="VALVE1" class="0">
<segment>
<wire x1="-53.34" y1="-10.16" x2="-50.8" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="28"/>
<label x="-53.34" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-60.96" y1="-127" x2="-73.66" y2="-127" width="0.1524" layer="91"/>
<label x="-78.74" y="-127" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-73.66" y1="-127" x2="-78.74" y2="-127" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-127" x2="-109.22" y2="-128.27" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-128.27" x2="-109.22" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-132.08" x2="-73.66" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-132.08" x2="-73.66" y2="-127" width="0.1524" layer="91"/>
<junction x="-73.66" y="-127"/>
<pinref part="IR17" gate="G$1" pin="-"/>
<junction x="-109.22" y="-128.27"/>
</segment>
<segment>
<wire x1="-99.06" y1="-96.52" x2="-99.06" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="P$3"/>
<wire x1="-99.06" y1="-99.06" x2="-99.06" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-99.06" y="-99.06"/>
<label x="-99.06" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-93.98" y1="-76.2" x2="-93.98" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="P$2"/>
<wire x1="-93.98" y1="-73.66" x2="-93.98" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-93.98" y="-73.66"/>
<label x="-93.98" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="VALVE2" class="0">
<segment>
<wire x1="-35.56" y1="-7.62" x2="-33.02" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="29"/>
<label x="-33.02" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-60.96" y1="-144.78" x2="-73.66" y2="-144.78" width="0.1524" layer="91"/>
<label x="-78.74" y="-144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-73.66" y1="-144.78" x2="-78.74" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-147.32" x2="-109.22" y2="-148.59" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-148.59" x2="-109.22" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-152.4" x2="-73.66" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-152.4" x2="-73.66" y2="-144.78" width="0.1524" layer="91"/>
<junction x="-73.66" y="-144.78"/>
<pinref part="IR18" gate="G$1" pin="-"/>
<junction x="-109.22" y="-148.59"/>
</segment>
<segment>
<wire x1="-55.88" y1="-76.2" x2="-55.88" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="CON6" gate="G$1" pin="P$2"/>
<wire x1="-55.88" y1="-73.66" x2="-55.88" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-55.88" y="-73.66"/>
<label x="-55.88" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-60.96" y1="-96.52" x2="-60.96" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="CON6" gate="G$1" pin="P$3"/>
<wire x1="-60.96" y1="-99.06" x2="-60.96" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-60.96" y="-99.06"/>
<label x="-60.96" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG45" class="0">
<segment>
<wire x1="-53.34" y1="-22.86" x2="-50.8" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="18"/>
<label x="-53.34" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-203.2" y1="-246.38" x2="-204.47" y2="-246.38" width="0.1524" layer="91"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="-204.47" y1="-246.38" x2="-210.82" y2="-246.38" width="0.1524" layer="91"/>
<junction x="-204.47" y="-246.38"/>
<label x="-210.82" y="-246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DG46" class="0">
<segment>
<wire x1="-35.56" y1="-5.08" x2="-33.02" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="31"/>
<label x="-33.02" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-111.76" x2="-266.7" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$6"/>
<wire x1="-266.7" y1="-111.76" x2="-264.16" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-266.7" y="-111.76"/>
<label x="-264.16" y="-111.76" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DG47" class="0">
<segment>
<wire x1="-53.34" y1="-5.08" x2="-50.8" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="32"/>
<label x="-53.34" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-114.3" x2="-266.7" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="P$7"/>
<wire x1="-266.7" y1="-114.3" x2="-264.16" y2="-114.3" width="0.1524" layer="91"/>
<junction x="-266.7" y="-114.3"/>
<label x="-264.16" y="-114.3" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DG49" class="0">
<segment>
<wire x1="-53.34" y1="-7.62" x2="-50.8" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="30"/>
<label x="-53.34" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-200.66" y1="-200.66" x2="-201.93" y2="-200.66" width="0.1524" layer="91"/>
<pinref part="R69" gate="G$1" pin="1"/>
<wire x1="-201.93" y1="-200.66" x2="-208.28" y2="-200.66" width="0.1524" layer="91"/>
<junction x="-201.93" y="-200.66"/>
<label x="-208.28" y="-200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="-109.22" y1="-121.92" x2="-109.22" y2="-120.65" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-120.65" x2="-109.22" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-119.38" x2="-110.49" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="-110.49" y1="-119.38" x2="-111.76" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-110.49" y="-119.38"/>
<pinref part="IR17" gate="G$1" pin="+"/>
<junction x="-109.22" y="-120.65"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="-111.76" y1="-139.7" x2="-110.49" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="-110.49" y1="-139.7" x2="-109.22" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-139.7" x2="-109.22" y2="-140.97" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="2"/>
<junction x="-110.49" y="-139.7"/>
<pinref part="IR18" gate="G$1" pin="+"/>
<wire x1="-109.22" y1="-140.97" x2="-109.22" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-109.22" y="-140.97"/>
</segment>
</net>
<net name="LOAD4" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="D"/>
<wire x1="-187.96" y1="-195.58" x2="-187.96" y2="-190.5" width="0.1524" layer="91"/>
<label x="-187.96" y="-190.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-91.44" x2="-266.7" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$8"/>
<wire x1="-266.7" y1="-91.44" x2="-264.16" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-266.7" y="-91.44"/>
<label x="-264.16" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="G"/>
<wire x1="-193.04" y1="-200.66" x2="-194.31" y2="-200.66" width="0.1524" layer="91"/>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="-194.31" y1="-200.66" x2="-195.58" y2="-200.66" width="0.1524" layer="91"/>
<junction x="-194.31" y="-200.66"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="-193.04" y1="-119.38" x2="-193.04" y2="-120.65" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="-193.04" y1="-120.65" x2="-193.04" y2="-128.27" width="0.1524" layer="91"/>
<junction x="-193.04" y="-120.65"/>
<pinref part="D30" gate="G$1" pin="+"/>
<wire x1="-193.04" y1="-128.27" x2="-193.04" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-193.04" y="-128.27"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-175.26" y1="-116.84" x2="-175.26" y2="-118.11" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="-118.11" x2="-175.26" y2="-125.73" width="0.1524" layer="91"/>
<junction x="-175.26" y="-118.11"/>
<pinref part="D31" gate="G$1" pin="+"/>
<wire x1="-175.26" y1="-125.73" x2="-175.26" y2="-127" width="0.1524" layer="91"/>
<junction x="-175.26" y="-125.73"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="20.32" y1="-53.34" x2="20.32" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-54.61" x2="20.32" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-60.96" x2="30.48" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="Q9" gate="G$1" pin="D"/>
<wire x1="30.48" y1="-60.96" x2="30.48" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<junction x="20.32" y="-54.61"/>
<wire x1="30.48" y1="-53.34" x2="30.48" y2="-54.61" width="0.1524" layer="91"/>
<junction x="30.48" y="-60.96"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="30.48" y1="-54.61" x2="30.48" y2="-60.96" width="0.1524" layer="91"/>
<junction x="30.48" y="-54.61"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="76.2" y1="-53.34" x2="76.2" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-54.61" x2="76.2" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-60.96" x2="86.36" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-60.96" x2="86.36" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-54.61" x2="86.36" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-60.96" x2="86.36" y2="-76.2" width="0.1524" layer="91"/>
<junction x="86.36" y="-60.96"/>
<pinref part="R18" gate="G$1" pin="1"/>
<junction x="76.2" y="-54.61"/>
<pinref part="R19" gate="G$1" pin="1"/>
<junction x="86.36" y="-54.61"/>
<pinref part="Q10" gate="G$1" pin="D"/>
<wire x1="86.36" y1="-76.2" x2="86.36" y2="-78.74" width="0.1524" layer="91"/>
<junction x="86.36" y="-76.2"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="132.08" y1="-53.34" x2="132.08" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-54.61" x2="132.08" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-60.96" x2="142.24" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-60.96" x2="142.24" y2="-54.61" width="0.1524" layer="91"/>
<pinref part="Q11" gate="G$1" pin="D"/>
<wire x1="142.24" y1="-54.61" x2="142.24" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-60.96" x2="142.24" y2="-76.2" width="0.1524" layer="91"/>
<junction x="142.24" y="-60.96"/>
<pinref part="R20" gate="G$1" pin="1"/>
<junction x="132.08" y="-54.61"/>
<pinref part="R21" gate="G$1" pin="1"/>
<junction x="142.24" y="-54.61"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="187.96" y1="-53.34" x2="187.96" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-54.61" x2="187.96" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-58.42" x2="198.12" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-58.42" x2="198.12" y2="-54.61" width="0.1524" layer="91"/>
<pinref part="Q12" gate="G$1" pin="D"/>
<wire x1="198.12" y1="-54.61" x2="198.12" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-58.42" x2="198.12" y2="-76.2" width="0.1524" layer="91"/>
<junction x="198.12" y="-58.42"/>
<pinref part="R22" gate="G$1" pin="1"/>
<junction x="187.96" y="-54.61"/>
<pinref part="R23" gate="G$1" pin="1"/>
<junction x="198.12" y="-54.61"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="243.84" y1="-53.34" x2="243.84" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-54.61" x2="243.84" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-63.5" x2="254" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="254" y1="-63.5" x2="254" y2="-54.61" width="0.1524" layer="91"/>
<pinref part="Q13" gate="G$1" pin="D"/>
<wire x1="254" y1="-54.61" x2="254" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="254" y1="-63.5" x2="254" y2="-76.2" width="0.1524" layer="91"/>
<junction x="254" y="-63.5"/>
<pinref part="R24" gate="G$1" pin="1"/>
<junction x="243.84" y="-54.61"/>
<pinref part="R25" gate="G$1" pin="1"/>
<junction x="254" y="-54.61"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="299.72" y1="-53.34" x2="299.72" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-54.61" x2="299.72" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-60.96" x2="309.88" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-60.96" x2="309.88" y2="-54.61" width="0.1524" layer="91"/>
<pinref part="Q14" gate="G$1" pin="D"/>
<wire x1="309.88" y1="-54.61" x2="309.88" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="309.88" y1="-60.96" x2="309.88" y2="-76.2" width="0.1524" layer="91"/>
<junction x="309.88" y="-60.96"/>
<pinref part="R26" gate="G$1" pin="1"/>
<junction x="299.72" y="-54.61"/>
<pinref part="R27" gate="G$1" pin="1"/>
<junction x="309.88" y="-54.61"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="355.6" y1="-53.34" x2="355.6" y2="-54.61" width="0.1524" layer="91"/>
<wire x1="355.6" y1="-54.61" x2="355.6" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="355.6" y1="-60.96" x2="365.76" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-60.96" x2="365.76" y2="-54.61" width="0.1524" layer="91"/>
<pinref part="Q15" gate="G$1" pin="D"/>
<wire x1="365.76" y1="-54.61" x2="365.76" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-60.96" x2="365.76" y2="-76.2" width="0.1524" layer="91"/>
<junction x="365.76" y="-60.96"/>
<pinref part="R28" gate="G$1" pin="1"/>
<junction x="355.6" y="-54.61"/>
<pinref part="R29" gate="G$1" pin="1"/>
<junction x="365.76" y="-54.61"/>
</segment>
</net>
<net name="LOAD1" class="0">
<segment>
<pinref part="Q19" gate="G$1" pin="D"/>
<wire x1="-195.58" y1="-335.28" x2="-195.58" y2="-330.2" width="0.1524" layer="91"/>
<label x="-195.58" y="-330.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-83.82" x2="-266.7" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$5"/>
<wire x1="-266.7" y1="-83.82" x2="-264.16" y2="-83.82" width="0.1524" layer="91"/>
<junction x="-266.7" y="-83.82"/>
<label x="-264.16" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="Q17" gate="G$1" pin="G"/>
<wire x1="-195.58" y1="-246.38" x2="-196.85" y2="-246.38" width="0.1524" layer="91"/>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="-196.85" y1="-246.38" x2="-198.12" y2="-246.38" width="0.1524" layer="91"/>
<junction x="-196.85" y="-246.38"/>
</segment>
</net>
<net name="LOAD2" class="0">
<segment>
<pinref part="Q18" gate="G$1" pin="D"/>
<wire x1="-193.04" y1="-289.56" x2="-193.04" y2="-284.48" width="0.1524" layer="91"/>
<label x="-193.04" y="-284.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-86.36" x2="-266.7" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$6"/>
<wire x1="-266.7" y1="-86.36" x2="-264.16" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-266.7" y="-86.36"/>
<label x="-264.16" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="Q18" gate="G$1" pin="G"/>
<wire x1="-198.12" y1="-294.64" x2="-199.39" y2="-294.64" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="-199.39" y1="-294.64" x2="-200.66" y2="-294.64" width="0.1524" layer="91"/>
<junction x="-199.39" y="-294.64"/>
</segment>
</net>
<net name="LOAD3" class="0">
<segment>
<pinref part="Q17" gate="G$1" pin="D"/>
<wire x1="-190.5" y1="-241.3" x2="-190.5" y2="-236.22" width="0.1524" layer="91"/>
<label x="-190.5" y="-236.22" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-269.24" y1="-88.9" x2="-266.7" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="P$7"/>
<wire x1="-266.7" y1="-88.9" x2="-264.16" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-266.7" y="-88.9"/>
<label x="-264.16" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="Q19" gate="G$1" pin="G"/>
<wire x1="-200.66" y1="-340.36" x2="-201.93" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="-201.93" y1="-340.36" x2="-203.2" y2="-340.36" width="0.1524" layer="91"/>
<junction x="-201.93" y="-340.36"/>
</segment>
</net>
<net name="10" class="0">
<segment>
<wire x1="132.08" y1="33.02" x2="132.08" y2="34.29" width="0.1524" layer="91"/>
<wire x1="132.08" y1="34.29" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="132.08" y1="38.1" x2="142.24" y2="38.1" width="0.1524" layer="91"/>
<wire x1="142.24" y1="38.1" x2="142.24" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="+"/>
<junction x="132.08" y="34.29"/>
<pinref part="D6" gate="G$1" pin="+"/>
<wire x1="142.24" y1="34.29" x2="142.24" y2="33.02" width="0.1524" layer="91"/>
<junction x="142.24" y="34.29"/>
<junction x="142.24" y="38.1"/>
<wire x1="142.24" y1="38.1" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
<label x="142.24" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="25.4" x2="-68.58" y2="25.4" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$15"/>
<label x="-68.58" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="11" class="0">
<segment>
<wire x1="187.96" y1="33.02" x2="187.96" y2="34.29" width="0.1524" layer="91"/>
<wire x1="187.96" y1="34.29" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="187.96" y1="38.1" x2="198.12" y2="38.1" width="0.1524" layer="91"/>
<wire x1="198.12" y1="38.1" x2="198.12" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="+"/>
<junction x="187.96" y="34.29"/>
<pinref part="D8" gate="G$1" pin="+"/>
<wire x1="198.12" y1="34.29" x2="198.12" y2="33.02" width="0.1524" layer="91"/>
<junction x="198.12" y="34.29"/>
<junction x="198.12" y="38.1"/>
<wire x1="198.12" y1="38.1" x2="198.12" y2="43.18" width="0.1524" layer="91"/>
<label x="198.12" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="27.94" x2="-68.58" y2="27.94" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$14"/>
<label x="-68.58" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="12" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="+"/>
<wire x1="254" y1="34.29" x2="254" y2="33.02" width="0.1524" layer="91"/>
<junction x="254" y="34.29"/>
<wire x1="254" y1="34.29" x2="254" y2="38.1" width="0.1524" layer="91"/>
<wire x1="254" y1="38.1" x2="254" y2="43.18" width="0.1524" layer="91"/>
<wire x1="254" y1="38.1" x2="243.84" y2="38.1" width="0.1524" layer="91"/>
<wire x1="243.84" y1="38.1" x2="243.84" y2="34.29" width="0.1524" layer="91"/>
<junction x="254" y="38.1"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="243.84" y1="34.29" x2="243.84" y2="33.02" width="0.1524" layer="91"/>
<junction x="243.84" y="34.29"/>
<label x="254" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="30.48" x2="-68.58" y2="30.48" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$13"/>
<label x="-68.58" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="13" class="0">
<segment>
<wire x1="299.72" y1="33.02" x2="299.72" y2="34.29" width="0.1524" layer="91"/>
<wire x1="299.72" y1="34.29" x2="299.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="299.72" y1="38.1" x2="309.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="309.88" y1="38.1" x2="309.88" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="+"/>
<junction x="299.72" y="34.29"/>
<pinref part="D12" gate="G$1" pin="+"/>
<wire x1="309.88" y1="34.29" x2="309.88" y2="33.02" width="0.1524" layer="91"/>
<junction x="309.88" y="34.29"/>
<junction x="309.88" y="38.1"/>
<wire x1="309.88" y1="38.1" x2="309.88" y2="43.18" width="0.1524" layer="91"/>
<label x="309.88" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="33.02" x2="-68.58" y2="33.02" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$12"/>
<label x="-68.58" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="14" class="0">
<segment>
<wire x1="355.6" y1="33.02" x2="355.6" y2="34.29" width="0.1524" layer="91"/>
<wire x1="355.6" y1="34.29" x2="355.6" y2="38.1" width="0.1524" layer="91"/>
<wire x1="355.6" y1="38.1" x2="365.76" y2="38.1" width="0.1524" layer="91"/>
<wire x1="365.76" y1="38.1" x2="365.76" y2="34.29" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="+"/>
<junction x="355.6" y="34.29"/>
<pinref part="D14" gate="G$1" pin="+"/>
<wire x1="365.76" y1="34.29" x2="365.76" y2="33.02" width="0.1524" layer="91"/>
<junction x="365.76" y="34.29"/>
<junction x="365.76" y="38.1"/>
<wire x1="365.76" y1="38.1" x2="365.76" y2="43.18" width="0.1524" layer="91"/>
<label x="365.76" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="35.56" x2="-68.58" y2="35.56" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$11"/>
<label x="-68.58" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="15" class="0">
<segment>
<wire x1="411.48" y1="33.02" x2="411.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="411.48" y1="38.1" x2="421.64" y2="38.1" width="0.1524" layer="91"/>
<wire x1="421.64" y1="38.1" x2="421.64" y2="43.18" width="0.1524" layer="91"/>
<label x="421.64" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="D15" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="-71.12" y1="38.1" x2="-68.58" y2="38.1" width="0.1524" layer="91"/>
<pinref part="BUSS_RES1" gate="G$1" pin="P$10"/>
<label x="-68.58" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>

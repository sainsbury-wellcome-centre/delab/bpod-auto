function [dff_signal, pp_ts] = fp_preprocess_dff(file_path)

%% 
% Inputs
% file_path                  Path to photometry data to analyse

% Outputs
% pp_figure                  effect of each step of preprocessing in figure
% pp_data                    Preprocessed data
% pp_ts                      FP timestamps for each datapoint, in sec

%% import data
data_struct = utils.import_ppd(file_path);
data_size = length(data_struct.analog_1);
data_range = 1:data_size;
ch465 = data_struct.analog_1(data_range);
ch405 = data_struct.analog_2(data_range);
time_window = data_struct.time';
time_window = time_window(data_range)/1000;
fs = data_struct.sampling_rate;

%sessionDuration = data_struct.time(end)/1000;      % (s)

figure;

% 0. plot raw signal
subplot(5,1,1); % Creates a subplot layout with 5 rows and 1 column, selects the first subplot
plot(time_window,ch465,'Color','g');
hold on;
plot(time_window,ch405,'Color','r');
hold off;
xlabel('Time (s)');
ylabel('Analog (V)');
title('Raw Data');

% 1. median filter (visually inspect whether have noise spikes first)
% if yes, filter should be 5x length of the noise spikes
med_filt_ch465 = medfilt1(ch465, 5);
med_filt_ch405 = medfilt1(ch405, 5);
%med_filt_ch465 = ch465;
%med_filt_ch405 = ch405;

subplot(5,1,2); % Selects the second subplot
plot(time_window,med_filt_ch465,'Color','g');
hold on;
plot(time_window,med_filt_ch405,'Color','r');
hold off;
xlabel('Time (s)');
ylabel('Analog (V)');
title('After Median Filter');


% 2. low-pass filter
d1 = designfilt("lowpassiir", ...
    FilterOrder=2, ...
    HalfPowerFrequency=10, ...
    SampleRate=fs, ...
    DesignMethod="butter");
lowpass_ch465 = filtfilt(d1,med_filt_ch465);
lowpass_ch405 = filtfilt(d1,med_filt_ch405);

subplot(5,1,3); % Selects the second subplot
plot(time_window,lowpass_ch465,'Color','cyan');
hold on;
plot(time_window,lowpass_ch405,'Color','yellow');
plot(time_window,med_filt_ch465,'Color','g');
plot(time_window,med_filt_ch405,'Color','r');
hold off;
xlabel('Time (s)');
ylabel('Analog (V)');
title('After Low-pass Filter');
% 
% signal = lowpass_ch465;
% control = lowpass_ch405;
% pp_ts = time_window;



% try something else:
dF_F = lowpass_ch465 ./ lowpass_ch405;

subplot(5,1,4);
plot(time_window, dF_F, 'Color', 'black');
xlabel('Time (s)');
ylabel('dF/F');
title('just dF/F');

% photobleaching + movement + normalization
betas = polyfit(lowpass_ch405, lowpass_ch465, 1);
fitted_control = betas(1) .* lowpass_ch405 + betas(2);
final = (lowpass_ch465 - fitted_control) ./ fitted_control;

subplot(5,1,5);
plot(time_window, final, 'Color','r');
xlabel('Time (s)');
ylabel('dF/F');
title('Current Final Data');

% csvwrite('fp0703.csv', final(:));

%pp_figure = figure;
dff_signal = final;
pp_ts = time_window;

% refer to Thomas Akam's photometry primer and their preprocessing code below
% https://github.com/ThomasAkam/photometry_preprocessing/blob/master/Photometry%20data%20preprocessing.ipynb

% 
% 
% 
% % 3. slow-/bleaching- correction
% % Assess bleaching
% % Photobleaching happens aggressively at first and then at a slow steady
% % rate: https://www.nature.com/articles/s41586-023-05828-9/figures/9.
% % Here we will look for fast and slow bleaching.
% % We can use this to inform our led power.
% lrm465 = fitlm(data_struct.time/1000,data_struct.analog_1);
% lrm405 = fitlm(data_struct.time/1000,data_struct.analog_2);
% bleachSlow465 = lrm465.Coefficients.Estimate(2);
% bleachSlow405 = lrm405.Coefficients.Estimate(2);
% 
% % Slow correction
% winTime = 50;                                   % Window time (s)
% windowLength = winTime*fs;      % Finds window length in number of samples
% slowCorrected465 = slowCorrect(lowpass_ch465,windowLength,5);
% slowCorrected405 = slowCorrect(lowpass_ch405,windowLength,5);
% 
% nexttile
% plot(data_struct.time/1000,slowCorrected465,'Color','g')
% hold on
% plot(data_struct.time/1000,slowCorrected405,'Color','r')
% xlabel('Time (s)')
% ylabel('Bleaching-corrected signal (V)')
% title('Slow Correction')
% leg = legend('465','405');
% title(leg,'Excitation wavelength (nm)');
% 
% % Motion correction
% % transforming the slow-corrected autofluorescence signal to fit the
% % slow-corrected GCaMP6s signal via linear regression,
% % and then subtracting this fitted control signal from the GCaMP6s signal.
% lrm = fitlm(slowCorrected465,slowCorrected405);
% slowCorrected405fitted = slowCorrected465*lrm.Coefficients.Estimate(2) + lrm.Coefficients.Estimate(1);
% pp_struct = slowCorrected465 - slowCorrected405fitted;
% pp_struct = pp_struct';
% 
% nexttile
% plot(data_struct.time/1000,pp_struct,'Color','g')
% xlabel('Time (s)')
% ylabel('Motion + bleaching corrected signal')
% title('Motion Correction')
% leg = legend('465');
% title(leg,'Excitation wavelength (nm)');
% hold off
% 
% channels = {'465nm', '405nm'};
% lgd = legend(channels);
% lgd.Title.String = 'Excitation wavelength (nm)';
% set(lgd, 'Position', [1,1,1,1]); % Adjust the position as needed
% 
% % % Calculate ΔF/F0
% % % ΔF/F0 was calculated as (F–F0)/F0, where F0 is the mode of fluorescence F 
% % % over the entire session after slow correction and motion correction.
% % % Usually we will not do this in this function, but use a baseline F0 in
% % % reference to the behaviour data...
% % F0 = mode(mc465);
% % dFoF0 = (mc465 - F0) / F0;
% % 
% % nexttile
% % plot(fpData.time/1000,dFoF0,'Color','#00b359')
% % xlabel('Time (s)')
% % ylabel('ΔF/F')
% % title('GCaMP6s')
% 
% function slowCorrected = slowCorrect(data,windowLength,percentile)
% 
% % Inputs
% % data:             Signal readout
% % windowLength:     Length of the window (window time * sample rate)
% % percentile:       The %ile to use when subtracting mov avg from signal
% 
% winHalf = floor(windowLength/2);
% slowCorrected = zeros(length(data),1);
% for i = 1:length(data)
%     toStart = i-1;
%     toEnd = length(data)-i;
%     if winHalf>toStart && winHalf>toEnd
%         slowCorrected(i) = data(i) - prctile(data,percentile);
%     elseif winHalf>toStart
%         slowCorrected(i) = data(i) - prctile(data(1:i+winHalf),percentile);
%     elseif winHalf>toEnd
%         slowCorrected(i) = data(i) - prctile(data(i-winHalf:end),percentile);
%     else
%         slowCorrected(i) = data(i) - prctile(data(i-winHalf:i+winHalf),percentile);
%     end
% end
% end
% 
% end

% connect to db
dbc = db.labdb.getConnection();

% create the fibertype if it doesn't exist in pho.fibertype yet
C = struct();
C.vendor = 'Doric'; % which company
C.ordercode = 'MFC_200/245-0.37_3mm_ZF1.25(G)_FLT'; % supplier item/ordercode by the company
dbc.saveData('pho.fibertype',C);

% create the fiber in pho.fibers
F = struct();
F.subjid = 'COS-M-0090';
F.fibertypeid = 1; % the fibertypeid of this type of fiber (determined by its order code)
F.n_channels = 1; % always 1 for fiber
F.target_location = 'right NAc';
F.target_AP = +1.1; % anterior is positive
F.target_ML = +0.95; % right is positive
F.target_DV = -3.5; % fiber tip coordinates, always negative
F.made_by = 'SUN.Cong'; % gitlab account of the surgeon
F.made_on = '2024-06-14'; % fiber implantation surgery date
F.notes = 'virus: 300nl 1:2 diluted dLight1.1 4x10^12gc/ml from MSJ_lab addgene injected at ML0.95 AP1.1 DV-3.8. vertical fiber 300um above injection'; % information about virus, surgery, etc.
F.probename = 'COS-M-0090_2'; % fiber name, always subjid_incrementFromIntOne (note the dash and underscore) % left is odd, right is even. 1 is the first left fiber, 2 is the first right fiber, 3 is the second left fiber etc.
F.tilt_angle = ''; % if you insert with an angle, degrees relative to vertical, clockwise is positive, 10~20 usually
F.tilt_direction = ''; % if you insert with an angle, degrees relative to nose, clockwise is positive, 10~20 usually
dbc.saveData('pho.fibers',F);
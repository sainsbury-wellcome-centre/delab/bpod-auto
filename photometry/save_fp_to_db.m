function M_struct = save_fp_to_db(varargin)
    % this is the one and only function that does everything to save fp data to db
    % it's a wrapper function that calls other functions to
    % check/sync ts, preprocess fp data, adjust pyPhotometry ts to bpod ts
    % then save to db.pho.samples and db.pho.sampledata
    %
    % to run this, you need bpod stuff, bpod-auto/Analysis, bpod-auto/photometry, elutils in the path
    % add this by:
    % cd ~/repos/bpod-auto;
    % add_bpod_path;
    % addpath(genpath('./Analysis'));
    % addpath(genpath('./photometry'));
    % addpath(genpath('~/repos/elutils'));

    % you can run this from within the session folder, or specify the session folder as an input
    % optional inputs:
    % session_folder: path to the session folder, which might include one or more recording ppd files
    % comment: comment for this session, e.g. 'COS-M-0044_1: 21mA ch405, 100mA ch465'
    % 
    % example:
    % save_fp_to_db()
    % save_fp_to_db('session_folder', 'COS-M-0066_2024-05-24_22222')

    % SUN.Cong, 2024.07

    %%

    dbc = db.labdb.getConnection();

    inpd = @utils.inputordefault;
    [session_folder, varargin]=inpd('session_folder', pwd, varargin);

    [~,folder_name,~] = fileparts(session_folder);
    parts = split(folder_name, '_');
    sessid = str2double(parts{3});

    %% use get_sess_pho_data to sync and return processed synced photometry data
    M_struct = get_sess_pho_data('session_folder', session_folder);
    fibers = fieldnames(M_struct);

    %% save pho data to ogma and upload URL to db
    for f = 1:length(fieldnames(M_struct))
        % each fiber is saved separately since they might not have the same ts
        partss = split(fibers{f}, '_');
        fiberid = str2double(partss{2});
        M = M_struct.(fibers{f});

        %% save to db -- only keep this part, use get_sess_pho_data for before
        [comment, varargin] = inpd('comment','',varargin);
        [savedb,~] = inpd('save',true,varargin);
        if savedb
            % first generate or query sampleid for this photometry data

            % check whether this sessid+fiberid is in pho.samples
            exist_sampleid = dbc.get('select sampleid from pho.samples where sessid = %d and fiberid = %d',{sessid, fiberid});

            % if not, save the fiberid+sessid combination to pho.samples (this requires that you have created the fiber entry in pho.fibers)
            if isempty(exist_sampleid)
                sp = struct();      
                sp.sessid = sessid;
                sp.fiberid = fiberid;
                sp.comment = comment;
                % this will only create the sessid-fiberid entry in pho.samples if this fiber is actually used in this session!!
                fprintf(1,"Insert a new sample in pho.samples table\n");
                dbc.saveData('pho.samples',sp);
            else
                fprintf(1,"This sessid+fiberid is already in the pho.samples table! Check it.\n");
                % if return, then it requires pho.samples and
                % pho.sampledata always inserted together
                %return;
            end

            % then get the sampleid
            sampleid = dbc.get('select sampleid from pho.samples where sessid = %d and fiberid = %d',{sessid, fiberid}); 

            % then save the pre-processed data as a parquet file (do this on hpc to save it to ogma)
            ts = M(:,1);
            background = M(:,2);
            signal = M(:,3);
            t = table(ts(:), background(:), signal(:), ...
                'VariableNames',{'ts','bg','signal'});
            %all_pho_data_folder = '/Users/cong/data/photometry_data';
            % save the pre-processed parquet file to pho recording NUC's data folder
            % so as to rsync to ogma later
            all_pho_data_folder = '/home/delab/code/data';
            filename = strjoin({num2str(sampleid), '.parq'},'');
            save_full_path = fullfile(all_pho_data_folder, 'synced_data', filename);
            parquetwrite(save_full_path, t);
            % then you can use the sampleid to get data files from ogma or wherever you saved it
                   
            % %t = table(ats(:), pp_data(:));
            % %parquetwrite('~/test.parq', t);
            % %D.data = '~/test.parq';
            % %tp = parquetread('~/test.parq');
            % %isequaln(t, tp);
            % D.sampleid = sampleid;
            % D.data = save_full_path;
            
            % % then save the pre-processed data to pho.sampledata if it's not already there
            % saved_before = dbc.get('select sampleid from pho.sampledata where sampleid = %d',{sampleid});
            
            % if ~isempty(saved_before)
            %     fprintf(1,"This sampleid %d is already in the pho.sampledata table!\n", sampleid);
                
            %     ButtonName = questdlg('Do you want to reupload the sampledata for this sampleid?', ...
            %         'Reupload', ...
            %         'Yes', 'No', 'Yes');
            %     if strcmp(ButtonName,'No')
            %         return;
            %     else
            %         dbc.saveData('pho.sampledata',D);
            %     end
            % else
            %     dbc.saveData('pho.sampledata',D);
            % end
        end
    end
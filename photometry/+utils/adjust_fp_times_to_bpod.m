function ats = adjust_fp_times_to_bpod(ts,M)
% ats = adjustts(ts,M)
% Takes the timestamps ts, and the structure returned from sync_fp_bpod_ts
% and returns timestamps which are interpolated to be in bpod time.

% This line finds which timestamps are in which trials,
% by returning which are the last timestamps still in each trial.
ind = stats.qfind(ts, M(:,2)); % The 2nd column is FP times.

ats = nan+ts; % initialize the adjusted TS to be a nan array same size as ts.

for tx = 1:numel(ind) % loop through the last FP timestamps of each trial
    if tx == 1
    % The first trial is a special case, we will only shift, not scale, 
    % since we don't have a bookend.  
        ats(1:ind(tx)) = ts(1:ind(tx)) - (M(tx,2)-M(tx,3));
        % the 2nd column is FP ts, the 3rd column is Bpod ts
        % for the first trial, (FP ts - Bpod ts) is basically just:
        % how long after recording starting did we start bpod protocol
        % then subtract this delay from every FP ts during the first trial
        % so the first several FP datapoint will have a negative bpod ts!!
    else
    % Find the slope and the intercept of a 2 point line (the sync_times
    % from one trial to the next) and use that to shift the FP times in
    % that segment.
        % what's the scaling of bpod ts relative to FP ts
        slope = (M(tx,3)-M(tx-1,3))/(M(tx,2)-M(tx-1,2)); % bpod / FP
        % after scaling FP ts back to bpod ts, how much shift there still is
        intcpt = M(tx,3) - slope*M(tx,2);
        % then use the scaling and shift found
        % to adjust every ts within this trial --- ind(tx-1)+1:ind(tx)
        ats((ind(tx-1)+1):ind(tx)) = slope*ts((ind(tx-1)+1):ind(tx)) + intcpt;        
    end
end

ats(ind(tx)+1:end) = ts(ind(tx)+1:end) - (M(end,2)-M(end,3));
 

end
function M_struct = get_sess_pho_data(varargin)
    % this function syncs and preprocesses fp data and return it to you
    % so you can decide how to save it later, or use it for analysis
    % it's a wrapper function that calls other functions to
    % check/sync ts, preprocess fp data, adjust pyPhotometry ts to bpod ts
    %
    % to run this, you need bpod stuff, bpod-auto/Analysis, bpod-auto/photometry, elutils in the path
    % add this by:
    % cd ~/repos/bpod-auto;
    % add_bpod_path;
    % addpath(genpath('./Analysis'));
    % addpath(genpath('./photometry'));
    % addpath(genpath('~/repos/elutils'));

    % you can run this from within the session folder, or specify the session folder as an input
    % optional inputs:
    % session_folder: path to the session folder, which might include one or more recording ppd files
    % comment: comment for this session, e.g. 'COS-M-0044_1: 21mA ch405, 100mA ch465'
    % 
    % example:
    % get_sess_pho_data()
    % get_sess_pho_data('session_folder', 'COS-M-0066_2024-05-24_22222')

    % SUN.Cong, 2024.07

    dbc = db.labdb.getConnection();

    inpd = @utils.inputordefault;
    [session_folder, varargin]=inpd('session_folder', pwd, varargin);
    [output_mode, varargin]=inpd('output_mode', 'separate', varargin);
    cd(session_folder);

    [~,folder_name,~] = fileparts(session_folder);
    parts = split(folder_name, '_');
    sessid = str2double(parts{3});
    subjid = parts{1};

    session_files = dir(fullfile(session_folder,'*.ppd'));
    num_session_files = length(session_files);

    %% check files are correct in this session folder
    % check that all ppd files have the same subjid and unique fiber_withinsubjid_id and the fiber all has corresponding entry in pho.fibers
    
    assert(num_session_files > 0, "No ppd files found in the session folder. Check the path!");
    
    check_subjid = cell(num_session_files, 1);
    check_fiber_withinsubjid_id = cell(num_session_files, 1);
    check_fibername = cell(num_session_files, 1);
    fiberid_array = cell(num_session_files, 1);

    for fp = 1:num_session_files
        %file_path = fullfile(session_folder, session_files(fp).name);
        file_path = session_files(fp).name;
        subject_ID_info = utils.import_ppd(file_path).subject_ID;

        underscorePosition = strfind(subject_ID_info, '_');
        if ~isempty(underscorePosition)
            check_subjid{fp} = subject_ID_info(1:underscorePosition-1);
            assert(strcmp(subjid, check_subjid{fp}), "folder_name subjid part is not the same with the ppd files. Check it!");
            check_fiber_withinsubjid_id{fp} = subject_ID_info(underscorePosition+1:end);
            check_fibername{fp} = subject_ID_info;
            fiberid_array{fp} = dbc.get('select fiberid from pho.fibers where probename = "%s" and subjid = "%s"',{subject_ID_info, subjid});
            assert(~isempty(fiberid_array{fp}), 'probename "%s" for subjid "%s" not found in pho.fibers. Insert the entry first!', subject_ID_info, subjid);
        else
            check_subjid{fp} = subject_ID_info;
            fiberid_array{fp} = 0;
            error('wrong naming convention for the subject_ID. cannot find fiber info!');
        end
    end

    if num_session_files > 1
        assert(isequal(check_fiber_withinsubjid_id, unique(check_fiber_withinsubjid_id)), "fiber_withinsubjid_id is not unique. Check it!");
    end


        
    %% check or sync the session
    S = sync_fp_bpod_ts(subjid, sessid, fiberid_array, session_files, varargin);

    %% preprocess each fiber's data and save separately to db
    M_struct = struct();
    for fiber = 1:num_session_files
        fiberid = fiberid_array{fiber};
        M_fiberid = ['M_', num2str(fiberid)];
        if strcmp(output_mode, 'separate')
            % first run preprocessing with fp_preprocess
            [signal, control, ts] = pp.fp_preprocess(session_files(fiber).name);
            assert(length(signal) == length(control),"signal is not of the same length as control! check it!");
    
            % then run utils.adjust_fp_times_to_bpod to get the struct to upload
            ats = utils.adjust_fp_times_to_bpod(ts,S.(M_fiberid));
            assert(length(signal) == length(ats),"data is not of the same length as timestamps! check it!");

        elseif strcmp(output_mode, 'dff')
            % first run preprocessing with fp_preprocess
            [signal, ts] = pp.fp_preprocess_dff(session_files(fiber).name);

            % then run utils.adjust_fp_times_to_bpod to get the struct to upload
            ats = utils.adjust_fp_times_to_bpod(ts,S.(M_fiberid));
            assert(length(signal) == length(ats),"data is not of the same length as timestamps! check it!");
    
            control = zeros(length(signal),1);
        end
        M = [ats, control, signal];
    end

        fieldname = strjoin({'fiberid',num2str(fiberid)},'_');
        M_struct.(fieldname) = M;
end


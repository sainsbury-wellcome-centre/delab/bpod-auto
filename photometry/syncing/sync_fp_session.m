function sync_fp_session(session_file)
    % Function to sync photometry data with Bpod behavior data and upload the combined data to the database.
    % session_file: path to the session file to sync.
    % example: sync_photometry_session('COS-M-0066-2024-05-24-080808.ppd')
    % don't allow void input

    if nargin < 1
        error('Please provide the path to the session file to sync.');
    end

    % check if the file exists
    if ~isfile(session_file)
        error('The file %s does not exist.', session_file);
    end

    recording_software = 'pyPhotometry';

    % import the photometry data
    data_struct = utils.import_ppd(session_file);

    subjid = data_struct.subject_ID;

    % extract sessid

end

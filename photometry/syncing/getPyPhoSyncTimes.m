function [trial_time, trial_num, total_duration] = getPyPhoSyncTimes(file_path, varargin)
    % Function to extract trial times and trial numbers from pyPhotometry data files.
    % Required Input:
    % file_path - path to the pyPhotometry data file
    % Optional Inputs:
    % Outputs:
    % trial_time - time of each trial relative to the start of the recording (s)
    % trial_num - trial number for each trial
    % total_duration - total duration of the recording (ms)
    % add the same varargin as the original function: 13, 4 samples, 3. change to varargin

    iod = @utils.inputordefault;

    [ev_type, varargin] = iod('event_type',3,varargin);

    [timebin, varargin] = iod('secsperbit',4/130,varargin);
    % The pyPhotometry 2excitation_2emission_mode sample rate is 130Hz
    % so each bit is 4/130 seconds long, so the minimal edge-to-edge time is 4/130 seconds

    [bits, varargin] = iod('trialbits',12,varargin);
    % We will use 12 bits to encode the trial #, allowing > 4096 trials per session

    [header, varargin] = iod('header',5, varargin);
    % We will use a 3 bit 101 header to indicate the start of the sync state. 
    % This also allows us to measure the samples/bit if the sampling rate changes.

    [fs, varargin] = iod('samping_rate',130, varargin);
    % for 'flat binary' format of pyPhotometry data, need to extract ts from index

    %[path_info, varargin] = iod('path_info',[], varargin);

    % for this to run load_open_ephys_data needs to be on the path

    [ttl_correct, varargin] = iod('allow_ttl_correct',true, varargin);
    
    % extract the digital channel from the pyPhotometry data file
    
    % usually it should be digital_1 (make sure your hardware plugin matches this!)
    data_struct = utils.import_ppd(file_path);
    syncing_states = data_struct.digital_1;
    assert(length(unique(syncing_states))==2,'your syncing channel is not digital_1. check your hardware plugin.');

    % check whether the stored sampling rate is the same as the default sampling rate
    sampling_rate = data_struct.sampling_rate;
    assert(fs==sampling_rate,'sampling rate of this session is %dHz, check your recording settings.',sampling_rate);

    % extract whether edges (0, 1, -1)
    channel_states = diff(data_struct.digital_1);
    % extract timestamps for whether edges; pyPhotometry timestamps are in ms not in sampling index
    timestamps = data_struct.time(2:end)' / 1000;
    % extract only edges (1, -1)
    good = abs(channel_states)==1;
    % extract timestamps for edges.
    ttls = timestamps(good);

    % total_duration in seconds
    total_duration = (data_struct.time(end) - data_struct.time(1)) / 1000;

    % verify that the min ttl matches the timebin
    dts = diff(ttls); % time between each edge, the smallest should be the timebin
    sdts = sort(dts);
    % the first 20% percentile of the time differences should be the same as the timebin (i.e. the time difference to the timebin should be less then 10e-10)
    assert(all(abs(sdts(1:(ceil(end/5)))-timebin)<10e-10)|ttl_correct,'The ttl durations do not match the timebin');

    if any(dts<timebin./2) && ttl_correct
        % have very short ttl pulse needed to be excluded
        short_ttl_idx = find(dts<timebin./2);
        ttls([short_ttl_idx;short_ttl_idx+1]) = [];
        dts = diff(ttls);
        fprintf(2,'short ttl pulse detected, sync quality is not very good\n')
    end

    % convert times to timebins, i.e. how many timebins between each edge
    ttl_bins = round(dts/timebin);

    % assume that the time in between trials is > 500 ms, 
    % i.e. if between two edges, the time is more than 0.5/timebin (these many timebins)
    % then it is a new trial
    trial_bounds = find(ttl_bins > 0.5/timebin);

    % initialize outputs to nans
    trial_time = nan(size(trial_bounds));
    trial_num = trial_time;
    this_bit_ind = 1;

    headerbits = numel(dec2bin(header));

    for xi=1:numel(trial_bounds)
        try
            if xi == 1
                trial_time(xi) = ttls(1);
                temp_ts = ttl_bins(1:trial_bounds(xi)-1);
            else
                trial_time(xi) = ttls(trial_bounds(xi-1)+1);
                temp_ts = ttl_bins(trial_bounds(xi-1)+1:trial_bounds(xi)-1);
            end

            last_bit = 1;
            bitfield = zeros(1,bits + headerbits);
            bit_idx = 1;

            % fill the bitfield with the bits, by each edge (fill how many bits with 1, how many with 0)
            for xj=1:numel(temp_ts)
                bitfield(bit_idx:bit_idx+temp_ts(xj)-1) = last_bit;
                last_bit = 1-last_bit;
                bit_idx = bit_idx+temp_ts(xj); % really +1-1
            end
            % convert the bitfield to a decimal number
            trial_num(xi) = bin2dec(num2str(bitfield(headerbits+1:end)));
        catch
            warning('cannot extract trial_num from digital channel')
        end
    end

    % if the last trial number is 0, remove it
    if trial_num(end) == 0
        trial_num = trial_num(1:end-1);
        trial_time = trial_time(1:end-1);
    end

    % check if the trial numbers are monotonically increasing to check whether trial_num is extracted correctly.
    assert(all(diff(trial_num)>=1),"Trial numbers are not in increasing order. Cannot sync this session.")

end
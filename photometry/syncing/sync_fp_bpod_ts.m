function S = sync_fp_bpod_ts(subjid, sessid, fiberid, session_files, varargin)
    % sync photometry data with Bpod behavior data and save to db
    % inputs:
    % fiberid is a nx1 cell array of used fiberids in the session
    % session_files is a nx1 struct array of each fp data file for each fiber used in the session
    % with fields: name, folder, date, bytes, isdir, datenum
    % session_files: path to the session file to sync.
    % example:
    % sync_fp_bpod_ts('COS-M-0066', 22222, 1, 'COS-M-0066-2024-05-24-080808.ppd')
    % sync_fp_bpod_ts('COS-M-0066', 22222, 1, 'COS-M-0066-2024-05-24-080808.ppd','comment','dLight NAc')

    % SUN.Cong, 2024 revised from bpod-auto/ephys/syncing.sync_times.m

    inpd = @utils.inputordefault;
    
    [Rsq_threshold,varargin] = inpd('rsq_threshold',0.98,varargin); 
    [comment, varargin] = inpd('comment','',varargin);
    [savedb,~] = inpd('save',true,varargin);
    
    %% check whether this session is already synced and saved to db
    
    dbc = db.labdb.getConnection();
    data = dbc.get('select sync_data from pho.photometry_sess where sessid = %d',{sessid});
    if ~isempty(data)
        fprintf(1,"Session %d is already in the pho.photometry_sess table\n", sessid);
        S = json.mloads(data{1});
        return;
    end
    
    %% syncing
    % first get bpod syncing stuff for this session
    [bts, btn] = getBpodSyncTimes(sessid,'secsperbit',4/130,'trialbits',12,'plot',0);
    % currently we don't deal with potentially restarted recordings yet (20240711)
    % [bts, btn] = discard_doubles(bts, btn);
    
    % get syncing stuff from pyPhotometry
    for fiber = 1:length(fiberid)
        [pts, ptn, fpdur] = getPyPhoSyncTimes(session_files(fiber).name);
        [match_trials, oi, bi] = intersect(ptn, btn);
        M = [match_trials, pts(oi), bts(bi)];
        % struct with three fields: trial_num, bpod_ts, photometry_ts

        % for more than one fiber, we will have multiple M and multiple rec_duration within still only one S
        M_fiberid = ['M_', num2str(fiberid{fiber})];
        S.(M_fiberid) = M;
        S.cols = {'Trial Number','FP Times', 'Bpod Times'};
        rec_duration_fiberid = ['rec_duration_', num2str(fiberid{fiber})];
        S.(rec_duration_fiberid) = fpdur;
    end
    
    %rec_duration = fpdur;
    D.sessid = sessid;
    D.subjid = subjid;
    D.sync_data = json.mdumps(S);
    %D.rec_duration = rec_duration; % don't save this outside of S, since it's fiber specific
    D.comment = comment;
    
    % now quality_manual_check is a list rather than a single value of true/false
    quality_manual_check = zeros(1,length(fiberid));
    for fiber = 1:length(fiberid)
        M_fiberid = ['M_', num2str(fiberid{fiber})];
        quality_manual_check(fiber) = check_syncing(S.(M_fiberid),Rsq_threshold);
    end

    if savedb && all(quality_manual_check)
        dbc.saveData('pho.photometry_sess',D);
    end
    
end

function out = check_syncing(M,Rsq_threshold)
        % We check whether it is a good sync by fitting a linear model between
        % the times. A good session should be almost perfect. 
        out = true;
        lm = fitlm(M(:,2),M(:,3));

        assert(lm.Rsquared.Adjusted > Rsq_threshold,"The sync times do not match. May be wrong session");
        % May want to put other checks in here.
        
        % plot sync result, ask people to check whether it's good
        figure;
        plot(M(:,2),M(:,3));
        xlabel('recording time')
        ylabel('bpod time')
        title('fp-bpod sync result, should be like a straight line')
        
        ButtonName = questdlg('Does the data look good?', ...
                         'Good Data', ...
                         'Yes', 'No', 'Yes');
                     
        if strcmp(ButtonName,'No')
            warndlg('Please check syncing result.', 'check syncing.');
            out = false;
        end
end
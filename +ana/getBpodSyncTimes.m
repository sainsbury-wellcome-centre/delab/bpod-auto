function [sync_times, trial_num] = getBpodSyncTimes(val, varargin)
% [times, trial] = getBpodSyncTimes(sessid, varargin)
% [times, trial] = getBpodSyncTimes(SD, varargin)
% [times, trial] = getBpodSyncTimes(peh, varargin)
%
% Get the times of the first TTL of the first sync_state for each trial.
% Also return the trial number decoded from the sync states. This is just a
% sanity check.
%
% Optional Inputs
% sync_state_name       sync_states
%
% Jeff Erlich, 2018
%%

iod = @utils.inputordefault;

[sync_state, varargin] = iod('sync_state_name','sync_states',varargin);
% if you don't you the default name for sync_states in your protocol, you
% need to pass the used name as an optional argument.

[timebin, varargin] = iod('secsperbit',1e-3,varargin);
% The open-ephys sample rate is 30KHz, so 1ms is 30samples.

[bits, varargin] = iod('trialbits',16,varargin);
% We will use 16 bits to encode the trial #, allowing > 65000 trials per session

[header, varargin] = iod('header',5, varargin);
% We will use a 3 bit 101 header to indicate the start of the sync state. 
% This also allows us to measure the samples/bit if the sampling rate changes.

[plotit, varargin] = iod('plot',0, varargin);

[check_trial_num] = iod('check_trialnum',1,varargin);
tall = @(x)(x(:)); % a little function to make a vector tall (a col vector)

%%
if isnumeric(val) && isscalar(val)
    % if the input is a number and just one number then treat it as a
    % sessid
    peh = db.getParsedEvents(val);
elseif isstruct(val) && isfield(val, 'peh')
    % if the input is a struct and has a field "peh" it is SessData
    peh = val.peh;
elseif isstruct(val) && isfield(val, 'States')
    % if the input is a struct and has a field "States" it is ParsedEvents
    peh = val;
else
    % if it is none of the above, then throw an error, because we don't
    % know how to deal with it. 
    error(sprintf(['The input to this function should be a single sessid,\n', ...
        'a single SessionData structure (e.g. as returned from db.getSessData)\n',...
        'or a single parsed events history (e.g. from db.getParsedEvents)'])) 
end
% If you want to get times for a list of sessid or a list of peh, just use
% arrayfun(@(x)getSyncTimes(x), list_of_sessids, 'UniformOutput',0);
% This will return a cell array of times.

fun = @(x)x.States.(sprintf('%s_01', sync_state))(1); 
% x is a peh for one trial.
% For each x get the time of onset of the first sync_state

sync_times = tall([peh.StartTime]) - peh(1).StartTime + tall(arrayfun(fun, peh));
% The StartTime is the time of each trial since the Arduino turned on. So we subtract off
% the first StartTime so that the times are relative to session start.
if check_trial_num
    trial_num = tall(arrayfun(@(x)sync_states_to_trial_num(x.States, sync_state, timebin, header, bits), peh));
    assert(isequal(trial_num(:), (1:numel(trial_num))'),"There was a problem with the sending of the sync_states.");
else
    trial_num = 1:numel(peh);
end

if numel(trial_num) ~= trial_num(end)
    fprintf(2,'There is something fishy going on.')
end


if plotit
    figure(101);clf
    plot(trial_num,sync_times,'.');
    xlabel('Trial #');
    ylabel('Trial Start Time (sec)');
end

end

function trial_num = sync_states_to_trial_num(States, sync_state, timebin, header, bits)
    state_names = fieldnames(States);
    sync_states = state_names(strncmpi(sync_state, state_names, numel(sync_state)));
    enum = @(x)1:numel(x);
    bit_time = nan(size(sync_states));
    for sx = enum(sync_states)
        bit_time(sx) = diff(States.(sync_states{sx}));
    end
    bit_time = round(bit_time/timebin);
    bitnum = 0;
    for bx = 1:numel(bit_time)
        bitnum = bitshift(bitnum,bit_time(bx)) + mod(bx,2)*(2^bit_time(bx)-1);
    end
    % Check header.
    assert(header == bitshift(bitnum,-16),"The header of the sync_states does not match the header");
    trial_num = bitnum - bitshift(header,bits);
    
end


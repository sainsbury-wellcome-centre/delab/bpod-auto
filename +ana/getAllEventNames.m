function events = getAllEventNames(peh)

events = {};
for px = 1:numel(peh)
    events = union(events, fieldnames(peh(px).Events));
end
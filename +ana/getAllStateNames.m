function states = getAllStateNames(peh)

states = {};
for px = 1:numel(peh)
    states = union(states, fieldnames(peh(px).States));
end
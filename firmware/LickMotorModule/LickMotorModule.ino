#include <Dynamixel2Arduino.h>
#include "ArCOM.h" 

#define DXL_SERIAL   Serial2 //OpenCM9.04 EXP Board's DXL port Serial. (Serial1 for the DXL port on the OpenCM 9.04 board)
const uint8_t DXL_DIR_PIN = 9; //using pin number 2 (next to TX1 and RX1 for DIR control)
const float DXL_PROTOCOL_VERSION = 2.0;

// debuggers
//#define DEBUG_PLOT // uncomment this line for serial plotter readout, useful for PID tuning
//#define DEBUG_CNTL // print motor XY for every loop, remove for formal version
//#define DEBUG_COMM

#define X_ID 1
#define Y_ID 2

// motors appear to have the center NEAR 512 units but not exactly
#define X_CENTER 520
#define Y_CENTER 526

//ArCOM myUSB(Serial); // Creates an ArCOM object called myUSB, wrapping SerialUSB
ArCOM Serial1COM(Serial1); // Creates an ArCOM object called myUART, wrapping Serial1

IntervalTimer MotorController; // run all motor controlling stuff in the hardware timer

uint32_t FirmwareVersion = 1;
char moduleName[] = "LM"; // Name of module for manual override UI and state machine assembler: Lick Motor
char* eventNames[] = {"ComdOK", "PosiOK"};
byte nEventNames = (sizeof(eventNames)/sizeof(char *));

volatile byte opCode = 0; 
volatile byte opSource = 0;
volatile boolean newOp = false;
volatile boolean runCntl = false;
volatile boolean update_motor_posi_now = false;

// Timing
uint64_t nMicrosRollovers = 0;
uint64_t sessionStartTimeMicros = 0;
uint64_t currentTime = 0;
uint32_t lastMicrosTime = 0;
uint32_t microsTime = 0;

union {
    byte uint8[16];
    uint64_t uint64[2];
} usbEventBuffer;

volatile boolean usbStreaming = false;
volatile boolean usbPrinting = false;


const float UNITS_PER_DEG = 1024.0/300.0; // determined empirically, might not be exact

// set physical parameters
const float X_ARM_L = 11.0; // mm
const float Y_ARM_L = 16.0; // mm
const float X_MAX = 7.0; // mm
const float X_MIN = -7.0; // mm
const float X_BIAS_MAX = 5.0;
const float X_BIAS_MIN = -5.0;
const float Y_MAX = 10.0; // mm
const float Y_MIN = 0; // mm
const float XY_STEP_SIZE = 0.25 ; // mm

float xTarget = 0.0; // mm, range
float yTarget = 0.0; // mm, range
float currrent_yLim = 10.0; // mm, max y position
volatile float xCommand = X_CENTER; // dynamixel step, 0-1023
volatile float yCommand = Y_CENTER; // dynamixel step, 0-1023
volatile float xPresent = 0.0; // dynamixel step, 0-1023
volatile float yPresent = 0.0; // dynamixel step, 0-1023

Dynamixel2Arduino dxl(DXL_SERIAL, DXL_DIR_PIN);

//This namespace is required to use Control table item names
using namespace ControlTableItem;

void setup() {
  #if defined (DEBUG_COMM)  || defined (DEBUG_CNTL) || defined (DEBUG_PLOT)
    // Use UART port of DYNAMIXEL Shield to debug.
    Serial.begin(115200);   //set debugging port baudrate to 115200bps
  #endif
  //while(!Serial);         //Wait until the serial port is opened

  Serial1.begin(1312500); 

  // wait for ping message to starting
  while(!runCntl){
    if (Serial1COM.available()) {
      opCode = Serial1COM.readByte();
      opSource = 1; newOp = true;
    }

    if (newOp) {
      newOp = false;
      if (opCode==255 && opSource == 1) {
        //ping message
        returnModuleInfo();
        runCntl = true;
      }
    }
    delay(5);
  }

  pinMode(6, OUTPUT);
  digitalWrite(6, HIGH);

  // Set Port baudrate to 1Mbps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(1000000);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);

  // Turn off torque when configuring items in EEPROM area
  //   using position mode for now
  dxl.torqueOff(X_ID);
  dxl.torqueOff(Y_ID);
  dxl.setOperatingMode(X_ID, OP_POSITION);
  dxl.setOperatingMode(Y_ID, OP_POSITION);
  dxl.torqueOn(X_ID);
  dxl.torqueOn(Y_ID);
  
  // Limit the maximum velocity in Position Control Mode. Use 0 for Max speed
  dxl.writeControlTableItem(MOVING_SPEED, X_ID, 0);
  dxl.writeControlTableItem(MOVING_SPEED, Y_ID, 0);

  // mess with PID values
  //                                         def min max
  //  27   1   D Gain  Derivative Gain   RW  0   0   254
  //  28   1   I Gain  Integral Gain     RW  0   0   254
  //  29   1   P Gain  Proportional Gain RW  32  0   254
  dxl.writeControlTableItem(P_GAIN, X_ID, 32);
  dxl.writeControlTableItem(I_GAIN, X_ID, 4);
  dxl.writeControlTableItem(D_GAIN, X_ID, 0);
  dxl.writeControlTableItem(P_GAIN, Y_ID, 32);
  dxl.writeControlTableItem(I_GAIN, Y_ID, 4);
  dxl.writeControlTableItem(D_GAIN, Y_ID, 0);
   
  // check PID values
  #ifdef DEBUG_CNTL
    if (Serial){
      Serial.print("XP "); Serial.println(dxl.readControlTableItem(P_GAIN, X_ID));
      Serial.print("XI "); Serial.println(dxl.readControlTableItem(I_GAIN, X_ID));
      Serial.print("XD "); Serial.println(dxl.readControlTableItem(D_GAIN, X_ID));
      Serial.print("YP "); Serial.println(dxl.readControlTableItem(P_GAIN, Y_ID));
      Serial.print("YI "); Serial.println(dxl.readControlTableItem(I_GAIN, Y_ID));
      Serial.print("YD "); Serial.println(dxl.readControlTableItem(D_GAIN, Y_ID));
    }
  #endif
  
  // move both motors to center positions
  dxl.setGoalPosition(X_ID,X_CENTER);
  dxl.setGoalPosition(Y_ID,Y_CENTER);
  
  delay(1000); // time to reach position

  MotorController.begin(motorFunc, 5000);  // this motorFunc will be called every 5000us
  
}

void loop() {
  #if defined (DEBUG_COMM)  || defined (DEBUG_CNTL) || defined (DEBUG_PLOT)
    if (Serial){
      usbPrinting = true;
    }
  #endif
  if (usbStreaming) {
    // Calculate 64-bit rollover-compensated time in microseconds
    microsTime = micros();
    currentTime = ((uint64_t)microsTime + (nMicrosRollovers*4294967295)) - sessionStartTimeMicros;
    if (microsTime < lastMicrosTime) {
      nMicrosRollovers++;
    }
    lastMicrosTime = microsTime;
    usbEventBuffer.uint64[0] = currentTime;
  }

  // Handle incoming byte messages
  if (Serial1COM.available()) {
    opCode = Serial1COM.readByte();
    opSource = 1; newOp = true;
  }

  if (newOp) {
    newOp = false;
    switch (opCode) {
      case 255:
        if (opSource == 1) {
          returnModuleInfo();
          runCntl = true;
        } else if (opSource == 0) {
          //myUSB.writeByte(254); // Confirm
          //myUSB.writeUint32(FirmwareVersion); // Send firmware version
          //sessionStartTimeMicros = (uint64_t)microsTime;
        }
      break;
      case 'P': // Pause/stop motor controlling
        opCode = 'T';// run stop after this command
        newOp = true;
        xTarget = 0; yTarget = 0; // reset position, return to x, y = 0
        currrent_yLim = Y_MAX;//reset y_lim
      break;
      case 'T': // Pause/stop motor controlling NOW
        runCntl = false;
      break;
      case 'Z': // start motor controlling
        runCntl = true;
      break;
      case 'a':
        xTarget += XY_STEP_SIZE; // nudge left (+x)
      break;
      case 'd':
        xTarget -= XY_STEP_SIZE; // nudge right (-x)
      break;
      case 'w':
        yTarget += XY_STEP_SIZE; // nudge forward (+y)
      break;
      case 's':
        yTarget -= XY_STEP_SIZE; // nudge backward (-y)
      break;
      case 'R':
        yTarget = Y_MIN; // Fast retract
        if (runCntl) {update_motor_posi_now = true;}  //update motor position immediately after receiving this command
      break;
      case 'F':
        yTarget = currrent_yLim; // Fast forward to the current target y_forward position
        if (runCntl) {update_motor_posi_now = true;}   //update motor position immediately after receiving this command
      break;
      case 'A':
        xTarget = X_BIAS_MAX; // full bias one side
      break;
      case 'D':
        xTarget = X_BIAS_MIN;  // full bias other side
      break;
      case 'L':
        currrent_yLim = yTarget;  // set the front limit positioning
      break;
      case 'H':
        xTarget = 0; yTarget = 0; // return to x, y = 0
        currrent_yLim = Y_MAX;//reset y_lim
      break;
      case 'h':
        xTarget = 0; // return to x = 0 ONLY
      break;
    }
    #ifdef DEBUG_COMM
      if (usbPrinting){
        Serial.print("Incoming message received: "); Serial.write(opCode);
        Serial.print(" Targets (x,y): "); Serial.print(xTarget,1); Serial.print(", "); Serial.print(yTarget,1); Serial.print("\n");
      }
    #endif
  }

  #ifdef DEBUG_COMM
    // check if user input in serial buffer
    if (Serial.available() > 0) {
      char userInput = Serial.read();
      if      (userInput == 'a') { xTarget += XY_STEP_SIZE; }  
      else if (userInput == 'd') { xTarget -= XY_STEP_SIZE; }  // nudge right (-x)
      else if (userInput == 'w') { yTarget += XY_STEP_SIZE; }  // nudge forward (+y)
      else if (userInput == 's') { yTarget -= XY_STEP_SIZE; }  // nudge backward (-y)
      else if (userInput == 'R') { yTarget = Y_MIN; }          // Fast retract
      else if (userInput == 'F') { yTarget = Y_MAX; }          // Fast forward
      else if (userInput == 'A') { xTarget = X_BIAS_MAX; }     // full bias one side
      else if (userInput == 'D') { xTarget = X_BIAS_MIN; }     // full bias other side
      else if (userInput == 'H') { xTarget = 0; yTarget = 0; } // return to x, y = 0
      else if (userInput == 'h') { xTarget = 0; }              // return to x = 0 ONLY
      else { Serial.println("Only accepting 'wasd' nudge, and 'R' or 'F' retract keyboard input."); }
      
      while(Serial.available()){Serial.read();}  // make sure the buffer is empty
    }
  #endif

  // check that values are acceptable
  if (xTarget > X_MAX) { xTarget = X_MAX; }//Serial.println("X_MAX limit."); 
  if (xTarget < X_MIN) { xTarget = X_MIN; }//Serial.println("X_MIN limit.");
  if (yTarget > Y_MAX) { yTarget = Y_MAX; }//Serial.println("Y_MAX limit."); 
  if (yTarget < Y_MIN) { yTarget = Y_MIN; }//Serial.println("Y_MIN limit."); 
  
  // convert xTarget and yTarget to xCommand (dynamixel unit) and yCommand
  xCommand = round( degrees(asin(xTarget/X_ARM_L)) * UNITS_PER_DEG + X_CENTER );
  yCommand = round( degrees(asin(yTarget/Y_ARM_L)) * UNITS_PER_DEG + Y_CENTER );

  if (update_motor_posi_now){
    update_motor_posi_now = false;

    // move
    dxl.setGoalPosition(X_ID,xCommand);
    dxl.setGoalPosition(Y_ID,yCommand);

    // check position
    xPresent = dxl.getPresentPosition(X_ID);
    yPresent = dxl.getPresentPosition(Y_ID);

    #ifdef DEBUG_CNTL
      if (usbPrinting){
        Serial.print("Targets (x,y): "); Serial.print(xTarget,1); Serial.print(", "); Serial.print(yTarget,1); Serial.print("\t");
        Serial.print("Pos. Command/Present (x,y): "); Serial.print(xCommand,0); Serial.print("/"); Serial.print(xPresent,0);
        Serial.print(", ");                           Serial.print(yCommand,0); Serial.print("/"); Serial.print(yPresent,0);
        Serial.print("\t (ms "); Serial.print(millis()); Serial.println(")");
      }
    #endif
    /*
    #ifdef DEBUG_PLOT
        Serial.print(xCommand); 
        Serial.print("\t");
        
        if (xPresent > 1.0) {
          Serial.print(xPresent); 
        } else { 
          Serial.print(xCommand); // just something close
        }
        Serial.print("\t");

        Serial.print(yCommand);
        Serial.print("\t");
        
        if (yPresent > 1.0) {
          Serial.print(yPresent); 
        } else { 
          Serial.print(yCommand); // just something close
        }
        Serial.print("\n");
    #endif
    */ 

  }

}

void motorFunc() {
  if (runCntl) {update_motor_posi_now = true;}  
}

void returnModuleInfo() {
  Serial1COM.writeByte(65); // Acknowledge
  Serial1COM.writeUint32(FirmwareVersion); // 4-byte firmware version
  Serial1COM.writeByte(sizeof(moduleName)-1);
  Serial1COM.writeCharArray(moduleName, sizeof(moduleName)-1); // Module name
  Serial1COM.writeByte(1); // 1 if more info follows, 0 if not
  Serial1COM.writeByte('#'); // Op code for: Number of behavior events this module can generate
  Serial1COM.writeByte(8); // 4 Ports, 2 states each
  Serial1COM.writeByte(1); // 1 if more info follows, 0 if not
  Serial1COM.writeByte('E'); // Op code for: Behavior event names
  Serial1COM.writeByte(nEventNames);
  for (int i = 0; i < nEventNames; i++) { // Once for each event name
    Serial1COM.writeByte(strlen(eventNames[i])); // Send event name length
    for (int j = 0; j < strlen(eventNames[i]); j++) { // Once for each character in this event name
      Serial1COM.writeByte(*(eventNames[i]+j)); // Send the character
    }
  }
  Serial1COM.writeByte(0); // 1 if more info follows, 0 if not
}

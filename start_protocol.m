function start_protocol(protocol, subjid,varargin)
    % start_protocol(subjid)
    % start_protocol(protocol_name, subjid, ...)
    %
    % protocol_name is a string which matches an Object that inherits from ProtoObj
    % subjid is an integer which refers to a subject.
    %
    % Optional Inputs
    % test      [false] if set to true then do not connect to database or save any data.
    %           useful for debugging a new protocol when you are offline
    % video_trigger [video_trigger] if set to true will trigger video in test
    %            mode, default false in test mode, defaulr true in notmal mode
    % debug     [false] if set to true then open a window with a button that when  
    %           set to DEBUG goes into the debugger in between each trial.
    % notify    [true] if true, errors are sent to mattermost. if false, errors are     %           written to the output and to the DB but not to mattermost. Also useful
    %           when debugging.
    %
    % Examples
    % start_protocol('JLI-R-0001'); % running real animal session manually
    % start_protocol('operant_fm','JLI-T-0001','test',1); % running test
    % session without video
    % start_protocol('operant_fm','JLI-T-0001','test',1,'video_trigger',1); %
    % running test session with video trigger test
    
    
    test = utils.inputordefault('test',false,varargin);
    debug_flag = utils.inputordefault('debug',false,varargin);
    notify = utils.inputordefault('notify',true,varargin);
    video_trigger	= utils.inputordefault('video_trigger', ~test, varargin);
    
    boolean_ = {'false' , 'true'};
    test_rig = boolean_(1 + int.isTestRig());
    
    global BpodSystem
    
    BpodLoaded = 0;
    try
        evalin('base', 'BpodSystem;'); % BpodSystem is a global variable in the base workspace, representing the hardware
        isEmpty = evalin('base', 'isempty(BpodSystem);');
        if isEmpty
            evalin('base', 'clear global BpodSystem;')
        else
            BpodLoaded = 1;
        end
    catch
    end
    
    if ~BpodLoaded
        error('Bpod Not Started')
        %start_bpod
    end
    
        % Make a little window with a big button.
        figure(666); clf
        set(666,'Position',[100 100 340 100],'ToolBar','none','MenuBar','none','Name','Bpod Debug Controller','NumberTitle','off');
        debug_button = uicontrol(666,'Style','togglebutton','Position',[20 20 300 60],'Callback',@toggle_break,'Value',1,'FontSize',32,'FontWeight','bold','String','DEBUG');
    if debug_flag    
        BpodSystem.Debug = true;
        debug_button.Value = true;
    else
        BpodSystem.Debug = false;
        debug_button.Value = false;
        debug_button.String = 'Run';
    
    end
    
    
    try
        bpodstr = which('Bpod');
        BPODHOME = bpodstr(1:find(bpodstr==filesep,1,'last'));
        dbprotocol_repo_url ='https://gitlab.com/sainsbury-wellcome-centre/delab/bpod-protocols';
        dbprotocol_dir = 'bpod-protocols';
    
        
        if test
            % create a "degenerate" SaveLoad object. So no data is loaded or saved.
            sl = sys.SaveLoad;
            use_db = false;
        else
            dbc = db.labdb.getConnection();   
            use_db = true;
            sl = [];
            if nargin==1
                % if the function is called with only one argument, that argument is
                % subjid.
                subjid = protocol;
                protocol = '';
    
            end
            
            res = regexp(subjid,'[a-zA-Z]+-[a-zA-Z]-[0-9]+');
            if isempty(res)
                res = 0; % give false
            end
            if ~res
                error('start_protocol:bad_subjid','subjid must be a AAA-B-NNNN. e.g. start_protocol(JLI-M-0001) or start_protocol(''Operant'',JLI-M-0001). The input you gave was a %s',class(subjid));
            end
    
            % Check that the subjid has status running.
            current_status = db.getAnimalStatus(subjid);
            assert(~isempty(current_status),'There is no status for "%s". Does this animal exist?',subjid)
            if ~strcmp(current_status{1},'running')
                try
                    error('start_protocol:bad_status','The status of subject "%s" is %s. Only animals with status "running" can be run using this function. Please double check the subjid or update the status.',subjid,current_status{1});
                catch me
                    utils.showerror(me);
                    db.log_error(me,'subjid',subjid, 'notify',notify,'caught',1,'comment',sprintf('The status of subject %d is %s. Only animals with status "running" can be run using this function. Please double check the subjid or update the status.',subjid,current_status{1}));
                end
                return
    
            end
    
            
            [dbprotocol,dbprotocol_repo_url] = dbc.get('select protocol,url from met.current_settings where subjid = "%s"',{subjid});
            if ~isempty(dbprotocol)
                if ~strcmp(protocol, dbprotocol{1}) && nargin >= 2
                    fprintf(1,'Warning using protocol from database: %s\n', dbprotocol{1});
                end
                protocol = dbprotocol{1};
            else
                % There are no settings. Let's see if this is a new animal.
                expgid = dbc.get('select expgroupid from met.animals where subjid="%s"',{subjid});
                if isempty(expgid)
                    fprintf(2,'Did not find settings for subjid "%s", please specify protocol\n',subjid);
                    return;
                else
                    [dbprotocol,dbprotocol_repo_url] = dbc.get('select protocol,url from met.settings where stage=1 and expgroupid=%d',{expgid});
                    protocol = dbprotocol{1};
                    fprintf(1,'Running subjid %d for the first time!\n',subjid);
    
                end
    
            end
    
    
            if ~isempty(dbprotocol_repo_url)
                if isempty(dbprotocol_repo_url{1})
                    fprintf('Using the default db protocols repository!');
                    dbprotocol_repo_url = 'https://gitlab.com/sainsbury-wellcome-centre/delab/bpod-protocols';
                    dbprotocol_dir = 'bpod-protocols';
                else
                    %dbprotocol_dir  is made by removing the'https://'/'http://' from the dbprotocol_repo_url
                    dbprotocol_repo_url = dbprotocol_repo_url{1};
                    dbprotocol_dir = erase(dbprotocol_repo_url,'http' + wildcardPattern + '://' + wildcardPattern + '@');
                    dbprotocol_dir = strrep(dbprotocol_dir,'.', '_');
                    dbprotocol_dir = strrep(dbprotocol_dir,filesep, '_');
                    dbprotocol_dir = ['bpod-protocols-repos/' dbprotocol_dir];
                end
            end
    
               
    
        end
    
        
    
    
    
    
        path_parts = strsplit(BPODHOME, filesep);
        repo_path = strjoin(path_parts(1:end-2), filesep);
        
        %check if the repository is cloned 
    
        dbprotocol_dir = [repo_path '/' dbprotocol_dir];
    
        fprintf(['protocol dir being used: ' dbprotocol_dir '\n']);
    
        cmd_ = ['sh ' repo_path '/bpod-auto/bpod_protocol_git_pull.sh ' dbprotocol_repo_url ' ' dbprotocol_dir ' ' test_rig{1} ];
    
        fprintf([cmd_ '\n']);
    
        [status,std_output] = system(cmd_);
    
        if status == 0 
            fprintf('Pulled the latest changes for the protocols.Checked out to main!\n');
        else
            error('start_protocol:bad_protocol_repo', [' ' std_output]);
        end
    
    
        
        
        % here the repository to take the procotol from is decided
        addpath([dbprotocol_dir,filesep,protocol]);
        
        db.markRunning();
        obj = eval(protocol);
        dispatch = sys.dispatchObj(obj,'name',subjid,'saveload',sl,'use_db',use_db,'notify',notify,'video_trigger',video_trigger);
        dispatch.runAll();
        db.markReady();
        
    catch me
        utils.showerror(me);
        db.markReady();
        if ~test
            try
                if dispatch.video_trigger
                    dispatch.video_handler.terminate_video(dispatch.saveload.sessid, dispatch.saveload.subjid, dispatch.saveload.test);
                    dispatch.video_trigger = false;
                end
                sessid = dispatch.saveload.sessid;
                db.log_error(me, 'sessid', sessid ,'notify',notify,'subjid',subjid);
                S.sessid = sessid;
                S.status = 'error';
                dbc.saveData('beh.sess_status', S);
     
            catch
                sessid = 0;
                
            end
        end
        try
            dispatch.end_session();
        catch
            fprintf('Failed to properly end session on error');
        end
        % system('~/matlab_bpod.sh');
        if ~test
            fprintf(2,'\nRestarting Bpod in 5 seconds. Press ctrl-C to cancel.\n')
            pause(5)
            start_bpod;
        end
    end
    
    end
    
    function toggle_break(h,e)
        global BpodSystem
        BpodSystem.Debug = h.Value;
        if h.Value
            h.String = 'DEBUG';
            h.ForegroundColor = [0.8, 0, 0];
            
        else
            h.String = 'Run';
            h.ForegroundColor = [0, 0.8, 0];
        end
        
         
    end
    

    
    
    
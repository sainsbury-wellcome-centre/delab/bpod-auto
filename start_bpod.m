function start_bpod(varargin)

add_bpod_path();

Bpod;
int.check_fsm_config();
global BpodSystem
BpodSystem.Sync = int.isSyncRig();
BpodSystem.Opto = int.isOptoRig();

if BpodSystem.Opto
    global opto_unit_amp
    opto_unit_amp = [1,1];
end

if BpodSystem.FirmwareVersion ~= 22 %check firmware version
% 	fprintf(2,'Firmware version does not match matlab version.\n');
% 	fprintf(2,'This can sometimes be a result of a bad hardware handshake.\n');
% 	fprintf(2,'Restarting MATLAB in 3 seconds.\n');
% 	pause(3);
% 	system('~/matlab_bpod.sh &');
	
end

% We successfully connected to a bpod so clear the bad loop log.
system('rm -f ~/bad_bpod_loop')

try
BpodSystem.use_db = true;

BpodSystem.dbc = db.labdb.getConnection();
db.setTesting(0);
[rigid, roomid] = db.getRigID();
if isempty(roomid) || isnan(roomid) %might also for bpod-emulator, but we don't have it yet
    fprintf(2, 'This is not a real rig\n');
    rigid = 0;
else
    fprintf('This is a real rig, starting bpodrobot timer\n');
    sys.bpod_robot();
    db_config = utils.ini2struct('~/.dbconf');
    if isfield(db_config,'zmq')
        BpodSystem.zmqconn_pub = net.zmqpush();
    end
end

BpodSystem.CalibrationTables.LiquidCal = db.loadWaterCalibration(rigid);
%BpodSystem.ForcedValveNumber = db.loadForcedValve(rigid); % no longer needed for gen2?
%BpodSystem.InputsEnabled.WiresEnabled = [0 0 0 0]; % Disable wire inputs that are not being used.


db.clearOldSessionStatus(rigid);
catch me
    out = ver('MATLAB');
	mver = str2double(out.Version);
	if mver<8.6

		fprintf(1,'You are running an old version of MATLAB which is not compatible with the Erlich Lab software. You must use 8.6 or later. Note: version 8.6 is not strictly required for running bpod, but it required for running the rigs in the erlichlab. You can execute lines 12-27 and then run a protocol from the command line \n');
		return
	end
	utils.showerror(me)
	answer = questdlg('Failed to connect the database. Do you want to run without database access? Your data will not be saved. If this is a real rig answer NO.', 'Run without DB?','YES','NO','NO');
	if answer(1)=='Y'
		BpodSystem.use_db = false;
	else
		error('Failed to connect to DB. Please diagnose before re-running. Check wiki or #tech-help.')
	end
end

if BpodSystem.Opto %for an opto rig, try to opto pulse pul and make connection
    try
        if ~exist('PulsePal.m', 'file') %add pulase pal path only if not been added yet
            addpath(genpath('~/repos/PulsePal/MATLAB'))
        end
        PulsePal;
    catch me
        utils.showerror(me);
        error('PulsePal cannot be connected. Please check that it is powered on and software is installed at ~/repos/PulsePal')
    end
end

end

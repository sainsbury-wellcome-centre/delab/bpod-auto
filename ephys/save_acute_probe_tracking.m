function [all_geom] = save_acute_probe_tracking(varargin)
%save_acute_probe_tracking(varargin)
% will save probe tracking histology result (from IBL app) to DB channel by channel (phy.atlas_coord)
% will check the position of each channel mapped by depth
%
% need to upload probe basic information to `phy.probes` and get probeid
% first, and then finish probe 1d tracking with brainglobe, then use
% tracking file and probeid as input for this function
%
% Please note that the default filename for the tracking result will be
% 'channel_locations.json', unless you specify with 'track_result_filename'
% input
%  example:
%   use 'channel_locations.json' in IBL result folder
%   [all_geom] = save_acute_probe_tracking('track_result_path','/media/jingjie/spike/spk_sorting/npx/CFB-M-0024_2024-01-28_17-19-00_g0/alf_imec0','probeid',67,'saving', false);
%  
%   use 'channel_locations_ALM.json' in the current MATLAB folder
%   [all_geom] = save_acute_probe_tracking('track_result_filename','channel_locations_ALM.json','probeid',67,'saving', false);
%
%   use 'channel_locations_ALM.json' in a given folder
%   [all_geom] = save_acute_probe_tracking('track_result_path','/media/jingjie/spike/spk_sorting/npx/CFB-M-0024_2024-01-28_17-19-00_g0/alf_imec0','track_result_filename','channel_locations_ALM.json','probeid',67,'saving', false);
%
% Jingjie Li, 2024-03-11
inpd = @utils.inputordefault;
[track_result_path, varargin]=inpd('track_result_path', '', varargin);
[track_result_filename, varargin]=inpd('track_result_filename', 'channel_locations.json', varargin);
[probeid, varargin]=inpd('probeid', [], varargin);

[use_db, varargin]=inpd('use_db', 'client', varargin);
[using_atlas, varargin]=inpd('using_atlas', 'allen_mouse', varargin);
[saving, varargin]=inpd('saving', true, varargin);
[plot_check, varargin]=inpd('plot', true, varargin);

dbc = db.labdb.getConnection(use_db);
allen_CCF = hist.loadAtlasTable('allen_mouse');
if isempty(track_result_path)
    track_result = track_result_filename;
else
    track_result = fullfile(track_result_path,track_result_filename);
end
%get probe mapping
[probe_map] = dbc.get('select data from phy.probes where probeid = %d',{probeid});
[channels_tab] = dbc.query('select * from phy.channels where probeid = %d',{probeid});
all_geom = struct2table(json.mloads(probe_map{1}));
all_geom = sortrows(all_geom,{'probe','spikeGLX'},{'ascend','ascend'});% all_geom.y is the depth

[tracking_res] = hist.load_IBL_probe_tracking(track_result);
tracking_res = struct2table(tracking_res);
%the inserved depth can be found at tracking_res.depth;

all_geom.region = cell(height(all_geom),1);
all_geom.region_name = cell(height(all_geom),1);
all_geom.atlas = cell(height(all_geom),1);
all_geom.atlas_id = zeros(height(all_geom),1);
all_geom.region_id = zeros(height(all_geom),1);
all_geom.xx = zeros(height(all_geom),1);
all_geom.yy = zeros(height(all_geom),1);
all_geom.zz = zeros(height(all_geom),1);
for ii = 1:height(all_geom)
    all_geom.atlas{ii} = using_atlas;
    idx = find(tracking_res.channel_id+1==all_geom.spikeGLX(ii));
    if isempty(idx)||tracking_res.RegionID(idx)==0 %out of range, should be outside brain
        all_geom.region{ii} = '';
        all_geom.region_id(ii) = nan;
        all_geom.region_name{ii} = '';
    else
        all_geom.region{ii} = tracking_res.RegionAcronym{idx};
        all_geom.region_id(ii) = tracking_res.RegionID(idx);
        [all_geom.atlas_id(ii),all_geom.region_name{ii}] = find_allen_region_name(tracking_res.RegionID(idx),allen_CCF);
        all_geom.xx(ii) = tracking_res.x(idx);
        all_geom.yy(ii) = tracking_res.y(idx);
        all_geom.zz(ii) = tracking_res.z(idx);
    end
    
end

if plot_check
    hist.plot_npx_tragjectory(all_geom)
end

if saving
    PC = struct();
    for ii = 1:height(channels_tab)
        PC(ii).channelid = channels_tab.channelid(ii);
        this_channel = channels_tab.ad_channel(ii);
        this_channel_idx = find(this_channel==all_geom.spikeGLX);
        if ~isnan(all_geom.region_id(this_channel_idx))
            PC(ii).x = all_geom.xx(this_channel_idx);
            PC(ii).y = all_geom.yy(this_channel_idx);
            PC(ii).z = all_geom.zz(this_channel_idx);
            PC(ii).atlas = all_geom.atlas{this_channel_idx};
            PC(ii).region = all_geom.region{this_channel_idx};
        end
    end

    dbc.saveData('phy.atlas_coord',PC);
end
end

function [atlas_id,region_name] = find_allen_region_name(region_id,allen_table)

region_name = allen_table.name{allen_table.id == region_id};
atlas_id = allen_table.atlas_id(allen_table.id == region_id);
end
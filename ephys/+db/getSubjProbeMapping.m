function [all_geom] = getSubjProbeMapping(subjid,varargin)
%[all_geom] = db.getSubjProbeMapping(subjid)
%   query geom mapping table from phy.probes table
inpd = @utils.inputordefault;
[use_db, varargin]=inpd('use_db', 'client', varargin);
dbc = db.labdb.getConnection(use_db);
if isscalar(subjid)
    out = dbc.query('select data from phy.probes where subjid = %d',{subjid});
else
    out = dbc.query('select data from phy.probes where subjid = "%s"',{subjid});
end
if height(out) > 1
    %animals might have multiple probes implanted, therefore we need to
    %adjust the geom to support tmore channels
    num_probe = height(out);
    all_geom = [];
    for  ii = 1:num_probe
        if isempty(all_geom)
            current_channels = 0;
            current_shanks = 0;
            current_x = 0;
            current_probe = 0;
        else
            current_channels = height(all_geom);
            current_shanks = max(all_geom.shank);
            current_x = max(all_geom.shank) + 1000;
            current_probe = max(all_geom.probe);
            % this x is for the x coordniate for each recording channel.
            % But if we have multiple probes, we will want to have a very
            % large x_offset because we want the spike sorting software
            % (kilosort) to treat those channels from different probe
            % differently. This basically tells the spike sorting software
            % that those channels are far from each other.
        end
        this_geom = struct2table(json.mloads(out.data{ii}));
        this_geom.openephys = this_geom.openephys + current_channels;
        this_geom.SE = this_geom.SE + current_channels;
        this_geom.shank = this_geom.shank + current_shanks;
        this_geom.x = this_geom.x + current_x;
        this_geom.probe = this_geom.probe + current_probe;
        all_geom = [all_geom;this_geom];
    end
    for ii = 1:num_probe
    end
else
    %only one probe
    all_geom = struct2table(json.mloads(out.data{1}));
end

end

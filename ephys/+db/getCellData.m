function CD = getCellData(cellid, varargin)

iod = @utils.inputordefault;

full_info = iod('full_info',false,varargin);
use_db = iod('use_db','client',varargin);

dbc = db.labdb.getConnection(use_db);
cellstr = sprintf('%d,',cellid);
cellstr(end)=[];

if full_info
    CD = dbc.query('select a.*, b.wave,b.ts from phy.cellview a join phy.spktimes b using (cellid) where cellid in (%s)',{cellstr});
else
   colstr =  'cellid, ss_cluster, nSpikes   , overlap    , frac_bad_isi , recorded_on_right, sessid, ad_channel, subjid ,region,label,firing_rate,presence_ratio,isi_viol,amp_cutoff,isolation_distance,l_ratio,d_prime,nn_hit_rate,nn_miss_rate,silhouette_score,max_drift,cum_drift,spike_duration,half_width,pt_ratio,repolarization_slope,recovery_slope,snr_best_chan,snr_total,amplitude,spread,velocity_above,velocity_below,nPeaks,nTroughs,spatialDecaySlope,waveformBaselineFlatness,waveformDuration,isSomatic,bc_label,ts';       
   sqlstr = sprintf('select %s from phy.cellview join phy.spktimes using (cellid) where cellid in (%s)',colstr, cellstr);
   CD = dbc.query(sqlstr);
end
    
CD.ts = cellfun(@(x)typecast(x,'double'), CD.ts, 'UniformOutput',0);

end
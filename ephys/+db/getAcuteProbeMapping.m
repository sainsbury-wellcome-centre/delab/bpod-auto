function [probeid,all_geom] = getAcuteProbeMapping(sessid,varargin)
%[probeid,all_geom] = getAcuteProbeMapping(sessid,varargin)
%   query geom mapping table from phy.probes table
% with sessid input, this only support SpikeGLX recording with neuropixel
% now.
% output: probeid: n x 1 array for n_probes
%         all_geom: 1 x n cell array for n_probes. 
%                   each cell will have a table that store the
%                   location/index/region(if available) of this channel
inpd = @utils.inputordefault;
[use_db, varargin]=inpd('use_db', 'client', varargin);
dbc = db.labdb.getConnection(use_db);
[probeid,probe_map] = dbc.get('select probeid,data from phy.probes where probeid in (select probeid from phy.sessionprobes where sessid = %d)',{sessid});
all_geom = cell(1,numel(probe_map));
for ii = 1:numel(probe_map)
    this_geom = struct2table(json.mloads(probe_map{ii}));
    this_geom = sortrows(this_geom,{'probe','spikeGLX'},{'ascend','ascend'});

    %check whether we have call location info and add that
    out = dbc.query('select a.ad_channel as ad_channel, b.region as region from phy.channels a join phy.atlas_coord b on (a.channelid=b.channelid) where a.probeid = %d',{probeid(ii)});
    if isempty(out)
        this_geom.region = cell(height(this_geom),1);
    else
        % have channel location info, attach that into all_geom
        out = sortrows(out,'ad_channel','ascend');
        if height(out)==height(this_geom)
            this_geom.region = out.region;
        end
    end
    all_geom{ii} = this_geom;
    
end


end

%% add sessinfo for one session
% function add_one_sessinfo(this_sess_fldr)
% input: ephys data folder
% default input: current_dir
% you can run this function directly at the ephys data folder
% example: add_one_sessinfo('session_dir','/Volumes/ephys/2257/2257_2021-10-30_12-57-38')
%
% sessid will be automatically picked, but you may need to double check it
% 
% Jingjie Li, 2021-10-30
%
function add_one_sessinfo(varargin)

inpd = @utils.inputordefault;
[this_sess_fldr,varargin] = inpd('session_dir','',varargin); 
[target_sessid_to_write,varargin] = inpd('sessid',[],varargin); 
[acute,varargin] = inpd('acute',0,varargin); 
    
if isempty(this_sess_fldr)
    this_sess_fldr = pwd;
end
dbc = db.labdb.getConnection();

% you may want to change the two vars below
%ephys_fldr = '/Volumes/data/ephys/2209';%base folder for this subject
%this_sess_fldr = '';%recoding folder for the session, you can say [] if you want to add sessinfo for the current folder
%

target_path = fullfile(this_sess_fldr,'sessinfo');
if exist(target_path,'file')
    fprintf('sessinfo already exist, please check!!\n');
elseif ~isempty(target_sessid_to_write)
    % have a sessid input, use this sessid
    write_sessinfo(target_path,target_sessid_to_write,acute);
else
    sep_folder = strsplit(this_sess_fldr,filesep);
    sess_fldr = sep_folder{end};

    target_sessid_to_write = extract_sessid_for_sess_folder(dbc,sess_fldr);
    write_sessinfo(target_path,target_sessid_to_write,acute);
end

end
function target_sessid_to_write = extract_sessid_for_sess_folder(dbc,this_sess_fldr)
sessiondate = regexp(this_sess_fldr,'\d{4}-\d{2}-\d{2}','match');
sessiondate = sessiondate{1};
if this_sess_fldr(4) == '-' %new subjid format: like JLI-R-0004
    subjstr = this_sess_fldr(1:10);
else
    subjstr = this_sess_fldr(1:4);%old subjid format, four digits
end
oe_start_time = regexp(this_sess_fldr,'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}','match');
oe_start_t = datevec(oe_start_time{1},'yyyy-mm-dd_HH-MM-ss');
oe_start_time_str = datestr(oe_start_t,31);
%this_sessinfo_path = fullfile(ephys_fldr,subjstr,this_sess_fldr,'sessinfo');
if this_sess_fldr(4) == '-' %new subjid format: like JLI-R-0004
    sessinfo_out = dbc.query(sprintf('select sessid,TIMESTAMPDIFF(SECOND,''%s'',start_time) as time_diff,start_time,sess_min from beh.sessview where subjid = ''%s'' and sessiondate = ''%s''',oe_start_time_str,subjstr,sessiondate));
else
    sessinfo_out = dbc.query(sprintf('select sessid,TIMESTAMPDIFF(SECOND,''%s'',start_time) as time_diff,start_time,sess_min from beh.sessview where subjid = %s and sessiondate = ''%s''',oe_start_time_str,subjstr,sessiondate));
end
%sessid = sessinfo_out.sessid(sessinfo_out.sessmin>5 & )
target_sessid_to_write = [];
if height(sessinfo_out) == 1
    ask_str = sprintf('sessid %d for recording folder %s, please confirm! y/n?',sessinfo_out.sessid(1),this_sess_fldr);
    answers = input(ask_str,'s');
    if strcmp(answers,'y')
        target_sessid_to_write = sessinfo_out.sessid(1);
    end
elseif height(sessinfo_out) > 1
    potential_sessid = sessinfo_out.sessid(sessinfo_out.sess_min>5 & abs(sessinfo_out.time_diff)<100);
    ask_str = sprintf('sessid %d for recording folder %s, please DOUBLE CHECK! y/sessid?',potential_sessid(1),this_sess_fldr);
    answers = input(ask_str,'s');
    if strcmp(answers,'y')
        target_sessid_to_write = potential_sessid(1);
    elseif ~isnan(str2double(answers))
        target_sessid_to_write = str2double(answers);
    else
        fprintf(2,'please provide a valid sessid for %s\n',this_sess_fldr);
        target_sessid_to_write = [];
    end
else
    fprintf(2,'no corresponding sessid for, please check! %s\n',this_sess_fldr);
    target_sessid_to_write = [];
end
end
function write_sessinfo(target_path,sessid,acute)
    sep_folder = strsplit(target_path,filesep);
    this_sess_fldr = sep_folder{end-1};
    if this_sess_fldr(4) == '-' %new subjid format: like JLI-R-0004
        subjstr = this_sess_fldr(1:10);
    else
        subjstr = this_sess_fldr(1:4);%old subjid format, four digits
    end
    fid = fopen(target_path,'w');
    fprintf(fid,'[sessinfo]\n');
    fprintf(fid,'subjid = %s\n',subjstr);
    fprintf(fid,'sessid = %d\n',sessid);
    fprintf(fid,'acute = %d\n',acute);
    fclose(fid);
    fprintf('sessinfo for sessid %d and folder %s saved!\n',sessid,this_sess_fldr);
end
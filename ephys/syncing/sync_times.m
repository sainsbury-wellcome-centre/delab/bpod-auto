function S = sync_times(subjid, sessid, pathinfo, varargin)
% function M = sync_times(subjid, sessid, oedir, varargin)
%
% synchronize the open ephys and bpod times and save to the database.
%
% Jeffrey Erlich, 2018/ Jingjie Li, 2023

    inpd = @utils.inputordefault;
    
    [Rsq_threshold,varargin] = inpd('rsq_threshold',0.98,varargin); 
    [savedb,~] = inpd('save',true,varargin);
    % I'm not sure exactly what this should be.

    
    %% Is this already done?
    
     dbc = db.labdb.getConnection();
     data = dbc.get('select sync_data from phy.phys_sess where sessid = %d',{sessid});
     if ~isempty(data)
         fprintf(1,"This session is already in the phy.phys_sess table\n");
         S = json.mloads(data{1});
         return;
     end
    %%    
    olddir = pwd;
    
    if numel(pathinfo) > 1 % this should be a npx recording session with multiple probes
        recording_software = pathinfo(1).recording_software;
    else
        recording_software = pathinfo.recording_software;
    end
    

    % first get bpod syncing stuff for this session
    [bts, btn] = getBpodSyncTimes(sessid,'plot',0);
    [bts, btn] = discard_doubles(bts, btn);
    
    % get syncing resupt for npx, or oe
    if strcmp(recording_software,'spikeGLX')
        for ii = 1:numel(pathinfo)
            [ots, otn, SGLdur] = utils.getSpikeGLXSyncTimes('folder_name',pathinfo(ii).recording_data_path);
            [match_trials, oi, bi] = intersect(otn, btn);
            % This will just find the trials where recording was on. 
            % This way if recording was stopped and started, we can still sync
            % the session. The only problem could be if it was stopped or started
            % during a single sync pulse, but it only lasts 19ms so it seems
            % unlikely.


            M = [match_trials, ots(oi), bts(bi)];
            if ii == 1
                % for the first probe, plot the result and ask people to
                % check whether the sync is good
                %out = check_syncing(M,Rsq_threshold);
                out = true;
                assert(out,'something wrong with the sync')
            end
            S(ii).M = M;
            S(ii).cols = {'Trial Number','OE Times', 'Bpod Times'};
            S(ii).rec_duration = SGLdur;
            
        end
        rec_duration = SGLdur;
    elseif strcmp(recording_software,'open-ephys')
         [ots, otn, oedur] = utils.getOESyncTimes('path_info',pathinfo);
         [match_trials, oi, bi] = intersect(otn, btn);
         M = [match_trials, ots(oi), bts(bi)];
         S.M = M;
         S.cols = {'Trial Number','OE Times', 'Bpod Times'};
         S.rec_duration = oedur;
         rec_duration = oedur;
    else
        assert(false,'recording not supported, or incorrect path_info inputs')
    end

    D.sessid = sessid;
    D.subjid = subjid;
    D.sync_data = json.mdumps(S);
    D.rec_duration = rec_duration; 

    quality_manual_check = check_syncing(M,Rsq_threshold);

         if savedb && quality_manual_check
             dbc.saveData('phy.phys_sess',D);
         end
    
end

function out = check_syncing(M,Rsq_threshold)
        % We check whether it is a good sync by fitting a linear model between
        % the times. A good session should be almost perfect. 
        out = true;
        lm = fitlm(M(:,2),M(:,3));

        assert(lm.Rsquared.Adjusted > Rsq_threshold,"The sync times do not match. May be wrong session");
        % May want to put other checks in here.
        
        % plot sync result, ask people to check whether it's good
        figure;
        plot(M(:,2),M(:,3));
        xlabel('recording time')
        ylabel('bpod time')
        title('oe-bpod sync result, should be like a straight line')
        
        ButtonName = questdlg('Does the data look good?', ...
                         'Good Data', ...
                         'Yes', 'No', 'Yes');
                     
        if strcmp(ButtonName,'No')
            warndlg('Please check syncing result.', 'check syncing.');
            out = false;
        end
end

function [ts, tn] = discard_doubles(ts, tn)
    
    if numel(unique(tn)) ~= numel(tn)
        % Check repeats
        repeats = find(diff(tn)==0);
        fprintf(2,'Found %d repeats.\n', numel(repeats))
        ts(repeats) = [];
        tn(repeats) = [];
        
        % Check skips
        skips = find(diff(tn)>1);
        fprintf(2,'Found %d skips.\n', numel(skips))
        fprintf(2,'It seems recording was stopped and re-started. Please double check\n');
        
        % Check out of order trials.
        assert(all(diff(tn)>0), 'oeutils:sync','This session has trials out of order.\n');
               
    end    
end
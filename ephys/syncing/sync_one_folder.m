function sync_one_folder(session_folder)
% read from sessinfo, upload oe-bpod syncing
% sync_one_folder(session_folder)
% 
% example: sync_one_folder('/Volumes/ephys/2257/2257_2021-10-30_12-57-38')
% void input - running at current path
if nargin < 1
    session_folder = pwd;
end

current_dir = pwd;
cd(session_folder);

recording_software = utils.check_recording_software(session_folder);
if strcmp(recording_software,'npx')
    path_info = utils.find_npx_recording_dir(session_folder);
else
    path_info = utils.find_oe_recording_dir(session_folder);
end
sessinfo = utils.ini2struct('sessinfo');
sessinfo = sessinfo.sessinfo;

sep_folder = strsplit(pwd(),filesep);
this_sess_fldr = sep_folder{end};

% upload syncing
SyncS = sync_times(sessinfo.subjid, sessinfo.sessid, path_info);
fprintf('ephys session %s sync complete\n',this_sess_fldr)

cd(current_dir)
end

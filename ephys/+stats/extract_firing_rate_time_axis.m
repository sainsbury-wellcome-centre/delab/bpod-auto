function [time,firing_rate] = extract_firing_rate_time_axis(ts, varargin)
%[time,firing_rate] = extract_firing_rate_time_axis(ts, 'krn',0.1,'binsz',0.01)
%   create a kernal template and apply it to compute the firing rate across
%   the whole session
%   to get an overall view of how this cell fires during the whole session
%   [time,firing_rate] = extract_firing_rate_time_axis(ts);
iod = @utils.inputordefault;
krn = iod('krn',0.1,varargin);
binsz = iod('binsz',0.01,varargin);


if isscalar(krn)
    dx=ceil(5*krn);
    kx=-dx:binsz:dx;
    krn=normpdf(kx,0, krn);
	if isempty(find(kx==0, 1))
		error('Your binsz needs to divide 1 second into interger # of bins');
	end
    krn(kx<0)=0;
    krn=krn/sum(krn);
end

[firing_rate,time]=stats.spike_filter([0],ts,krn,'pre',0,'post',ts(end)-1,'kernel_bin_size',binsz);


end


function [channels,ch_x,ch_y,shk] = extract_channel_same_shank(channel,all_geom,shank_override,exclude_channel)
    if nargin < 3
        shank_override = [];
        exclude_channel = [];
    end
    %
    % 
    if any("openephys" == string(all_geom.Properties.VariableNames))
        all_geom = sortrows(all_geom,{'openephys'});
    else
        all_geom = sortrows(all_geom,{'spikeGLX'});
    end
    if ~isempty(exclude_channel)
        all_geom(exclude_channel,:) = [];
        all_geom.openephys = (1:height(all_geom))';
    end
    if isempty(shank_override)
        channels = find(all_geom.shank == all_geom.shank(channel));
        shk = all_geom.shank(channel);
        if length(unique(all_geom.shank))<2
            % only one single shank
            % extract the recent 7 chs
            dis = sqrt((all_geom.x-all_geom.x(channel)).^2 + (all_geom.y-all_geom.y(channel)).^2);
            dis_threshold = sort(dis);
            dis_threshold = dis_threshold(7);
            channels = find(dis<=dis_threshold);
        end
    else
        channels = find(all_geom.shank == shank_override);
        shk = shank_override;
    end
    ch_x = all_geom.x(channels);
    ch_y = all_geom.y(channels);
    ch_x = ch_x - min(ch_x);
    ch_y = ch_y - min(ch_y);
end

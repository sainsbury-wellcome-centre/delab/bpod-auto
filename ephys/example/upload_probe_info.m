% add phy.probes
D = struct();
D.subjid = 'JLI-R-0023';
D.n_channels = 64;
D.probe_num = '22S5-11';
D.target_location = 'left FOF';
D.target_AP = 2.2;
D.target_ML = 1.4;
D.target_DV = -1;
D.made_by = 'CambridgeNeurotech';
D.made_on = '2023-03-14';
D.notes = 'open: short:';
D.probename = 'JLI-R-0023_1';
%recording_info = utils.readSpikeGLXmeta('/media/jingjie/spike/spk_sorting/npx/CFB-M-0022_2023-12-06_17-11-00_g0/CFB-M-0022_2023-12-06_17-11-00_g0_imec0/CFB-M-0022_2023-12-06_17-11-00_g0_t0.imec0.ap.meta');
%all_geom = utils.makeChanMapFromSGLMeta(recording_info);
all_geom = utils.make_cambridge_probe_geom({'ASSY-236-H6'});
D.data = json.mdumps(table2struct(all_geom));
%%
dbc = db.labdb.getConnection('manage');
dbc.saveData('phy.probes',D);
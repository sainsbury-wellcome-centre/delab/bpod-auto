%% upload probe info for npx, using one example session (nload from npx meta file for probe info)
% only for one probe
recording_dir = '/media/jingjie/spike/spk_sorting/sndmap/JLI-R-0042_2024-08-31_09-17-00_g0';
recording_path_info = utils.find_npx_recording_dir(recording_dir);

recording_settings = utils.readSpikeGLXmeta(recording_path_info(1).meta_data_ap);
all_geom = utils.makeChanMapFromSGLMeta(recording_settings);

nSites = recording_settings.nSites;%for npx, normally 384
    
D = struct();
D.subjid = 'JLI-R-0042';
D.n_channels = nSites;
D.probe_num = recording_settings.imDatPrb_pn;
D.target_location = 'left FOF';
D.made_by = 'Neuropixel';
D.made_on = '2024-08-24';
D.probename = 'JLI-R-0042_1';
D.data = json.mdumps(table2struct(all_geom));
dbc.saveData('phy.probes',D);

%% path configuration and loading probe mapping
%addpath(genpath('/media/erlichlab/hdd/Erlichlab_repos/Kilosort-2.0')) % path to kilosort folder
%addpath('/home/jingjie/repos/npy-matlab') % for converting to Phy
%% ypath need to be filled
pathToYourConfigFile = '/home/jingjie/repos/Kilosort-2.0/configFiles'; 
recording_path = '/media/jingjie/spike/spk_sorting/npx/CFB-M-0022_2023-12-06_17-11-00_g0';
%%
[path_info] = utils.find_npx_recording_dir(recording_path);
trange = [0 inf]; % time range to sort, this is in secs, for example if you want to only do sorting from 0 to 100 minus, do [0,100*60]
output_folder_name = 'kilosort';

current_dir = pwd;
n_probes = numel(path_info);
for ii = 1:numel(path_info)
    %cd(path_info(1).recording_data_path)
    if ~exist(fullfile(path_info(ii).recording_data_path,output_folder_name),'dir')
        % create a folder to store kilosort output
        mkdir(fullfile(path_info(ii).recording_data_path,output_folder_name));
    end

    recording_settings = utils.readSpikeGLXmeta(path_info(ii).meta_data_ap);
    all_geom = utils.makeChanMapFromSGLMeta(recording_settings);
    rootZ = path_info(ii).recording_data;

    ops.NchanTOT = recording_settings.nSavedChans;
    ops.trange = trange;

    rootH = fullfile(path_info(ii).recording_data_path,output_folder_name);
    rootO = rootH;

    all_geom = sortrows(all_geom,{'spikeGLX'});
    %all_geom.connected(ch_to_exclude) = false; %seems we don't need to
    %exclude channels for npx?
    chanMapFile = 'temp_kilosortChanMap.mat';
    ops.chanMap = fullfile(rootH,chanMapFile);
    %ops.chanMap = fullfile(pathToYourConfigFile,'neuropixPhase3B2_kilosortChanMap.mat');
    utils.convert_save_kilo_sort_probe_geom(all_geom,ops.chanMap)
    
    run(fullfile(pathToYourConfigFile, 'configFile384.m'))
    ops.fproc   = fullfile(rootH, 'temp_wh.dat'); % proc file on a fast SSD
    % this block runs all the steps of the algorithm
    fprintf('Recording %d in %d, Looking for data inside %s \n',ii,n_probes, rootZ)

    % load binary file
    ops.fbinary = path_info(ii).recording_data;
    
    %% start to run kilosort here
    % preprocess data to create temp_wh.dat
    rez = preprocessDataSub(ops);
    %
    % time-reordering as a function of drift
    rez = clusterSingleBatches(rez);
    %
    % saving here is a good idea, because the rest can be resumed after loading rez
    save(fullfile(rootO, 'rez.mat'), 'rez', '-v7.3');
    
    % main tracking and template matching algorithm
    rez = learnAndSolve8b(rez);
    
    % OPTIONAL: remove double-counted spikes - solves issue in which individual spikes are assigned to multiple templates.
    % See issue 29: https://github.com/MouseLand/Kilosort2/issues/29
    %rez = remove_ks2_duplicate_spikes(rez);
    
    % final merges
    rez = find_merges(rez, 1);
    
    % final splits by SVD
    rez = splitAllClusters(rez, 1);
    
    % final splits by amplitudes
    rez = splitAllClusters(rez, 0);
    
    % decide on cutoff
    rez = set_cutoff(rez);
    
    fprintf('Recording %d in %d, found %d good units \n',ii,n_probes, sum(rez.good>0))

    %% write to Phy
    fprintf('Recording %d in %d, Saving results to Phy  \n',ii,n_probes)
    rezToPhy(rez, rootO);
    
    % if you want to save the results to a Matlab file...
    
    % discard features in final rez file (too slow to save)
    rez.cProj = [];
    rez.cProjPC = [];
    
    % final time sorting of spikes, for apps that use st3 directly
    [~, isort]   = sortrows(rez.st3);
    rez.st3      = rez.st3(isort, :);
    
    % Ensure all GPU arrays are transferred to CPU side before saving to .mat
    rez_fields = fieldnames(rez);
    for i = 1:numel(rez_fields)
        field_name = rez_fields{i};
        if(isa(rez.(field_name), 'gpuArray'))
            rez.(field_name) = gather(rez.(field_name));
        end
    end
    
    % save final results as rez2
    fprintf('Recording %d in %d, Saving final results in rez2  \n',ii,n_probes)
    fname = fullfile(rootO, 'rez2.mat');
    save(fname, 'rez', '-v7.3');
    
    ksversion = 'ks2.0';
    fname = fullfile(rootO, 'version.mat');
    save(fname, 'ksversion');
end
%% path configuration and loading probe mapping
%addpath(genpath('/media/erlichlab/hdd/Erlichlab_repos/Kilosort-2.0')) % path to kilosort folder
%addpath('/home/jingjie/repos/npy-matlab') % for converting to Phy

%% import inputs: EDIT THESE!!
ops.NchanTOT  = 64; % total number of channels in your recording (not include AUX)
% if you encounter with the error about 'Error in MasterKiloSortLJB (line
% 59)_rez = clusterSingleBatches(rez);_Error using gpuArray/eig'
%ops.reorder = 0;
 % channls to exclude (if you know you have some bad channels during recording, ch starts from 1 (ch = se + 1))
ch_to_exclude = [40,42,48,53,55,57,59,62:64];

ops.trange    = [0 inf]; % time range to sort, this is in secs, for example if you want to only do sorting from 0 to 100 minus, do [0,100*60]
all_geom = make_cambridge_probe_geom({'ASSY-236-H6'});
%all_geom = make_cambridge_probe_geom('ASSY-236-E-2');
%all_geom = db.getSubjProbeMapping('JLI-R-0016');

ephys_data_folder = '/home/jingjie/Desktop/spk_sorting/sndmap/JLI-R-0023_2024-03-21_14-22-08';
sorting_result_output_folder= fullfile(ephys_data_folder,'kilosort');
%% process folder info and generate binary data file
[path_info] = utils.find_oe_recording_dir(ephys_data_folder);
[rootZ,~,~] = fileparts(path_info.recording_data);
[recording_info] = utils.extract_xml_recording_config(path_info);
ops.NchanTOT = recording_info.n_total_channels;
%%
%rootZ = '/mnt/ephys/2234/2234_2021-10-15_09-36-14/Record Node 101/experiment1/recording1/continuous/Rhythm_FPGA-100.0'; % the raw data binary file is in this folder
rootH = sorting_result_output_folder; % path to temporary binary file (same size as data, should be on fast SSD)
rootO = sorting_result_output_folder;% output folder
pathToYourConfigFile = '/home/jingjie/repos/Kilosort-2.0/configFiles'; % take from Github folder and put it somewhere else (together with the master_file)
%make_H_probe_geom;
%load('/mnt/ephys/2169/all_geom.mat'); % geneate all_geom file, you may want to modify this if you are using another probe
%all_geom.connected = true(64,1);

%se_to_exclude = [];
% we will generate a temp probe mapping file at rootH
%ch_to_exclude = [ch_to_exclude,se_to_exclude+1];
all_geom = sortrows(all_geom,{'openephys'});
all_geom.connected(ch_to_exclude) = false;
chanMapFile = 'temp_kilosortChanMap.mat';
ops.chanMap = fullfile(rootH,chanMapFile);
convert_save_kilo_sort_probe_geom(all_geom,ops.chanMap)

run(fullfile(pathToYourConfigFile, 'configFile384.m'))
ops.fproc   = fullfile(rootH, 'temp_wh.dat'); % proc file on a fast SSD
% this block runs all the steps of the algorithm
fprintf('Looking for data inside %s \n', rootZ)

% main parameter changes from Kilosort2 to v2.5
%ops.sig        = 20;  % spatial smoothness constant for registration
%ops.fshigh     = 300; % high-pass more aggresively
%ops.nblocks    = 5; % blocks for registration. 0 turns it off, 1 does rigid registration. Replaces "datashift" option. 
%ops.ThPre = 8; %default:8
%ops.Th = [10 4];  %[10 4]
% is there a channel map file in this folder?
%fs = dir(fullfile(rootZ, 'chan*.mat'));
%if ~isempty(fs)
%    ops.chanMap = fullfile(rootZ, fs(1).name);
%end

% find the binary file
fs          = [dir(fullfile(rootZ, '*.bin')) dir(fullfile(rootZ, '*_fixed.dat')) dir(fullfile(rootZ, 'continuous.dat'))];
ops.fbinary = fullfile(rootZ, fs(1).name);

% preprocess data to create temp_wh.dat
rez = preprocessDataSub(ops);
%
% time-reordering as a function of drift
rez = clusterSingleBatches(rez);
%
% saving here is a good idea, because the rest can be resumed after loading rez
save(fullfile(rootO, 'rez.mat'), 'rez', '-v7.3');

% main tracking and template matching algorithm
rez = learnAndSolve8b(rez);

% OPTIONAL: remove double-counted spikes - solves issue in which individual spikes are assigned to multiple templates.
% See issue 29: https://github.com/MouseLand/Kilosort2/issues/29
%rez = remove_ks2_duplicate_spikes(rez);

% final merges
rez = find_merges(rez, 1);

% final splits by SVD
rez = splitAllClusters(rez, 1);

% final splits by amplitudes
rez = splitAllClusters(rez, 0);

% decide on cutoff
rez = set_cutoff(rez);

fprintf('found %d good units \n', sum(rez.good>0))

%% write to Phy
fprintf('Saving results to Phy  \n')
rezToPhy(rez, rootO);

% if you want to save the results to a Matlab file...

% discard features in final rez file (too slow to save)
rez.cProj = [];
rez.cProjPC = [];

% final time sorting of spikes, for apps that use st3 directly
[~, isort]   = sortrows(rez.st3);
rez.st3      = rez.st3(isort, :);

% Ensure all GPU arrays are transferred to CPU side before saving to .mat
rez_fields = fieldnames(rez);
for i = 1:numel(rez_fields)
    field_name = rez_fields{i};
    if(isa(rez.(field_name), 'gpuArray'))
        rez.(field_name) = gather(rez.(field_name));
    end
end

% save final results as rez2
fprintf('Saving final results in rez2  \n')
fname = fullfile(rootO, 'rez2.mat');
save(fname, 'rez', '-v7.3');

ksversion = 'ks2.0';
fname = fullfile(rootO, 'version.mat');
save(fname, 'ksversion');
%cdelete ops.chanMap %remove temp channal map file

%% functions
function name_str = extract_recording_file_name_str(oe_dir)
    current_dir = pwd;
    cd(oe_dir);
    if exist('100_CH1.continuous','file')
        name_str = 'CH';
    elseif exist('100_1.continuous','file')
        name_str = '100_';
    else
        name_str = '';
        error('cannot find open ephys recording file, please check your path');
    end
    cd(current_dir);
end
import sys
import os

sys.path.append('/home/jingjie/repos/ecephys_spike_sorting')#path for ecephys_spike_sorting repo: clone from https://github.com/AllenInstitute/ecephys_spike_sorting
from ecephys_spike_sorting.modules.quality_metrics.__main__ import calculate_quality_metrics
from ecephys_spike_sorting.modules.mean_waveforms.__main__ import calculate_mean_waveforms


def main():
    print('ephys file main directory: ')
    base_dir = input()
    
    kilosort_folders = find_kilosort_folders(base_dir)
    print(kilosort_folders)
    for ks_folders in kilosort_folders:
        print("doing metrics for stuff: "+ks_folders)
        run(ks_folders)


def run(kilosort_dir,spikes_per_epoch=3000):  
    input_args = {'quality_metrics_params':{
            'isi_threshold' : 0.0015,
            'min_isi' : 0.000166,
            'num_channels_to_compare' : 7,
            'max_spikes_for_unit' : 500,
            'max_spikes_for_nn' : 10000,
            'n_neighbors' : 4,
            'n_silhouette' : 10000,
            'drift_metrics_interval_s' : 51,
            'drift_metrics_min_spikes_per_interval' : 10,
            'include_pc_metrics':True,
            'quality_metrics_output_file':''},
            'directories':{'kilosort_output_directory':''},
            'ephys_params':{
            'ap_band_file' : '',
            'num_channels' : 385,
            'sample_rate' : 30000,
            'bit_volts' : 2.34375,
            'vertical_site_spacing' : 10e-6},
            'mean_waveform_params':{'mean_waveforms_file':'','samples_per_spike':91,'pre_samples':30,'num_epochs':1,'spikes_per_epoch':spikes_per_epoch,'upsampling_factor':1,'spread_threshold':0.12,'site_range':16},
            'waveform_metrics':{'waveform_metrics_file':''}} 
    
    
    input_args['directories']['kilosort_output_directory'] = kilosort_dir
    input_args['quality_metrics_params']['quality_metrics_output_file'] = os.path.join(kilosort_dir, 'cluster_quality.csv')
    input_args['mean_waveform_params']['mean_waveforms_file'] = os.path.join(kilosort_dir, 'mean_waveform.npy')
    input_args['waveform_metrics']['waveform_metrics_file'] = os.path.join(kilosort_dir, 'waveform_quality.csv')
    input_args['ephys_params']['ap_band_file'] = find_ap_bin_files(kilosort_dir)

    if 'imec' in kilosort_dir:
        print("npx")
        calculate_mean_waveforms(input_args)
    calculate_quality_metrics(input_args)



def find_kilosort_folders(root_dir):
    kilosort_paths = []
    for root, dirs, files in os.walk(root_dir):
        if 'kilosort' in dirs:
            kilosort_paths.append(os.path.abspath(os.path.join(root, 'kilosort')))
        elif 'imec' in dirs and os.path.isdir(os.path.join(root, dirs)):
            imec_dir = os.path.join(root, dirs)
            kilosort_paths.append(os.path.abspath(os.path.join(imec_dir, 'kilosort')))
    return kilosort_paths
    
def find_ap_bin_files(kilosort_dir):
    root_dir = os.path.join(kilosort_dir, os.pardir)
    ap_bin_files = []
    for root, dirs, files in os.walk(root_dir):
        for file_name in files:
            if 'ap.bin' in file_name:
                ap_bin_files=os.path.abspath(os.path.join(root, file_name))
    return ap_bin_files


if __name__ == "__main__":
    main()

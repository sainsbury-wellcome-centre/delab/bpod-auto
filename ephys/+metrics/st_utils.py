# Use spikeinterface sorting_analyzer object to analyze the quality of the sorting
# Chaofei Bao, 26/4/2024

import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy import stats
import os

def spk_sliding_count(we, win_size_s=5*60, start_s=0, step_s=1, unit_ids=None):
    win_size = win_size_s * we.sampling_frequency
    start = start_s * we.sampling_frequency
    step = step_s * we.sampling_frequency
    
    win_fr_drop = {}
    
    if unit_ids is None:
        unit_ids = we.unit_ids
        
    num_segs = we.get_num_segments()
    seg_lengths = [we.get_num_samples(i) for i in range(num_segs)]
    total_length = we.get_total_samples()
    
    # sorting can be read from sorting_analyzer object
    sorting = we.sorting
    
    for unit_id in unit_ids:
        spike_train=[]
        for segment_indx in range(num_segs):
            st = sorting.get_unit_spike_train(unit_id=unit_id, segment_index=segment_indx)
            st = st + np.sum(seg_lengths[:segment_indx])
            spike_train = np.append(spike_train, st)
            
        fr_wins = sliding_counter(spike_train, start, total_length, win_size, step)
        win_fr_drop[unit_id] = (np.mean(fr_wins)-np.min(fr_wins))/np.mean(fr_wins)
    return win_fr_drop

def spk_linear_drift_check(we, win_size_s=3*60, start_s=0, unit_ids=None):
    win_size = win_size_s * we.sampling_frequency
    start = start_s * we.sampling_frequency
    
    linear_sig = {}
    shapiro_sig = {}
    
    if unit_ids is None:
        unit_ids = we.unit_ids
        
    num_segs = we.get_num_segments()
    seg_lengths = [we.get_num_samples(i) for i in range(num_segs)]
    total_length = we.get_total_samples()
    
    # sorting can be read from sorting_analyzer object
    sorting = we.sorting
    
    for unit_id in unit_ids:
        spike_train=[]
        for segment_indx in range(num_segs):
            st = sorting.get_unit_spike_train(unit_id=unit_id, segment_index=segment_indx)
            st = st + np.sum(seg_lengths[:segment_indx])
            spike_train = np.append(spike_train, st)
            
        linear_sig[unit_id],shapiro_sig[unit_id] = bin_linear_check(spike_train, start, total_length, win_size, we.sampling_frequency)
        
    return linear_sig,shapiro_sig
    
    
    
    
def sliding_counter(spike_train, start, end, win, step):
    bin_starts = np.arange(start, end+1-win, step)
    bin_ends = bin_starts + win
    last_index = np.searchsorted(spike_train, bin_ends, side='right')
    first_index = np.searchsorted(spike_train, bin_starts, side='left')
    
    win_c = last_index - first_index
    return  win_c


def bin_linear_check(spike_train, start, end, win, fs):
    total_time = end-start
    num_bins = round(total_time / win)+1
    bins = np.linspace(start, end, num_bins)
    
    x = bins/fs/60 #np.arange(bins.size-1), make the regression meaningful
    x = x[1:]/2
    y,_ = np.histogram(spike_train, bins)
    y = sm.add_constant(y)
    
    m = sm.OLS(x, y).fit()
    return m.pvalues[1], stats.shapiro(m.resid).pvalue


def compute_sliding_fr_qm(we):
    fr_sliding_5m = spk_sliding_count(we, win_size_s=5*60, start_s=0, step_s=2)
    fr_sliding_10m = spk_sliding_count(we, win_size_s=10*60, start_s=0, step_s=2)
    fr_sliding_15m = spk_sliding_count(we, win_size_s=15*60, start_s=0, step_s=2)

    linear_sig, shapiro_sig = spk_linear_drift_check(we, win_size_s=3*60, start_s=0)
    
    fr_sld_5min = pd.Series(fr_sliding_5m)
    fr_sld_5min.name = 'fr_sld_5m'
    fr_sld_10min = pd.Series(fr_sliding_10m)
    fr_sld_10min.name = 'fr_sld_10m'
    fr_sld_15min = pd.Series(fr_sliding_15m)
    fr_sld_15min.name = 'fr_sld_15m'

    linear_sig = pd.Series(linear_sig)
    linear_sig.name = 'drift_linear_sig'
    shapiro_sig = pd.Series(shapiro_sig)
    shapiro_sig.name = 'drift_resid_sig'

    qm_more = fr_sld_5min.to_frame().merge(fr_sld_10min, 
                                           left_index=True, right_index=True).merge(fr_sld_15min, 
                                            left_index=True, right_index=True).merge(linear_sig, 
                                             left_index=True, right_index=True).merge(shapiro_sig, 
                                              left_index=True, right_index=True)
    return qm_more

def make_clus_tb(st_path):
    # from JenniferColonell's methods
    cluLabel = np.load(os.path.join(st_path, 'spike_clusters.npy'))
    spkTemplate = np.load(os.path.join(st_path,'spike_templates.npy'))
    cluLabel = np.squeeze(cluLabel)
    spkTemplate = np.squeeze(spkTemplate)
    unqLabel, labelCounts = np.unique(cluLabel, return_counts = True)
    nLabel = unqLabel.shape[0]
    maxLabel = np.max(unqLabel)
    templates = np.load(os.path.join(st_path, 'templates.npy'))
    channel_map = np.load(os.path.join(st_path, 'channel_map.npy'))
    channel_map = np.squeeze(channel_map)
    w_inv = np.load((os.path.join(st_path, 'whitening_mat_inv.npy')))
    peak_channels = np.zeros([nLabel,],'uint32')
    
    for i in np.arange(0,nLabel):
        curr_spkTemplate = spkTemplate[np.where(cluLabel==unqLabel[i])]
        template_mode = np.argmax(np.bincount(curr_spkTemplate))
        currT = templates[template_mode,:].T
        curr_unwh = np.matmul(w_inv, currT)
        currdiff = np.max(curr_unwh,1) - np.min(curr_unwh,1)
        peak_channels[i] = channel_map[np.argmax(currdiff)]
        
    clus_Table = np.zeros((maxLabel+1, 2), dtype='uint32')
    clus_Table[unqLabel, 0] = labelCounts
    clus_Table[unqLabel, 1] = peak_channels
    return clus_Table

def gen_cwave_file(st_path):
    clu_table = make_clus_tb(st_path)
    
    spk_times = np.load(os.path.join(st_path, 'spike_times.npy')).astype('uint64')
    spk_clu = np.load(os.path.join(st_path, 'spike_clusters.npy')).astype('uint32')
    
    np.save(os.path.join(st_path, 'clus_Table.npy'), clu_table)
    np.save(os.path.join(st_path, 'spike_clusters_uint32.npy'), spk_clu)
    np.save(os.path.join(st_path, 'spike_times_uint64.npy'), spk_times)
    
    
    
    
    
    
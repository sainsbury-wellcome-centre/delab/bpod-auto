function [waveform_metrics_out,wf_out] = waveform_metrics(datPars, spikeTimes,ad_channel)
%Extract important 1d and 2d waveform metrics
%   [waveform_metrics] = waveform_metrics(datPars, spikeTimes)
%   
%   input: 
% datPars.nCh = 64; % if you excluded certain channels this will not be 64!
% datPars.dataType = 'int16';
% datPars.wfWin = [-30 30];
% datPars.Fs = 30000;
% datPars.makePlots = true;
% datPars.nSpikesToUse = 5000; % usually we can randomly pick 5000 spikes
% to save computing resources
% datPars.useAllSpikes = false; % set this if you want to use all the
% spikes for the clusters
% datPars.geom = all_geom;%order by openpehys
% datPars.connection = site_connection;%at the order of all_geom
% datPars.wrot - whitening matrix kilosort used for this session, can be
% found at rez.Wrot at the rez2.mat
% 
% 
% sorting_result_folder = '/media/erlichlab/ssd2/spk_sorting_sndmap/2234_kilosort_20210904'
% datPars.filename = fullfile(sorting_result_folder,'temp_wh.dat');
% 
% 
% 
% st = double(readNPY(fullfile(sorting_result_folder, 'spike_times.npy')))./datPars.Fs;
% clu = readNPY(fullfile(sorting_result_folder, 'spike_clusters.npy'));
% 
% spikeTimes = st(clu==22);
%
% 
% Output: waveform_metrics as struct array
% waveform duration, peak-trough ratio, repolarization slope, recovery
% slope, snr
%
% Jingjie Li, 2021/09/29
waveform_metrics_out = struct();
wf_out = struct();
bitVolts = datPars.bitVolts; % from intan chip data sheet, LSB of the ADC
filter_offset = 64;

if numel(spikeTimes)>200000
    datPars.useAllSpikes = 0;%cannot compute meanWf use all spikes if nSpikes>200k
end

% from trueSpikeSNR, read from preprocessed data to get raw waveform.
mmf =datPars.mmf;

if ~isempty(datPars.filter)
    datPars.wfWin(1) = datPars.wfWin(1)-filter_offset;
    datPars.wfWin(2) = datPars.wfWin(2)+filter_offset;
else
    filter_offset=0;
end
wfNSamps = numel(datPars.wfWin(1):datPars.wfWin(end))-2*filter_offset;
if datPars.useAllSpikes
    stToUse = spikeTimes;
else
    stToUse = spikeTimes(randi(numel(spikeTimes), [datPars.nSpikesToUse 1]));
end
stToUse(stToUse<100)=[];
stToUse((datPars.nSamp-stToUse)<1000) = [];
%wfSamps = ceil(stToUse*datPars.Fs)+datPars.wfWin(1);
wfSamps = stToUse+datPars.wfWin(1);%we directly feed the index into it
wfs = zeros(numel(datPars.chanMap), wfNSamps, numel(wfSamps));
parfor q = 1:numel(wfSamps)
    tmpWf = mmf.Data.x(:,wfSamps(q):wfSamps(q)+wfNSamps-1+2*filter_offset);
    if isfield(datPars,'wrot') %ks2.5 need to remove whienting
        tmpWf = (double(tmpWf)'/datPars.wrot)';
    elseif ~isempty(datPars.filter)
        tmpWf = filtfilt(datPars.filter{1},datPars.filter{2},double(tmpWf'));
        tmpWf = tmpWf';
    end
    wfs(:,:,q) = tmpWf(datPars.chanMap,filter_offset+1:diff(datPars.wfWin)+1-filter_offset);
end

wfs = wfs .* bitVolts; %convert to uV voltage
mnWF = mean(wfs, 3); 
varWF = std(wfs,0,3);

% restore excluded channels by inserting nans
raw_waveform_mat = mnWF';%mnWF';
raw_waveform_var_mat = varWF';
if any(~datPars.connection) %have unconnected/exclueded channels during spk sorting
    % by default, kilosort skipped the unconnected channles,
    % here we are adding the template of unconnected channles back
    % but leaving it as nan to indicate them.
    raw_waveform_mat_adding=[raw_waveform_mat,nan(size(raw_waveform_mat,1),length(find(datPars.connection==0)))];%add cols of nan at the back
    raw_waveform_var_mat_adding = [raw_waveform_var_mat,nan(size(raw_waveform_var_mat,1),length(find(datPars.connection==0)))];
    idx_uncon_chan = 1:height(datPars.geom);idx_uncon_chan(~datPars.connection)=[]; idx_uncon_chan = [idx_uncon_chan,find(datPars.connection==0)'];
    % generate a channel order index, to move those nan cols back
    % to where kilosort skipped (ignored channels)
    raw_waveform_mat_adding = [idx_uncon_chan;raw_waveform_mat_adding];
    raw_waveform_var_mat_adding = [idx_uncon_chan;raw_waveform_var_mat_adding];
    raw_waveform_mat = sortrows(raw_waveform_mat_adding',1)';
    raw_waveform_var_mat = sortrows(raw_waveform_var_mat_adding',1)';
    raw_waveform_mat = raw_waveform_mat(2:end,:);
    raw_waveform_var_mat = raw_waveform_var_mat(2:end,:);
end

% below are our own code to process basic SNR and waveform info
[peak_chan,pead_idx] = find(mnWF == min(min(mnWF)));
wfs_on_peak_channel = reshape(wfs(peak_chan,:,:),size(wfs,2),size(wfs,3));
mean_wfs_on_peak_channel = mnWF(peak_chan,:)';%here extracted the mean waveform at the max channel
timestamps = (1:numel(mean_wfs_on_peak_channel))'./datPars.Fs;

wf_out.wave_mn = single(raw_waveform_mat);%convert to single to save db space
wf_out.wave_sd = single(raw_waveform_var_mat);
if any(strcmp('probe', datPars.geom.Properties.VariableNames))
    % have multiple probes in this recording file, remove all waveform from
    % another probe
    if numel(unique(datPars.geom.probe))>1
        this_probe_idx = datPars.geom.probe(datPars.geom.openephys==ad_channel);
        % have a col for probe in geom file, might have two probes?
        if numel(unique(datPars.geom.probe))
            wf_out.wave_mn(:,datPars.geom.probe ~= this_probe_idx) = nan;
            wf_out.wave_sd(:,datPars.geom.probe ~= this_probe_idx) = nan;
        end
    end
end





% 2d features
[amplitude, spread, velocity_above, velocity_below] = calculate_2D_features(datPars.geom,raw_waveform_mat,timestamps,0.12,20e-6);

% 1d features
waveform_metrics_out.duration = calculate_waveform_duration(mean_wfs_on_peak_channel,timestamps);
waveform_metrics_out.halfwidth = calculate_waveform_halfwidth(mean_wfs_on_peak_channel, timestamps);
waveform_metrics_out.pt_ratio = calculate_waveform_PT_ratio(mean_wfs_on_peak_channel);
waveform_metrics_out.repolarization_slope = calculate_waveform_repolarization_slope(mean_wfs_on_peak_channel, timestamps);
waveform_metrics_out.recovery_slope = calculate_waveform_recovery_slope(mean_wfs_on_peak_channel, timestamps);
waveform_metrics_out.snr_best_chan = one_channel_snr(wfs_on_peak_channel);

waveform_metrics_out.amplitude = amplitude;
waveform_metrics_out.spread = spread;
waveform_metrics_out.velocity_above = velocity_above;
waveform_metrics_out.velocity_below = velocity_below;

% from trueSpikeSNR.m, normal snr computing process
% SNR for projections, reshape and just multiply
mnWFlin = reshape(mnWF,1,[]);
pOwn = mnWFlin*reshape(wfs, numel(mnWF), []);

% SNR 2. read many other random points and get their projections onto the mean

% now pick random times to look at - but NOT random times around existing
% spikes. 
%sampsTaken = ceil(spikeTimes*datPars.Fs)+(datPars.wfWin(1):datPars.wfWin(end));
sampsTaken = double(spikeTimes) + (datPars.wfWin(1):datPars.wfWin(end));
allSamps = 1000:datPars.nSamp-1000;%in case of go beyond the bound
sampsAvail = allSamps(~ismember(allSamps, sampsTaken(:)));
wfSamps = sampsAvail(randi(numel(sampsAvail), [numel(stToUse) 1])); 
wfs = zeros(numel(datPars.chanMap), wfNSamps, numel(wfSamps));
for q = 1:numel(wfSamps)
    tmpWf = mmf.Data.x(:,wfSamps(q):wfSamps(q)+wfNSamps-1+2*filter_offset);
    if isfield(datPars,'wrot')
        tmpWf = (double(tmpWf)'/datPars.wrot)';
    elseif ~isempty(datPars.filter)
        tmpWf = filtfilt(datPars.filter{1},datPars.filter{2},double(tmpWf'));
        tmpWf = tmpWf';
    end
    wfs(:,:,q) = tmpWf(datPars.chanMap,filter_offset+1:diff(datPars.wfWin)+1-filter_offset);
end
wfs = wfs .* bitVolts; %convert to uV voltage
pOther = mnWFlin*reshape(wfs, numel(mnWF), []);

% SNR 3. plot histogram/density, compute SNR by gaussian approximation

waveform_metrics_out.snr_total = (mean(pOwn)-mean(pOther))./std(pOther);
end

function duration = calculate_waveform_duration(waveform,timestamps)
[~,trough_idx] = min(waveform);
[~,peak_idx] = max(waveform);

    % to avoid detecting peak before trough
    if waveform(peak_idx) > abs(waveform(trough_idx))
        ts_win = timestamps(peak_idx:numel(timestamps));
        duration =  ts_win(waveform(peak_idx:numel(waveform))==min(waveform(peak_idx:numel(waveform)))) - timestamps(peak_idx);
    else
        ts_win = timestamps(trough_idx:numel(timestamps));
        duration =  ts_win(waveform(trough_idx:numel(waveform))==max(waveform(trough_idx:numel(waveform)))) - timestamps(trough_idx);
    end
    
    duration = duration * 1000;
end

function halfwidth = calculate_waveform_halfwidth(waveform, timestamps)

    [~,trough_idx] = min(waveform);
    [~,peak_idx] = max(waveform);

    try
        if waveform(peak_idx) > abs(waveform(trough_idx))
            threshold = waveform(peak_idx) * 0.5;
            thresh_crossing_1 = find(waveform(1:peak_idx) > threshold, 1 );
            thresh_crossing_2 = find(waveform(peak_idx:end) < threshold, 1 ) + peak_idx;
        else
            threshold = waveform(trough_idx) * 0.5;
            thresh_crossing_1 = find(waveform(1:trough_idx) < threshold, 1 );
            thresh_crossing_2 = find(waveform(trough_idx:end) > threshold, 1 ) + trough_idx;
        end
        halfwidth = (timestamps(thresh_crossing_2) - timestamps(thresh_crossing_1)) * 1000;%convert to ms
    catch
        halfwidth = nan;
    end

end

function pt_ratio = calculate_waveform_PT_ratio(waveform)
[~,trough_idx] = min(waveform);
[~,peak_idx] = max(waveform);

pt_ratio = abs(waveform(peak_idx) / waveform(trough_idx));
end

function repolarization_slope = calculate_waveform_repolarization_slope(waveform, timestamps, window)
    if nargin <3
        window = 20;
    end
    
    [~,max_point] = max(abs(waveform));

    waveform = - waveform * (sign(waveform(max_point))); % invert if we're using the peak
    
    if max_point<numel(timestamps) && (max_point+window)<numel(timestamps)
        repolarization_slope = polyfit(timestamps(max_point:max_point+window), waveform(max_point:max_point+window),1);
        repolarization_slope = repolarization_slope(1) * 1e-6; %convert to V/s
    else
        repolarization_slope = nan;
    end
end

function recovery_slope = calculate_waveform_recovery_slope(waveform, timestamps, window)
    if nargin <3
        window = 20;
    end
    
    [~,max_point] = max(abs(waveform));
    waveform = - waveform * (sign(waveform(max_point))); % invert if we're using the peak

    [~,peak_idx] = max(waveform(max_point:end));
    peak_idx = peak_idx + max_point;
    
    if peak_idx<numel(timestamps) && (peak_idx+window)<numel(timestamps)
        recovery_slope = polyfit(timestamps(peak_idx:peak_idx+window), waveform(peak_idx:peak_idx+window),1);
        recovery_slope = recovery_slope(1) * 1e-6; %convert to V/s
    else
        recovery_slope = nan;
    end
    
end

function snr = one_channel_snr(waveforms)
W_bar = nanmean(waveforms, 2);%here extracted the mean waveform at the max channel
A = max(W_bar) - min(W_bar);
e = bsxfun(@minus,waveforms,W_bar);
snr = A/(2*nanstd(reshape(e,1,[])));
end

function [amplitude, spread, velocity_above, velocity_below] = calculate_2D_features(all_geom,waveform,timestamps,spread_threshold,site_spacing)
    if nargin < 6
        spread_threshold = 0.12;
        site_spacing=20e-6;
    end
    raw_waveform_mat = waveform;
    
    all_geom = sortrows(all_geom,'openephys');
    [~,rk] = sort(all_geom.y);
    raw_waveform_mat_sorted = raw_waveform_mat(:,rk)';% nChan x nSamp mat
    [peak_amp,peak_idx] = max(raw_waveform_mat_sorted');
    [trough_amp,trough_idx] = min(raw_waveform_mat_sorted');
    overall_amp = peak_amp - trough_amp;
    [max_amp,max_idx] = max(overall_amp);
    amplitude = max_amp;
    
    if numel(unique(all_geom.x)) == 1 && numel(unique(all_geom.shank)) == 1%we process 2d info only for one shank probe
        points_above_threshold = find(overall_amp>max_amp*spread_threshold);
        points_above_threshold = points_above_threshold(isnot_outlier(points_above_threshold));
        spread = numel(points_above_threshold) * site_spacing * 1e6;

        trough_times = timestamps(trough_idx) - timestamps(trough_idx(max_idx));
        trough_times = trough_times(points_above_threshold);
        channels = (points_above_threshold - max_idx);% >0, above, <0, below

        [velocity_above,velocity_below] = get_velocity(channels,trough_times',site_spacing);
    else
        velocity_above = nan;
        velocity_below = nan;
        spread = nan;
    end
end

function [v_above,v_below] = get_velocity(channels,times,distance_between_channels)
if nargin > 3
    distance_between_channels = 20e-6;
end
    above_soma = (channels >= 0);
    below_soma = (channels <= 0);
    
    if sum(above_soma)>1
        v_above = polyfit(channels(above_soma), times(above_soma),1);
        v_above = v_above(1)./distance_between_channels;
    else
        v_above = nan;
    end
    if sum(below_soma)>1
        v_below = polyfit(channels(below_soma), times(below_soma),1);
        v_below = v_below(1)./distance_between_channels;
    else
        v_below = nan;
    end
end

function out = isnot_outlier(points,thresh)
if nargin < 2
    thresh = 1.5;
end
median_val = median(points);
diff_val = sqrt((points-median_val).^2);
med_abs_deviation = median(diff_val);
modified_z_score = 0.6745 .* diff_val ./ med_abs_deviation;
out = (modified_z_score <= thresh);
end

function waveform_metrics_and_wave = extract_phy_waveform_metrics(varargin)
%waveform_metrics_and_wave = extract_phy_waveform_metrics(subjid_or_all_geom,data_dir)
%   extarct waveform metrics stuff to a stuct for uploading
%   
% Examples:
% For SpikeGLX recording, we will load geometry from recording file, 
% so you just need to call this in recording folder
% waveform_metrics_and_wave = metrics.extract_phy_waveform_metrics()
%
% For intan recording, you need to give input for subjid, or for geom table
% waveform_metrics_and_wave = metrics.extract_phy_waveform_metrics('subjid','JLI-R-0001')
% waveform_metrics_and_wave = metrics.extract_phy_waveform_metrics('probe_geom',all_geom)

inpd = @utils.inputordefault;
[subjid, varargin]=inpd('subjid', '', varargin);
[all_geom, varargin]=inpd('probe_geom', [], varargin);
[path_info, varargin]=inpd('path_info', '', varargin);
[recording_dir, varargin]=inpd('recording_dir', pwd(), varargin);

recording_software = utils.check_recording_software(recording_dir);
if isempty(path_info)
    if strcmp(recording_software,'oe')
        [path_info] = utils.find_oe_recording_dir(pwd);
    else
        [path_info] = utils.find_npx_recording_dir(pwd);
    end
end

if isempty(all_geom) && isempty(subjid)
	[sessinfo] = utils.load_sessinfo(path_info);
    all_geom = db.getSubjProbeMapping(sessinfo.subjid);
else if isempty(all_geom) && ~isempty(subjid)
    all_geom = db.getSubjProbeMapping(subjid);
end



if strcmp(recording_software,'npx')
    % npx session with potentially probes
    for ii = 1:numel(path_info)
        all_geom = utils.readSpikeGLXmeta(path_info(ii).meta_data_ap);
        datPars = metrics.default_params_spike_metrics_extraction(all_geom,path_info);
        waveform_metrics_and_wave = extract_this_data(datPars,path_info(ii));
    end
else
    assert(~isempth(all_geom),'no geom loaded, check subjid input or geom input format')
    datPars = metrics.default_params_spike_metrics_extraction(all_geom,path_info);
    waveform_metrics_and_wave = extract_this_data(datPars,path_info);
end




end


function waveform_metrics_and_wave = extract_this_data(datPars,path_info)
spike_times = readNPY(fullfile(path_info.spk_sorting_path,'spike_times.npy'));
spike_clusters = readNPY(fullfile(path_info.spk_sorting_path,'spike_clusters.npy'));% cluseter maybe merged from template
clusterinfo = utils.readClusterInfoTSV(path_info);
    all_geom = datPars.geom;
    % reset channelid to oe channel
    clusterinfo.ch = clusterinfo.ch+1;%ch start at 1 instead of 0
    ks_version = '2.5';
    if exist(fullfile(path_info.spk_sorting_path,'version.mat'),'file') %if exist this key, check version, or set version as ks 2.5
        load(fullfile(path_info.spk_sorting_path,'version.mat'),'ksversion');
        ks_version = ksversion(3:5);%version could be 'ks2.0' or 'ks2.5' here we skip 'version'
    end
    if strcmp(ks_version,'2.5')
        % have a custome Channal Map, might have excluded some bad
        % connection channels, map unconnected channels
        % by default kilosort just skip unconnected channels, so channel
        % index is not corresponding with the raw one. Here we re-map those
        % channel id bases on which channel was skipped.
        load(fullfile(path_info.spk_sorting_path,'temp_kilosortChanMap.mat'),'connected');
        channel_connection = connected;
        if any(~channel_connection) %have unconnected/exclueded channels during spk sorting
            channels_remap = 1:height(all_geom);
            channels_remap(~channel_connection)=[];%set skipped channels to empty
            clusterinfo.ch = cellfun(@(x) channels_remap(x), clusterinfo.ch,'UniformOutput',false);%remapped skipped channel id
        end
    end

labeled_noise = strcmp(clusterinfo.group,'noise');
if ismember('bc_unitType',clusterinfo.Properties.VariableNames) %bombcell labeled cell
    % merge two lables from user/bombcell
    bombcell_labeled_noise = strcmp(clusterinfo.bc_unitType,'NOISE');
    % by default, we will take all clusters
    final_label = false(numel(bombcell_labeled_noise),1);
    no_label = cellfun(@(x) isempty(x),clusterinfo.group,'UniformOutput',true);
    %for not labeled trials, let's trust bombcell
    final_label(no_label) = bombcell_labeled_noise(no_label);
    % if user labeled it as noise, we trust the users
    final_label(labeled_noise) = true;

    labeled_noise = final_label;
end
neuron_list = clusterinfo.cluster_id(~labeled_noise);%takeing mux+good
for cx = 1:length(neuron_list)
    selected_cluster_id = neuron_list(cx);
    [waveform_metrics_and_wave_data,wavedata] = metrics.waveform_metrics(datPars, spike_times(spike_clusters==selected_cluster_id), clusterinfo.ch(clusterinfo.cluster_id==selected_cluster_id));
    waveform_metrics_and_wave_data.wavedata = wavedata;
    waveform_metrics_and_wave_data.cluster_id = selected_cluster_id;
    waveform_metrics_and_wave_data.n_Spikes = numel(spike_times(spike_clusters==selected_cluster_id));
    waveform_metrics_and_wave(cx) = waveform_metrics_and_wave_data;
    fprintf('finishing process %d cluster in %d\n',cx,length(neuron_list));
end

save(fullfile(path_info.spk_sorting_path,'waveform_metrics.mat'),'waveform_metrics_and_wave');
end

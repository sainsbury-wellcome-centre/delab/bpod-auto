function [datPars] = default_params_spike_metrics_extraction(probe_geom,recording_path_info)
%[datPars] = default_parms_waveform_extraction(kilosort_dir,probe_geom,recording_dir)
%   generate default data Pars for waveform performance
%   input: 
%   1) probe_geom: all_geom mapping table
%   2) path_info: from find_oe_recording_dir()


if strcmp(recording_path_info.recording_software,'spikeGLX')
    recording_settings = utils.readSpikeGLXmeta(recording_path_info.meta_data_ap);
    nCH = recording_settings.nSites;
    bitVolts = recording_settings.uV_per_bit;
else
    recording_info = utils.extract_xml_recording_config(recording_path_info);
    nCH = recording_info.n_total_channels;
    bitVolts = 0.1950;
end
ks_version = '2.5';
if exist(fullfile(recording_path_info.spk_sorting_path,'version.mat'),'file') %if exist this key, check version, or set version as ks 2.5
    load(fullfile(recording_path_info.spk_sorting_path,'version.mat'),'ksversion');
    ks_version = ksversion(3:5);%version could be 'ks2.0' or 'ks2.5' here we skip 'version'
end

if strcmp(ks_version,'2.5')
if exist(fullfile(recording_path_info.spk_sorting_path,'rez.mat'),'file')
    load(fullfile(recording_path_info.spk_sorting_path,'rez.mat'),'rez');
else
    load(fullfile(recording_path_info.spk_sorting_path,'rez2.mat'),'rez');
end

channel_connection = height(probe_geom);
if exist(fullfile(recording_path_info.spk_sorting_path,'temp_kilosortChanMap.mat'),'file')
    load(fullfile(recording_path_info.spk_sorting_path,'temp_kilosortChanMap.mat'),'connected');
    channel_connection = connected;
end

elseif strcmp(ks_version,'2.0')
    Fs = 30000;  % Sampling Frequency

    N  = 3;   % Order
    Fc = 300;  % Cutoff Frequency

    [b1, a1] = butter(3, Fc/Fs*2, 'high');
    Hd = {b1,a1};
    
    ChanMap = readNPY(fullfile(recording_path_info.spk_sorting_path,'channel_map.npy'));
    channel_connection = zeros(height(probe_geom),1);
    channel_connection(ChanMap+1)=1;
end

datPars = struct();
datPars.version = ks_version;
datPars.bitVolts = bitVolts;
datPars.chanMap = readNPY(fullfile(recording_path_info.spk_sorting_path,'channel_map.npy'))+1;
datPars.nCh = sum(channel_connection); % if you excluded certain channels this will not be 64!
datPars.dataType = 'int16';
datPars.wfWin = [-30 60];
datPars.Fs = 30000;
datPars.nSpikesToUse = 5000;
datPars.useAllSpikes = true;
if strcmp(ks_version,'2.5')
    datPars.wrot = rez.Wrot;
    datPars.filter = [];% will load the filtered data, so no need for filtering
    binary_file_name = 'temp_wh.dat';
    datPars.filename = fullfile(recording_path_info.spk_sorting_path,binary_file_name);
elseif strcmp(ks_version,'2.0')
    datPars.filter = Hd;
    datPars.nCh = nCH;
    datPars.filename = recording_path_info.recording_data;
end
datPars.n_recording_channels = recording_info.n_recording_channels;
datPars.connection = channel_connection;
datPars.geom = probe_geom;

% we will make memery map here
d = dir(datPars.filename);
%datPars.chanMap = 1:sum(datPars.connection);
datPars.nSamp = d.bytes/(datPars.nCh*numel(typecast(cast(0, datPars.dataType), 'uint8')));
datPars.mmf = memmapfile(datPars.filename, 'Format', {datPars.dataType, [datPars.nCh datPars.nSamp], 'x'});

end

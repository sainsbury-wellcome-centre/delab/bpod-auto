function [sessinfo] = load_sessinfo(path_info)
    recording_dir = path_info(1).recording_base_path;
    sessinfo = utils.ini2struct(fullfile(recording_dir,'sessinfo'));
    sessinfo = sessinfo.sessinfo;

    if ~isfield(sessinfo,'acute')
        sessinfo.acute=0;
    end
end

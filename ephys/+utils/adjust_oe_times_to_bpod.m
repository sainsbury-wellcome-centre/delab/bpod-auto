function ats = adjust_oe_times_to_bpod(ts,S)
% ats = adjustts(ts,M)
% Takes the timestamps ts, and the structure returned from sync_times
% and returns timestamps which are interpolated to be in
% bpod time.

M = S.M;

% This line finds which timestamps are in which trials.
ind = stats.qfind(ts, M(:,2)); % The 2nd column is the OE times.

ats = nan+ts; % initialize the adjusted TS to be a nan array same size as ts.

for tx = 1:numel(ind) % loop through the trials
    if tx == 1 
    % The first trial is a special case, we will only shift, not scale, 
    % since we don't have a bookend.  
        ats(1:ind(tx)) = ts(1:ind(tx)) - (M(tx,2)-M(tx,3));
    else
    % Find the slope and the intercept of a 2 point line (the sync_times
    % from one trial to the next) and use that to shift the OE times in
    % that segment.
        slope = (M(tx,3)-M(tx-1,3))/(M(tx,2)-M(tx-1,2));
        intcpt = M(tx,3) - slope*M(tx,2);
        ats((ind(tx-1)+1):ind(tx)) = slope*ts((ind(tx-1)+1):ind(tx)) + intcpt;        
    end
end

ats(ind(tx)+1:end) = ts(ind(tx)+1:end) - (M(end,2)-M(end,3));
 

end

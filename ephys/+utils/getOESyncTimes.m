function [trial_time, trial_num, total_duration] = getOESyncTimes(varargin)
% [trial_time, trial_num] = getOESyncTimes(varargin)
% Need to add documentation

iod = @utils.inputordefault;

[ev_type, varargin] = iod('event_type',3,varargin);


[timebin, varargin] = iod('secsperbit',1e-3,varargin);
% The open-ephys sample rate is 30KHz, so 1ms is 30samples.

[bits, varargin] = iod('trialbits',16,varargin);
% We will use 16 bits to encode the trial #, allowing > 65000 trials per session

[header, varargin] = iod('header',5, varargin);
% We will use a 3 bit 101 header to indicate the start of the sync state. 
% This also allows us to measure the samples/bit if the sampling rate
% changes.

[fs, varargin] = iod('samping_rate',30000, varargin);%for 'flat binary' format of oe data, need to extract ts from index

[path_info, varargin] = iod('path_info',[], varargin);

% for this to run load_open_ephys_data needs to be on the path

[ttl_correct, varargin] = iod('allow_ttl_correct',true, varargin);


timestamps = readNPY(path_info.syncing_ts);
channel_states = readNPY(path_info.syncing_state);

if max(timestamps)<3e5
    % the timestamp is for actua time, no need for conversion
    fs = 1;%for this fs is already considered (new version oe)
end
good = abs(channel_states)==1; % +1 / -1 at channel_states means rising/falling edge of it
ttls = double(timestamps(good))/fs;
total_duration = round(double(timestamps(end)-timestamps(1))/fs); % ts are in seconds.

%% Verify that the min ttl matches the timebin.

dts = diff(ttls);
sdts = sort(dts);

assert(all(abs(sdts(1:(ceil(end/5)))-timebin)<10e-10)|ttl_correct,'The ttl durations do not match the timebin'); % The first 50 diffs should be the same duration as the timebin. We have to use <10e-10 because you can't compare floats.

if any(dts<timebin./2) && ttl_correct
% have very short ttl pulse needed to be excluded
    short_ttl_idx = find(dts<timebin./2);
    ttls([short_ttl_idx;short_ttl_idx+1]) = [];
    dts = diff(ttls);
    fprintf(2,'short ttl pulse detected, sync quality is not very good\n')
end
%%
% Convert times to timebins
ttl_bins = round(dts/timebin);

% Assume that the time in between trials is > 500 ms
trial_bounds = find(ttl_bins > 0.5/timebin);

% initial outputs to nans.
trial_time = nan(size(trial_bounds));
trial_num = trial_time;
this_bit_ind = 1;

headerbits = numel(dec2bin(header));
   
     
for xi=1:numel(trial_bounds)
    try
   if xi == 1
       trial_time(xi) = ttls(1);
       temp_ts=ttl_bins(1:trial_bounds(xi)-1);
   else
       trial_time(xi)= ttls(trial_bounds(xi-1)+1);
       temp_ts=ttl_bins(trial_bounds(xi-1)+1:trial_bounds(xi)-1);
   
   end
   
   last_bit=1;
   bitfield=zeros(1,bits + headerbits);
   bit_idx=1;
   for xj=1:numel(temp_ts)
       bitfield(bit_idx:bit_idx+temp_ts(xj)-1)=last_bit;
       last_bit=1-last_bit;
       bit_idx=bit_idx+temp_ts(xj); % really +1-1       
   end
   trial_num(xi)=bin2dec(num2str(bitfield(headerbits+1:end)));
    catch
        warning('you are fucked in trialnumtimes')
    end
end

if trial_num(end)==0
	trial_num=trial_num(1:end-1);
	trial_time=trial_time(1:end-1);
end

assert(all(diff(trial_num)>=1),"Trial numbers are not in increasing order. Cannot sync this session.")
    
end




%[times, trial_num] = 
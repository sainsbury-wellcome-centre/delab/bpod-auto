function all_geom = makeChanMapFromSGLMeta(m, varargin)
% function cm = makeChanMapFromSGLMeta(spikeglx_settings,varargin)

% example: 
% function cm = makeChanMapFromSGLMeta(spikeglx_settings,'saveDir', 'path_to_save_kilosort_geom')

inpd = @utils.inputordefault;
[shankSep, varargin]=inpd('shankSep', 0, varargin);
[rowSep, varargin]=inpd('rowSep', 1, varargin);
[colSep, varargin]=inpd('colSep', 1, varargin);
[ksVer, varargin]=inpd('ksVer', 4, varargin);
[save_ks_geom, varargin]=inpd('saveDir', '', varargin);

    % assume NP 2.0 parameters
    %shankSep = 250; 
    %rowSep = 15; 
    %colSep = 32;
    
    % assume NP 1.0 params
%     shankSep = 0; 
%     rowSep = 1; 
%     colSep = 1;
%     ksVer = 4;


shankMap = m.snsGeomMap; 

% why tf does this keep changing...?
if isfield(m, 'acqApLfSy')
    nCh = m.acqApLfSy;
elseif isfield(m, 'snsApLfSy')
    nCh = m.snsApLfSy;
elseif isfield(m, 'nChans')
    nCh = m.nChans-1;
elseif isfield(m, 'nSavedChans')
    nCh = m.nSavedChans-1;
end
nCh = m.nChans-1;

chanMap = [1:nCh(1)]'; 
chanMap0ind = chanMap-1;
connected = true(size(chanMap)); 

openParen = find(shankMap=='('); 
closeParen = find(shankMap==')'); 
for c = 1:nCh(1)
    thisMap = shankMap(openParen(c+1)+1: closeParen(c+1)-1); 
    thisMap(thisMap==':') = ',';
    n = str2num(thisMap); 
    xcoords(c) = (n(1)-1)*shankSep + (n(2))*colSep; 
    ycoords(c) = (n(3))*rowSep; 
end

% cm = struct();
% cm.chanMap = chanMap; 
% cm.chanMap0ind = chanMap0ind;
% cm.xcoords = xcoords'; 
% cm.ycoords = ycoords'; 
% cm.connected = connected;
%[~,name] = fileparts(m.imRoFile); 
%cm.name = name;

%% erlichlab geom format

all_geom.headstage = (1:nCh)';
all_geom.x = xcoords';
all_geom.y = ycoords';
all_geom.shank = ones(nCh,1); %now we only support NP 1.0, will add support later for NP 2.0 for multiple shanks
all_geom.connected = true(nCh, 1);
all_geom.probe = ones(nCh, 1);
all_geom.spikeGLX = all_geom.headstage;%npx don't really need to remap it because there are no connectors
all_geom = struct2table(all_geom);
all_geom = sortrows(all_geom,{'headstage'});


if ksVer == 4
    shankind = ones(nCh,1);
    xcoords = xcoords';
    ycoords = ycoords';
end

if ~isempty(save_ks_geom)
    save(fullfile(save_ks_geom,'npx1_ks4_chanMap.mat'),"shankind","connected","xcoords","ycoords","chanMap","chanMap0ind")
end
end


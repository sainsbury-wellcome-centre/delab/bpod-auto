function [path_info] = find_oe_recording_dir(oe_folder)
%[path_info] = find_recording_dir(oe_folder)
%    return the path of several key files from oe recording
%    setting:
%       recording settings XML: path_info.settings
%    for recording:
%       binary_file (prefer using the one with _fix):
%       path_info.recording_data
%       timestamp_file: path_info.recording_ts
%    for syncing:
%       'timestamps.npy': path_info.syncing_ts
%       'states.npy': path_info.syncing_state

%
if nargin < 1
    oe_folder = pwd;
end
path_info = struct();
path_info.recording_base_path = oe_folder;
path_info.recording_data_path = oe_folder;
path_info.spike_sorting_method = 'kilosort';%we only support kilosort for now
path_info.recording_software = 'open-ephys';
path_info.settings = fullfile(oe_folder,'Record Node 101','settings.xml');
if exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Rhythm_FPGA-100.0'),'dir')
    recording_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Rhythm_FPGA-100.0');
    path_info.recording_format = 'flat_binary';
elseif exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Acquisition_Board-100.Rhythm Data'),'dir')
    recording_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Acquisition_Board-100.Rhythm Data');
    path_info.recording_format = 'flat_binary';
elseif exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Acquisition_Board-115.Rhythm Data'),'dir')
    recording_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','continuous','Acquisition_Board-115.Rhythm Data');
    path_info.recording_format = 'flat_binary';
elseif exist(fullfile(oe_folder,'kilosort','continuous_raw_data.bin'),'file')
    recording_data_path = oe_folder;  
    path_info.recording_format = 'open_ephys';
    path_info.oe_recording_data_path = fullfile(oe_folder,'Record Node 101');
elseif exist(fullfile(oe_folder,'continuous_raw_data.bin'),'file')
    recording_data_path = oe_folder; 
    path_info.recording_format = 'open_ephys';
    path_info.oe_recording_data_path = fullfile(oe_folder,'Record Node 101');
elseif exist(fullfile(oe_folder,'Record Node 101'),'dir')
    recording_data_path = oe_folder; 
    path_info.recording_format = 'open_ephys';
    path_info.oe_recording_data_path = oe_folder;
end
if strcmp(path_info.recording_format,'flat_binary')
    fs   = [dir(fullfile(recording_data_path, '*.bin')) dir(fullfile(recording_data_path, '*_fixed.dat')) dir(fullfile(recording_data_path, 'continuous.dat'))];   
    path_info.recording_data = fullfile(recording_data_path, fs(1).name);
    path_info.recording_ts = fullfile(recording_data_path, 'timestamps.npy');
else %old open ephys recording session
    path_info.recording_data = fullfile(recording_data_path,'kilosort','continuous_raw_data.bin');
    path_info.recording_ts = fullfile(recording_data_path,'Record Node 101','100_CH1.continuous');
end

if exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Rhythm_FPGA-100.0'),'dir')
    syncing_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Rhythm_FPGA-100.0','TTL_1');
elseif exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Acquisition_Board-100.Rhythm Data'),'dir')
    syncing_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Acquisition_Board-100.Rhythm Data','TTL'); 
elseif exist(fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Acquisition_Board-115.Rhythm Data'),'dir')
    syncing_data_path = fullfile(oe_folder,'Record Node 101','experiment1','recording1','events','Acquisition_Board-115.Rhythm Data','TTL'); 
elseif exist(fullfile(oe_folder,'Record Node 101','all_channels.events'),'file')
    syncing_data_path = fullfile(oe_folder,'Record Node 101'); 
end

if strcmp(path_info.recording_format,'flat_binary')
    path_info.syncing_ts = fullfile(syncing_data_path, 'timestamps.npy');
    if exist(fullfile(syncing_data_path,'channel_states.npy'),'file')
        path_info.syncing_state = fullfile(syncing_data_path,'channel_states.npy');
    elseif exist(fullfile(syncing_data_path,'states.npy'),'file')
        path_info.syncing_state = fullfile(syncing_data_path,'states.npy');
    end
else
    path_info.syncing_ts = fullfile(syncing_data_path, 'all_channels.events');
    path_info.syncing_state = fullfile(syncing_data_path,'all_channels.events');
end

% checking kilosort result folder
if exist(fullfile(oe_folder,'kilosort','spike_times.npy'),'file')
    path_info.spk_sorting_path = fullfile(oe_folder,'kilosort');
end
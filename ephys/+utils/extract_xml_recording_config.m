function [recording_info] = extract_xml_recording_config(path_info)
%[recording_info] = extract_xml_recording_config(path_info)
%   extrct information from settings.xml of open-ephys recording file
%   saved core info as a struct contains FPGA acqusition, channels config
%   and main settings.
%
%   Jingjie Li, 2021-10-13.
recording_info = struct();
xmlDoc = xmlread(path_info.settings);

% FPGA acq info
editor_node = xmlDoc.getElementsByTagName('SIGNALCHAIN').item(0).getElementsByTagName('EDITOR');
samping_rate_str = editor_node.item(0).getAttribute('SampleRateString');
samping_rate_str = char(samping_rate_str);
recording_info.samping_rate = str2double(samping_rate_str(1:find(samping_rate_str == ' ')-1)) * 1000;

recording_info.acqusition_lowCut = str2double(char(editor_node.item(0).getAttribute('LowCut')));
recording_info.acqusition_highCut = str2double(char(editor_node.item(0).getAttribute('HighCut')));
recording_info.dsp_offset = (str2double(char(editor_node.item(0).getAttribute('DSPOffset')))==1);
recording_info.dspCutoffFreq = str2double(char(editor_node.item(0).getAttribute('DSPCutoffFreq')));
recording_info.clockDivideRatio = str2double(char(editor_node.item(0).getAttribute('ClockDivideRatio')));
% main settings info
infonode = xmlDoc.getElementsByTagName('SETTINGS').item(0).getElementsByTagName('INFO');
recording_info.oe_version = char(infonode.item(0).getElementsByTagName('VERSION').item(0).getTextContent());
recording_info.plugin_api_version = char(infonode.item(0).getElementsByTagName('PLUGIN_API_VERSION').item(0).getTextContent());
recording_info.date = char(infonode.item(0).getElementsByTagName('DATE').item(0).getTextContent());
recording_info.os = char(infonode.item(0).getElementsByTagName('OS').item(0).getTextContent());
recording_info.machine = char(infonode.item(0).getElementsByTagName('MACHINE').item(0).getTextContent());
% channels info
if strcmp(recording_info.oe_version(1:3),'0.5')
    channel_node = xmlDoc.getElementsByTagName('SIGNALCHAIN').item(0).getElementsByTagName('CHANNEL_INFO').item(0).getElementsByTagName('CHANNEL');
    n_total_channels = channel_node.getLength();
    n_recording_channels = 0;
    for cx = 1:n_total_channels
        channel_config(cx).name = char(channel_node.item(cx-1).getAttribute('name')); %this item start from 0
        channel_config(cx).number = str2double(char(channel_node.item(cx-1).getAttribute('number')));
        channel_config(cx).gain = str2double(char(channel_node.item(cx-1).getAttribute('gain')));% gain is the LSB of int16 bits (in uV)
        if strcmp(channel_config(cx).name(1:2),'CH')
            n_recording_channels = n_recording_channels + 1;
        end
    end
elseif strcmp(recording_info.oe_version(1:3),'0.6')
    stream_node = xmlDoc.getElementsByTagName('SIGNALCHAIN').item(0).getElementsByTagName('STREAM');
    channel_count_str = char(stream_node.item(0).getAttribute('channel_count'));
    n_total_channels = str2num(channel_count_str);
    n_AUX_channels = mod(n_total_channels,64);
    n_recording_channels = n_total_channels - n_AUX_channels;
    for cx = 1:n_total_channels 
        if cx > n_recording_channels
            channel_config(cx).name = sprintf('ADC%d',cx); %this item start from 0
            channel_config(cx).gain = 3.7400001019705086946e-05;
        else
            channel_config(cx).name = sprintf('CH%d',cx); %this item start from 0
            channel_config(cx).gain = 0.19499999284744262695;% gain is the LSB of int16 bits (in uV)
        end
        channel_config(cx).number = cx-1;
    end
end
recording_info.n_recording_channels = n_recording_channels;
recording_info.n_total_channels = n_total_channels;
recording_info.channel_config = struct2table(channel_config);

end


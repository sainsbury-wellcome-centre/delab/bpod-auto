function [bad_chunk_edge_by_probe] = remove_bad_recording_chunk(filename,varargin)
%[ok] = remove_bad_recording_chunk(filename,varargin)
%   varargin: output_filename/output_path
% example:
% [bad_chunk_edge_by_probe] = utils.remove_bad_recording_chunk('','saving',true,'threshold',3e7)
default_total_channels = 136;
if nargin < 2 || isempty(filename)
    [path_info] = utils.find_oe_recording_dir(pwd());
    filename = path_info.recording_data;
    recording_info = utils.extract_xml_recording_config(path_info);
    default_total_channels = recording_info.n_total_channels;
end
[ filepath , ~ , ~ ] = fileparts( filename ) ;

iod = @utils.inputordefault;

[output_filename, varargin] = iod('output_filename','continuous_fixed.dat',varargin);
[output_path, varargin] = iod('output_path',filepath,varargin);
[geom, varargin] = iod('geom',utils.make_cambridge_probe_geom({'ASSY-236-H9','ASSY-236-H6'}),varargin);
[plot_example, varargin] = iod('plot_example_clip',false,varargin);
[thr, varargin] = iod('threshold',1e8,varargin);%this should be in int18 unit
[saving, varargin] = iod('saving',true,varargin);%this should be in int18 unit
[NchanTOT, varargin] = iod('nCH',default_total_channels,varargin);%this should be in int18 unit

file_info = dir(filename);
nSamp = file_info.bytes/2/NchanTOT;
data_map=memmapfile(filename,'Format',{'int16' [NchanTOT nSamp] 'mapped'});%data will be able to be accessed from data_map.Data.mapped


NT                  = 64*1024; % must be multiple of 32 + ntbuff. This is the batch size (try decreasing if out of memory). 
Nbatch      = ceil(nSamp /(NT)); % number of data batches

channel_to_check = zeros(1,numel(unique(geom.probe)));
bad_chunk_edge_by_probe = cell(1,numel(unique(geom.probe)));
longest_good_recording_start_idx = zeros(1,numel(unique(geom.probe)));
for ii = 1:numel(unique(geom.probe)) %check how many probes we have
    probe_idx{ii} = geom.openephys(geom.probe == ii);
    channel_to_check(ii) = find(geom.probe==ii,1,'first');
end

%first we want to plot a chunk for user to see if we can take this chunk to
%get the range
if plot_example
    fH = figure;
    fH.Position = [0 0 1260 1782];
    preview_start_idx = 1604026-10000;
    preview_end_idx = 1604026+10000;
    data2plot =  double(data_map.Data.mapped(1:max(geom.openephys),preview_start_idx:preview_end_idx))/300;
    data2plot = [bsxfun(@plus,data2plot,5*(1:size(data2plot,1))'),nan(size(data2plot,1),1)];
    plot(repmat(1:size(data2plot,2),[1,size(data2plot,1)]),reshape(data2plot',[1,numel(data2plot)]))
end

if saving
    fidW        = fopen(fullfile(output_path,output_filename),   'w'); % open for writing processed data
    if fidW<3
        error('Could not open %s for writing.',output_filename);    
    end
end
% Merge edges that are too close
merge_distance = 3000;


for ii = 1:numel(channel_to_check)
    data_loaded = data_map.Data.mapped(channel_to_check(ii),:);
    buff = medfilt1(double(data_loaded).^2,1000); %do a 10 order median filter, and then draw it
    buff_thresholding = (buff>thr); %do thresholding, this might creat a 10ms lag if the threshold is at 1e8, therefore we will want to offset it by 300 samples
    if any(buff_thresholding) %having bad chunk from this recording batch, let's figure out where
        % Find all start and end positions of "1" sequences
        one_starts = find(diff([0 buff_thresholding == 1 0]) == 1);
        one_ends = find(diff([0 buff_thresholding == 1 0]) == -1) - 1;
        
        % Expand each "1" chunk by buffer_size
        for i = 1:length(one_starts)
            start_idx = max(1, one_starts(i) - merge_distance);
            end_idx = min(nSamp, one_ends(i) + merge_distance);
            buff_thresholding(start_idx:end_idx) = 1;
        end

        % Step 2: Merge close "1" chunks (less than 3000 samples of "0"s between them)
        % Find all start and end positions of "0" sequences after expansion
        zero_starts = find(diff([0 buff_thresholding == 0 0]) == 1);
        zero_ends = find(diff([0 buff_thresholding == 0 0]) == -1) - 1;
        
        % Calculate lengths of zero sequences
        zero_lengths = zero_ends - zero_starts + 1;
        
        % Fill short zero periods with ones
        for i = 1:length(zero_lengths)
            if zero_lengths(i) < merge_distance
                buff_thresholding(zero_starts(i):zero_ends(i)) = 1;
            end
        end
        
        % Step 3: Extract the upedge (0 -> 1) and downedge (1 -> 0) indices
        upedge_indices = find(diff([0 buff_thresholding]) == 1);
        downedge_indices = find(diff([buff_thresholding 0]) == -1);
        
        % Combine upedge and downedge indices into a matrix
        bad_chunk_edge = [upedge_indices', downedge_indices'];
        bad_chunk_edge(bad_chunk_edge<1) = 1;
        bad_chunk_edge(bad_chunk_edge>nSamp) = nSamp;
        bad_chunk_edge_by_probe{ii} = bad_chunk_edge;
        upedge_list = [1;bad_chunk_edge(:,1)];downedge_list=[2;bad_chunk_edge(:,2)];
        [~,idx_good_recording_start] = max(diff(upedge_list));
        longest_good_recording_start_idx(ii) = downedge_list(idx_good_recording_start);
%         for ci = 1:size(bad_chunk_edge,1)
%             buff(bad_chunk_edge(ci,1):bad_chunk_edge(ci,2)) = 0;
%         end
    end
end
if saving
    for ibatch = 1:Nbatch
        % we'll create a binary file of batches of NT samples, which overlap consecutively on ops.ntbuff samples
        % in addition to that, we'll read another ops.ntbuff samples from before and after, to have as buffers for filtering
        start_idx = 1+NT * (ibatch-1);ending_idx = min(NT * ibatch,nSamp);
        idx_this_batch = start_idx:ending_idx;
        batch_length = numel(idx_this_batch);
        idx_to_removed = false(1,numel(idx_this_batch));
        buff = data_map.Data.mapped(:,idx_this_batch);

        datr  = buff;
        for ii = 1:numel(channel_to_check)
            if ~isempty(bad_chunk_edge_by_probe{ii}) % have some bad chunk for this probe
                for jj = 1:size(bad_chunk_edge_by_probe{ii},1)
                    if bad_chunk_edge_by_probe{ii}(jj,1)>start_idx && bad_chunk_edge_by_probe{ii}(jj,1)<ending_idx %the start point of this bad chunk is in this batch
                        idx_to_removed(bad_chunk_edge_by_probe{ii}(jj,1)-start_idx:min(bad_chunk_edge_by_probe{ii}(jj,2)-start_idx,batch_length)) = true;
                    elseif bad_chunk_edge_by_probe{ii}(jj,2)>start_idx && bad_chunk_edge_by_probe{ii}(jj,2)<ending_idx %only found the end point of this bad chunk is in this batch
                        idx_to_removed(1:bad_chunk_edge_by_probe{ii}(jj,2)-start_idx) = true;
                    elseif bad_chunk_edge_by_probe{ii}(jj,1)<start_idx && bad_chunk_edge_by_probe{ii}(jj,2)>ending_idx %this batch is completely in a bad period
                        idx_to_removed = ~idx_to_removed;
                    end
                end
                if any(idx_to_removed)
                    datr(probe_idx{ii},idx_to_removed) = data_map.Data.mapped(probe_idx{ii},longest_good_recording_start_idx(ii):longest_good_recording_start_idx(ii)+sum(idx_to_removed)-1); %replace with a long piece of good recording
                end
            end
        end
        
        %datcpu  = int16(datr); % convert to int16, and gather on the CPU side

        count = fwrite(fidW, datr, 'int16'); % write this batch to binary file
        if count~=numel(datr)
            error('Error writing batch %g to %s. Check available disk space.',ibatch,output_filename);
        end
        if (rem(ibatch, 100)==1)
            % this is some of the relevant diagnostic information to be printed during training
            fprintf('%d / %d batches done \n', ...
                ibatch, Nbatch)
        end
    end
    fclose(fidW); % close the files
end


end
function [] = convert_save_kilo_sort_probe_geom(all_geom,output_path)
%convert_save_kilo_sort_probe_geom(all_geom,output_path)
%   all_geom prob geom file that we use in our lab
%   output_path: default is pwd
if nargin < 2
    output_path = pwd;
end
if any("openephys" == string(all_geom.Properties.VariableNames))
    all_geom = sortrows(all_geom,{'openephys'});
elseif any("spikeGLX" == string(all_geom.Properties.VariableNames))
    all_geom = sortrows(all_geom,{'spikeGLX'});
else
    assert(false,'incorrect geom format')
end

Nchannels = height(all_geom);
connected = all_geom.connected;
chanMap   = 1:Nchannels;
chanMap0ind = chanMap - 1;
xcoords   = all_geom.x;
ycoords   = all_geom.y;
kcoords   = all_geom.shank;

fs = 30000; % sampling frequency
save(output_path, ...
    'chanMap','connected', 'xcoords', 'ycoords', 'kcoords', 'chanMap0ind', 'fs')
end


function [all_geom] = make_cambridge_probe_geom(probe_name,varargin)
%[all_geom] = make_cambridge_probe_geom(probe_name,varargin)
%   generate probe mapping table for cambridge probes
%
%   input:
%   probe_name: a cell array or str contains the name of the probe
%   it can be {'ASSY-236-H6','ASSY-236-H9'} for dual probe animals, or
%   'ASSY-236-H6' for single probe animals
%
%   varargin: 'intan_config','mini-amp' or 'intan_config','intan' for intan
%   chip/mini-amp acqusition
%   'probeinterface_library': the path for robeinterface_library, if not
%   specified or empty, will load probeinterface_library from github
%   
%   output:
%   all_geom table
%   each line is a channel, with coordination, openephys channel id and
%   shank id
%
%   example:
%   [all_geom] = make_cambridge_probe_geom('ASSY-236-H9','probeinterface_library','C:\Users\Jingjie Li\repos\probeinterface_library')
%   [all_geom] = make_cambridge_probe_geom('ASSY-236-H9')
%   [all_geom] = make_cambridge_probe_geom({'ASSY-236-H6','ASSY-236-H9'})
%
%  By: Jingjie Li, 2023-07-03

iod = @utils.inputordefault;
[intan_config, varargin] = iod('intan_config','mini-amp',varargin);
[probeinterface_library, varargin] = iod('probeinterface_library','',varargin);

library_url = 'https://raw.githubusercontent.com/SpikeInterface/probeinterface_library/main/cambridgeneurotech';

if iscell(probe_name) % have multiple probe loaded, let's run the function one by one and concatinate the all_geom
    num_probe = numel(probe_name);
    all_geom = [];
    for  ii = 1:num_probe
        if isempty(all_geom)
            current_channels = 0;
            current_shanks = 0;
            current_x = 0;
            current_probe = 0;
        else
            current_channels = height(all_geom);
            current_shanks = max(all_geom.shank);
            current_x = max(all_geom.shank) + 1000;
            current_probe = max(all_geom.probe);
            % this x is for the x coordniate for each recording channel.
            % But if we have multiple probes, we will want to have a very
            % large x_offset because we want the spike sorting software
            % (kilosort) to treat those channels from different probe
            % differently. This basically tells the spike sorting software
            % that those channels are far from each other.
        end
        this_geom = utils.make_cambridge_probe_geom(probe_name{ii},'intan_config',intan_config,'probeinterface_library',probeinterface_library);
        this_geom.openephys = this_geom.openephys + current_channels;
        this_geom.SE = this_geom.SE + current_channels;
        this_geom.shank = this_geom.shank + current_shanks;
        this_geom.x = this_geom.x + current_x;
        this_geom.probe = this_geom.probe + current_probe;
        all_geom = [all_geom;this_geom];
    end
    return;
end

if strcmp(intan_config,'mini-amp')
    intan_ch_order = [63:-1:56,7:-1:0,55,53,54,52:-1:47,15,13,12,11,9,10,8,46:-1:42,40,36,31,27,23,21,18,19,17,16,14,41,39,38,37,35,34,33,32,29,30,28,26,25,24,22,20];
    probe_ch_order = [49:64,33:48,17:32,1:16];
    intan_mapping = [probe_ch_order',intan_ch_order'];
elseif strcmp(intan_config,'intan')
    % Cambridge headstage map: DATA SHEET-ORDER NUMBER C-06-0518 Erlich_02
    Headstage_front_to_back = [32:-2:2;...
        31:-2:1;...
        33:2:63;...
        34:2:64];
    % Intan connector map: http://www.intantech.com/RHD_headstages.html?tabSelect=RHD64ch
    Intan_chip_to_back= [16:2:46;...
        17:2:47;...
        15:-2:1,63:-2:49;...
       14:-2:0,62:-2:48];
    intan_mapping = [Headstage_front_to_back',Intan_chip_to_back'];
end
intan_mapping_sort = sortrows(intan_mapping,1);

if isempty(probeinterface_library)
    str = webread(sprintf('%s/%s/%s.json',library_url,probe_name,probe_name));
else
    fileName = fullfile(probeinterface_library,'cambridgeneurotech',probe_name,sprintf('%s.json',probe_name));
    str = fileread(fileName); % dedicated for reading files as text 
end
data = jsondecode(str); % 

all_geom.headstage = (1:64)';
all_geom.x = data.probes.contact_positions(:,1);
all_geom.y = data.probes.contact_positions(:,2);
if isempty(data.probes.shank_ids{1})
    all_geom.shank = ones(64,1);
else
    all_geom.shank = cellfun(@str2num,data.probes.shank_ids) +1;
end
all_geom.connected = true(64, 1);
all_geom.probe = ones(64, 1);

all_geom.intan = intan_mapping_sort(:,2);
all_geom.openephys = all_geom.intan + 1;
all_geom.SE = all_geom.intan;

if isstruct(all_geom)
    all_geom = struct2table(all_geom);
end
all_geom = sortrows(all_geom,{'shank','y'});
end
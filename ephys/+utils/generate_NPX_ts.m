function [ts] = generate_NPX_ts(path_info)
%[ts] = generate_NPX_ts(path_info)
%   load meta file and recording file to generate timestampe index
recording_settings = utils.readSpikeGLXmeta(path_info(1).meta_data_ap);
nCH = recording_settings.nSavedChans;%for npx, normally 385
           
filenamestruct = dir(path_info(1).recording_data);
dataTypeNBytes = numel(typecast(cast(0, 'int16'), 'uint8')); % determine number of bytes per sample
nSamp = filenamestruct.bytes/(nCH*dataTypeNBytes);  % Number of samples per channel

ts = (0:nSamp)./recording_settings.sRateHz;
end
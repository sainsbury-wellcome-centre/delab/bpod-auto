function recording_software = check_recording_software(recording_dir)

files = dir(recording_dir);
files = struct2table(files);

if any(cellfun(@(x) contains(x,'_imec'),files.name))
    % should be a npx recording session
    fprintf('npx recording session detected\n')
    recording_software = 'npx';
else %we assume it might be the open ephys
    fprintf('open ephys recording session\n')
    recording_software = 'oe';
end
end
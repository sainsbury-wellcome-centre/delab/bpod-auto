function S = process_spksorts(sessinfo, M, chtab,spksort_cmts,varargin)
%
% running example for kilosort
% should be run inside path '/Volumes/data/ephys/2224/2224_2021-06-18_12-48-04/'
% S = process_spksorts(sessinfo, M, [], 'kilosort',[],'geom_path','/Volumes/data/ephys/2224','oe_raw_folder','/Volumes/data/ephys/2224/2224_2021-06-18_12-48-04/Record Node 101')

inpd = @utils.inputordefault;

[recording_path_info, varargin]=inpd('recording_path_info', '', varargin);
[manually_curated, args]=inpd('manually_curated', '', varargin);
[use_db, args]=inpd('use_db', 'client', varargin);
[min_spikes, args]=inpd('min_spikes', 100, varargin);

dbc = db.labdb.getConnection(use_db);
try
    switch(lower(recording_path_info.spike_sorting_method(1:5)))
        % Which software did we use for sorting?
        % Each of these functions will extract the info into a common format for syncing.
        case 'kilos' % kilosort
            if strcmp(recording_path_info.recording_software,'spikeGLX')
                samplerate = 30000;
                % only support flatbinary for npx recording
                recording_settings = utils.readSpikeGLXmeta(recording_path_info.meta_data_ap);
                all_geom = utils.makeChanMapFromSGLMeta(recording_settings);
                all_geom = sortrows(all_geom,{'spikeGLX'});
                nCH = recording_settings.nSavedChans;%for npx, normally 385      
                filenamestruct = dir(recording_path_info.recording_data);
                dataTypeNBytes = numel(typecast(cast(0, 'int16'), 'uint8')); % determine number of bytes per sample
                nSamp = filenamestruct.bytes/(nCH*dataTypeNBytes);  % Number of samples per channel
                contts = (0:nSamp)./30000;

                [S, D, ES, tmp, Ccmt,Cquality,spikeinterface_quality]= do_kilosort(contts,samplerate,all_geom,recording_path_info);
            elseif strcmp(recording_path_info.recording_software,'open-ephys')
                samplerate = 30000;
                if strcmp(recording_path_info.recording_format,'flat_binary')
                    contts = readNPY(recording_path_info.recording_ts);
                    if contts(2) - contts(1) > .8 %ts is actually index, time by time
                        contts = double(contts)./samplerate;
                    end
                else
                    error('only supporting for flat binary format')
                end
                all_geom = db.getSubjProbeMapping(sessinfo.subjid);
                all_geom = sortrows(all_geom,{'openephys'});
                [S, D, ES, tmp, Ccmt,Cquality,spikeinterface_quality]= do_kilosort(contts,samplerate,all_geom,recording_path_info);
            end
        otherwise
            error('Do not know how to process %s',recording_path_info.spike_sorting_method);
    end
    [~,data_folder] = fileparts(recording_path_info.recording_data_path);
    S.sessid = sessinfo.sessid;
    if ~isempty(spksort_cmts)
        S.comments =spksort_cmts;
    end
    S.manually_curated = manually_curated;
    sstab = dbc.query('select * from phy.spksorts where sessid=%d and sorttime="%s" and pathname="%s" and manually_curated=%d',{S.sessid, S.sorttime, data_folder,manually_curated});
    if isempty(sstab)
        dbc.saveData('phy.spksorts',S);
        S.spksortid = dbc.last_insert_id();
    else
        S = sstab;
    end
    D = compute_overlap(ES, D);
    done_cells = dbc.query('select cellid, ss_cluster from phy.cells where spksortid=%d',{S.spksortid});
    for cx = 1:numel(D)
        % Check if this cell is already in the DB
        already_in_db = ~isempty(done_cells) && ismember(D(cx).ss_cluster, done_cells.ss_cluster);
        if already_in_db
           cellid = done_cells.cellid(done_cells.ss_cluster == D(cx).ss_cluster);
           fprintf(1,'Cluster %d from file %s already in DB as cellid: %d\n',D(cx).ss_cluster,cellid);
        else
            % Inherited.
            %  D(clust_offset).ad_channel = ch_offset + ad_channel;
            %  D(clust_offset).cluster = clust_offset;
            %  D(clust_offset).ss_cluster = this_clust; % The cluster # assigned by software
            %  D(clust_offset).oets = A(2,this_clust_ind);
            %  D(clust_offset).filename = fire_files(fx).name;
            %  D(clust_offset).ch_offset = ch_offset;
            Dcx = D(cx);
            chind = chtab.ad_channel==tmp(cx).ad_channel;
            Dcx.channelid = chtab.channelid(chind);
            Dcx.channelid = Dcx.channelid(1);
            n_spk = numel(ES(cx).oets);
            if n_spk>min_spikes
                Dcx.nSpikes = n_spk;
                ISI = diff(ES(cx).oets);
                Dcx.frac_bad_isi = mean(ISI<0.001); % What fraction of ISIs are less than 1ms?
                % JCE: not implemented yet.
                %D(cx).recorded_on_right = chtab.ML(chind)>0;
                Dcx.rate=numel(ES(cx).oets)/M.rec_duration;
                Dcx.spksortid = S.spksortid;
                dbc.saveData('phy.cells', Dcx);
                cellid = dbc.last_insert_id();
                EScx = struct();
                EScx.cellid = cellid;
                EScx.ts = typecast(utils.adjust_oe_times_to_bpod(ES(cx).oets, M), 'uint8');
                EScx.oets = typecast(ES(cx).oets, 'uint8');
                EScx.wave = json.mdumps(ES(cx).wave,'compress',1);
                dbc.saveData('phy.spktimes', EScx);

                if ~isempty(fieldnames(Cquality))
                    Cqualitycx = Cquality(cx);
                    Cqualitycx.cellid = cellid;
                    Cqualitycx = utils.remove_nan_struct_fields(Cqualitycx);
                    dbc.saveData('phy.cell_metrics',Cqualitycx);
                elseif ~isempty(fieldnames(spikeinterface_quality))
                    spikeinterface_qualitycx = spikeinterface_quality(cx);
                    spikeinterface_qualitycx.cellid = cellid;
                    spikeinterface_qualitycx = utils.remove_nan_struct_fields(spikeinterface_qualitycx);
                    dbc.saveData('test.spikeinterface_matrics',spikeinterface_qualitycx)
                else
                    error('Please check your quality matrics!')
                end
            else
                fprintf(1,'Cluster %d only have %d spikes, excluded\n',D(cx).ss_cluster,n_spk);
            end
        end     
    end
catch me
     utils.showerror(me);
end

    

end






function [S, D, ES, tmp,Ccmt,Cquality, spikeinterface_quality] = do_kilosort(ts,oefs,all_geom,recording_path_info)
    [clusterinfo] = utils.readClusterInfoTSV(recording_path_info);
    spike_times = readNPY(fullfile(recording_path_info.spk_sorting_path,'spike_times.npy'))+1;%correct indexing to be started from 1 instead of 0
    spike_templates = readNPY(fullfile(recording_path_info.spk_sorting_path,'spike_templates.npy'));%template if from original clustering
    spike_clusters = readNPY(fullfile(recording_path_info.spk_sorting_path,'spike_clusters.npy'));% cluseter maybe merged from template
    templates = readNPY(fullfile(recording_path_info.spk_sorting_path,'templates.npy'));
    %templates_ind = readNPY('templates_ind.npy');
    
    ks_version = '4.0';
    if exist(fullfile(recording_path_info.spk_sorting_path,'version.mat'),'file') %if exist this key, check version, or set version as ks 2.5
        load(fullfile(recording_path_info.spk_sorting_path,'version.mat'),'ksversion');
        ks_version = ksversion(3:5);%version could be 'ks2.0' or 'ks2.5' here we skip 'version'
    elseif exist(fullfile(recording_path_info.spk_sorting_path,'kilosort4.log'),'file')
        ks_version = '4.0';
    end

    if exist(fullfile(recording_path_info.spk_sorting_path,'cluster_quality.csv'), 'file') % handle quanlity matrics with ecephys from Allen
        T = readtable(fullfile(recording_path_info.spk_sorting_path,'cluster_quality.csv'));
        T_struct = table2struct(T); %this contains cluster performance information
        spikeinterface_pipeline = 0;
    elseif exist(fullfile(recording_path_info.spk_sorting_path,'spikeinterface_cluster_quality.csv'), 'file')
        spikeinterface_quality_tb = readtable(fullfile(recording_path_info.spk_sorting_path,'spikeinterface_cluster_quality.csv'));
        spikeinterface_pipeline = 1;
    end

    sort_file = dir(fullfile(recording_path_info.spk_sorting_path,'spike_times.npy'));
    sort_date = datestr(sort_file.date, 31);
    if exist(fullfile(recording_path_info.spk_sorting_path,'waveform_metrics.mat'),'file')
        load(fullfile(recording_path_info.spk_sorting_path,'waveform_metrics.mat'),'waveform_metrics_and_wave');
        loading_waveform = 1;
    elseif exist(fullfile(recording_path_info.spk_sorting_path,'waveform_quality.csv'),'file')
        waveform_metrics = readtable(fullfile(recording_path_info.spk_sorting_path,'waveform_quality.csv'));
        waveform_metrics_and_wave = table2struct(waveform_metrics);
        mean_wv = readNPY(fullfile(recording_path_info.spk_sorting_path,'mean_waveform.npy'));
        loading_waveform = 2;
    elseif spikeinterface_pipeline
        mean_wv = readNPY(fullfile(recording_path_info.spk_sorting_path,'templates_average.npy'));
        std_wv = readNPY(fullfile(recording_path_info.spk_sorting_path,'templates_std.npy'));
    else
        loading_waveform = 0;
        error('you need to have waveform preprocessed with MATLAB or python')
    end
    
    clusterinfo.ch = clusterinfo.ch + 1;
    
    if exist(fullfile(recording_path_info.spk_sorting_path,'temp_kilosortChanMap.mat'),'file') && strcmp(ks_version,'2.5')
        % have a custome Channal Map, might have excluded some bad
        % connection channels, map unconnected channels
        % by default kilosort just skip unconnected channels, so channel
        % index is not corresponding with the raw one. Here we re-map those
        % channel id bases on which channel was skipped.
        %
        % this is only for kilosort v2.5
        load(fullfile(recording_path_info.spk_sorting_path,'temp_kilosortChanMap.mat'),'connected');
        channel_connection = connected;
        if any(~channel_connection) %have unconnected/exclueded channels during spk sorting
            channels_remap = 1:height(all_geom);
            channels_remap(~channel_connection)=[];%set skipped channels to empty
            %clusterinfo.ch = cellfun(@(x) channels_remap(x), clusterinfo.ch,'UniformOutput',false);%remapped skipped channel id
            clusterinfo.ch = channels_remap(clusterinfo.ch); %need verification
        end
    elseif strcmp(ks_version,'2.0') || strcmp(ks_version,'4.0')
        ChanMap = readNPY(fullfile(recording_path_info.spk_sorting_path,'channel_map.npy'));
        channel_connection = zeros(height(all_geom),1);
        channel_connection(ChanMap+1)=1;
    end

    spikes_ts = ts(spike_times);
    labeled_noise = strcmp(clusterinfo.group,'noise');
    if ismember('bc_unitType',clusterinfo.Properties.VariableNames) %bombcell labeled cell
        % merge two lables from user/bombcell
        bombcell_labeled_noise = strcmp(clusterinfo.bc_unitType,'NOISE');
        % by default, we will take all clusters
        final_label = false(numel(bombcell_labeled_noise),1);
        no_label = cellfun(@(x) isempty(x),clusterinfo.group,'UniformOutput',true);
        %for not labeled trials, let's trust bombcell
        final_label(no_label) = bombcell_labeled_noise(no_label);
        % if user labeled it as noise, we trust the users
        final_label(labeled_noise) = true;
    
        labeled_noise = final_label;
    end
    neuron_list = clusterinfo.cluster_id(~labeled_noise);%takeing mux+good

    Ccmt = [];
    for cx = 1:length(neuron_list)
        selected_cluster_id = neuron_list(cx);
        oe_channel = clusterinfo.ch(clusterinfo.cluster_id==selected_cluster_id);
        shank = all_geom.shank(oe_channel);
        
        tmp(cx).ad_channel = oe_channel;
        D(cx).cluster = cx;% cluster index
        D(cx).ss_cluster = selected_cluster_id; % The cluster # assigned by software
        ES(cx).oets = spikes_ts(spike_clusters==selected_cluster_id);%time stamp, 2nd row in the A file

        if ~spikeinterface_pipeline

            if loading_waveform==0 %this first situation is not inuse
                % no waveformmetrics stuff, load waveform from template
                %process raw waveform (this was from kilosort re-constracted template, but we may more want the actual mean waveform)
                target_template_id = spike_templates(find(spike_clusters==selected_cluster_id,1))+1;
                raw_waveform_mat=reshape(templates(target_template_id,:,:),size(templates,2),size(templates,3));
                if any(~channel_connection) %have unconnected/exclueded channels during spk sorting
                    % by default, kilosort skipped the unconnected channles,
                    % here we are adding the template of unconnected channles back
                    % but leaving it as nan to indicate them.
                    raw_waveform_mat_adding=[raw_waveform_mat,nan(size(raw_waveform_mat,1),length(find(channel_connection==0)))];%add cols of nan at the back
                    idx_uncon_chan = 1:height(all_geom);idx_uncon_chan(~channel_connection)=[]; idx_uncon_chan = [idx_uncon_chan,find(channel_connection==0)'];
                    % generate a channel order index, to move those nan cols back
                    % to where kilosort skipped (ignored channels)
                    raw_waveform_mat_adding = [idx_uncon_chan;raw_waveform_mat_adding];
                    raw_waveform_mat = sortrows(raw_waveform_mat_adding',1)';
                    raw_waveform_mat = raw_waveform_mat(2:end,:);
                end
                ES(cx).wave = raw_waveform_mat;
            elseif loading_waveform==2 %loaded from python
                waveform_struct = waveform_metrics_and_wave(waveform_metrics.cluster_id == selected_cluster_id);
                Cquality(cx).spike_duration = waveform_struct.duration;
                Cquality(cx).half_width = waveform_struct.halfwidth;
                Cquality(cx).pt_ratio = waveform_struct.PT_ratio;
                Cquality(cx).repolarization_slope = waveform_struct.repolarization_slope;
                Cquality(cx).recovery_slope = waveform_struct.recovery_slope;
                Cquality(cx).snr_best_chan = waveform_struct.snr;
                Cquality(cx).amplitude = waveform_struct.amplitude;
                Cquality(cx).spread = waveform_struct.spread;
                Cquality(cx).velocity_above = waveform_struct.velocity_above;
                Cquality(cx).velocity_below = waveform_struct.velocity_below;

                wave_data = squeeze(mean_wv(selected_cluster_id+1,1:384,:))';%need test here
                ES(cx).wave.wave_mn = single(wave_data);

            else %load from MATLAB
                %load waveform metrics stuff from a mat file, skip comuting that and do data conversion directly
                cluster_id_in_struct = [waveform_metrics_and_wave.cluster_id];
                cluster_id_index = find(cluster_id_in_struct==selected_cluster_id);
                if waveform_metrics_and_wave(cluster_id_index).n_Spikes == numel(ES(cx).oets)
                    % one more check for n_spike match
                    waveform_metrics_struct = waveform_metrics_and_wave(cluster_id_index);
                    wavedata = waveform_metrics_struct.wavedata;
                else
                    error('nSpikes not match for wave form matrics file! please check!');
                end

                Cquality(cx).spike_duration = waveform_metrics_struct.duration;
                Cquality(cx).half_width = waveform_metrics_struct.halfwidth;
                Cquality(cx).pt_ratio = waveform_metrics_struct.pt_ratio;
                Cquality(cx).repolarization_slope = waveform_metrics_struct.repolarization_slope;
                Cquality(cx).recovery_slope = waveform_metrics_struct.recovery_slope;
                Cquality(cx).snr_best_chan = waveform_metrics_struct.snr_best_chan;
                Cquality(cx).snr_total = waveform_metrics_struct.snr_total;
                Cquality(cx).amplitude = waveform_metrics_struct.amplitude;
                Cquality(cx).spread = waveform_metrics_struct.spread;
                Cquality(cx).velocity_above = waveform_metrics_struct.velocity_above;
                Cquality(cx).velocity_below = waveform_metrics_struct.velocity_below;


                ES(cx).wave = wavedata;
            end

            quality_struct = T_struct(T.cluster_id == selected_cluster_id);

            Cquality(cx).firing_rate = quality_struct.firing_rate;
            Cquality(cx).presence_ratio = quality_struct.presence_ratio;
            Cquality(cx).isi_viol = quality_struct.isi_viol;
            Cquality(cx).amp_cutoff = quality_struct.amplitude_cutoff;
            Cquality(cx).isolation_distance = quality_struct.isolation_distance;
            Cquality(cx).l_ratio = quality_struct.l_ratio;
            Cquality(cx).d_prime = quality_struct.d_prime;
            Cquality(cx).nn_hit_rate = quality_struct.nn_hit_rate;
            Cquality(cx).nn_miss_rate = quality_struct.nn_miss_rate;
            Cquality(cx).silhouette_score = quality_struct.silhouette_score;
            Cquality(cx).max_drift = quality_struct.max_drift;
            Cquality(cx).cum_drift = quality_struct.cumulative_drift;

            if ismember('bc_unitType',clusterinfo.Properties.VariableNames) %bombcell labeled session, add a default lable for those
                % bc_specific information
                Cquality(cx).nPeaks = clusterinfo.n_peaks(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).nTroughs = clusterinfo.n_troughs(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).spatialDecaySlope = clusterinfo.spatial_decay_slope(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).waveformBaselineFlatness = clusterinfo.wv_baseline_flatness(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).waveformDuration = clusterinfo.waveform_dur(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).isSomatic = clusterinfo.is_somatic(clusterinfo.cluster_id==selected_cluster_id);
                Cquality(cx).bc_label= clusterinfo.bc_unitType{clusterinfo.cluster_id==selected_cluster_id};

                %for shared metrics, we will figure out later %jingjie-20231228
            end
            spikeinterface_quality = struct();
        else
            Cquality = struct();
            wave_data_mn = squeeze(mean_wv(clusterinfo.cluster_id == selected_cluster_id,:,:));
            ES(cx).wave.wave_mn = single(wave_data_mn);

            wave_data_sd = squeeze(std_wv(clusterinfo.cluster_id == selected_cluster_id,:,:));
            ES(cx).wave.wave_sd = single(wave_data_sd);

            % quality matrics
            quality_names = {'amplitude_cutoff', 'amplitude_cv_median', 'amplitude_cv_range', 'amplitude_median', 'drift_ptp', 'drift_std',...
                'drift_mad', 'firing_range', 'firing_rate', 'isi_violations_ratio', 'isi_violations_count', 'num_spikes', 'presence_ratio',...
                'rp_contamination', 'rp_violations', 'sd_ratio', 'sliding_rp_violation', 'snr', 'sync_spike_2', 'sync_spike_4', 'sync_spike_8',...
                'd_prime', 'isolation_distance', 'l_ratio', 'silhouette', 'nn_hit_rate', 'nn_miss_rate', 'exp_decay', 'half_width',...
                'num_negative_peaks', 'num_positive_peaks', 'peak_to_valley', 'peak_trough_ratio', 'recovery_slope', 'repolarization_slope', 'spread',...
                'velocity_above', 'velocity_below', 'fr_sld_5m', 'fr_sld_10m', 'fr_sld_15m', 'drift_linear_sig', 'drift_resid_sig', 'cluster_id'};

            quality_columns = spikeinterface_quality_tb.Properties.VariableNames;
            ismember_quality_names = ismember(quality_columns, quality_names);
            quanlity_columns_rm = quality_columns(~ismember_quality_names);
            spikeinterface_quality(cx) = rmfield(table2struct(spikeinterface_quality_tb(spikeinterface_quality_tb.cluster_id==selected_cluster_id, :)), quanlity_columns_rm);
        end

        
        D(cx).height = clusterinfo.amp(clusterinfo.cluster_id==selected_cluster_id);
        this_note = clusterinfo.group{clusterinfo.cluster_id==selected_cluster_id};
        if ~isempty(this_note)
            %skip if no manual label
            switch(lower(this_note(1:3)))
                case 'goo'
                    D(cx).label = 'single';
                case 'mua'
                    D(cx).label = 'multi';
                case 'noi'
                    D(cx).label = 'noise';
                otherwise
                    D(cx).label = 'other';
                    Ccmt(cx).comment = this_note;
            end
        end
        fprintf('finishing process %d cluster in %d\n',cx,length(neuron_list));
    end
    S.software = 'kilosort';
    S.sorttime = sort_date; %spike sorting date
    [~,data_folder] = fileparts(recording_path_info.recording_data_path);
    S.pathname = data_folder;
    
    if strcmp(recording_path_info.recording_software,'spikeGLX')
        recording_settings = utils.readSpikeGLXmeta(recording_path_info.meta_data_ap);
        header = struct();
        header.probe_pn = recording_settings.imDatPrb_pn;
        header.HS_sn = recording_settings.imDatHs_sn;
        header.HS_pn = recording_settings.imDatHs_pn;
        header.ks_version = ks_version;
    elseif strcmp(recording_path_info.recording_software,'open-ephys')
        recording_info = utils.extract_xml_recording_config(recording_path_info);
        header = struct();
        header.oe_version = recording_info.oe_version;
        header.ks_version = ks_version;
        header.highCut = sprintf('%.0f',recording_info.acqusition_highCut);
        header.lowCut = sprintf('%.0f',recording_info.acqusition_lowCut);
    end
    S.settings = json.mdumps(header);
end


function D = compute_overlap(ES, D)
% compute_overlap
% Check if we recorded the same cell on two shanks.

for icx = 1:numel(ES)
    overlap=0;
    for jcx = 1:numel(ES)
        if icx~=jcx
            c_overlap=numel(intersect(ES(icx).oets,ES(jcx).oets));
            overlap=max(overlap,c_overlap);
        end
    end
    overlap=overlap/numel(ES(icx).oets);
    D(icx).overlap = overlap;
end
end

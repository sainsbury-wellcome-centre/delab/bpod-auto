function [trial_time, trial_num, total_duration] = getSpikeGLXSyncTimes(varargin)
% [trial_time, trial_num] = getSpikeGLXSyncTimes(varargin)
% Need to add documentation

iod = @utils.inputordefault;

[ev_type, varargin] = iod('event_type',3,varargin);


[timebin, varargin] = iod('secsperbit',1/2500,varargin);
% The npx LFT sample rate is 2500Hz, so 1ms is 30samples.

[bits, varargin] = iod('trialbits',16,varargin);
% We will use 16 bits to encode the trial #, allowing > 65000 trials per session

[header, varargin] = iod('header',5, varargin);
% We will use a 3 bit 101 header to indicate the start of the sync state. 
% This also allows us to measure the samples/bit if the sampling rate
% changes.

[fs, varargin] = iod('samping_rate',30000, varargin);%for 'flat binary' format of oe data, need to extract ts from index

[folder_name, varargin] = iod('folder_name','imec0', varargin);%'/home/jingjie/repos/data/synctest_20230605_g0/synctest_20230605_g0_imec0'

% for this to run load_open_ephys_data needs to be on the path

[ttl_correct, varargin] = iod('allow_ttl_correct',true, varargin);

syncDat = extractSyncChannel(folder_name, 385, 385);
eventTimes = spikeGLXdigitalParse(syncDat, 2500);
eventCellIdx = find(cellfun(@numel, cellfun(@(x) x{1}, eventTimes, 'UniformOutput', false))>1)
ttls = eventTimes{eventCellIdx}{1};
total_duration = round((ttls(end)-ttls(1)));
%% Verify that the min ttl matches the timebin.

dts = diff(ttls);
sdts = sort(dts);

assert(all(abs(sdts(1:(ceil(end/5)))-timebin)<10e-10)|ttl_correct,'The ttl durations do not match the timebin'); % The first 50 diffs should be the same duration as the timebin. We have to use <10e-10 because you can't compare floats.

if any(dts<timebin-10e-9) && ttl_correct
% have very short ttl pulse needed to be excluded
    short_ttl_idx = find(dts<timebin-10e-9);
    ttls([short_ttl_idx;short_ttl_idx+1]) = [];
    dts = diff(ttls);
    fprintf(2,'short ttl pulse detected, sync quality is not very good\n')
end
%%
% Convert times to timebins
ttl_bins = round(dts/timebin);

% Assume that the time in between trials is > 500 ms
trial_bounds = find(ttl_bins > 0.5/timebin);

% initial outputs to nans.
trial_time = nan(size(trial_bounds));
trial_num = trial_time;
this_bit_ind = 1;

headerbits = numel(dec2bin(header));
   
     
for xi=1:numel(trial_bounds)
    try
   if xi == 1
       trial_time(xi) = ttls(1);
       temp_ts=ttl_bins(1:trial_bounds(xi)-1);
   else
       trial_time(xi)= ttls(trial_bounds(xi-1)+1);
       temp_ts=ttl_bins(trial_bounds(xi-1)+1:trial_bounds(xi)-1);
   end
   temp_ts = round(temp_ts/2.5);
   
   last_bit=1;
   bitfield=zeros(1,bits + headerbits);
   bit_idx=1;
   for xj=1:numel(temp_ts)
       bitfield(bit_idx:bit_idx+temp_ts(xj)-1)=last_bit;
       last_bit=1-last_bit;
       bit_idx=bit_idx+temp_ts(xj); % really +1-1       
   end
   trial_num(xi)=bin2dec(num2str(bitfield(headerbits+1:end)));
    catch
        warning('you are fucked in trialnumtimes')
    end
end

if trial_num(end)==0
	trial_num=trial_num(1:end-1);
	trial_time=trial_time(1:end-1);
end

assert(all(diff(trial_num)>=1),"Trial numbers are not in increasing order. Cannot sync this session.")
    
end


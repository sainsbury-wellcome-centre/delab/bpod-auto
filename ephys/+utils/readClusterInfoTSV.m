function [clusterinfo] = readClusterInfoTSV(path_info_struct)
%[clusterinfo] = ReadClusterInfoTSV(filename)
%   load info from kilosort cluster_info.tsv
%   by default if you are not speficing any input it will check
%   cluster_info.tsv directly from your current folder
%
%   or, load from each tsv file and some npy file directly if this session
%   was not even processed by phy
%   Jingjie Li, 06/15/2021

if exist(fullfile(path_info_struct.spk_sorting_path,'cluster_info.tsv'),'file')
    %have cluster_info file in this dir, use this
    clusterinfo = struct2table(tdfread(fullfile(path_info_struct.spk_sorting_path,'cluster_info.tsv')));
else %not processed by phy, load each file
    assert(false,'you have to open it with phy and save to have the cluster_info.tsv file to be processed');

    % we want to open it without phy, but seems it's still missing some key
    % information like depth,channel,shank, etc. will check in the future.
    clusterinfo_default = struct();
    clusterinfo_bombcell = struct();
    fs = dir(fullfile(path_info_struct.spk_sorting_path, '*.tsv'));
    for ii = 1:numel(fs)
        if strcmp(fs(ii).name(1:8),'cluster_') && contains(fs(ii).name,{'Amplitude','ContamPct','KSLabel','group'})
            % kilosort default clusters
            this_tab = tdfread(fullfile(path_info_struct.spk_sorting_path,fs(ii).name));
            field_names = fields(this_tab);
            contents_info = this_tab.(field_names{2});
            [cluster_id,sort_idx] = sort(this_tab.cluster_id);
            clusterinfo_default.(fs(ii).name(9:end-4)) = contents_info(sort_idx,:);
            clusterinfo_default.cluster_id = cluster_id;
        elseif strcmp(fs(ii).name(1:8),'cluster_') && ~contains(fs(ii).name,{'Amplitude','ContamPct','KSLabel','group'})
            this_tab = tdfread(fullfile(path_info_struct.spk_sorting_path,fs(ii).name));
            field_names = fields(this_tab);
            contents_info = this_tab.(field_names{2});
            [cluster_id,sort_idx] = sort(this_tab.cluster_id);
            clusterinfo_bombcell.(fs(ii).name(9:end-4)) = contents_info(sort_idx,:);
            clusterinfo_bombcell.cluster_id = cluster_id;
        end
    end
    % after bombcell some units might be lost, let's ignore those
    % losted units.
    if numel(clusterinfo_bombcell.cluster_id)<numel(clusterinfo_default.cluster_id)
        clusterinfo_default_fields = fields(clusterinfo_default);
        for ii = 1:numel(clusterinfo_default_fields)
            contents = clusterinfo_default.(clusterinfo_default_fields{ii});
            clusterinfo_default.(clusterinfo_default_fields{ii}) = contents(clusterinfo_bombcell.cluster_id+1,:);
        end
    end
    clusterinfo_default = struct2table(clusterinfo_default);
    clusterinfo_bombcell = struct2table(rmfield(clusterinfo_bombcell,'cluster_id'));
    clusterinfo = [ clusterinfo_default ,clusterinfo_bombcell ];
    %clusterinfo.group = cellstr(clusterinfo.group); 
    clusterinfo.group = nan(height(clusterinfo),1); %this session was not even processed with kilosort, so no manual label
end

% some col of the table is in a very weird char array format, convert it
clusterinfo.KSLabel = cellstr(clusterinfo.KSLabel); 
if ismember('bc_unitType',clusterinfo.Properties.VariableNames)
    clusterinfo.bc_unitType = cellstr(clusterinfo.bc_unitType); 
end
if isa(clusterinfo.group(1),'char')
    clusterinfo.group = cellstr(clusterinfo.group); 
elseif isa(clusterinfo.group(1),'double') %this group is double, should be all NaN, replace it with empty
    clusterinfo.group = cell(height(clusterinfo),1);
end
%clusterinfo.n_spikes(cell2mat(cellfun(@(x) isempty(x),clusterinfo.n_spikes,'UniformOutput',false)))={0};%replace some empty element ahead
end


function [path_info] = find_npx_recording_dir(npx_folder)
%[path_info] = find_npx_recording_dir(npx_folder)
%    return the path of several key files from npx recording
%    this will be a 1xn struct, for n numbers of probes
%    meta settings file:
%       path_info(ii).meta_data_ap / path_info(ii).meta_data_lfp
%    for recording:
%       path_info(ii).recording_data / path_info(ii).recording_data_lfp
%    path:
%       path_info(ii).recording_data_path
%    probe index:
%       path_info(ii).probe_idx

%    Jingjie Li, 2023-12

if nargin < 1
    npx_folder = pwd;
end
path_info = struct();
files = dir(npx_folder);
files = struct2table(files);
idx_recording_folder = find(cellfun(@(x) numel(x),files.name)>10 & files.isdir);% have to be a long name folder to be npx recording folder for one probe
assert(any(cellfun(@(x) contains(x,'_imec'),files.name)),'not in correct recording session folder, where it is?')
assert(~any(cellfun(@(x) contains(x,'.bin'),files.name)),'not in correct recording session folder, this might be a recording data folder')
for ii = 1:numel(idx_recording_folder)
    path_info(ii).recording_data_path = fullfile(npx_folder,files.name{idx_recording_folder(ii)});
    path_info(ii).recording_base_path = npx_folder;
    fs = dir(fullfile(path_info(ii).recording_data_path, '*.ap.bin')); 
    if isempty(fs)
        path_info(ii).recording_data = ''; %no raw data, might be a processed folder
    else
        path_info(ii).recording_data = fullfile(path_info(ii).recording_data_path, fs.name);
    end
    path_info(ii).probe_idx = str2double(path_info(ii).recording_data_path(end))+1;
    fs = dir(fullfile(path_info(ii).recording_data_path, '*.ap.meta')); 
    assert(~isempty(fs),'no meta file detected, is this the correct recording folder?');
    path_info(ii).meta_data_ap = fullfile(path_info(ii).recording_data_path, fs.name);
    fs = dir(fullfile(path_info(ii).recording_data_path, '*.lf.bin')); 
    if isempty(fs)
         path_info(ii).recording_data_lfp = ''; %no raw data, might be a processed folder
    else
        path_info(ii).recording_data_lfp = fullfile(path_info(ii).recording_data_path, fs.name);
    end
    fs = dir(fullfile(path_info(ii).recording_data_path, '*.lf.meta')); 
    path_info(ii).meta_data_lfp = fullfile(path_info(ii).recording_data_path, fs.name);
    path_info(ii).recording_software = 'spikeGLX';

    % we only support kilosort for now, other will need to be add if needed
    path_info(ii).spk_sorting_path = fullfile(path_info(ii).recording_data_path, 'kilosort');
    path_info(ii).spike_sorting_method = 'kilosort';

end
end
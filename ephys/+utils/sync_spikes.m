function SpksortInfo = sync_spikes(recording_path_info, S,spksort_cmts, varargin)
% S = sync_spikes(subjid, sessid, thisoe, M)
% Input:
% subjid, sessid
% thisoe    the path to the OpenEphys folder to sync
% S         A struct with M , the timing data from Bpod and OE.
% 
% Optional Input
%
% exmple:
% OUT = sync_spikes('/Volumes/data/ephys/2224/2224_2021-06-18_12-48-04', S,'', 'geom_path','/Volumes/data/ephys/2224','oe_raw_folder','/Volumes/data/ephys/2224/2224_2021-06-18_12-48-04/Record Node 101')
inpd = @utils.inputordefault;
[manually_curated, varargin]=inpd('manually_curated', 1, varargin);
[use_db, varargin]=inpd('use_db', 'client', varargin);

[sessinfo] = utils.load_sessinfo(recording_path_info);
dbc = db.labdb.getConnection(use_db);

try
    
    
%% Fill channels table

    fill_channels_table(sessinfo,recording_path_info,use_db);

    try
        % for process_spksorts, if we have multiple probe recording(for
        % acute), do it one by one
        SpksortInfo = cell(1,numel(recording_path_info));
        if numel(recording_path_info) > 1
            probe_tab = dbc.query('select probeid from phy.sessionprobes where sessid = %d order by probeid ASC',{sessinfo.sessid});
            for ii = 1:numel(recording_path_info)
                current_probeid = probe_tab.probeid(ii);
                chout = dbc.query('select channelid, ad_channel, probeid from phy.channels where probeid = %d',{current_probeid});
                SpksortInfo{ii} = utils.process_spksorts(sessinfo, S(ii), chout, spksort_cmts,'recording_path_info',recording_path_info(ii),'manually_curated',manually_curated);
            end
        else
            chout = dbc.query('select channelid, ad_channel, probeid from phy.channels where probeid in (select probeid from phy.sessionprobes where sessid = %d)',{sessinfo.sessid});
            SpksortInfo{1} = utils.process_spksorts(sessinfo, S, chout, spksort_cmts,'recording_path_info',recording_path_info,'manually_curated',manually_curated);
        end
    catch me
        utils.showerror(me)
        error(sprintf('Failed to process folder %s in %s\n',recording_path_info.spk_sorting_path, recording_path_info.recording_base_path));
    end

    %% Sync LFPS - TODO.


catch me
    utils.showerror(me)
end
% if the syncing is successful, copy the currently sorted files into a new folder, documented with spksortid and last_modified ts.
% archive_spksorts(SpksortInfo,ss_dirs{sx});
%  fprintf('sorting result files for %s ,spksortid = %d is archived.' ,ss_dir{sx},SpksortInfo.ts);

end

function [probeid, num_channels] = check_or_fill_sessprobe(sessinfo,recording_path_info,use_db)
    dbc = db.labdb.getConnection(use_db);
    if sessinfo.acute==1
        % acute recording session, 
        % first let's see if there is any information already
        [probeid, num_channels] = dbc.get('select probeid, n_channels from phy.probes where probeid in (select probeid from phy.sessionprobes where sessid = %d)',{sessinfo.sessid});
        if isempty(probeid)
            % nothing from db
            % create probe first. and then fill
            probeid = zeros(numel(recording_path_info),1);
            num_channels = zeros(numel(recording_path_info),1);
            for ii = 1:numel(recording_path_info)
                recording_settings = utils.readSpikeGLXmeta(recording_path_info(ii).meta_data_ap);
                all_geom = utils.makeChanMapFromSGLMeta(recording_settings);
                all_geom = sortrows(all_geom,{'spikeGLX'});
                nSites = recording_settings.nSites;%for npx, normally 384
    
                D = struct();
                D.subjid = sessinfo.subjid;
                D.n_channels = nSites;
                D.probe_num = recording_settings.imDatPrb_pn;
                D.target_location = sessinfo.target_location{ii};
                D.made_by = 'Neuropixel';
                D.made_on = recording_settings.fileCreateTime(1:10);
                D.probename = sprintf('%s_%d',sessinfo.subjid,ii);
                D.data = json.mdumps(table2struct(all_geom));
                dbc.saveData('phy.probes',D);
                this_probeid = dbc.last_insert_id();
                sp.sessid = sessinfo.sessid;
                sp.probeid = this_probeid;
                dbc.saveData('phy.sessionprobes',sp);
                probeid(ii) = this_probeid;
                num_channels(ii) = nSites;
            end
        else
            % already have in db, just check whether it match with local
            % files
            assert(numel(probeid)==numel(recording_path_info),'probe in db does not match with probes in local recording folder, please double check!');
        end
    else
        % chronic probe, assume you have already created probe table
        [this_sessid,this_probeid]=dbc.get('select sessid,probeid from phy.sessionprobes where sessid = %d',{sessinfo.sessid});
        [probeid, num_channels] = dbc.get('select probeid, n_channels from phy.probes where subjid="%s"',{sessinfo.subjid});
        if isempty(this_sessid)
            sp = struct();      
            assert(~isempty(probeid),'no probe in db, please create probe in phy.probes for this subject first!');
            for ii = 1:numel(probeid)
                sp(ii).sessid = sessinfo.sessid;
                sp(ii).probeid = probeid(ii);
            end
            dbc.saveData('phy.sessionprobes',sp);
        end
    end
end

function fill_channels_table(sessinfo,recording_path_info,use_db)
dbc = db.labdb.getConnection(use_db);
[probeid, num_channels] = check_or_fill_sessprobe(sessinfo,recording_path_info,use_db);
% Check if done.
channels = dbc.query('select channelid, ad_channel from phy.channels where probeid in (select probeid from phy.sessionprobes where sessid = %d)',{sessinfo.sessid});
if strcmp(recording_path_info(1).recording_software,'spikeGLX')
    oind = 1;
    % spike GLX recording session
    if numel(channels)==0 % no existing channels
        npx_ch = table();
        for ii = 1:numel(probeid)
            % for spikeGLX, do each probe one by one for multiple probe
            % recording

            npx_ch_table = table((1:num_channels)', repmat(probeid(ii),[num_channels,1]), 'VariableNames',{'ad_channel' 'probeid'});
            npx_ch = [npx_ch;npx_ch_table];
            oind = oind+1;
        end
        OUT = table2struct(npx_ch);
    end

elseif strcmp(recording_path_info.recording_software,'open-ephys')
    all_geom = db.getSubjProbeMapping(sessinfo.subjid);
    if strcmp(recording_path_info.recording_format,'flat_binary')
        % running on flat binary recording format of the oe
        recording_info = utils.extract_xml_recording_config(recording_path_info);
        si = recording_info.channel_config.name(contains(recording_info.channel_config.name,'CH'));%total recording CHs (not AUX)
        %this will be n_ch x 1 cell contain ch name
        fnames = si;
    else
        error('recording format not supported');
    end
    
    is_cont_file = cellfun(@(x)~isempty(x), si);
    oind = 1;
    for fx = 1:numel(si)
        if is_cont_file(fx)  % Only process CH files (not AUX)
            channel_id = str2double(si{fx}(3:end));
            % for saving flat binary recording format
            if numel(channels)==0
                %none of those channels are synced
                OUT(oind).ad_channel = channel_id;
                % for oe, probe is concatenated, use try to remap it
                this_probe = map_probe_ch(OUT(oind).ad_channel, probeid, num_channels,all_geom);
                OUT(oind).probeid = this_probe;

                oind = oind + 1;
            end
        end
    end
else
    oind = 1;
end


if oind>1 % There was something to save
    dbc.saveData('phy.channels', OUT)
end
end % fill_channels_table


function probeid = map_probe_ch(chnum, probelist, chlist,geom)
    if nargin < 4
        geom = [];
    end
    cclist = cumsum(chlist);
    pind = stats.qfind(cclist, chnum-1)+1;
    probeid = probelist(pind);
    if ~isempty(geom)
        if any(strcmp('probe', geom.Properties.VariableNames))
            probeid = probelist(geom.probe(geom.openephys==chnum));
        end
    end
    
    

end

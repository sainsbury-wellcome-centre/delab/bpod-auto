function [tracking_res] = load_brainglobe_probe_tracking(varargin)
%[tracking_res] = load_probe_tracking(varargin)
%   for each probe, you can input either the folder of subject or the
%   folder of tracks results
%   [tracking_res] = hist.load_probe_tracking('subj_folder','/media/jingjie/spike/histology/CFB-M-0022','probe_filename','probe_1_ALM');
%   [tracking_res] = hist.load_probe_tracking('result_folder','/media/jingjie/spike/histology/CFB-M-0022/downsampled_stacks/brainreg/segmentation/atlas_space/tracks','probe_filename','probe_1_ALM');
%
%   Jingjie Li, 2024-02-17
inpd = @utils.inputordefault;
[subj_folder, varargin]=inpd('subj_folder', '', varargin);
[result_folder, varargin]=inpd('result_folder', pwd, varargin);
[probe_filename, varargin]=inpd('probe_filename', '', varargin);

if ~isempty(subj_folder)
    result_folder = fullfile(subj_folder,'downsampled_stacks/brainreg/segmentation/atlas_space/tracks');
end

tracking_res = readtable(fullfile(result_folder,strcat(probe_filename,'.csv')));
tracking_res.depth = tracking_res.DistanceFromFirstPosition_um_(end:-1:1);

region_coordination = readNPY(fullfile(result_folder,strcat(probe_filename,'.npy')));
tracking_res.x = region_coordination(:,1);
tracking_res.y = region_coordination(:,2);
tracking_res.z = region_coordination(:,3);
end
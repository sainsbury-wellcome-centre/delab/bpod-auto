function [AtlasTable] = loadAtlasTable(atlas)
%[AtlasTable] = loadAtlasTable(atlas)
%   load atlas table from file, given atlas name input
%
%   [AtlasTable] = loadAtlasTable('allen_mouse')
if strcmp(atlas,'allen_mouse')
    bpodstr = which('start_bpod');
    BPODHOME = bpodstr(1:find(bpodstr==filesep,1,'last'));
    path_parts = strsplit(BPODHOME, filesep);
    repo_path = strjoin(path_parts(1:end-2), filesep);%extact repo dir

    AtlasTable = readtable(fullfile(repo_path,'bpod-auto','ephys','helpers','allen_atlas_name.csv'));
else
    AtlasTable = [];
end
end
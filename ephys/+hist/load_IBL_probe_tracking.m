function [tracking_res] = load_IBL_probe_tracking(path_to_json)
%[tracking_res] = load_probe_tracking(varargin)
%   for each probe, you can input either the folder of subject or the
%   folder of tracks results
%   [tracking_res] = hist.load_probe_tracking('subj_folder','/media/jingjie/spike/histology/CFB-M-0022','probe_filename','probe_1_ALM');
%   [tracking_res] = hist.load_probe_tracking('result_folder','/media/jingjie/spike/histology/CFB-M-0022/downsampled_stacks/brainreg/segmentation/atlas_space/tracks','probe_filename','probe_1_ALM');
%
%   Jingjie Li, 2024-02-17


text = fileread(path_to_json);
tracking_json = jsondecode(text);
tracking_json_names = fieldnames(tracking_json);

for ii = 1:numel(tracking_json_names)
    if numel(tracking_json_names{ii})>8
        %this is a channel tab
        channel_name = tracking_json_names{ii};
        tracking_res(ii).channel_id=str2double(channel_name(9:end));
        this_coord = [tracking_json.(channel_name).x,tracking_json.(channel_name).y,tracking_json.(channel_name).z];
        this_coord = xyz2ccf(this_coord);
        tracking_res(ii).x = this_coord(1);
        tracking_res(ii).y = this_coord(2);
        tracking_res(ii).z = this_coord(3);
        tracking_res(ii).RegionID = tracking_json.(channel_name).brain_region_id;
        tracking_res(ii).RegionAcronym = tracking_json.(channel_name).brain_region;
    end
end
end

function [ccf_coord] = xyz2ccf(xyz)
%coordination tranformation from IBL loaded data to CCF atlas
%   xyz:Coordinates in the ephys alignment GUI are given with respect to bregma with 
% x == ML, y == AP and z == DV. Bregma is defined to be located at a distance of 
% ML = 5739 um, AP = 5400 um and DV = 332 um from the front, top, left corner (from point of view of mouse) 
% of the Allen CCF data volume.
%
% This function will remove this offset and then put to CCF order (x=AP,y=DV,Z=ML)
bregma = [5739,5400,332];
xyz = bregma - xyz;
ccf_coord = xyz(:,[2,3,1]);
end
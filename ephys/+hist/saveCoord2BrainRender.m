function saveCoord2BrainRender(saving_dir,coord)
%saveCoord2BrainRender(saving_dir,coord)
%   example:
%   hist.saveCoord2BrainRender('testing_coord.npy',[all_geom.xx,all_geom.yy,all_geom.zz])
%   can use this example to plot results with brainrender: https://github.com/brainglobe/brainrender/blob/main/examples/probe_tracks.py
coord( all(~coord,2), : ) = [];
writeNPY(coord,saving_dir);
end
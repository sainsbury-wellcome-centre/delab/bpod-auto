function [] = plot_npx_tragjectory(all_geom,varargin)
%plot_npx_tragjectory(all_geom,varargin)
%   draw npx trajectory after re-construction
%
% example input:
% plot_npx_tragjectory(all_geom,'ax',this_axis,'highlight_channels',[100,200])
%      input: all_geom: geom mapping table for this probe, including the
%      region location of each channel
%      ax (optional): draw this plot on given axes
%      highlight_channels (optional): draw a line for the position of a
%      list of given channels
%
% Jingjie Li- 2024-03-13
inpd = @utils.inputordefault;
[this_axis, varargin]=inpd('ax', [], varargin);
[highlight_channels, varargin]=inpd('highlight_channels', [], varargin);


allen_CCF = hist.loadAtlasTable('allen_mouse');
if any("region_id" == string(all_geom.Properties.VariableNames))
    region_id_list = all_geom.region_id;
else
    assert(any("region" == string(all_geom.Properties.VariableNames)),'no region col in geom input, please check')
    region_id_list = nan(height(all_geom),1);
    for ii = 1:height(all_geom)
        if ~isempty(all_geom.region{ii})
            region_id_list(ii) = allen_CCF.id(strcmp(allen_CCF.acronym,all_geom.region{ii}));
        end
    end
end

repeats = [false;(diff(region_id_list) == 0)]|isnan(region_id_list);
repeats(find(isnan(region_id_list),1,'first')) = false;
new_area_idx = find(~repeats);
region_id_list(repeats) = [];
%region_id_list = region_id_list(~isnan(region_id_list));
if isempty(this_axis)
    %clf;
    figure;
    this_axis = draw.jaxes([0.1,0.1,0.2,0.8]);
end
box off;
hold on;
for ii = 1:numel(region_id_list)
     %idx_this_region = [find(all_geom.region_id==region_id_list(ii),1,'first'),find(all_geom.region_id==region_id_list(ii),1,'last')];
     if ii<numel(region_id_list) 
         % use the channel before next region as the last idx
         idx_this_region = [new_area_idx(ii),new_area_idx(ii+1)-1];
     else
         idx_this_region = [new_area_idx(ii),height(all_geom)];
     end
     this_region_depth = max(all_geom.y)-all_geom.y(idx_this_region);
     if diff(idx_this_region) == 0
         % have only one channel in this region
         % put 10um width
         this_region_depth(2) = this_region_depth(2)+10;
     end
     if isnan(region_id_list(ii))
         rectangle(this_axis,'Position',[1 this_region_depth(2) 100 -diff(this_region_depth)],'FaceColor','#FFFFFF')
         text(10,mean(this_region_depth),'unknown')
     else
        rectangle(this_axis,'Position',[1 this_region_depth(2) 100 -diff(this_region_depth)],'FaceColor',strcat('#',allen_CCF.color_hex_triplet{allen_CCF.id==region_id_list(ii)}))
        text(10,mean(this_region_depth),allen_CCF.acronym{allen_CCF.id==region_id_list(ii)})
     end
     
end
xticks([])
ylabel('Depth (um)')
set(gca, 'YDir','reverse')

if max(all_geom.y)==3820 
    % NP1.0 probe, use NP1.0 ticks
    ylim([0,3820])
    yticks([0,1000,2000,3000,3820])
end

if ~isempty(highlight_channels)
    for ii = 1:numel(highlight_channels)
        yline(this_axis,all_geom.y(all_geom.spikeGLX==highlight_channels(ii)),'Color','r','LineWidth',2)
    end
end

end

function save_recording_to_db(varargin)
% save_oe_to_db(thisoe,spksort_cmts,binary_data_dir)
% run this from inside of an open ephys folder.
% To run this you need a bunch of things in the path: oe-tools, oeutils,
% and bpod stuff. 
% thisoe: open ephys recording directory to upload
% spksort_cmts 'comment for this session'
% binary_data_dir path for large binary filtered AP band data, leave empty
% if using same spike sorting data (sometimes want to use a local path for speeding up)
%
% for me this is done by:
% cd ~/repos/bpod-auto; add_bpod_path
% addpath genpath(~/repos/bpod-auto/ephys)
% addpath ~/repos/elutils
%
% example
% save_recording_to_db() % upload spikes from this pwd() session directory, no
% comments, session was manually curated, read raw AP band binary data file
% from the current sorting path
% save_recording_to_db('thisoe',/path,'spksort_cmts','excluded ch 1,2,3.','binary_data_dir','/local_path_from_ssd_can_speed_up_processing','manually_curated',1)
% save_recording_to_db('thisoe',/path,'spksort_cmts','excluded ch 1,2,3.','binary_data_dir','/local_path_from_ssd_can_speed_up_processing','manually_curated',0)
% manually_curated can be 1/0, by default it will be 1, 0 means raw output
% from kilosort
%
% Jingjie Li, 10/11/2021
inpd = @utils.inputordefault;
[session_folder, varargin]=inpd('recording_path', pwd, varargin);
[spksort_cmts, varargin]=inpd('spksort_cmts', '', varargin);
[manually_curated, varargin]=inpd('manually_curated', 1, varargin);
    
    recording_software = utils.check_recording_software(session_folder);
    if strcmp(recording_software,'npx')
        path_info = utils.find_npx_recording_dir(session_folder);
    else
        path_info = utils.find_oe_recording_dir(session_folder);
    end
	[sessinfo] = utils.load_sessinfo(path_info);
	if isempty(sessinfo.subjid) || isempty(sessinfo.sessid)
		error("Could not figure out which session to sync")
	end
	S = sync_times(sessinfo.subjid, sessinfo.sessid, path_info);
	utils.sync_spikes(path_info, S,spksort_cmts,'manually_curated',manually_curated);
    % ok = sync_accelerometer(subjid, sessid, thisoe, S);
	
    
end


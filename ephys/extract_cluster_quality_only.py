import sys
import os

sys.path.append('/home/jingjie/repos/ecephys_spike_sorting')#path for ecephys_spike_sorting repo: clone from https://github.com/AllenInstitute/ecephys_spike_sorting
from ecephys_spike_sorting.modules.quality_metrics.__main__ import calculate_quality_metrics


def main():
    print('Kilosort result file directory: ')
    kilosort_dir = input()
    print('Output CSV file directory (just press Enter if you want to use kilosort result directoyr): ')
    quality_output_dir = input()
    if len(quality_output_dir) < 1:
        quality_output_dir = kilosort_dir
    
    input_args = {'quality_metrics_params':{
            'isi_threshold' : 0.0015,
            'min_isi' : 0.000166,
            'num_channels_to_compare' : 7,
            'max_spikes_for_unit' : 500,
            'max_spikes_for_nn' : 10000,
            'n_neighbors' : 4,
            'n_silhouette' : 10000,
            'drift_metrics_interval_s' : 51,
            'drift_metrics_min_spikes_per_interval' : 10,
            'include_pc_metrics':True,
            'quality_metrics_output_file':'/media/erlichlab/ssd2/spk_sorting_sndmap/2209_kilosort_20210826/quality_out.csv'},
            'directories':{'kilosort_output_directory':'/media/erlichlab/ssd2/spk_sorting_sndmap/2209_kilosort_20210826'},
            'ephys_params':{'sample_rate':30000},
            'waveform_metrics':{'waveform_metrics_file':'aaa'}}

    if not os.path.exists(kilosort_dir):
        raise ValueError('Kilosort result directory not exist!')

    if not os.path.exists(kilosort_dir):
        raise ValueError('output directory not exist!')
    
    input_args['directories']['kilosort_output_directory'] = kilosort_dir
    input_args['quality_metrics_params']['quality_metrics_output_file'] = os.path.join(quality_output_dir, 'cluster_quality.csv')

    #print(input_args)
    calculate_quality_metrics(input_args)



if __name__ == "__main__":
    main()

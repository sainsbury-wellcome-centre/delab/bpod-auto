# bpod-auto

This project has code to make bpod more automated.

In the Erlich lab at NYUSH, we forked the github repo from Sanworks and made extensive changes. This has some downsides - mostly that it is pretty tricky to know which code is his and which is ours. It also can make it hard to pull bug fixes.

For the training setup at UCL, i would like our code (which mostly serves to make Bpod more automated) to sit _beside_ the official bpod repo. 

Both this repo and the official repo will be in the matlab path, and the assumption will be that `bpod-auto` and `bpod` folders sit in the same parent folder. As long as that assumption is met, we can call our functions.

I think this project will use the `+folder` convention in matlab to create namespaces. That way if we want to have function names that are the same as in Bpod, we can without collision. 

## Folder Structure



### `+db`

Anything that talks to the database

+ `SaveLoadDB.m`

### `+proto`

Protocol Objects. 

+ `ProtoObj`
+ `Operant`
+ `Classical`

### `+sys`

+ `dispatch.m`
+ `events.m` - 
+ `add_bpod_path.m`
+ `start_bpod_auto.m` - call this function on matlab start. 
+ `bpod_robot.m`

### `+sma`

All the custom functions for our sma

+ `AddState.m`
+ `AddRewardStates.m`
+ `AddFixationStates.m`

### `+tt`

Code that is commonly used for trial-selection.

+ For future use. 

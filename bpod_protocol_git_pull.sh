#!/bin/bash


# this sh file will basically pull the bpod-protocols repo from gitlab using a url
# if the folder heirarchy corresponding to this github repo does not exist, it will be created
#  this is assuming each '/' in the url corresponds to a new folder and the last being the repo name
# this folder will be created in the in a folder called 'bpod-protocols-repos' in the current directory


set -e 

# get the url from the command line which will be passed by the user 
url=$1
dir_path=$2
test=$3

echo $dir_path


# if url ends with '/'  remove it
url=$(echo $url | sed 's/\/$//')

#  if url ends with .git, remove it
# url=$(echo $url | sed 's/\.git$//')

# get the repo name from the url
#repo_name=$(echo $dir_path | rev | cut -d'/' -f1 | rev)


#folder_path=$(echo $dir_path | rev | cut -d'/' -f2- | rev)





# if the folder path does not exist, create it
if [ ! -d "$dir_path" ]; then
    mkdir -p $dir_path
fi



# if repo does not exist clone it in the folder path else update it to the latest commit (main branch)
if [ ! -d "$dir_path/.git" ]; then
    git -C $dir_path clone $url .
else

    if [ "$test" = "true" ]; then
        echo "test"
        exit 0
    fi
    
    # force cehckout to main branch
    git -C $dir_path checkout main -f
    git -C $dir_path reset --hard origin/main

    # pull the latest commit force overwrite local changes
    git -C $dir_path pull


fi


# exit the script
exit 0